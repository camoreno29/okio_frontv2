/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiUrl: 'https://pgplitewebapi.azurewebsites.net',
  // apiUrl: 'http://localhost:8084',
  /* ******************** START - Credentials connect firebase ******************** */
  // 2. Add your credentials from step 1
  firebaseConfig: {
    // apiKey: 'AIzaSyA4NDrUL_AlkKO9lyXhOKTDhodKklqFo-M',
    // authDomain: 'pgplite-527fc.firebaseapp.com',
    // databaseURL: 'https://pgplite-527fc.firebaseio.com',
    // projectId: 'pgplite-527fc',
    // storageBucket: 'pgplite-527fc.appspot.com',
    // messagingSenderId: '74420760695',
    // appId: '1:74420760695:web:8b06f6c401e4c93e',
    apiKey: 'AIzaSyDxZR9_DWV_1AcoPPUpbsDouPqHvlwrkSE',
    authDomain: 'prexus-pgp.firebaseapp.com',
    databaseURL: 'https://prexus-pgp.firebaseio.com',
    projectId: 'prexus-pgp',
    storageBucket: 'prexus-pgp.appspot.com',
    messagingSenderId: '598957255032',
    appId: '1:598957255032:web:baacdb8624158c5831c08d',
    // apiKey: 'AIzaSyDjtXqjI-vazUzJeolHr5EO6OYLuUR74eA',
    // authDomain: 'test-e5199.firebaseapp.com',
    // databaseURL: 'https://test-e5199.firebaseio.com',
    // projectId: 'test-e5199',
    // storageBucket: 'test-e5199.appspot.com',
    // messagingSenderId: '344316150567',
    // appId: '1:344316150567:web:f382eed286f18828709f02',
  },
  /* ********************* END - Credentials connect firebase ********************* */
};