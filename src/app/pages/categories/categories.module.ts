import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

//import { CategoriesComponent } from './categories.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { DeleteCategoriesComponent } from './delete-categories/delete-categories.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';
import {  DeleteAllCategoriesComponent} from './delete-all-categories/delete-all-categories.component';

const ENTRY_COMPONENTS = [
  AddCategoryComponent,
  DeleteCategoriesComponent,
  EditCategoryComponent,
  DeleteAllCategoriesComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    NgSelectModule,
    FormsModule,
    TooltipModule.forRoot(),
    ModalModule,
    NgxPaginationModule,
  ],
  declarations: [
  //  CategoriesComponent,
    AddCategoryComponent,
    DeleteCategoriesComponent,
    EditCategoryComponent,
    DeleteAllCategoriesComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class CategoriesModule { }
