import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';

@Component({
  selector: 'ngx-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit {

  submitted: boolean = false;
  current_payload: any = null;
  category_data: any = {};
  state_loading: boolean = true;

  list_options_show_complementarity_relation: any = [{ 'value': true, 'name': 'Yes' }, { 'value': false, 'name': 'No' }];
    
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  @Input() id_category: string;
  @Input() bAppliesComplementarityRelation: Boolean;
  @Input() iIDCategory: Number;
  @Input() iIDVersion: Number;
  @Input() tCategoryName: string;
  
  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private categoriesService: CategoriesService,
    protected ref: NbDialogRef<EditCategoryComponent>,
  ) { }

  ngOnInit() {
    const self = this;

    self.utilitiesService.fnGetLanguage('categories', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        const category_data = {
          'bAppliesComplementarityRelation': this.bAppliesComplementarityRelation,
          'iIDCategory': this.iIDCategory,
          'iIDVersion': this.iIDVersion,
          'tCategoryName': this.tCategoryName,
        };
        console.log('category_data: ', category_data);
        console.log('this.current_payload: ', this.current_payload);
        console.log('id_category======================>: ', this.id_category);
        console.log('bAppliesComplementarityRelation======================>: ', this.bAppliesComplementarityRelation);
        console.log('iIDCategory======================>: ', this.iIDCategory);
        console.log('iIDVersion======================>: ', this.iIDVersion);
        console.log('tCategoryName======================>: ', this.tCategoryName);
        // this.items_menu = JSON.parse(JSON.stringify(this.menu));
        // this.fnGetAllItemsGroup(this.current_payload);
        this.category_data = category_data;
        this.state_loading = false;
      }
    });
  }

  dismiss() {
    this.ref.close();
  }

  fnCancelEditCategory() {
    this.submitted = false;
    this.dismiss();
  }

  fnUpdateCategory(category_data) {
    this.submitted = true;
    console.log('category_data: ', category_data);
    const data_object = {
      'bAppliesComplementarityRelation': category_data.bAppliesComplementarityRelation,
      'iIDCategory': category_data.iIDCategory,
      'iIDVersion': category_data.iIDVersion,
      'tCategoryName': category_data.tCategoryName,
      'tLanguage': localStorage.getItem('language_app'),
    };
    console.log('data_object: ', data_object);

    this.categoriesService.fnHttpSetEditDataCategory(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.category_data = {};
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessUpdateCategory.text , 'fas fa-window-close');
        this.dismiss();
        // this.router.navigate(['/pages/projects/' + id_company]);
        // this.companies = r.body;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(null, r.body.codMessage);
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnChangeSourceCheckRelation(event) {
    console.log('event: ', event);
  }

}
