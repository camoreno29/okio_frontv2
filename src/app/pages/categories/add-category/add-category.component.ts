import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';

@Component({
  selector: 'ngx-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {

  submitted: boolean = false;
  current_payload: any = null;
  category_data: any = {};

  list_options_show_complementarity_relation: any = [{ 'value': true, 'name': 'Yes' }, { 'value': false, 'name': 'No' }];

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  @Input() id_version: string;
  // @Input() iIDCategory: string;
  // @Input() iIDVersion: string;
  // @Input() tCategoryName: string;
  // @Input() bAppliesComplementarityRelation: string;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private categoriesService: CategoriesService,
    protected ref: NbDialogRef<AddCategoryComponent>,
  ) { }

  ngOnInit() {
    
    const self = this;

    self.utilitiesService.fnGetLanguage('categories', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });

    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
        console.log('id_version======================>: ', this.id_version);
        this.category_data['bAppliesComplementarityRelation'] = true;
        // this.items_menu = JSON.parse(JSON.stringify(this.menu));
        // this.fnGetAllItemsGroup(this.current_payload);
      }
    });
  }

  dismiss() {
    this.ref.close();
  }

  fnCancelCreateNewCategory() {
    this.submitted = false;
    this.dismiss();
  }

  fnCreateNewCategory(category_data) {
    this.submitted = true;
    console.log('category_data: ', category_data);
    const data_object = {
      'iIDCategory': 0,
      'iIDVersion': parseInt(this.id_version, 10),
      'tCategoryName': category_data.tCategoryName,
      'bAppliesComplementarityRelation': category_data.bAppliesComplementarityRelation,
      'tLanguage': localStorage.getItem('language_app'),
    };
    console.log('data_object: ', data_object);

    this.categoriesService.fnHttpSetSaveNewCategory(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.category_data = {};
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessCreateCategory.text , 'fas fa-window-close');
        this.dismiss();
        // this.router.navigate(['/pages/projects/' + id_company]);
        // this.companies = r.body;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(null, r.body.codMessage);
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnChangeSourceCheckRelation(event) {
    console.log('event: ', event);
    console.log('this.category_data: ', this.category_data);
  }
}
