import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

import { StateService } from '../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';
import { environment } from '../../../environments/environment';

import { UtilitiesService } from '../../shared/api/services/utilities.service';
import { CategoriesService } from '../../shared/api/services/categories.service';

import { ModalsComponent } from '../../shared/components/modals/modals.component';

import { AddCategoryComponent } from './add-category/add-category.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';
import { DeleteCategoriesComponent } from './delete-categories/delete-categories.component';
import { DeleteAllCategoriesComponent } from './delete-all-categories/delete-all-categories.component';

declare var $: any;

@Component({
  selector: 'ngx-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
})
export class CategoriesComponent implements OnInit {

  url_host: any = environment.apiUrl;

  obj_category: any = {};
  current_payload: string = null;
  id_version: any = null;
  submitted: any = false;
  list_categories: any = null;
  categories_original_collection: any = null;
  search_input: any = '';
  file_import_input: any = null;
  state_sort_data: any = 'ASC';
  fileToUpload: File = null;

  numItemsPage: any = null;
  currentPage: any = null;

  loadingChart: boolean = true;

  layout_state: string = 'show_loading_layout'; // State: 'show_loading_layout' => Display layout pulse loading, 'show_table_layout' => Display table results, 'show_empty_data_layout' => Display layout empty data results

  lang_nav: any = window.navigator.language;
  DATA_LANG: any = null;
  DATA_LANG_GENERAL: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    private categoriesService: CategoriesService,
  ) { }

  ngOnInit() {

    const self = this;
    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => {
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    // });
    self.currentPage = 1;
    self.numItemsPage = 12;

    self.route.params.subscribe(params => {
      console.log('params: ', params);
      if (params.id_version) {
        self.id_version = params.id_version;
        self.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          console.log('token: ', token);
          if (token.isValid()) {
            // here we receive a payload from the token and assigne it to our `user` variable
            self.current_payload = token.getValue();
            console.log('self.current_payload: ', self.current_payload);
            if (self.current_payload) {
              self.fnGetAllCategoriesByVersion(self.current_payload, self.id_version);
              return false;
            }
          }
        });
      }
    });
  }

  /*********Show modal parameters function *********/

  open(_title, _body) {
    this.dialogService.open(ModalsComponent, {
      context: {
        title: _title,
        body: _body
      },
    });
  }

  /*********Get all categories by version*********/
  fnGetAllCategoriesByVersion(current_payload, id_version?) {
    this.layout_state = 'show_loading_layout';
    console.log('current_payload: ', current_payload);

    const object_data_send = {
      'iIDCompany': id_version,
    }
    this.list_categories = [];
    this.categoriesService.fnHttpGetAllCategoriesByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_categories = JSON.parse(JSON.stringify(r.body));
        console.log('this.list_categories: ', this.list_categories);
        console.log('this.list_categories: type ', typeof this.list_categories);
        this.categories_original_collection = JSON.parse(JSON.stringify(r.body));
        this.loadingChart = false;
        if (this.list_categories.length > 0) {
          this.layout_state = 'show_table_layout';
        } else {
          this.layout_state = 'show_empty_data_layout';
        }
      }
    }, err => {
      console.log('err: ', err);
    });
  }

   /*********Show modal add category*********/
  showModalAddCategory(obj_category) {
    console.log('obj_category: ', obj_category);
    obj_category.id_version = this.id_version;
    this.dialogService.open(AddCategoryComponent, { context: obj_category }).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllCategoriesByVersion(this.current_payload, this.id_version);
    });
  }

  /*********Show modal edit category*********/
  showModalEditCategory(obj_category, id_category) {
    console.log('obj_category: ', obj_category);
    obj_category.id_category = id_category;
    this.dialogService.open(EditCategoryComponent, { context: obj_category }).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllCategoriesByVersion(this.current_payload, this.id_version);
    });
  }

  /*********Show modal delete category*********/
  fnDeleteCategories(data_category) {
    data_category.id_version = this.id_version;
    console.log('data_category: ', data_category);
    this.dialogService.open(DeleteCategoriesComponent, { context: data_category }).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllCategoriesByVersion(this.current_payload, this.id_version);
    });
  }

  /*********Show modal delete all categories*********/
  fnDeleteAllCategories(obj_category) {
    console.log('obj_category: ', obj_category);
    obj_category.id_version = this.id_version;
    console.log('obj_category: ', obj_category);
    this.dialogService.open(DeleteAllCategoriesComponent, { context: obj_category }).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllCategoriesByVersion(this.current_payload, this.id_version);
    });
  }

  /*********Show modal edit category*********/
  fnFilterCategories(categories, text_typing) {
    console.log('text_typing: ', text_typing);
    console.log('categories: ', categories);
    this.filterItems(categories, text_typing);
  }

  filterItems(categories, text_typing, field?) {
    const self = this;
    const toSearch = text_typing.toLowerCase();
    if (toSearch) {
      categories = JSON.parse(JSON.stringify(self.categories_original_collection));
      self.utilitiesService.fnGetDataFilter(categories, toSearch, function (data_collection) {
        self.list_categories = data_collection;
        console.log('data_collection: ', data_collection);
        console.log('self.list_categories: ', self.list_categories);
      });
    } else {
      self.list_categories = JSON.parse(JSON.stringify(self.categories_original_collection));
    }
  }

  fnExportCategories() {
    const lang = localStorage.getItem('language_app');
    window.open(
      this.url_host + '/api/Categories/GetExportCategories?iIDVersion=' + this.id_version + '&language=' + lang,
      '_blank');
  }

  sortColumn(field) {
    console.log(`sortColumn(field)`);
    console.log('field: ', field);
    const self = this;
    switch (field) {
      case 'category_name':
        if (self.state_sort_data === 'ASC') {
          self.list_categories.sort(function (a, b) { return (a.tCategoryName > b.tCategoryName) ? 1 : ((b.tCategoryName > a.tCategoryName) ? -1 : 0); });
          self.state_sort_data = 'DESC';
        } else {
          self.list_categories.sort(function (a, b) { return (b.tCategoryName > a.tCategoryName) ? 1 : ((a.tCategoryName > b.tCategoryName) ? -1 : 0); });
          self.state_sort_data = 'ASC';
        }
        break;
      case 'category_complementary_relaction':
        if (self.state_sort_data === 'ASC') {
          self.list_categories.sort(function (a, b) { return (a.bAppliesComplementarityRelation > b.bAppliesComplementarityRelation) ? 1 : ((b.bAppliesComplementarityRelation > a.bAppliesComplementarityRelation) ? -1 : 0); });
          self.state_sort_data = 'DESC';
        } else {
          self.list_categories.sort(function (a, b) { return (b.bAppliesComplementarityRelation > a.bAppliesComplementarityRelation) ? 1 : ((a.bAppliesComplementarityRelation > b.bAppliesComplementarityRelation) ? -1 : 0); });
          self.state_sort_data = 'ASC';
        }
        break;
    }
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.uploadFileToActivity(this.fileToUpload);
  }

  uploadFileToActivity(fileToUpload) {
    console.log('fileToUpload: ', fileToUpload);
    if (fileToUpload) {
      this.loadingChart = true;
      const end_point_url = '/api/Categories/PostImportCategories';
      const parameter = 'iIDVersion';
      this.utilitiesService.fnHttSetUploadFile(this.current_payload, this.fileToUpload, this.id_version, end_point_url, parameter).subscribe(data => {
        console.log('data: ', data);
        // do something, if upload success
        if (data.status == 200) {
          this.loadingChart = false;
          this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessImport.text, 'fas fa-window-close');
          this.fnGetAllCategoriesByVersion(this.current_payload, this.id_version);
        }
        if (data.status == 206) {
          this.loadingChart = false;
          console.log('data.body.codMessage: ', data.body.codMessage);
          const error = this.utilitiesService.fnSetErrors(null, data.body.codMessage);
          console.log('error: ', error);
          this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
        }
      }, error => {
        this.loadingChart = false;
        console.log(error);
      });
    }

    this.file_import_input = null;

  }

  /********Funcion que trae los textos del modulo categorias*********/
  fnGetLanguage() {
    const self = this;
    self.utilitiesService.fnGetLanguage('categories', function (res_lang) {
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module;
      self.DATA_LANG_GENERAL = res_lang.general;
    });
  }

}
