import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { StateService } from '../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';
import { environment } from '../../../environments/environment';

import { FormControl, FormGroup, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';

import { UtilitiesService } from '../../shared/api/services/utilities.service';
import { PriceCalculationSummaryService } from '../../shared/api/services/price-calculation-summary.service';
import { CategoriesService } from '../../shared/api/services/categories.service';

import { ForecastResultsService } from '../../shared/api/services/forecast-results.service';
import { AdvancedSearchPriceCalculationSummaryComponent } from './advanced-search-price-calculation-summary/advanced-search-price-calculation-summary.component';
import { ApplyRulesPriceCalculationSummaryComponent } from './apply-rules-price-calculation-summary/apply-rules-price-calculation-summary.component';
import { ProductInformationService } from '../../shared/api/services/product-information.service';

import { ModalsComponent } from '../../shared/components/modals/modals.component';
import { EditPriceCalculationSummaryComponent } from './edit-price-calculation-summary/edit-price-calculation-summary.component';

@Component({
  selector: 'ngx-price-calculation-summary',
  templateUrl: './price-calculation-summary.component.html',
  styleUrls: ['./price-calculation-summary.component.scss'],
})

export class PriceCalculationSummaryComponent implements OnInit {

  url_host: any = environment.apiUrl;
  suggestedStage: boolean = true;

  obj_category: any = {};
  current_payload: string = null;
  id_version: any = null;
  id_category: any = null;
  submitted: any = false;
  /* */
  list_price_calculation_summary: any = null;
  list_price_calculation_summary_original_collection: any = null;
  /* */
  search_input: any = '';
  fileToUpload: File = null;

  config: any = null;
  numItemsPage: any = null;
  currentPage: any = null;
  totalItems: any = null;
  file_import_input: any = null;

  object_filter: any = {};

  layoud_component_display: String = 'grid';
  bShowCents: any = null;
  quantiy_decimals: any = 0;
  tCurrencyISO: any = '';

  loading: boolean = true;
  filter_state: boolean = false;
  items_filtered_advance_search: any = [];

  sorted: boolean = false;
  field_sort: any = null;
  state_sort_data: any = 'asc';

  filterState: boolean = false;

  text_searching: any = '';
  
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;
  DATA_LANG_GENERAL: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    private priceCalculationSummaryService: PriceCalculationSummaryService,
    private categoriesService: CategoriesService,
    private forecastResultsService: ForecastResultsService,
    private productInformationService: ProductInformationService,
  ) { }

  ngOnInit() {
    const self = this;
    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => { 
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    // });
    
    this.currentPage = 1;
    this.numItemsPage = 12;

    this.route.params.subscribe(params => {
      console.log('params: ', params);
      if (params.id_version) {
        this.id_version = params.id_version;
        this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          console.log('token: ', token);
          if (token.isValid()) {
            this.current_payload = token.getValue();
            console.log('this.current_payload: ', this.current_payload);
            if (this.current_payload) {
              this.object_filter['competitors'] = [];
              this.fnGetAllPriceCalculationSummaryByVersion(this.current_payload, this.id_version, 1);
              this.fnGetConfigPriceCalculationSummary(this.current_payload, this.id_version);
              return false;
            }
          }
        });
      }
    });
  }

  /* *************** START - Get all products information *************** */
  fnGetAllPriceCalculationSummaryByVersion(current_payload, id_version, page?, text_search?) {
    console.log('current_payload: ', current_payload);

    let obj_Sort = null;

    if (this.sorted) {
      obj_Sort = {
        'field': this.field_sort,
        'dir': this.state_sort_data,
      };
    }

    const object_data_send = {
      'products': [],
      'priceList': [],
      'category': [],
      'tProductsType': null,
      'dCurrentPrice': {
        'dMin': null,
        'dMax': null,
      },
      'dPricePerceivedValue': {
        'dMin': null,
        'dMax': null,
      },
      'dPriceProductType': {
        'dMin': null,
        'dMax': null,
      },
      'dTheoreticalPrice': {
        'dMin': null,
        'dMax': null,
      },
      'dSuggestedPrice': {
        'dMin': null,
        'dMax': null,
      },
      'dUserPrice': {
        'dMin': null,
        'dMax': null,
      },
      'dSuggestedPriceWithRelationships': {
        'dMin': null,
        'dMax': null,
      },
      'dVarRecommendedPrice': {
        'dMin': null,
        'dMax': null,
      },
      'dVarUserPrice': {
        'dMin': null,
        'dMax': null,
      },
      'Sort': obj_Sort,
      'page': (page) ? page : 1,
      'pageSize': 12,
      'tSearch': text_search,
    };

    this.loading = true;

    this.priceCalculationSummaryService.fnHttpGetAllPriceCalculationSummaryByVersion(current_payload, id_version, object_data_send).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.filter_state = false;
        this.list_price_calculation_summary = JSON.parse(JSON.stringify(r.body));
        console.log('this.list_price_calculation_summary: ', this.list_price_calculation_summary);
        this.list_price_calculation_summary_original_collection = JSON.parse(JSON.stringify(r.body));
        console.log('this.list_price_calculation_summary_original_collection: ', this.list_price_calculation_summary_original_collection);
        this.totalItems = r.body['totalItems'];
        console.log('this.totalItems: ', this.totalItems);
        this.fnGetConfigStage(this.current_payload, this.id_version);
        this.fnValidStateSearch(this.items_filtered_advance_search);
      }
      if (r.status == 206) {
        this.list_price_calculation_summary = [];
        this.list_price_calculation_summary_original_collection = [];
      }
    }, err => {
      console.log('err: ', err);
    });
  }
  /* **************** END - Get all products information **************** */

  /* *************** START - Get all products information *************** */
  fnGetAllPriceCalculationSummaryAdvanceSearch(current_payload, id_version, data_object, page?) {
    console.log('current_payload: ', current_payload);

    const object_data_send = data_object;

    let obj_Sort = null;

    if (this.sorted) {
      obj_Sort = {
        'field': this.field_sort,
        'dir': this.state_sort_data,
      };
    }

    object_data_send['Sort'] = obj_Sort;
    object_data_send['page'] = (page) ? page : 1,

      this.priceCalculationSummaryService.fnHttpGetAllPriceCalculationSummaryByVersion(current_payload, id_version, object_data_send).subscribe(r => {
        console.log('r: ', r);
        if (r.status == 200) {
          this.loading = false;
          this.list_price_calculation_summary = JSON.parse(JSON.stringify(r.body));
          console.log('this.list_price_calculation_summary:', this.list_price_calculation_summary);
          this.list_price_calculation_summary_original_collection = JSON.parse(JSON.stringify(r.body));
          console.log('this.list_price_calculation_summary_original_collection: ', this.list_price_calculation_summary_original_collection);
          this.totalItems = r.body['totalItems'];
          console.log('this.totalItems: ', this.totalItems);
        }
        if (r.status == 206) {
          this.list_price_calculation_summary = [];
          this.list_price_calculation_summary_original_collection = [];
        }
      }, err => {
        console.log('err: ', err);
      });
  }
  /* **************** END - Get all products information **************** */

  fnGetAllPriceCalculationSummaryFilter(current_payload, object_filter, id_category?, page?) {
    console.log('id_category: ', id_category);
    console.log('object_filter: ', object_filter);
    console.log('current_payload: ', current_payload);

    this.list_price_calculation_summary.products = [];
    this.fnGetAllPriceCalculationSummaryAdvanceSearch(current_payload, this.id_version, object_filter, page);

  }

  fnGetConfigStage(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);
    this.config = null;
    this.loading = true;
    this.submitted = true;
    this.productInformationService.fnHttpGetSuggestedPrice(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.submitted = false;
        this.suggestedStage = JSON.parse(JSON.stringify(r.body));
        console.log('suggestedStage: ', this.suggestedStage);
      }
    }, err => {
      this.loading = false;
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  /** Funciones para filtrar y remover filtros **/
  fnFilteSearch(list_forecast_results, text_typing) {
    if (text_typing) {
      if (text_typing.length > 2) {
        console.log('Text ok');
        if (this.filter_state) {
          console.log('this.filter_state - 1: ', this.filter_state);
          this.fnGetAllPriceCalculationSummaryFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
        } else {
          this.text_searching = text_typing;
          console.log('this.filter_state - 2: ', this.filter_state);
          this.fnGetAllPriceCalculationSummaryByVersion(this.current_payload, this.id_version, 1, text_typing);
        }
      }
    } else {
      this.fnGetAllPriceCalculationSummaryByVersion(this.current_payload, this.id_version, 1);
    }

  }

  showModalAdvancedSearchPriceCalculationSummary(obj_data_filter) {
    this.search_input = '';
    obj_data_filter.id_version = this.id_version;
    obj_data_filter.items_collection_filtered = this.items_filtered_advance_search;
    obj_data_filter.suggestedStage = this.suggestedStage;
    console.log('this.items_filtered_advance_search: ', this.items_filtered_advance_search);
    console.log('obj_data_filter: ', obj_data_filter);

    this.dialogService.open(AdvancedSearchPriceCalculationSummaryComponent, { context: obj_data_filter, hasScroll: true }).onClose.subscribe((res) => {
      console.log('res: ', res);
      if (res) {
        this.loading = true;
        this.filter_state = true;
        const data_object = res;
        console.log('data_object: ', data_object);
        this.items_filtered_advance_search = JSON.parse(JSON.stringify(res));
        this.fnGetAllPriceCalculationSummaryAdvanceSearch(this.current_payload, this.id_version, data_object);
        console.log('this.items_filtered_advance_search: >>>>>>>>>>>>>>>>>>>>>========== ', this.items_filtered_advance_search);
        this.fnValidStateSearch(this.items_filtered_advance_search);
      }
    });
  }
  /****/

  /** Funciones para editar y borrar **/
  showModalEdit(data) {
    console.log('data: ', data);
    // console.log('id_version: ', this.id_version);
    data.id_version = this.id_version;
    data.suggestedStage = this.suggestedStage;
    console.log('data: ', data);

    this.dialogService.open(EditPriceCalculationSummaryComponent, { context: { data } }).onClose.subscribe((res) => {
      console.log('res: ', res);
      console.log('data: ', data);
      this.id_version = data['id_version'];
      this.fnGetAllPriceCalculationSummaryByVersion(this.current_payload, this.id_version, this.currentPage);
    });
  }
  /****/

  fnExportPriceCalculationSummary() {
    const lang = localStorage.getItem('language_app');
    window.open(
      this.url_host + '/api/PriceCalculationSummary/GetExportPriceCalculationSummary?iIDVersion=' + this.id_version + '&language=' + lang,
      '_blank');
  }

  fnRestorePriceCalculationSummary() {
    this.config = null;
    this.loading = true;
    this.submitted = true;
    this.forecastResultsService.fnHttpRestorepriceCalulationSummary(this.current_payload, this.id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.submitted = false;
        this.fnGetConfigStage(this.current_payload, this.id_version);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessDeleteAll.text);
      }
    }, err => {
      this.loading = false;
      this.submitted = false;
      console.log('err: ', err);
    });


  }
  sortColumn(field) {
    console.log(`sortColumn(field)`);
    console.log('field: ', field);
    const self = this;

    self.sorted = true;
    self.field_sort = field;

    if (self.state_sort_data === 'asc') {
      self.state_sort_data = 'desc';
    } else {
      self.state_sort_data = 'asc';
    }

    if (this.filterState) {
      this.fnGetAllPriceCalculationSummaryFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    } else {
      this.fnGetAllPriceCalculationSummaryByVersion(this.current_payload, this.id_version, 1, this.text_searching);
    }
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.uploadFileToActivity(this.fileToUpload);
  }

  uploadFileToActivity(fileToUpload) {
    this.id_version;
    console.log('fileToUpload: ', fileToUpload);
    const end_point_url = '/api/ForecastResult/PostImportForecastResult';
    const parameter = 'iIDVersion';
    this.submitted = true;
    this.loading = true;
    this.utilitiesService.fnHttSetUploadFile(this.current_payload, this.fileToUpload, this.id_version, end_point_url, parameter).subscribe(data => {
      console.log('data: ', data);
      if (data.status == 200) {
        this.submitted = false;
        this.loading = false;
        this.file_import_input = null;
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessImport.text);
        this.fnGetAllPriceCalculationSummaryByVersion(this.current_payload, this.id_version);
      }
      if (data.status == 206) {
        this.submitted = false;
        this.loading = false;
        const error = this.utilitiesService.fnSetErrors(data.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, error => {
      this.submitted = false;
      this.loading = false;
      console.log(error);
    });
    this.file_import_input = null;
  }

  fnSetRemoveItemSearchFilteredAdvanced(index, data_collection) {
    console.log('index: ', index);
    console.log('data_collection: ', data_collection);
    data_collection.splice(index, 1);
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    console.log('data_collection: ', data_collection);
    this.object_filter = this.items_filtered_advance_search;
    console.log('this.items_filtered_advance_search: ', this.items_filtered_advance_search);
    this.fnGetAllPriceCalculationSummaryFilter(this.current_payload, this.items_filtered_advance_search, this.id_category);
  }

  fnGetConfigPriceCalculationSummary(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);
    this.productInformationService.fnHttpGetConfigProducts(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.bShowCents = r.body['bShowCents'];
        this.tCurrencyISO = r.body['tCurrencyISO'];
        console.log('this.bShowCents: ', this.bShowCents);
        this.quantiy_decimals = (this.bShowCents) ? 2 : 0;
        console.log('this.quantiy_decimals: ', this.quantiy_decimals);
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnSetRemoveBadgePricePerceivedValue() {
    this.items_filtered_advance_search['dPricePerceivedValue']['dMin'] = null;
    this.items_filtered_advance_search['dPricePerceivedValue']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllPriceCalculationSummaryFilter(this.current_payload, this.items_filtered_advance_search, this.id_category);
  }

  fnSetRemoveBadgePriceProductType() {
    this.items_filtered_advance_search['dPriceProductType']['dMin'] = null;
    this.items_filtered_advance_search['dPriceProductType']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllPriceCalculationSummaryFilter(this.current_payload, this.items_filtered_advance_search, this.id_category);
  }

  fnSetRemoveProductType() {
    this.items_filtered_advance_search['tProductsType'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllPriceCalculationSummaryFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
  }

  fnSetRemoveBadgeTheoreticalPrice() {
    this.items_filtered_advance_search['dTheoreticalPrice']['dMin'] = null;
    this.items_filtered_advance_search['dTheoreticalPrice']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllPriceCalculationSummaryFilter(this.current_payload, this.items_filtered_advance_search, this.id_category);
  }

  fnSetRemoveBadgetPriceWithVAT() {
    this.items_filtered_advance_search['dSuggestedPrice']['dMin'] = null;
    this.items_filtered_advance_search['dSuggestedPrice']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllPriceCalculationSummaryFilter(this.current_payload, this.items_filtered_advance_search, this.id_category);

  }

  fnSetRemoveBadgeSuggestedPriceWithRelationships() {
    if (this.suggestedStage) {
      this.items_filtered_advance_search['dSuggestedPriceWithRelationships']['dMin'] = null;
      this.items_filtered_advance_search['dSuggestedPriceWithRelationships']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllPriceCalculationSummaryFilter(this.current_payload, this.items_filtered_advance_search, this.id_category);
    } else {
      this.items_filtered_advance_search['dUserPrice']['dMin'] = null;
      this.items_filtered_advance_search['dUserPrice']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllPriceCalculationSummaryFilter(this.current_payload, this.items_filtered_advance_search, this.id_category);
    }

  }

  fnSetRemoveBadgetPriceWithVATVar() {
    if (this.suggestedStage) {
      this.items_filtered_advance_search['dVarRecommendedPrice']['dMin'] = null;
      this.items_filtered_advance_search['dVarRecommendedPrice']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllPriceCalculationSummaryFilter(this.current_payload, this.items_filtered_advance_search, this.id_category);
    } else {
      this.items_filtered_advance_search['dVarUserPrice']['dMin'] = null;
      this.items_filtered_advance_search['dVarUserPrice']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllPriceCalculationSummaryFilter(this.current_payload, this.items_filtered_advance_search, this.id_category);
    }
  }

  fnSetRemoveBadgeCurrentPrice() {
    this.items_filtered_advance_search['dCurrentPrice']['dMin'] = null;
    this.items_filtered_advance_search['dCurrentPrice']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllPriceCalculationSummaryFilter(this.current_payload, this.items_filtered_advance_search, this.id_category);
  }

  fnSwitchViewData(state_view) {
    console.log('state_view: ', state_view);
    this.layoud_component_display = state_view;
  }

  getPage(page: number) {
    console.log('page: ', page);
    this.loading = true;
    if (this.filterState) {
      this.fnGetAllPriceCalculationSummaryFilter(this.current_payload, this.items_filtered_advance_search, this.id_version, page);
    } else {
      this.fnGetAllPriceCalculationSummaryByVersion(this.current_payload, this.id_version, page, this.text_searching);
    }
    this.currentPage = page;
  }

  fnValidStateSearch(collection) {
    if (typeof collection['category'] === 'undefined') {
      console.log('indefinida');
      this.filterState = false;
    } else {
      if (
        collection['category'].length < 1 &&
        collection['priceList'].length < 1 &&
        collection['products'].length < 1 &&
        collection['dCurrentPrice']['dMin'] == null &&
        collection['dCurrentPrice']['dMax'] == null &&
        collection['dPricePerceivedValue']['dMin'] == null &&
        collection['dPricePerceivedValue']['dMax'] == null &&
        collection['dPriceProductType']['dMin'] == null &&
        collection['dPriceProductType']['dMax'] == null &&
        collection['dTheoreticalPrice']['dMin'] == null &&
        collection['dTheoreticalPrice']['dMax'] == null &&
        collection['dSuggestedPrice']['dMin'] == null &&
        collection['dSuggestedPrice']['dMax'] == null &&
        collection['dUserPrice']['dMin'] == null &&
        collection['dUserPrice']['dMax'] == null &&
        collection['dSuggestedPriceWithRelationships']['dMin'] == null &&
        collection['dSuggestedPriceWithRelationships']['dMax'] == null &&
        collection['dVarRecommendedPrice']['dMin'] == null &&
        collection['dVarRecommendedPrice']['dMax'] == null &&
        collection['dVarUserPrice']['dMin'] == null &&
        collection['dVarUserPrice']['dMax'] == null) {
        this.filterState = false;
        return false;
      } else {
        this.filterState = true;
        return true;
      }
    }
  }

  fnClearAllFiltersSearch() {
    this.search_input = '';
    this.filter_state = false;
    this.items_filtered_advance_search = [];
    this.fnGetAllPriceCalculationSummaryByVersion(this.current_payload, this.id_version);
  }

  fnApplyrules(obj_data_filter) {
    this.search_input = '';
    obj_data_filter.id_version = this.id_version;
    obj_data_filter.items_collection_filtered = this.items_filtered_advance_search;
    obj_data_filter.suggestedStage = this.suggestedStage;
    console.log('this.items_filtered_advance_search: ', this.items_filtered_advance_search);
    console.log('obj_data_filter: ', obj_data_filter);

    this.dialogService.open(ApplyRulesPriceCalculationSummaryComponent, { context: obj_data_filter, hasScroll: true }).onClose.subscribe((res) => {
      console.log('res: ', res);
      if (res) {
        this.forecastResultsService.fnHttpGetApplyRules(this.current_payload, this.id_version).subscribe(r => {
          console.log('r: ', r);
          if (r.status == 200) {
            this.loading = false;
            this.filter_state = false;
            this.submitted = false;
            this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgApplyRules.text);
          }
          if (r.status == 206) {
            this.loading = false;
            this.filter_state = false;
            this.submitted = false;
            const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
            console.log('error: ', error);
            this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
          }
        }, err => {
          this.loading = false;
          this.filter_state = false;
          this.submitted = false;
          console.log('err: ', err);
          this.utilitiesService.showToast('top-right', '', 'Does not apply rules wiht exist!');
        });
      }
    });

  }

  /*********Show modal parameters function *********/

  open(_title, _body) {
    this.dialogService.open(ModalsComponent, {
      context: {
        title: _title,
        body: _body,
      },
    });
  }

  fnGetLanguage(){
    const self = this;
    self.utilitiesService.fnGetLanguage('priceCalculation', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module,
      self.DATA_LANG_GENERAL = res_lang.general
    });
  }

}
