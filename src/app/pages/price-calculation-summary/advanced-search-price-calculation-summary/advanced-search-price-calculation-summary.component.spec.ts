import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedSearchRatingPerceivedComponent } from './advanced-search-rating-perceived.component';

describe('AdvancedSearchRatingPerceivedComponent', () => {
  let component: AdvancedSearchRatingPerceivedComponent;
  let fixture: ComponentFixture<AdvancedSearchRatingPerceivedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvancedSearchRatingPerceivedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedSearchRatingPerceivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
