import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options } from 'ng5-slider';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { RatingPerceivedService } from '../../../shared/api/services/rating-perceived.service';
import { CompetitorsService } from '../../../shared/api/services/competitors.service';
import { PricesListService } from '../../../shared/api/services/prices-list.service';
import { ProductInformationService } from '../../../shared/api/services/product-information.service';
import { PriceCalculationSummaryService } from '../../../shared/api/services/price-calculation-summary.service';

@Component({
  selector: 'ngx-advanced-search-price-calculation-summary',
  templateUrl: './advanced-search-price-calculation-summary.component.html',
  styleUrls: ['./advanced-search-price-calculation-summary.component.scss'],
})
export class AdvancedSearchPriceCalculationSummaryComponent implements OnInit {

  submitted: boolean = false;
  current_payload: any = null;
  price_calculation_summary_data: any = {};
  list_competitors: any = [];
  data_competitor: any = null;
  state_loading: boolean = true;

  list_options_show_complementarity_relation: any = [{ 'value': true, 'name': 'Yes' }, { 'value': false, 'name': 'No' }];

  @Input() id_version: string;
  @Input() items_collection_filtered: any;
  @Input() suggestedStage: boolean;

  id_category: any = null;
  list_categories_collection: any = [];
  list_prices_lists_collection: any = [];
  list_products_collection: any = [];
  list_product_type: any = [];

  /* ********** START - Data slider range - Current price ********* */
  min_value_current_price: number = 0;
  max_value_current_price: number = 0;
  /* *********** END - Data slider range - Current price ********** */
  /* ********** START - Data slider range - Price perceived value ********* */
  min_value_price_perceived_value: number = 0;
  max_value_price_perceived_value: number = 0;
  /* *********** END - Data slider range - Price perceived value ********** */
  /* ********** START - Data slider range - Price product type ********* */
  min_value_price_product_type: number = 0;
  max_value_price_product_type: number = 0;
  /* *********** END - Data slider range - Price product type ********** */
  /* ********** START - Data slider range - Theorical price ********* */
  min_value_theoretical_price: number = 0;
  max_value_theoretical_price: number = 0;
  /* *********** END - Data slider range - Theorical price ********** */
  /* ********** START - Data slider range - Suggested price ********* */
  min_value_SuggestedPriceWithVAT: number = 0;
  max_value_SuggestedPriceWithVAT: number = 0;
  /* *********** END - Data slider range - Suggested price ********** */
  /* ********** START - Data slider range - User price ********* */
  min_value_SuggestedPriceWithVAT_User: number = 0;
  max_value_SuggestedPriceWithVAT_User: number = 0;
  /* *********** END - Data slider range - Suggested price ********** */
  /* ********** START - Data slider range - Annual volume trend ********* */
  min_value_UserPrice: number = 0;
  max_value_UserPrice: number = 0;
  min_value_suggested_price_with_relationships: number = 0;
  max_value_suggested_price_with_relationships: number = 0;
  /* *********** END - Data slider range - Annual volume trend ********** */
  /* ********** START - Data slider range - Annual volume trend ********* */
  min_value_PriceWithVATVar: number = 0;
  max_value_PriceWithVATVar: number = 0;

  min_value_PriceWithVATVar_User: number = 0;
  max_value_PriceWithVATVar_User: number = 0;
  /* *********** END - Data slider range - Annual volume trend ********** */
  /* *********** START - Data vars class ********** */
  list_data_filters_collection: any = [];
  list_data_filters_original_collection: any = [];
  loading: Boolean = true;
  /* ************ END - Data vars class *********** */

  data_filter_products: Object = {};
  data_filter_price_list: Object = {};
  data_filter_categories: Object = {};
  data_filter_products_type: string = '';

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private ratingPerceivedService: RatingPerceivedService,
    private competitorsService: CompetitorsService,
    private categoriesService: CategoriesService,
    private pricesListService: PricesListService,
    private productInformationService: ProductInformationService,
    private priceCalculationSummaryService: PriceCalculationSummaryService,
    protected ref: NbDialogRef<AdvancedSearchPriceCalculationSummaryComponent>,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('priceCalculation', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      // console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('items_collection_filtered======================>: ', this.items_collection_filtered);

        this.data_filter_products = this.items_collection_filtered['products'];
        this.data_filter_price_list = this.items_collection_filtered['priceList'];
        this.data_filter_categories = this.items_collection_filtered['category'];
        this.data_filter_products_type = this.items_collection_filtered['tProductsType'];
        this.min_value_current_price = (this.items_collection_filtered['dCurrentPrice']) ? this.items_collection_filtered['dCurrentPrice']['dMin'] : '';
        this.max_value_current_price = (this.items_collection_filtered['dCurrentPrice']) ? this.items_collection_filtered['dCurrentPrice']['dMax'] : '';
        this.min_value_price_perceived_value = (this.items_collection_filtered['dPricePerceivedValue']) ? this.items_collection_filtered['dPricePerceivedValue']['dMin'] : '';
        this.max_value_price_perceived_value = (this.items_collection_filtered['dPricePerceivedValue']) ? this.items_collection_filtered['dPricePerceivedValue']['dMax'] : '';
        this.min_value_price_product_type = (this.items_collection_filtered['dPriceProductType']) ? this.items_collection_filtered['dPriceProductType']['dMin'] : '';
        this.max_value_price_product_type = (this.items_collection_filtered['dPriceProductType']) ? this.items_collection_filtered['dPriceProductType']['dMax'] : '';
        this.min_value_theoretical_price = (this.items_collection_filtered['dTheoreticalPrice']) ? this.items_collection_filtered['dTheoreticalPrice']['dMin'] : '';
        this.max_value_theoretical_price = (this.items_collection_filtered['dTheoreticalPrice']) ? this.items_collection_filtered['dTheoreticalPrice']['dMax'] : '';
        this.min_value_SuggestedPriceWithVAT = (this.items_collection_filtered['dSuggestedPrice']) ? this.items_collection_filtered['dSuggestedPrice']['dMin'] : '';
        this.max_value_SuggestedPriceWithVAT = (this.items_collection_filtered['dSuggestedPrice']) ? this.items_collection_filtered['dSuggestedPrice']['dMax'] : '';
        this.min_value_UserPrice = (this.items_collection_filtered['dUserPrice']) ? this.items_collection_filtered['dUserPrice']['dMin'] : '';
        this.max_value_UserPrice = (this.items_collection_filtered['dUserPrice']) ? this.items_collection_filtered['dUserPrice']['dMax'] : '';
        this.min_value_suggested_price_with_relationships = (this.items_collection_filtered['dSuggestedPriceWithRelationships']) ? this.items_collection_filtered['dSuggestedPriceWithRelationships']['dMin'] : '';
        this.max_value_suggested_price_with_relationships = (this.items_collection_filtered['dSuggestedPriceWithRelationships']) ? this.items_collection_filtered['dSuggestedPriceWithRelationships']['dMax'] : '';
        this.min_value_PriceWithVATVar = (this.items_collection_filtered['dVarRecommendedPrice']) ? this.items_collection_filtered['dVarRecommendedPrice']['dMin'] != null ? this.items_collection_filtered['dVarRecommendedPrice']['dMin'] * 100 : null : null;
        this.max_value_PriceWithVATVar = (this.items_collection_filtered['dVarRecommendedPrice']) ? this.items_collection_filtered['dVarRecommendedPrice']['dMax'] != null ? this.items_collection_filtered['dVarRecommendedPrice']['dMax'] * 100 : null : null;
        this.min_value_PriceWithVATVar_User = (this.items_collection_filtered['dVarUserPrice']) ? this.items_collection_filtered['dVarUserPrice']['dMin'] != null ? this.items_collection_filtered['dVarUserPrice']['dMin'] * 100 : null : null;
        this.max_value_PriceWithVATVar_User = (this.items_collection_filtered['dVarUserPrice']) ? this.items_collection_filtered['dVarUserPrice']['dMax'] != null ? this.items_collection_filtered['dVarUserPrice']['dMax'] * 100 : null : null;

        this.fnGetListCategories(this.current_payload, this.id_version);
        this.fnGetPricesLists(this.current_payload, this.id_version);
        this.fnGetListProducts(this.current_payload, this.id_version);
      }
    });
  }


  fnGetListCategories(current_payload, id_version) {
    this.categoriesService.fnHttpGetDataListCategoriesByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_categories_collection = JSON.parse(JSON.stringify(r.body));
        this.submitted = false;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnGetPricesLists(current_payload, id_version?) {
    const self = this;
    self.pricesListService.fnGetAllPricesListByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.list_prices_lists_collection = JSON.parse(JSON.stringify(r.body));
        console.log('self.list_prices_lists_collection: ', self.list_prices_lists_collection);
        self.submitted = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        let error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetListProducts(current_payload, id_version, text_search?) {
    const self = this;
    self.productInformationService.fnHttpGetListProducts(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.list_products_collection = JSON.parse(JSON.stringify(r.body));
        self.state_loading = false;
        self.submitted = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        let error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetListCompetitors(current_payload, id_version) {
    this.competitorsService.fnHttpGetDataListCompetitorsByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_competitors = JSON.parse(JSON.stringify(r.body));
        this.submitted = false;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  dismiss(object_response_onclose?) {
    console.log('object_response_onclose: ', object_response_onclose);
    if (object_response_onclose) {
      object_response_onclose['products'] = (this.data_filter_products) ? this.data_filter_products : [];
      object_response_onclose['priceList'] = (this.data_filter_price_list) ? this.data_filter_price_list : [];
      object_response_onclose['category'] = (this.data_filter_categories) ? this.data_filter_categories : [];
      object_response_onclose['tProductsType'] = (this.data_filter_products_type) ? this.data_filter_products_type : null;
      object_response_onclose['dCurrentPrice'] = (this.max_value_current_price) ? { 'dMin': this.min_value_current_price, 'dMax': this.max_value_current_price } : { 'dMin': this.min_value_current_price, 'dMax': null };
      object_response_onclose['dPricePerceivedValue'] = (this.max_value_price_perceived_value) ? { 'dMin': this.min_value_price_perceived_value, 'dMax': this.max_value_price_perceived_value } : { 'dMin': this.min_value_price_perceived_value, 'dMax': null };
      object_response_onclose['dPriceProductType'] = (this.max_value_price_product_type) ? { 'dMin': this.min_value_price_product_type, 'dMax': this.max_value_price_product_type } : { 'dMin': this.min_value_price_product_type, 'dMax': null };
      object_response_onclose['dTheoreticalPrice'] = (this.max_value_theoretical_price) ? { 'dMin': this.min_value_theoretical_price, 'dMax': this.max_value_theoretical_price } : { 'dMin': this.min_value_theoretical_price, 'dMax': null };
      object_response_onclose['dSuggestedPrice'] = (this.max_value_SuggestedPriceWithVAT) ? { 'dMin': this.min_value_SuggestedPriceWithVAT, 'dMax': this.max_value_SuggestedPriceWithVAT } : { 'dMin': this.min_value_SuggestedPriceWithVAT, 'dMax': null };
      object_response_onclose['dUserPrice'] = { 'dMin': this.min_value_UserPrice, 'dMax': this.max_value_UserPrice };
      object_response_onclose['dSuggestedPriceWithRelationships'] = { 'dMin': this.min_value_suggested_price_with_relationships, 'dMax': this.max_value_suggested_price_with_relationships };
      if (this.min_value_PriceWithVATVar && this.max_value_PriceWithVATVar) {
        object_response_onclose['dVarRecommendedPrice'] = { 'dMin': this.min_value_PriceWithVATVar / 100, 'dMax': this.max_value_PriceWithVATVar / 100 };
      } else if (!this.min_value_PriceWithVATVar && !this.max_value_PriceWithVATVar) {
        object_response_onclose['dVarRecommendedPrice'] = { 'dMin': null, 'dMax': null };
      } else if (!this.min_value_PriceWithVATVar && this.max_value_PriceWithVATVar) {
        object_response_onclose['dVarRecommendedPrice'] = { 'dMin': null, 'dMax': this.max_value_PriceWithVATVar / 100 };
      } else if (!this.max_value_PriceWithVATVar && this.min_value_PriceWithVATVar) {
        object_response_onclose['dVarRecommendedPrice'] = { 'dMin': this.min_value_PriceWithVATVar / 100, 'dMax': null };
      }

      if (this.min_value_PriceWithVATVar_User && this.max_value_PriceWithVATVar_User) {
        object_response_onclose['dVarUserPrice'] = { 'dMin': this.min_value_PriceWithVATVar_User / 100, 'dMax': this.max_value_PriceWithVATVar_User / 100 };
      } else if (!this.min_value_PriceWithVATVar_User && !this.max_value_PriceWithVATVar_User) {
        object_response_onclose['dVarUserPrice'] = { 'dMin': null, 'dMax': null };
      } else if (!this.min_value_PriceWithVATVar_User && this.max_value_PriceWithVATVar_User) {
        object_response_onclose['dVarUserPrice'] = { 'dMin': null, 'dMax': this.max_value_PriceWithVATVar_User / 100 };
      } else if (!this.max_value_PriceWithVATVar_User && this.min_value_PriceWithVATVar_User) {
        object_response_onclose['dVarUserPrice'] = { 'dMin': this.min_value_PriceWithVATVar_User / 100, 'dMax': null };
      }


      this.ref.close(object_response_onclose);
    } else {
      this.ref.close();
    }
  }

  fnCancelCreateNewRatingPerceivedValue() {
    this.submitted = false;
    this.dismiss();
  }

  fnAdvancedSearchRatingPerceivedValue(price_calculation_summary_data) {
    this.submitted = true;
    console.log('price_calculation_summary_data: ', price_calculation_summary_data);
    const data_object = {
      'products': (price_calculation_summary_data.products) ? price_calculation_summary_data.products : [],
      'priceList': (price_calculation_summary_data.priceList) ? price_calculation_summary_data.priceList : [],
      'category': (price_calculation_summary_data.priceList) ? price_calculation_summary_data.priceList : [],
      'tProductsType': (this.data_filter_products_type) ? this.data_filter_products_type : [],
      'dCurrentPrice': {
        'dMin': this.min_value_current_price,
        'dMax': this.max_value_current_price == 0 ? '' : this.max_value_current_price,
      },
      'dPricePerceivedValue': {
        'dMin': this.min_value_price_perceived_value,
        'dMax': this.max_value_price_perceived_value == 0 ? '' : this.max_value_price_perceived_value,
      },
      'dPriceProductType': {
        'dMin': this.min_value_price_product_type,
        'dMax': this.max_value_price_product_type == 0 ? '' : this.max_value_price_product_type,
      },
      'dTheoreticalPrice': {
        'dMin': this.min_value_theoretical_price,
        'dMax': this.max_value_theoretical_price == 0 ? '' : this.max_value_theoretical_price,
      },
      'dSuggestedPrice': {
        'dMin': this.min_value_SuggestedPriceWithVAT,
        'dMax': this.max_value_SuggestedPriceWithVAT == 0 ? '' : this.max_value_SuggestedPriceWithVAT,
      },
      'dUserPrice': {
        'dMin': this.min_value_UserPrice,
        'dMax': this.max_value_UserPrice == 0 ? '' : this.max_value_UserPrice,
      },
      'dSuggestedPriceWithRelationships': {
        'dMin': this.min_value_suggested_price_with_relationships,
        'dMax': this.max_value_suggested_price_with_relationships == 0 ? '' : this.max_value_suggested_price_with_relationships,
      },
      'dVarRecommendedPrice': {
        'dMin': this.min_value_PriceWithVATVar,
        'dMax': this.max_value_PriceWithVATVar == 0 ? '' : this.max_value_PriceWithVATVar,
      },
      'dVarUserPrice': {
        'dMin': this.min_value_PriceWithVATVar_User,
        'dMax': this.max_value_PriceWithVATVar_User == 0 ? '' : this.max_value_PriceWithVATVar_User,
      },
      'page': 1,
      'pageSize': 12,
      'tSearch': '',
    };
    console.log('data_object: ', data_object);
    this.dismiss(data_object);
  }

}
