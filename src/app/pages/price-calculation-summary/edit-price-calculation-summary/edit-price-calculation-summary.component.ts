import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { ConfigService } from '../../../shared/api/services/config.service';
import { PriceCalculationSummaryService } from '../../../shared/api/services/price-calculation-summary.service';
import { ProductInformationService } from '../../../shared/api/services/product-information.service';

@Component({
  selector: 'ngx-edit-price-calculation-summary',
  templateUrl: './edit-price-calculation-summary.component.html',
  styleUrls: ['./edit-price-calculation-summary.component.scss'],
})
export class EditPriceCalculationSummaryComponent implements OnInit {

  submitted: boolean = false;
  current_payload: any = null;

  id_version: string;
  suggestedStage: boolean;
  id_Product: string;

  obj_form: any = {};
  
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  @Input() data: any;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private priceCalculationSummaryService: PriceCalculationSummaryService,
    private productInformationService: ProductInformationService,
    private configService: ConfigService,
    protected ref: NbDialogRef<EditPriceCalculationSummaryComponent>,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('priceCalculation', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();

        this.id_version = this.data['id_version'];
        this.id_Product = this.data['iIDProduct'];
        this.suggestedStage = this.data['suggestedStage'];

        this.obj_form = {
          'iIDPriceCalculationSummary': this.data['iIDPriceCalculationSummary'],
          'iIDForecastResult': this.data['iIDForecastResult'],
          'iIDVersion': parseInt(this.id_version, 10),
          'iIDProduct': this.data['iIDProduct'],
          'dUserPrice': this.data['dUserPrice'],
          'dSuggestedPriceWithRelationships': this.data['dSuggestedPriceWithRelationships'],
          'tProductCode': this.data['tProductCode'],
          'tProductName': this.data['tProductName'],
          'tPriceListName': this.data['tPriceListName'],
        };
      }
    });
  }


  /** Funcion que envia los datos al servicio de actalizar **/
  fnUpdateData(obj) {
    this.submitted = true;

    if (!this.suggestedStage) {
      this.obj_form.dSuggestedPriceWithRelationships = this.obj_form.dUserPrice;
    }

    this.priceCalculationSummaryService.fnHttpSetEditDataPriceCalculationSummary(this.current_payload, this.obj_form).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.fnUpdateConfigStage(this.current_payload, this.obj_form.iIDVersion);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessUpdate.text);
        this.dismiss();
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }
  /** **/


  fnUpdateConfigStage(current_payload, id_version) {
    this.submitted = true;
    const Stage = false;
    this.productInformationService.fnHttpUpdateSuggestedPrice(current_payload, id_version, Stage).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }


  /** Funciones para cancelar y cerrar **/
  fnCancelEdit() {
    this.submitted = false;
    this.dismiss();
  }

  dismiss() {
    this.ref.close();
  }
  /** **/

}
