import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';


import { FormControl, FormGroup, ReactiveFormsModule, FormsModule, FormBuilder, Validators, NgForm } from '@angular/forms';


/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options } from 'ng5-slider';

import { NbDialogService } from '@nebular/theme';
import { ModalsComponent } from '../../../shared/components/modals/modals.component';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { RatingPerceivedService } from '../../../shared/api/services/rating-perceived.service';
import { CompetitorsService } from '../../../shared/api/services/competitors.service';
import { PricesListService } from '../../../shared/api/services/prices-list.service';
import { ProductInformationService } from '../../../shared/api/services/product-information.service';
import { PriceCalculationSummaryService } from '../../../shared/api/services/price-calculation-summary.service';
import { ConfigService } from '../../../shared/api/services/config.service';




@Component({
  selector: 'ngx-apply-rules-price-calculation-summary',
  templateUrl: './apply-rules-price-calculation-summary.component.html',
  styleUrls: ['./apply-rules-price-calculation-summary.component.scss'],
})
export class ApplyRulesPriceCalculationSummaryComponent implements OnInit {

  submitted: boolean = false;
  current_payload: any = null;
  state_loading: boolean = true;

  @Input() id_version: string;

  data_form_settings_version: any = {};
  data_types: any = [];
  myForm: FormGroup;
  data_filter_products_type: string = '';
  objValidate: any = {};
  state_form: boolean = true;
  
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private ratingPerceivedService: RatingPerceivedService,
    private competitorsService: CompetitorsService,
    private categoriesService: CategoriesService,
    private pricesListService: PricesListService,
    private productInformationService: ProductInformationService,
    private priceCalculationSummaryService: PriceCalculationSummaryService,
    private configService: ConfigService,
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService,
    protected ref: NbDialogRef<ApplyRulesPriceCalculationSummaryComponent>,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('priceCalculation', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      // console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        /**START - Form validator**/
        /** Funciones para validar input**/
        this.myForm = this.formBuilder.group({
          dMinimumPriceReduction1: ['', Validators.compose([Validators.required, Validators.min(-100), Validators.max(0)])],
          dMinimumPriceReduction2: ['', Validators.compose([Validators.required, Validators.min(-100), Validators.max(0)])],
          dMinimumPriceReduction3: ['', Validators.compose([Validators.required, Validators.min(-100), Validators.max(0)])],
          dMaximumPriceReduction1: ['', Validators.compose([Validators.required, Validators.min(-100), Validators.max(0)])],
          dMaximumPriceReduction2: ['', Validators.compose([Validators.required, Validators.min(-100), Validators.max(0)])],
          dMaximumPriceReduction3: ['', Validators.compose([Validators.required, Validators.min(-100), Validators.max(0)])],
          dMaximumPriceIncrease1: ['', Validators.compose([Validators.required])],
          dMaximumPriceIncrease2: ['', Validators.compose([Validators.required])],
          dMaximumPriceIncrease3: ['', Validators.compose([Validators.required])],
        });
        /**END - Form validator**/
        this.fnGetAllDataConfigByVersion(this.current_payload, this.id_version);
      }
    });
  }

  fnGetAllDataConfigByVersion(current_payload, id_version?) {

    const object_data_send = {
      'iIDVersion': id_version,
    };

    this.configService.fnHttpGetAllDataConfigByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        const data_config = r.body;
        console.log('data_config : ', data_config);


        const object_data = {
          'iIDConfig': parseInt(data_config.iIDConfig, 10),
          'iIDVersion': parseInt(data_config.iIDVersion, 10),
          'bRoundPsychologicalPrices': data_config.bRoundPsychologicalPrices,
        };

        this.data_form_settings_version = object_data;
        this.data_types = data_config.productTypesVersion;
        console.log('this.data_types: ', this.data_types);
        this.state_loading = false;
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  dismiss(object_response_onclose?) {
    console.log('object_response_onclose: ', object_response_onclose);
    if (object_response_onclose) {
      this.ref.close(object_response_onclose);
    } else {
      this.ref.close();
    }
  }

  fnCancelCreateNewRatingPerceivedValue() {
    this.submitted = false;
    this.dismiss();
  }

  onBlurMethod(id, property, event) {
    console.log('id: ', id);
    console.log('property: ', property);
    console.log('event: ', event);
    let valor = event.target.value;
    console.log('valor: ', valor);
    valor = valor.replace('%', '');
    console.log('valor: ', valor);
    if (valor) {
      event.target.value = (valor + '%');
      this.data_types[id][property] = Number(valor) / 100;
      console.log('this.data_types[id][property]: ', this.data_types[id][property]);
    }
  }

  onKeydown(event) {
    const key = event ? event.which : event.keyCode;
    if (!(key >= 48 && key <= 57) && !(key >= 96 && key <= 105) &&
      (key != 8 && key != 190 && key != 188 && key != 9 && key != 13 && key != 110 && key != 46 && key)) {
      event.preventDefault();
    }
  }

   /**Funcion para validar**/

  fnValidate(caseValidate, value, validator, valueReference?){
    //**** Case 1: value min 1 max 5
    //**** Case 2: value min 0% max 100%
    //**** Case 3: value dont min than reference
    //**** Case 4: value should be negative and dont max than reference
    if(value === '' || value === '%'){
      this.objValidate[validator] = '.';
      this.state_form = false;
    }else{
      switch (caseValidate) {
        case 0:
          this.objValidate[validator] = false;
          this.state_form = true;
        break;
        case 1:
          if(value > 5 || value < 1){
            this.objValidate[validator] = true;
            this.state_form = false;
          }else{
            this.objValidate[validator] = false;
            this.state_form = true;
          }
        break;
        case 2:
          if(value > 1 || value < 0){
            this.objValidate[validator] = true;
            this.state_form = false;
          }else{
            this.objValidate[validator] = false;
            this.state_form = true;
          }
        break;
        case 3:
          if(value <= valueReference){
            this.objValidate[validator] = true;
            this.state_form = false;
          }else{
            this.objValidate[validator] = false;
            this.state_form = true;
          }
        break;
        case 4:
          if(value < -1 || value > 0){
            this.objValidate[validator] = 'range';
            this.state_form = false;
          }else{
            if(value <= valueReference){
              this.objValidate[validator] = false;
              this.state_form = true;
            }else{
              this.objValidate[validator] = true;
              this.state_form = false;
            }
          }
        break;
      }
    }
  }

 

  fnSaveConfigVersion(data_form_settings_version) {    
    console.log('data_form_settings_version: ', data_form_settings_version);
    console.log('this.data_types: ', this.data_types);

    const data_types_sent = [];
    let object_data_types: any = null;


    this.data_types.forEach(item_menu => {
      object_data_types = {
        'dMaximumPriceIncrease': parseFloat(item_menu.dMaximumPriceIncrease),
        'dMaximumPriceReduction': parseFloat(item_menu.dMaximumPriceReduction),
        'dMinimumPriceReduction': parseFloat(item_menu.dMinimumPriceReduction),
        'dPriceWeight': parseFloat(item_menu.dPriceWeight),
        'iIDProductTypeVersion': parseFloat(item_menu.iIDProductTypeVersion),
        'iPriceLevelFloor': parseFloat(item_menu.iPriceLevelFloor),
        'tProductTypeName': item_menu.tProductTypeName,
      };
      data_types_sent.push(object_data_types);
    });

    console.log('data_types_sent ===========================>>>>>>>>>', data_types_sent);
    this.submitted = true;
    console.log('data_form_settings_version: ', data_form_settings_version);
    const object_data = {
      'iIDConfig': parseInt(data_form_settings_version.iIDConfig, 10),
      'iIDVersion': parseInt(data_form_settings_version.iIDVersion, 10),
      'bRoundPsychologicalPrices': data_form_settings_version.bRoundPsychologicalPrices,
      'productTypesVersion': data_types_sent,
    };

    console.log('object_data: ', object_data);
    this.configService.fnHttpSetEditApplyRulesVersion(this.current_payload, object_data).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.fnGetAllDataConfigByVersion(this.current_payload, this.id_version);
        this.submitted = false;
        this.dismiss(true);
      }

      if (r.status == 206) {
        this.submitted = false;
        this.dismiss(true);
        this.utilitiesService.showToast('top-right', 'warning', 'Error!');
      }
    });
  }

  open(_title, _body) {
    this.dialogService.open(ModalsComponent, {
      context: {
        title: _title,
        body: _body,
      },
    });
  }

}
