import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng5SliderModule } from 'ng5-slider';

import { NbStepperModule } from '@nebular/theme';

import { EditPriceCalculationSummaryComponent } from './edit-price-calculation-summary/edit-price-calculation-summary.component';
import { AdvancedSearchPriceCalculationSummaryComponent } from './advanced-search-price-calculation-summary/advanced-search-price-calculation-summary.component';
import { ApplyRulesPriceCalculationSummaryComponent } from './apply-rules-price-calculation-summary/apply-rules-price-calculation-summary.component';

const ENTRY_COMPONENTS = [
  EditPriceCalculationSummaryComponent,
  AdvancedSearchPriceCalculationSummaryComponent,
  ApplyRulesPriceCalculationSummaryComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    NgSelectModule,
    FormsModule,
    TooltipModule.forRoot(),
    ModalModule,
    NgxPaginationModule,
    Ng5SliderModule,
    NbStepperModule,
  ],
  declarations: [
    EditPriceCalculationSummaryComponent,
    AdvancedSearchPriceCalculationSummaryComponent,
    ApplyRulesPriceCalculationSummaryComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class PriceCalculationSummaryModule { }
