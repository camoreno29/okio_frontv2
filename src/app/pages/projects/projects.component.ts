import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
/* ************+ Import module auth ************ */
import { StateService } from '../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbMenuItem, NbMenuService, NbSidebarService, NbDialogService } from '@nebular/theme';
import { LayoutService } from '../../@core/utils';

import { ProjectService } from '../../shared/api/services/project.service';
import { MenuService } from '../../shared/api/services/menu.service';
import { UtilitiesService } from '../../shared/api/services/utilities.service';
import { VersionService } from '../../shared/api/services/version.service';
import { AddProjectComponent } from './add-project/add-project.component';
import { PagesComponent } from '../pages.component';
// import { HeaderComponent } from '../../@theme/components/header/header.component';
import { DeleteProjectComponent } from './delete-project/delete-project.component';

declare var $: any;

@Component({
  selector: 'ngx-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss'],
})
export class ProjectsComponent implements OnInit, AfterViewInit {

  current_payload: string = null;
  company_name: string = null;
  id_company: any = null;
  projects: any = null;
  projects_original_collection: any = null;
  search_input: any = '';
  data_company_object: any = {};

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private sidebarService: NbSidebarService,
    private nbMenuService: NbMenuService,
    private layoutService: LayoutService,
    private projectService: ProjectService,
    private versionService: VersionService,
    private menuService: MenuService,
    private utilitiesService: UtilitiesService,
    private pagesComponent: PagesComponent,
  ) { }

  /* *************** START - NngOnit - Start class function *************** */
  ngOnInit() {
    const self = this; // Re denomination object this
    
    self.sidebarService.collapse('settings-sidebar');

    sessionStorage.setItem('iIDindex_card', null);
    sessionStorage.setItem('objmenu_select', null);

    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => {
    //   console.log('data:====> ', data);
    //   self.fnGetLanguage();
    // });

    /* *** START - JQuery definition *** */
    // JQuery ready
    $(document).ready(function () {
      $('#pgp-btn_toogle_side_bar').click(); // Emulate click display right sidebar to hide
      $('.menu-sidebar').removeClass('d-block').addClass('d-none'); // Hide left sidebar to this component
      $('#toggle-settings').removeClass('was-expanded').addClass('was-collapse'); // Hide right sidebar to this component
      $('#toggle-settings').removeClass('d-block').addClass('d-none'); // Hide right sidebar to this component
    });
    /* **** END - JQuery definition **** */
    /* *** START - Delete instance sesssionStorage - Project *** */
    const object_project = {
      'id_project': null,
      'name_project': null,
    };
    sessionStorage.setItem('project', JSON.stringify(object_project));
    /* **** END - Delete instance sesssionStorage - Project **** */
    /* *** START - Service validate session on start *** */
    // this.utilitiesService.fnGetCurrentTokenSession() -- Implementation utility service
    self.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      if (token.isValid()) {
        self.current_payload = token.getValue();
        // console.log('self.current_payload: ', self.current_payload);
        if (self.current_payload) {
          self.fnGetAllProjects(self.current_payload); // Get data all projects user
        }
      }
    });
    /* **** END - Service validate session on start **** */
  }
  /* **************** END - NngOnit - Start class function **************** */

  ngAfterViewInit() {
    //
  }

  fnGetAllProjects(current_payload) {
    this.projectService.fnHttpGetAllProjects(current_payload).subscribe(r => {
      if (r.status == 200) {
        this.projects = JSON.parse(JSON.stringify(r.body));
        this.projects_original_collection = JSON.parse(JSON.stringify(r.body));
      }
    }, err => {
      // console.log('err: ', err);
    });
  }

  open() {
    this.dialogService.open(AddProjectComponent).onClose.subscribe((res) => {
      this.fnGetAllProjects(this.current_payload);
    });
  }

  fnRedirectVersionsProject(project_name, id_project) {
    console.log('project_name: ', project_name);
    // sessionStorage.removeItem('project_name');
    // this.pagesComponent.fnChangeMenu(true);
    // console.log('id_project: ', id_project);
    // const object_project = {
    //   'id_project': id_project,
    //   'name_project': project_name,
    // };

    // sessionStorage.setItem('project', JSON.stringify(object_project));
    sessionStorage.setItem('project_name', project_name);
    sessionStorage.setItem('id_project', id_project);
    this.router.navigate(['/pages/versions-project', id_project]);
  }

  fnRedirectSettingsProject(project_name, id_project) {
    sessionStorage.setItem('project_name', project_name);
    sessionStorage.setItem('id_project', id_project);
    this.router.navigate(['/pages/settings-project', id_project]);
  }


  // toggleSidebar() {
  //   console.log('toggleSidebar: ');
  //   const self = this;
  //   self.sidebarService.toggle(true, 'menu-sidebar');
  //   self.layoutService.changeLayoutSize();
  // }

  // fnRedirectListCompanies() {
  //   this.router.navigate(['/pages/list-companies']);
  // }

  fnRedirectVersionLevelProject(id_project, project_name) {
    sessionStorage.setItem('id_version', '');
    this.versionService.fnHttpGetVersionDefaultByProject(this.current_payload, id_project).subscribe(r => {
      // console.log('r: ', r);
      if (r.status == 200) {
        console.log('id_project: ', id_project);
        const object_project = {
          'id_project': id_project,
          'name_project': project_name,
        };

        sessionStorage.setItem('project', JSON.stringify(object_project));
        sessionStorage.setItem('id_version', r.body.iIDVersion);
        this.utilitiesService.setData({ version: r.body.iIDVersion });
        this.router.navigate(['/pages/home/', r.body.iIDVersion]);
        
      }
    }, err => {
      // console.log('err: ', err);
    });
  }

  fnFilterProjects(projects, text_typing) {
    this.filterItems(projects, text_typing);
  }

  filterItems(projects, text_typing, field?) {
    const self = this;
    const toSearch = text_typing.toLowerCase();
    if (toSearch) {
      projects = JSON.parse(JSON.stringify(this.projects_original_collection));
      self.utilitiesService.fnGetDataFilter(projects, toSearch, function (data_collection) {
        self.projects = data_collection;
      });
    } else {
      self.projects = JSON.parse(JSON.stringify(self.projects_original_collection));
    }
  }

  // fnDeleteProject(id_company){
  //   console.log('id_company: ', id_company);
  //   console.log('current_payload: ', this.current_payload);

  //   this.projectService.fnHttpDeleteProject(this.current_payload, id_company).subscribe(r => {
  //     console.log('r: ', r);
  //     if (r.status == 200) {
  //       this.utilitiesService.showToast('top-right', 'success', 'The project has been removed successfully!');
  //       this.fnGetAllProjects(this.current_payload);
  //     }
  //   }, err => {
  //     console.log('err: ', err);
  //   });
  // }

  fnShowModalDeleteProjects(project_data) {
    console.log('project_data: ', project_data);
    this.data_company_object.id_project = project_data.iIDProject;
    this.data_company_object.name_project = project_data.tProjectName;
    this.dialogService.open(DeleteProjectComponent, { context: this.data_company_object }).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllProjects(this.current_payload);
    });
  }

  fnGetLanguage(){
    const self = this;
    self.utilitiesService.fnGetLanguage('projects', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module;
    });
  }

}
