import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

import { ProjectsComponent } from './projects.component';
import { AddProjectComponent } from './add-project/add-project.component';
import { SettingsProjectComponent } from './settings-project/settings-project.component';
import { DeleteProjectComponent } from './delete-project/delete-project.component';

const ENTRY_COMPONENTS = [
  AddProjectComponent,
  SettingsProjectComponent,
];

@NgModule({
  imports: [ThemeModule],
  declarations: [
    ProjectsComponent,
    AddProjectComponent,
    SettingsProjectComponent,
    DeleteProjectComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
    DeleteProjectComponent,
  ],
})
export class ProjectsModule { }
