import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbToastrService } from '@nebular/theme';

import { ProjectService } from '../../../shared/api/services/project.service';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';

@Component({
  selector: 'ngx-settings-project',
  templateUrl: './settings-project.component.html',
  styleUrls: ['./settings-project.component.scss']
})
export class SettingsProjectComponent implements OnInit {

  private index: number = 0;
  current_payload: string = null;
  project: any = {};
  id_project: any = null;
  id_company: any = null;
  originName: string;
  state: boolean = false;
  submitted: boolean = false;
  messages_error: any = [];
  project_name: string = null;

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private toastrService: NbToastrService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private utilitiesService: UtilitiesService) { }

  ngOnInit() {
    const self = this;
    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => { 
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    // });
    
    this.route.params.subscribe(params => {
      if (params.id_project) {
        this.id_project = params.id_project;
        this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          console.log('token: ', token);
          if (token.isValid()) {
            this.current_payload = token.getValue();
            if (this.current_payload) {
              this.project_name = sessionStorage.getItem('project_name');
              this.fnGetProject(this.current_payload, this.id_project);
            } else {
              return false;
            }
          }
        });
      } else {
        console.log('no params ========>');
        this.router.navigate(['/pages/add-company']);
      }
    });
  }


  fnActivateEditState(state?) {
    this.state = (this.state === true) ? false : true;
  }

  fnGetProject(current_payload?, id_project?) {
    console.log('current_payload: ', current_payload);

    this.projectService.fnHttpGetAllProjectsByPproject(current_payload, id_project).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.project = r.body;
        this.originName = this.project.tProjectName;
        this.id_company = this.project.iIDCompany;
        console.log('this.company : ', this.project);
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnUpdateProject(project) {
    this.submitted = true;
    const data_object = {
      'iIDProject': project.iIDProject,
      'iIDCompany': project.iIDCompany,
      'tProjectName': project.tProjectName,
      'dtCreatedDate': null,
    };

    console.log('data_object: ', data_object);

    this.projectService.fnHttpSetUpdateProject(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        console.log('r.status: ', r.status);
        this.submitted = false;
        this.state = false;
        this.id_project = r.body.iIDProject;
        this.fnGetProject(this.current_payload, this.id_project);
        this.showToast('top-right', 'success', this.DATA_LANG.msgSuccessUpdateProject.text);
        // this.router.navigate(['/pages/projects/', this.id_project]);
        this.router.navigate(['/pages/projects']);
      } else if (r.status == 206) {
        console.log('r.status: ', r.status);
        this.submitted = false;
        this.state = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
      }
    }, err => {
      console.log('err: ', err);
      this.submitted = false;
      this.messages_error = [{
        'state': 'Error',
        'color': 'danger',
        'description': 'New error',
      }];
      console.log('err: ', err);
    });

    this.fnActivateEditState();
  }

  showToast(position, status, message, icon?) {
    this.index += 1;
    this.toastrService.show(status, message,
      { position, status, icon });
  }

  fnCancelSaveCompany() {
    this.submitted = false;
    this.project['tProjectName'] = this.originName;
    // this.router.navigate(['/pages/projects/', this.id_project]);
    this.router.navigate(['/pages/projects']);
  }

  fnRedirectListProjects() {
    this.router.navigate(['/pages/projects']);
  }

  fnGetLanguage(){
    const self = this;
    self.utilitiesService.fnGetLanguage('projects', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
  }

}
