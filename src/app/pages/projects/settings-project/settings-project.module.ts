import { NgModule } from '@angular/core';
import { ThemeModule } from '../../../@theme/theme.module';
// import { SettingsProjectComponent } from './settings-project.component';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';

const ENTRY_COMPONENTS = [
  // SettingsProjectComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    NgSelectModule,
    FormsModule,
  ],
  declarations: [
    // SettingsProjectComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})

export class SettingsProjectModule { }
