import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { ProjectService } from '../../../shared/api/services/project.service';
import { VersionService } from '../../../shared/api/services/version.service';
//import { HomeComponent } from '../../home/home.component';

declare let $: any;

@Component({
  selector: 'ngx-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.scss']
})
export class AddProjectComponent implements OnInit {

  // @Input() title: string;
  project: any = {};
  current_payload: any = null;
  title: string = null;
  submitted: boolean = false;

  lang_nav: any = window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private projectService: ProjectService,
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private versionService: VersionService,
    //  private homeComponent: HomeComponent,
    protected ref: NbDialogRef<AddProjectComponent>) {
    console.log('title: ', this.title);
  }

  dismiss() {
    this.ref.close();
  }

  ngOnInit() {

    const self = this;

    console.log('this.route.params: ', this.route.params);
    this.route.params.subscribe(params => {
      console.log('params: ', params);
    });

    self.utilitiesService.fnGetLanguage('projects', function (res_lang) {
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });

    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
      }
    });
  }

  fnAddProject(project) {
    console.log('project: ', project);
    // let data_object = {
    //   'iIDCompany': 0,
    //   'tCompanyLogo': 'url_company_logo_',
    //   'tCompanyName': company.tCompanyName,
    // };
    const id_company = sessionStorage.getItem('id_company');
    this.submitted = true;
    let data_object = {
      'iIDProject': 0,
      'tProjectName': project.tProjectName,
      'dtCreatedDate': null,
    }

    console.log('this.current_payload: ', this.current_payload);
    console.log('data_object: ', data_object);
    this.projectService.fnHttpSetSaveNewProject(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.project = {};
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessCreateProject.text);
        this.dismiss();
        let id_project = r.body['iIDProject'];
        this.versionService.fnHttpGetVersionDefaultByProject(this.current_payload, id_project).subscribe(res => {
          const object_project = {
            'id_project': r.body['iIDProject'],
            'name_project': r.body['tProjectName'],
          };
          sessionStorage.setItem('project', JSON.stringify(object_project));
          sessionStorage.setItem('project_name', r.body['tProjectName']);
          sessionStorage.setItem('id_project', r.body['iIDProject']);
          sessionStorage.setItem('CategoriesRedirect', 'true');
          this.utilitiesService.setData({ version: res.body['iIDVersion'] });
          this.router.navigate(['/pages/home/' + res.body['iIDVersion']]);

          // this.router.navigate(['/pages/versions-project/' + r.body['iIDProject']]);

        });
        // this.router.navigate(['/pages/versions-project/1']);
        // this.companies = r.body;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(null, r.body.codMessage);
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnCancelSaveProject() {
    this.submitted = false;
    this.project['tProjectName'] = '';
    this.dismiss();
    // this.router.navigate(['/pages/versions-project', id_project]);
  }

}
