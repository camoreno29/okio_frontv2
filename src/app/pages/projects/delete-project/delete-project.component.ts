import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

import { Router, ActivatedRoute } from '@angular/router';

import { StateService } from '../../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';

import { ProjectService } from '../../../shared/api/services/project.service';

@Component({
  selector: 'ngx-delete-project',
  templateUrl: './delete-project.component.html',
  styleUrls: ['./delete-project.component.scss']
})
export class DeleteProjectComponent implements OnInit {

  @Input() id_project: String;
  @Input() name_project: String;
  

  current_payload: string = null;
  submitted: Boolean = false;

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    protected ref: NbDialogRef<DeleteProjectComponent>,
    private projectService: ProjectService,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('projects', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });

    console.log('id_project: ', this.id_project);
    console.log('name_project: ', this.name_project);
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
      }
    });
  }

  fnSetDeleteProject(id_project) {
    this.submitted = true;
    this.projectService.fnHttpDeleteProject(this.current_payload, id_project).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessDeleteProject.text);
        // this.dismiss(false);
        
      }
      if (r.status == 206) {
        this.submitted = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
      this.dismiss();
    }, err => {
      console.log('err: ', err);
    });
  }

  dismiss() {
    this.ref.close();
  }

  fnCancelDeleteProject() {
    this.dismiss();
  }

}
