import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// import * as $ from 'jquery';// import Jquery here

import { StateService } from '../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';
import { environment } from '../../../environments/environment';

import { FormControl, FormGroup, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';

import { UtilitiesService } from '../../shared/api/services/utilities.service';

import { ProductInformationService } from '../../shared/api/services/product-information.service';
import { ForecastResultsService } from '../../shared/api/services/forecast-results.service';
import { DashboardService } from '../../shared/api/services/dashboard.service';


import { ModalsComponent } from '../../shared/components/modals/modals.component';
import { HomeComponent } from '../home/home.component';
import { debug } from 'util';

declare let $: any;

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {

  url_host: any = environment.apiUrl;
  layoud_component_display: String = 'ProyecResult';
  list_forecast_results: any = null;
  numItemsPage: any = null;
  currentPage: any = null;
  loading: boolean = true;
  current_payload: string = null;
  id_version: any = null;

  userStage: boolean = false;
  totalItems: any = null;
  bShowCents: any = null;
  quantiy_decimals: any = 0;

  loadingChartPR: boolean = true;
  data_dashboard: any = [];
  obj_cards_dashboard: any = [];

  loadingChartRP: boolean = true;
  chartForecastResult: any = [];
  objLimits: any = [];

  lang_nav: any = window.navigator.language;
  DATA_LANG: any = null;
  DATA_LANG_GENERAL: any = null;

  title_gc = '';
  type_gc: string = 'ColumnChart';
  dataChart_gc = [['Average price', 0, '#00A7E1', 0], ['Volume', 0, '#156282', 0]];
  columnNames_gc = ['name', '', { 'role': 'style' }, { 'role': 'tooltip' }];
  width_gc = '600';
  height_gc = '340';
  options_gc = {
    hAxis: {},
    vAxis: { format: 'percent' },
    legend: 'none',
    chartArea: { left: 40, right: 40, bottom: 40, top: 40 },
  };


  showChart: boolean = false;
  type_gs: string = 'ScatterChart';
  dataChart_gs = [];
  columnNames_gs = ['Current', 'Suggested', { 'type': 'string', 'role': 'tooltip', 'p': { 'html': true } }, { 'role': 'style' }];
  width_gs = '600';
  height_gs = '340';
  options_gs = {};

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    private productInformationService: ProductInformationService,
    private forecastResultsService: ForecastResultsService,
    private dashboardService: DashboardService,
    private homeComponent: HomeComponent,
  ) { }


  ngOnInit() {

    const self = this;
    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => {
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    //   self.fnGetDataGraphDashboard(self.current_payload, self.id_version);
    //   self.fnGetDataCharts(self.current_payload, self.id_version);
    // });
    
    this.currentPage = 1;
    this.numItemsPage = 10;

    this.route.params.subscribe(params => {
      console.log('params: ', params);
      if (params.id_version) {
        this.id_version = params.id_version;
        this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          console.log('token: ', token);
          if (token.isValid()) {
            this.current_payload = token.getValue();
            console.log('this.current_payload: ', this.current_payload);
            if (this.current_payload) {
              this.fnGetDataCharts(this.current_payload, this.id_version);
              this.fnGetAllForecastResults(this.current_payload, this.id_version, 1);
              this.fnGetConfigStage(this.current_payload, this.id_version);
              this.fnGetConfigProducts(this.current_payload, this.id_version);
              this.fnGetDashboard(this.current_payload, this.id_version);
              this.fnGetDataGraphDashboard(this.current_payload, this.id_version);
              return false;
            }
          }
        });
      }
    });
  }


  /** Funcion para cambiar de Results projection a Portfolio Review **/
  fnSwitchViewData(state_view) {
    console.log('state_view: ', state_view);
    this.layoud_component_display = state_view;
  }
  /** **/

  /** Funciones para cargar los datos y Ordenar por columna **/
  fnGetConfigStage(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);
    this.loading = true;
    this.productInformationService.fnHttpGetSuggestedPrice(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.userStage = JSON.parse(JSON.stringify(!r.body));
        console.log('userStage: ', this.userStage);
      }
    }, err => {
      this.loading = false;
      console.log('err: ', err);
    });
  }

  fnGetConfigProducts(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);
    this.loading = true;
    this.productInformationService.fnHttpGetConfigProducts(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.bShowCents = r.body['bShowCents'];
        console.log('this.bShowCents: ', this.bShowCents);
        this.quantiy_decimals = (this.bShowCents) ? 2 : 0;
        console.log('this.quantiy_decimals: ', this.quantiy_decimals);
      }
    }, err => {
      this.loading = false;
      console.log('err: ', err);
    });
  }

  fnGetDashboard(current_payload, id_version?) {
    this.loading = true;
    this.dashboardService.fnHttpGetDashboard(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.obj_cards_dashboard = [
          { 'ProductCode': r.body.length > 0 ? r.body[0].iProductCode : null },
          { 'IncomeNet': r.body.length > 0 ? r.body[0].dIncomeNet.toFixed(this.quantiy_decimals) : null },
          { 'GrossContribution': r.body.length > 0 ? r.body[0].dGrossContribution.toFixed(this.quantiy_decimals) : null },
        ];
      }
      if (r.status == 206) {
        this.loading = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.loading = false;
      console.log('err: ', err);
      this.utilitiesService.showToast('top-right', '', 'Error consultando la información del baner!');
    });

  }

  fnGetDataGraphDashboard(current_payload, id_version?) {

    this.loadingChartPR = true;

    this.dashboardService.fnHttpGetGraphDashboard(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {

        let dPriceWithVATVar = 0;
        let dUnitsYearVar = 0;
        let dIncomeYearVar = 0;
        let dGrossContributionYearVar = 0;

        if (this.userStage) {
          dPriceWithVATVar = r.body.length > 0 ? r.body[0].dPriceWithVATVar_User.toFixed(1) : 0;
          dUnitsYearVar = r.body.length > 0 ? r.body[0].dUnitsYearVar_User.toFixed(1) : 0;
          dIncomeYearVar = r.body.length > 0 ? r.body[0].dIncomeYearVar_User.toFixed(1) : 0;
          dGrossContributionYearVar = r.body.length > 0 ? r.body[0].dGrossContributionYearVar_User.toFixed(1) : 0;
        } else {
          dPriceWithVATVar = r.body.length > 0 ? r.body[0].dPriceWithVATVar_Suggested.toFixed(1) : 0;
          dUnitsYearVar = r.body.length > 0 ? r.body[0].dUnitsYearVar_Suggested.toFixed(1) : 0;
          dIncomeYearVar = r.body.length > 0 ? r.body[0].dIncomeYearVar_Suggested.toFixed(1) : 0;
          dGrossContributionYearVar = r.body.length > 0 ? r.body[0].dGrossContributionYearVar_Suggested.toFixed(1) : 0;
        }

        this.data_dashboard = [
          { 'dPriceWithVATVar': dPriceWithVATVar },
          { 'dUnitsYearVar': dUnitsYearVar },
          { 'dIncomeYearVar': dIncomeYearVar },
          { 'dGrossContributionYearVar': dGrossContributionYearVar },
        ];

        this.dataChart_gc = [
          [this.DATA_LANG.charColnAverage.text, dPriceWithVATVar != 0 ? dPriceWithVATVar / 100 : 0, '#00A7E1', dPriceWithVATVar + '%'],
          [this.DATA_LANG.charColnVol.text, dUnitsYearVar != 0 ? dUnitsYearVar / 100 : 0, '#156282', dUnitsYearVar + '%'],
          [this.DATA_LANG.charColnNet.text, dIncomeYearVar != 0 ? dIncomeYearVar / 100 : 0, '#F79420', dIncomeYearVar + '%'],
          [this.DATA_LANG.charColnGross.text, dGrossContributionYearVar != 0 ? dGrossContributionYearVar / 100 : 0, '#037C8C', dGrossContributionYearVar + '%'],
        ];

        this.loadingChartPR = false;
      }
      if (r.status == 206) {
        this.loadingChartPR = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.loadingChartPR = false;
      console.log('err: ', err);
      this.utilitiesService.showToast('top-right', '', 'Error consultando la información de la grafica!');
    });
  }

  fnGetAllForecastResults(current_payload, id_version?, page?, text_search?) {

    const object_data_send = {
      'products': [],
      'priceList': [],
      'category': [],
      'dCurrentPriceWithVAT': {
        'dMin': null,
        'dMax': null,
      },
      'dPriceWithVAT_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dPriceWithVAT_User': {
        'dMin': null,
        'dMax': null,
      },
      'dPriceWithVATVar_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dPriceWithVATVar_User': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsCurrentYear': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsYear_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsYear_User': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsYearVar_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsYearVar_User': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearWithVATCurrent': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearWithVAT_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearWithVAT_User': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearVar_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearVar_User': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionCurrentYear': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionYear_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionYear_User': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionYearVar_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionYearVar_User': {
        'dMin': null,
        'dMax': null,
      },
      'dCurrentGrossMargin': {
        'dMin': null,
        'dMax': null,
      },
      'dSuggestedGrossMargin': {
        'dMin': null,
        'dMax': null,
      },
      'dUserGrossMargin': {
        'dMin': null,
        'dMax': null,
      },
      'page': (page) ? page : 1,
      'pageSize': 10,
      'tSearch': text_search,
    };

    this.list_forecast_results = [];

    this.forecastResultsService.fnHttpGetAllForecastResults(current_payload, id_version, object_data_send).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.list_forecast_results = JSON.parse(JSON.stringify(r.body.forecastResult));
        this.totalItems = r.body['totalItems'];
        console.log('this.list_forecast_results: ', this.list_forecast_results);
      }
      if (r.status == 206) {
        this.loading = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.loading = false;
      console.log('err: ', err);
      this.utilitiesService.showToast('top-right', '', 'Does not exist forecast results!');
    });
  }

  fnGetDataCharts(current_payload, id_version?, page?, text_search?) {

    this.loadingChartRP = true;

    const object_data_send = {
      'products': [],
      'priceList': [],
      'category': [],
      'dCurrentPriceWithVAT': {
        'dMin': null,
        'dMax': null,
      },
      'dPriceWithVAT_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dPriceWithVAT_User': {
        'dMin': null,
        'dMax': null,
      },
      'dPriceWithVATVar_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dPriceWithVATVar_User': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsCurrentYear': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsYear_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsYear_User': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsYearVar_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsYearVar_User': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearWithVATCurrent': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearWithVAT_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearWithVAT_User': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearVar_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearVar_User': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionCurrentYear': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionYear_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionYear_User': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionYearVar_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionYearVar_User': {
        'dMin': null,
        'dMax': null,
      },
      'dCurrentGrossMargin': {
        'dMin': null,
        'dMax': null,
      },
      'dSuggestedGrossMargin': {
        'dMin': null,
        'dMax': null,
      },
      'dUserGrossMargin': {
        'dMin': null,
        'dMax': null,
      },
      'page': 1,
      'pageSize': 10,
      'tSearch': text_search,
    };

    this.forecastResultsService.fnHttpGetGraphFiltersForecastResult(current_payload, id_version, object_data_send).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.chartForecastResult = JSON.parse(JSON.stringify(r.body.resultforecastResult.forecastResult));
        this.objLimits = JSON.parse(JSON.stringify(r.body.objLimits));

        this.chartForecastResult.forEach((value, key) => {
          const obj_ = [value.dSuggestedPrice, value.dCurrentPrice, this.createCustomHTMLContent(value), '#156282'];
          this.dataChart_gs.push(obj_);
        });

        if (this.chartForecastResult.length > 0) {
          this.fnGetConfigCharts();
        }

        console.log('this.chartForecastResult: ', this.chartForecastResult);
        console.log('this.objLimits: ', this.objLimits);
        console.log('this.dataChart_gs: ', this.dataChart_gs);

        this.loadingChartRP = false;
        if (this.dataChart_gs.length > 0) {
          this.showChart = true;
        }
      }
      if (r.status == 206) {
        this.loadingChartRP = false;
        this.showChart = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.loadingChartRP = false;
      this.showChart = false;
      console.log('err: ', err);
      this.utilitiesService.showToast('top-right', '', 'Error consultando la información de la grafica!');
    });
  }

  fnGetConfigCharts() {

    const xminValue: number = this.userStage ? this.objLimits.dX_User.dMin : this.objLimits.dX_Suggest.dMin;
    const xmaxValue: number = this.userStage ? this.objLimits.dX_User.dMax : this.objLimits.dX_Suggest.dMax;
    const yminValue: number = this.userStage ? this.objLimits.dY_User.dMin : this.objLimits.dY_Suggest.dMin;
    const ymaxValue: number = this.userStage ? this.objLimits.dY_User.dMax : this.objLimits.dY_Suggest.dMax;

    this.options_gs = {
      title: '',
      hAxis: { title: this.userStage ? this.DATA_LANG.xAxisUserPrice.text : this.DATA_LANG.xAxisSugPrice.text, minValue: yminValue, maxValue: ymaxValue },
      vAxis: { title: this.DATA_LANG.yAxisSugPrice.text, minValue: xminValue, maxValue: xmaxValue },
      legend: 'none',
      pointSize: 10,
      trendlines: {
        0: {
          color: '#F79420',
          lineWidth: 5,
          tooltip: false,
        },
      },
      fontName: 'Roboto',
      tooltip: { isHtml: true },
      chartArea: { left: 70, top: 20, bottom: 70, width: '80%' },
    };
  }

  createCustomHTMLContent(objLimits: any) {
    const nameStage = this.userStage ? this.DATA_LANG.charUser.text : this.DATA_LANG.charSuggested.text;
    return '<div class="p-2">' +
      '<table class="table table-bordered pgp-table">' +
      '<tr><td><b>' + objLimits.tProductCode + '</b></td></tr>' +
      '<tr><td><b><small> ' + this.DATA_LANG.charCurrent.text + ' </small>' + parseFloat(objLimits.dCurrentPrice).toFixed(this.quantiy_decimals) + '</b></td></tr>' +
      '<tr><td><b><small> ' + nameStage + ' </small>' + parseFloat(objLimits.dSuggestedPrice).toFixed(this.quantiy_decimals) + '</b></td></tr>' +
      '</table>' + '</div>';
  }
  /** **/

  /** Funciones para Pagindo **/
  getPage(page: number) {
    console.log('page: ', page);
    this.loading = true;
    this.fnGetAllForecastResults(this.current_payload, this.id_version, page);
    this.currentPage = page;
  }
  /****/

  /** Funciones para redireccionar **/

  redirectResultProjection() {
    this.homeComponent.fnChangeDashboardMenu();
  }

  /****/

  /*********Show modal parameters function *********/
  open(_title, _body) {
    this.dialogService.open(ModalsComponent, {
      context: {
        title: _title,
        body: _body
      },
    });
  }
  /****/

  fnGetLanguage() {
    const self = this;
    self.utilitiesService.fnGetLanguage('dashboard', function (res_lang) {
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module,
        self.DATA_LANG_GENERAL = res_lang.general
    });
  }

}
