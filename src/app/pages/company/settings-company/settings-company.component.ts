import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbToastrService } from '@nebular/theme';

import { CompaniesService } from '../../../shared/api/services/companies.service';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';


@Component({
  selector: 'ngx-settings-company',
  templateUrl: './settings-company.component.html',
  styleUrls: ['./settings-company.component.scss'],
})

export class SettingsCompanyComponent implements OnInit {

  private index: number = 0;
  current_payload: string = null;
  company: any = {};
  originName: string;
  state: boolean = false;
  submitted: boolean = false;
  messages_error: any = [];

  constructor(
    private toastrService: NbToastrService,
    private authService: NbAuthService,
    public router: Router,
    private companiesService: CompaniesService,
    private utilitiesService: UtilitiesService) { }

  ngOnInit() {
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
        const data_id_company = sessionStorage.getItem('id_company');
        this.fnGetCompany(this.current_payload, data_id_company);
      }
    });
  }

  fnActivateEditState(state?) {
    this.state = (this.state === true) ? false : true;
  }

  fnGetCompany(current_payload?, id_company?) {
    console.log('current_payload: ', current_payload);
    this.companiesService.fnHttpGetCompaniesbyIDCompany(current_payload, id_company).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.company = r.body;
        this.originName = this.company.tCompanyName;
        console.log('this.company : ', this.company);
      }
    }, err => {
      console.log('err: ', err);
    });
  }


  fnUpdateCompany(company) {
    this.submitted = true;
    const data_object = {
      'iIDCompany': company.iIDCompany,
      'tCompanyName': company.tCompanyName,
    };

    console.log('data_object: ', data_object);

    this.companiesService.fnHttpSetUpdateCompanies(this.current_payload, data_object).subscribe(r => {
      if (r.status == 200) {
        console.log('r.status: ', r.status);
        this.submitted = false;
        this.state = false;
        this.fnGetCompany(this.current_payload);
        this.showToast('top-right', 'success', 'Company has been update successfully!');
      } else if (r.status == 206) {
        this.submitted = false;
        this.state = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
      }
    }, err => {
      this.submitted = false;
      this.messages_error = [{
        'state': 'Error',
        'color': 'danger',
        'description': 'New error',
      }];
      console.log('err: ', err);
    });

    this.fnActivateEditState();
  }

  showToast(position, status, message, icon?) {
    this.index += 1;
    this.toastrService.show(status, message,
      { position, status, icon });
  }

  fnCancelSaveCompany() {
    this.submitted = false;
    this.company['tCompanyName'] = this.originName;
    this.router.navigate(['/pages/list-companies']);
  }

}
