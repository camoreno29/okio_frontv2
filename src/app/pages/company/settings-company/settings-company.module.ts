import { NgModule } from '@angular/core';
import { ThemeModule } from '../../../@theme/theme.module';

import { SettingsCompanyComponent } from './settings-company.component';

@NgModule({
  imports: [ThemeModule],
  declarations: [
    SettingsCompanyComponent,
  ],
})
export class SettingsCompanyModule { }
