import { NgModule } from '@angular/core';
import { ThemeModule } from '../../../@theme/theme.module';

import { AddCompanyComponent } from './add-company.component';

@NgModule({
  imports: [ThemeModule],
  declarations: [
    AddCompanyComponent,
  ],
})
export class AddCompanyModule { }
