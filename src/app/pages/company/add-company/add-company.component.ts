import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbToastrService } from '@nebular/theme';

import { StateService } from '../../../@core/utils';
import { CompaniesService } from '../../../shared/api/services/companies.service';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';
declare var $: any;
@Component({
  selector: 'ngx-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.scss']
})
export class AddCompanyComponent implements OnInit {

  private index: number = 0;
  current_payload: string = null;
  company: any = {};
  companies: any = null;
  submitted: boolean = false;

  constructor(
    private toastrService: NbToastrService,
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private companiesService: CompaniesService,
    private utilitiesService: UtilitiesService) { }

  ngOnInit() {

    $(document).ready(function () {
      $('.menu-sidebar').addClass('d-none');
      $('i .nb-menu').addClass('d-none');
      // $('.menu-sidebar').removeClass('expanded').addClass('compacted');
    });

    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
      }
    });
  }

  fnAddCompany(company) {
    this.submitted = true;
    let data_object = {
      'iIDCompany': 0,
      'tCompanyLogo': 'url_company_logo_',
      'tCompanyName': company.tCompanyName,
    };

    console.log('this.current_payload: ', this.current_payload);
    console.log('data_object: ', data_object);

    this.companiesService.fnHttpSetCompanies(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.showToast('top-right', 'success', 'Company has been created successfully!');
        const iIDCompany = r.body.iIDCompany;
        setTimeout(() => {
          // this.router.navigateByUrl('/RefrshComponent', { skipLocationChange: true }).then(
          //   () => this.router.navigate(['/pages/projects', iIDCompany])
          // );
          this.router.navigate(['/pages/projects', iIDCompany]);
        }, 1700);
        this.companiesService.fnHttpGetCompanies(this.current_payload).subscribe(resp => {
          console.log('resp: ', resp);
        });
        // this.companies = r.body;
      } else {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  showToast(position, status, message, icon?) {
    this.index += 1;
    this.toastrService.show(status, message,
      { position, status, icon });
  }

  fnCancelSaveCompany() {
    this.submitted = false;
    this.company['tCompanyName'] = '';
    this.router.navigate(['/pages/list-companies']);
  }

}
