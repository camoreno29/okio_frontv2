import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
/* ************+ Import module auth ************ */
import { StateService, LayoutService } from '../../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbMenuItem, NbMenuService, NbSidebarService, NbDialogService } from '@nebular/theme';

import { ProjectService } from '../../../shared/api/services/project.service';
import { MenuService } from '../../../shared/api/services/menu.service';
// import { AddProjectComponent } from './add-project/add-project.component';
import { CompaniesService } from '../../../shared/api/services/companies.service';
import { PagesComponent } from '../../../pages/pages.component';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';
declare var $: any;

@Component({
  selector: 'ngx-list-companies',
  templateUrl: './list-companies.component.html',
  styleUrls: ['./list-companies.component.scss']
})
export class ListCompaniesComponent implements OnInit, AfterViewInit {

  current_payload: string = null;
  id_company: any = null;
  projects: any = null;
  companies: any = null;
  data_company_object: any = {};

  public customerData: any;
  public searchText: string;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private sidebarService: NbSidebarService,
    private nbMenuService: NbMenuService,
    private layoutService: LayoutService,
    private projectService: ProjectService,
    private menuService: MenuService,
    private utilitiesService: UtilitiesService,
    private companiesService: CompaniesService,
    private pagesComponent: PagesComponent,
  ) {

    this.customerData = [
      {'name': 'Anil kumar', 'Age': 34, 'blog' :'https://code-view.com'},
      {'name': 'Sunil Kumar Singh', 'Age': 28, 'blog' :'https://code-sample.xyz'},
      {'name': 'Sushil Singh', 'Age': 24, 'blog' :'https://code-sample.com'},
      {'name': 'Aradhya Singh', 'Age': 5, 'blog' :'https://code-sample.com'},
      {'name': 'Reena Singh', 'Age': 28, 'blog' :'https://code-sample.com'},
      {'name': 'Alok Singh', 'Age': 35, 'blog' :'https://code-sample.com'},
      {'name': 'Dilip Singh', 'Age': 34, 'blog' :'https://code-sample.com'}];

  }

  ngAfterViewInit() {
    const self = this;
    console.log('Hooooooooooooooooooooooooooooollllllaaaaaaa');
    $(document).ready(function() {
      $('.menu-sidebar').removeClass('d-block');
      // $('.menu-sidebar').removeClass('expanded').addClass('compacted');
    });
    self.toggleSidebar();
  }

  ngOnInit() {
    const self = this;

    $(document).ready(function() {
      $('.menu-sidebar').addClass('d-none');
      // $('.menu-sidebar').removeClass('expanded').addClass('compacted');
    });

    self.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        self.current_payload = token.getValue();
        console.log('self.current_payload: ', self.current_payload);
        self.fnListCompanies(self.current_payload);
      }
    });
  }

  fnListCompanies(current_payload?) {
    let token = (current_payload) ? current_payload : this.current_payload;
    this.companiesService.fnHttpGetCompanies(token).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.companies = r.body.reverse();
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnCompanySelected(company, id_company) {
    sessionStorage.removeItem('company_name');
    console.log('company: ', company);
    const self = this;
    console.log('id_company: ', id_company);
    // self.pagesComponent.fnSetMenuConfig();
    self.utilitiesService.fnGetCurrentTokenSession(function(current_token_session) {
      console.log('current_token_session: ', current_token_session);
      // self.sidebarService.toggle(true, 'right');
      // self.sidebarService.toggle(false, 'settings-sidebar');
      sessionStorage.setItem('id_company', id_company);
      sessionStorage.setItem('company_name', company.tCompanyName);
      sessionStorage.setItem('data_company', JSON.stringify(company));
      self.id_company = id_company;
      // $('#toggle-settings').click();
      // self.toggleSidebar();
      // self.router.navigate(['/pages/projects', {'id_company': id_company}]);
      self.router.navigate(['/pages/projects', id_company]);
      // self.projectComponenet.fnGetAllProjects(current_token_session, id_company);
      // self.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then( () => self.router.navigate(['/pages/projects/', {'id_company': id_company}]) );
      // self.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then( () => self.router.navigate(['/pages/projects/', id_company]) );
      
    });
  }

  createNewCompany(id_company) {
    this.router.navigate(['/pages/add-company']);
  }
















  fnGetAllProjects(current_payload, id_company) {
    console.log('current_payload: ', current_payload);

    const object_data_send = {
      'iIDCompany': id_company,
    }

    this.projectService.fnHttpGetAllProjects(current_payload).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.projects = r.body;
        console.log('this.projects : ', this.projects);
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  open(id_company) {
    console.log('id_company: ', id_company);
    // this.dialogService.open(AddProjectComponent, {
    //   context: {
    //     title: 'sefeffefeferergergregergregr',
    //   },
    // });

    // this.dialogService.open(AddProjectComponent).onClose.subscribe((res) => {
    //   this.fnGetAllProjects(this.current_payload, id_company);
    // });
  }

  fnRedirectVersionsProject(id_project) {
    // this.pagesComponent.fnChangeMenu(true);
    console.log('id_project: ', id_project);
    this.router.navigate(['/pages/versions-project', id_project]);
  }

  toggleSidebar() {
    console.log('toggleSidebar: ');
    const self = this;
    self.sidebarService.toggle(true, 'menu-sidebar');
    self.layoutService.changeLayoutSize();
  }

}
