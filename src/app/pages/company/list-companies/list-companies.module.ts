import { NgModule } from '@angular/core';
import { ThemeModule } from '../../../@theme/theme.module';

import { ListCompaniesComponent } from './list-companies.component';

@NgModule({
  imports: [ThemeModule],
  declarations: [
    ListCompaniesComponent,
  ],
})
export class ListCompaniesModule { }
