import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as $ from 'jquery';// import Jquery here

import { StateService } from '../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';
import { environment } from '../../../environments/environment';

import { FormControl, FormGroup, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';

import { UtilitiesService } from '../../shared/api/services/utilities.service';
import { RatingPerceivedService } from '../../shared/api/services/rating-perceived.service';
import { CategoriesService } from '../../shared/api/services/categories.service';

import { AddRatingPerceivedComponent } from './add-rating-perceived/add-rating-perceived.component';
import { EditRatingPerceivedComponent } from './edit-rating-perceived/edit-rating-perceived.component';
import { EditCategoryScoringComponent } from './edit-category-scoring/edit-category-scoring.component';
import { DeleteRatingsPerceivedComponent } from './delete-ratings-perceived/delete-ratings-perceived.component';
import { DeleteAllRatingsPerceivedComponent } from './delete-all-ratings-perceived/delete-all-ratings-perceived.component';
import { AdvancedSearchRatingPerceivedComponent } from './advanced-search-rating-perceived/advanced-search-rating-perceived.component';

import { ModalsComponent } from '../../shared/components/modals/modals.component';

@Component({
  selector: 'ngx-rating-perceived',
  templateUrl: './rating-perceived.component.html',
  styleUrls: ['./rating-perceived.component.scss'],
})
export class RatingPerceivedComponent implements OnInit {

  url_host: any = environment.apiUrl;

  obj_category: any = {};
  obj_scoring: any = {};
  categoryScoring: any = null;
  current_payload: string = null;
  id_version: any = null;
  id_category: any = null;
  submitted: any = false;
  list_categories_collection: any = null;
  categories_original_collection: any = null;
  list_perceived_value_qualifications: any = null;
  perceived_value_qualifications_original_collection: any = null;
  search_input: any = '';
  fileToUpload: File = null;
  state_sort_data: any = 'ASC';

  numItemsPage: any = null;
  currentPage: any = null;
  file_import_input: any = null;

  search_state: boolean = false;

  object_filter: any = {};

  data_category: any = {};
  data_category_select: any = {};
  list_options_show_cents: any = [{ 'value': true, 'name': 'Yes' }, { 'value': false, 'name': 'No' }];

  layout_state: string = 'show_loading_layout'; // State: 'show_loading_layout' => Display layout pulse loading, 'show_table_layout' => Display table results, 'show_empty_data_layout' => Display layout empty data results

  loadingChart: boolean = true;

  // lang_nav: any =  window.navigator.language;
  lang_nav: any = 'en';
  DATA_LANG: any = null;
  DATA_LANG_GENERAL: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    private ratingPerceivedService: RatingPerceivedService,
    private categoriesService: CategoriesService,
    // private deleteCategoriesComponent: DeleteCategoriesComponent,
    // private addRatingPerceivedComponent: AddRatingPerceivedComponent,
  ) { }

  ngOnInit() {
    const self = this;
    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => { 
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    // });
   
    self.currentPage = 1;
    self.numItemsPage = 12;
    // self.numItemsPage = 2;

    self.route.params.subscribe(params => {
      console.log('params: ', params);
      if (params.id_version) {
        self.id_version = params.id_version;
        self.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          console.log('token: ', token);
          if (token.isValid()) {
            // here we receive a payload from the token and assigne it to our `user` variable GET /api/PerceivedValueQualifications/GetCategoriesScoring
            self.current_payload = token.getValue();
            console.log('self.current_payload: ', self.current_payload);
            if (self.current_payload) {
              self.object_filter['competitors'] = [];
              self.fnGetListCategoriesByVersion(self.current_payload, self.id_version);
              return false;
            }
          }
        });
      }
    });
  }

  fnGetListCategoriesByVersion(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);
    this.layout_state = 'show_loading_layout';
    const object_data_send = {
      'iIDCompany': id_version,
    }
    this.list_categories_collection = [];
    this.categoriesService.fnHttpGetDataListCategoriesByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_categories_collection = JSON.parse(JSON.stringify(r.body));
        this.categories_original_collection = JSON.parse(JSON.stringify(r.body));
        // console.log('this.members : ', this.members );
        const id_first_category = this.list_categories_collection[0]['iIDCategory'];
        const id_category = this.list_categories_collection[0]['iIDCategory'];
        this.id_category = this.list_categories_collection[0]['iIDCategory'];
        console.log('this.id_category: ', this.id_category);
        this.data_category_select = this.list_categories_collection[0];
        this.fnGetAllRatingsValuePerceived(this.current_payload, id_first_category);
        this.fnGetCategoriesScoring(this.current_payload, id_first_category);
        this.loadingChart = false;
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetCategoriesScoring(current_payload, iIDCategory) {
    console.log('current_payload: ', current_payload);
    this.categoriesService.fnHttpGetDataCategoriesScoring(current_payload, iIDCategory).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.obj_scoring = JSON.parse(JSON.stringify(r.body));
        this.categoryScoring = this.obj_scoring ? this.obj_scoring[0].iVersionScoring : null;
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetAllRatingsValuePerceived(current_payload, id_category?) {
    this.layout_state = 'show_loading_layout';
    console.log('current_payload: ', current_payload);

    const object_data_send = {
      'competitors': [],
      'iMinWeightCompetitor': null,
      'iMaxWeightCompetitor': null,
      'iMinCompetitorQualification': null,
      'iMaxCompetitorQualification': null,
    };
    this.list_perceived_value_qualifications = [];
    this.ratingPerceivedService.fnHttpGetAllPerceivedValueQualifications(current_payload, id_category, object_data_send).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_perceived_value_qualifications = JSON.parse(JSON.stringify(r.body));
        this.perceived_value_qualifications_original_collection = JSON.parse(JSON.stringify(r.body));
        console.log('this.list_perceived_value_qualifications: ', this.list_perceived_value_qualifications);
        if (this.list_perceived_value_qualifications.length > 0) {
          this.data_category = this.list_perceived_value_qualifications[0];
          console.log('this.list_perceived_value_qualifications[0]: ', this.list_perceived_value_qualifications[0]);
          this.layout_state = 'show_table_layout';
        } else {
          this.utilitiesService.showToast('top-right', 'warning', 'Do not exist ratings, add new rating!');
          // this.layout_state = 'show_empty_data_layout';
          this.layout_state = 'show_table_layout';
        }
      }
      this.fnValidateStateSearch(this.object_filter);
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetAllRatingsValuePerceivedFilter(current_payload, object_filter, id_category?) {
    console.log('id_category: ', id_category);
    console.log('object_filter: ', object_filter);
    console.log('current_payload: ', current_payload);

    this.list_perceived_value_qualifications = [];
    this.ratingPerceivedService.fnHttpGetAllPerceivedValueQualifications(current_payload, id_category, object_filter).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_perceived_value_qualifications = JSON.parse(JSON.stringify(r.body));
        this.perceived_value_qualifications_original_collection = JSON.parse(JSON.stringify(r.body));
        console.log('this.list_perceived_value_qualifications: ', this.list_perceived_value_qualifications);
        if (this.list_perceived_value_qualifications.length > 0) {
          this.data_category = this.list_perceived_value_qualifications[0];
          console.log('this.list_perceived_value_qualifications[0]: ', this.list_perceived_value_qualifications[0]);
          this.layout_state = 'show_table_layout';
          // console.log('this.data_category: ', this.data_category);
          // this.id_category = this.list_perceived_value_qualifications[0]['iIDCategory'];
          // console.log('r: ', r);
        } else {
          this.utilitiesService.showToast('top-right', 'warning', 'Do not exist ratings, add new rating!');
          this.layout_state = 'show_empty_data_layout';
        }
      }
      this.fnValidateStateSearch(this.object_filter);
    }, err => {
      console.log('err: ', err);
    });
  }

  showModalAdvancedSearchRatingPerceived(obj_category) {
    console.log('obj_category: ', obj_category);
    console.log('this.data_category: ', this.data_category);

    console.log('this.object_filter: ', this.object_filter);
    obj_category.id_version = this.id_version;
    obj_category.data_category = this.data_category;
    obj_category.object_filter = this.object_filter;
    this.dialogService.open(AdvancedSearchRatingPerceivedComponent, { context: obj_category }).onClose.subscribe((res) => {
      console.log('res: ', res);

      // this.data_items_filtered = {
      //   'competitors': res['competitors'],
      //   'competitor_qualification': { 'min': res['iMinCompetitorQualification'], 'max': res['iMaxCompetitorQualification'] },
      //   'competitor_weight': { 'min': res['iMinWeightCompetitor'], 'max': res['iMaxWeightCompetitor'] },
      // }

      console.log('obj_category: ', obj_category);
      this.id_category = obj_category['data_category']['category']['iIDCategory'];
      console.log('this.id_category: ', this.id_category);

      const object_data_send = {
        'competitors': (res['competitors']) ? res['competitors'] : [],
        'iMinWeightCompetitor': res['iMinWeightCompetitor'],
        'iMaxWeightCompetitor': res['iMaxWeightCompetitor'],
        'iMinCompetitorQualification': res['iMinCompetitorQualification'],
        'iMaxCompetitorQualification': res['iMaxCompetitorQualification'],
      };
      this.object_filter = object_data_send;
      console.log('this.object_filter: ', this.object_filter);
      console.log('object_data_send: ', object_data_send);

      // this.competitors_filter = res.competitors;
      // this.competitors_filter = res.competitors;

      // this.fnGetAllRatingsValuePerceived(this.current_payload, this.id_version);
      this.fnGetAllRatingsValuePerceivedFilter(this.current_payload, object_data_send, this.id_category);
    });
  }

  showModalAddCategory(obj_category) {
    console.log('obj_category: ', obj_category);
    console.log('this.data_category: ', this.data_category);
    obj_category.id_version = this.id_version;
    obj_category.data_category = this.data_category_select;
    this.dialogService.open(AddRatingPerceivedComponent, { context: obj_category }).onClose.subscribe((res) => {
      console.log('res: ', res);
      console.log('obj_category: ', obj_category);
      this.id_category = obj_category.data_category['iIDCategory'];
      this.fnGetAllRatingsValuePerceived(this.current_payload, this.id_category);
      this.fnGetCategoriesScoring(this.current_payload, this.id_category);
    });
  }

  showModalEditCategory(obj_category, id_category) {
    console.log('obj_category: ', obj_category);
    console.log('id_category: ', id_category);
    // console.log('obj_category: ', obj_category);
    // obj_category.category['iIDCategory'] = id_category;
    // this.dialogService.open(EditRatingPerceivedComponent, { context: obj_category }).onClose.subscribe((res) => {
    //   console.log('res: ', res);
    //   this.fnGetAllRatingsValuePerceived(this.current_payload, this.id_category);
    // });

    // console.log('obj_category: ', obj_category);
    // console.log('this.data_category: ', this.data_category);
    obj_category.id_version = this.id_version;
    obj_category.data_category = obj_category;
    this.dialogService.open(EditRatingPerceivedComponent, { context: obj_category }).onClose.subscribe((res) => {
      console.log('res: ', res);
      console.log('obj_category: ', obj_category);
      this.id_category = obj_category['category']['iIDCategory'];
      this.fnGetAllRatingsValuePerceived(this.current_payload, this.id_category);
      this.fnGetCategoriesScoring(this.current_payload, this.id_category);
    });
  }

  showModalEditQualification(obj_qualification) {
    const data_qualification = { 'data_qualification': obj_qualification };
    this.dialogService.open(EditCategoryScoringComponent, { context: data_qualification }).onClose.subscribe((res) => {
      console.log('res: ', res);
      console.log('obj_qualification: ', obj_qualification);
      this.id_category = obj_qualification[0].iIDCategory;
      this.fnGetCategoriesScoring(this.current_payload, this.id_category);
    });
  }

  fnDeleteRatingsValuePerceived(data_rating_value_perceived) {
    data_rating_value_perceived.id_version = this.id_version;
    console.log('data_rating_value_perceived: ', data_rating_value_perceived);
    this.dialogService.open(DeleteRatingsPerceivedComponent, { context: data_rating_value_perceived }).onClose.subscribe((res) => {
      console.log('res: ', res);
      console.log('obj_category: ', data_rating_value_perceived);
      this.id_category = data_rating_value_perceived['category']['iIDCategory'];
      console.log('this.id_category: ', this.id_category);
      this.fnGetAllRatingsValuePerceived(this.current_payload, this.id_category);
      this.fnGetCategoriesScoring(this.current_payload, this.id_category);
    });
  }

  fnDeleteAllCategories(obj_category) {
    console.log('obj_category: ', obj_category);
    obj_category.id_version = this.id_version;
    console.log('obj_category: ', obj_category);
    this.dialogService.open(DeleteAllRatingsPerceivedComponent, { context: obj_category }).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllRatingsValuePerceived(this.current_payload, this.id_category);
      this.fnGetCategoriesScoring(this.current_payload, this.id_category);
    });
  }

  fnFilterRatingPersivedValue(list_perceived_value_qualifications, text_typing) {
    console.log('text_typing: ', text_typing);
    console.log('list_perceived_value_qualifications: ', list_perceived_value_qualifications);
    this.filterItems(list_perceived_value_qualifications, text_typing);
    this.fnValidateStateSearch(this.object_filter);
  }

  filterItems(list_perceived_value_qualifications, text_typing, field?) {
    console.log('list_perceived_value_qualifications: ', list_perceived_value_qualifications);
    const self = this;
    const toSearch = text_typing.toLowerCase();
    if (toSearch) {
      list_perceived_value_qualifications = JSON.parse(JSON.stringify(self.perceived_value_qualifications_original_collection));

      const data_perceived_value_qualifications: any = [];

      list_perceived_value_qualifications.forEach((value, key) => {

        const obj_ = {
          'iIDVersion': value.iIDVersion,
          'iIDPerceivedValueQualification': value.iIDPerceivedValueQualification,
          'tCompetitorName': value.competitor.tCompetitorName,
          'tComment': value.tComment,
          'iWeightCompetitor': value.iWeightCompetitor.toString(),
          'iCompetitorQualification': value.iCompetitorQualification.toString(),
        };
        data_perceived_value_qualifications.push(obj_);
      });

      self.utilitiesService.fnGetDataFilter(data_perceived_value_qualifications, toSearch, function (data_collection) {

        console.log('data_collection: ', data_collection);
        const new_data_collection: any = [];

        data_collection.forEach((value, key) => {
          self.list_perceived_value_qualifications.forEach((element, key2) => {
            if (value.iIDPerceivedValueQualification === element.iIDPerceivedValueQualification) {
              new_data_collection.push(element);
            }

          });
        });

        self.list_perceived_value_qualifications = new_data_collection;
        console.log('self.list_perceived_value_qualifications: ', self.list_perceived_value_qualifications);
      });

    } else {
      self.list_perceived_value_qualifications = JSON.parse(JSON.stringify(self.perceived_value_qualifications_original_collection));
      console.log('self.list_perceived_value_qualifications: ', self.list_perceived_value_qualifications);
    }
  }

  fnExportCategories() {
    const lang = localStorage.getItem('language_app');
    window.open(
      this.url_host + '/api/PerceivedValueQualifications/GetExportPerceivedValueQualifications?iIDVersion=' + this.id_version + '&language=' + lang,
      '_blank');
  }

  sortColumn(field) {
    console.log(`sortColumn(field)`);
    console.log('field: ', field);
    const self = this;
    switch (field) {
      case 'tCompetitorName':
        if (self.state_sort_data === 'ASC') {
          self.list_perceived_value_qualifications.sort(function(a, b) {return (a.competitor.tCompetitorName > b.competitor.tCompetitorName) ? 1 : ((b.competitor.tCompetitorName > a.competitor.tCompetitorName) ? -1 : 0); } );
          self.state_sort_data = 'DESC';
        } else {
          self.list_perceived_value_qualifications.sort(function(a, b) {return (b.competitor.tCompetitorName > a.competitor.tCompetitorName) ? 1 : ((a.competitor.tCompetitorName > b.competitor.tCompetitorName) ? -1 : 0); } );
          self.state_sort_data = 'ASC';
        }
        break;
      case 'tComment':
        if (self.state_sort_data === 'ASC') {
          self.list_perceived_value_qualifications.sort(function(a, b) {return (a.tComment > b.tComment) ? 1 : ((b.tComment > a.tComment) ? -1 : 0); } );
          self.state_sort_data = 'DESC';
        } else {
          self.list_perceived_value_qualifications.sort(function(a, b) {return (b.tComment > a.tComment) ? 1 : ((a.tComment > b.tComment) ? -1 : 0); } );
          self.state_sort_data = 'ASC';
        }
        break;
        case 'iWeightCompetitor':
        if (self.state_sort_data === 'ASC') {
          self.list_perceived_value_qualifications.sort(function(a, b) {return (a.iWeightCompetitor > b.iWeightCompetitor) ? 1 : ((b.iWeightCompetitor > a.iWeightCompetitor) ? -1 : 0); } );
          self.state_sort_data = 'DESC';
        } else {
          self.list_perceived_value_qualifications.sort(function(a, b) {return (b.iWeightCompetitor > a.iWeightCompetitor) ? 1 : ((a.iWeightCompetitor > b.iWeightCompetitor) ? -1 : 0); } );
          self.state_sort_data = 'ASC';
        }
        break;
        case 'iCompetitorQualification':
        if (self.state_sort_data === 'ASC') {
          self.list_perceived_value_qualifications.sort(function(a, b) {return (a.iCompetitorQualification > b.iCompetitorQualification) ? 1 : ((b.iCompetitorQualification > a.iCompetitorQualification) ? -1 : 0); } );
          self.state_sort_data = 'DESC';
        } else {
          self.list_perceived_value_qualifications.sort(function(a, b) {return (b.iCompetitorQualification > a.iCompetitorQualification) ? 1 : ((a.iCompetitorQualification > b.iCompetitorQualification) ? -1 : 0); } );
          self.state_sort_data = 'ASC';
        }
        break;
    }
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.uploadFileToActivity(this.fileToUpload);
  }

  uploadFileToActivity(fileToUpload) {
    console.log('fileToUpload: ', fileToUpload);
    console.log('this.id_category: ', this.id_category);
    const end_point_url = '/api/PerceivedValueQualifications/PostImportPerceivedValueQualifications';
    const parameter = 'iIDVersion';
    this.loadingChart = true;
    this.utilitiesService.fnHttSetUploadFile(this.current_payload, this.fileToUpload, this.id_version, end_point_url, parameter).subscribe(data => {
      console.log('data: ', data);
      // do something, if upload success

      if (data.status == 200) {
        this.loadingChart = false;
        this.file_import_input = null;
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessImport.text);
        this.fnGetAllRatingsValuePerceived(this.current_payload, this.id_category);
        this.fnGetCategoriesScoring(this.current_payload, this.id_category);
      }
      if (data.status == 206) {
        this.loadingChart = false;
        const error = this.utilitiesService.fnSetErrors(null, data.body.message);
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, error => {
      this.loadingChart = false;
      console.log(error);
    });
    this.file_import_input = null;
  }

  fnGetDataListPerceivedValueQualifications(data_category_select) {
    console.log('data_category_select: ', data_category_select);
    this.id_category = data_category_select.iIDCategory;
    $('#pgp-input_category_name').val('');
    this.fnGetAllRatingsValuePerceived(this.current_payload, this.id_category);
    this.fnGetCategoriesScoring(this.current_payload, this.id_category);
    this.object_filter['competitors'] = [];
    this.object_filter['iMinWeightCompetitor'] = null;
    this.object_filter['iMaxWeightCompetitor'] = null;
    this.object_filter['iMinCompetitorQualification'] = null;
    this.object_filter['iMaxCompetitorQualification'] = null;
    this.search_state = false;
  }

  fnSetRemoveItemSearchFilteredAdvanced(index, competitors) {
    console.log('index: ', index);
    console.log('competitors: ', competitors);
    // const competitors_new = competitors.slice();
    // console.log('competitors_new: ', competitors_new);
    competitors.splice(index, 1);
    console.log('competitors: ', competitors);
    // console.log('new_collection_competitor: ', new_collection_competitor);

    const object_data_send = {
      'competitors': competitors,
      'iMinWeightCompetitor': this.object_filter['iMinWeightCompetitor'],
      'iMaxWeightCompetitor': this.object_filter['iMaxWeightCompetitor'],
      'iMinCompetitorQualification': this.object_filter['iMinCompetitorQualification'],
      'iMaxCompetitorQualification': this.object_filter['iMaxCompetitorQualification'],
    };
    console.log('object_data_send: ', object_data_send);
    this.object_filter = object_data_send;
    this.fnGetAllRatingsValuePerceivedFilter(this.current_payload, object_data_send, this.id_category);
    this.fnValidateStateSearch(this.object_filter);
  }

  fnSetRemoveWightSearchFilteredAdvanced() {
    this.object_filter['iMinWeightCompetitor'] = null;
    this.object_filter['iMaxWeightCompetitor'] = null;
    this.fnGetAllRatingsValuePerceivedFilter(this.current_payload, this.object_filter, this.id_category);
    this.fnValidateStateSearch(this.object_filter);
  }

  fnSetRemoveQualificationSearchFilteredAdvanced() {
    this.object_filter['iMinCompetitorQualification'] = null;
    this.object_filter['iMaxCompetitorQualification'] = null;
    this.fnGetAllRatingsValuePerceivedFilter(this.current_payload, this.object_filter, this.id_category);
    this.fnValidateStateSearch(this.object_filter);
  }


  fnClearAllFiltersSearch() {
    this.search_input = '';
    this.object_filter['competitors'] = [];
    this.object_filter['iMinWeightCompetitor'] = null;
    this.object_filter['iMaxWeightCompetitor'] = null;
    this.object_filter['iMinCompetitorQualification'] = null;
    this.object_filter['iMaxCompetitorQualification'] = null;
    this.fnGetListCategoriesByVersion(this.current_payload, this.id_version);
    this.search_state = false;
    // this.object_filter['iMinWeightCompetitor'] = '';
    // this.object_filter['iMinCompetitorQualification'] = '';
    // this.fnSetRemoveWightSearchFilteredAdvanced();
    // this.fnSetRemoveQualificationSearchFilteredAdvanced();
  }

  fnValidateStateSearch(collection){
    if(collection['competitors'].length > 0 &&
    collection['iMinWeightCompetitor'] == null &&
    collection['iMaxWeightCompetitor'] == null &&
    collection['iMinCompetitorQualification'] == null &&
    collection['iMaxCompetitorQualification'] == null
    ){
      this.search_state = false;
    }else{
      this.search_state = true;
    }
  }


  /*********Show modal parameters function *********/

  open(_title, _body) {
    this.dialogService.open(ModalsComponent, {
      context: {
        title: _title,
        body: _body
      },
    });
  }

  /********Funcion que trae los textos del modulo calificacion por valor percibido*********/
  fnGetLanguage(){
    const self = this;
    self.utilitiesService.fnGetLanguage('ratingPerceivedValue', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module,
      self.DATA_LANG_GENERAL = res_lang.general
    });
  }

}
