import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPricesListComponent } from './edit-prices-list.component';

describe('EditPricesListComponent', () => {
  let component: EditPricesListComponent;
  let fixture: ComponentFixture<EditPricesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPricesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPricesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
