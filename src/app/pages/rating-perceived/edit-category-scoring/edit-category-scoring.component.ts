import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options, LabelType } from 'ng5-slider';


import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';

@Component({
  selector: 'ngx-edit-category-scoring',
  templateUrl: './edit-category-scoring.component.html',
  styleUrls: ['./edit-category-scoring.component.scss'],
})
export class EditCategoryScoringComponent implements OnInit {

  submitted: boolean = false;
  current_payload: any = null;
  scoring_data: any = {};

  @Input() data_qualification: any;

  // lang_nav: any =  window.navigator.language;
  lang_nav: any = 'en';
  DATA_LANG: any = null;

  value_rating: number = 1;
  options_rating: Options = {
    floor: 1,
    ceil: 9,
  };

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private categoriesService: CategoriesService,
    protected ref: NbDialogRef<EditCategoryScoringComponent>,
  ) { }

  ngOnInit() {
    const self = this;

    /********Llamado a la funcion que trae los textos del modulo calificacion por valor percibido*********/
    self.utilitiesService.fnGetLanguage('ratingPerceivedValue', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        this.current_payload = token.getValue();
        const scoring_data = {
          'iIDCategory': this.data_qualification[0].iIDCategory,
          'iVersionScoring': this.data_qualification[0].iVersionScoring,
        };
        this.scoring_data = scoring_data;
      }
    });
  }

  dismiss() {
    this.ref.close();
  }

  fnUpdateScoring(scoring_data) {
    this.submitted = true;
    const data_object = {
      'iIDCategory': scoring_data.iIDCategory,
      'iVersionScoring': scoring_data.iVersionScoring,
    };
    console.log('data_object: ', data_object);

    this.categoriesService.fnHttpSetEditDataScoring(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.scoring_data = {};
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessUpdateScore.text );
        this.dismiss();
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnCancelEditScoring() {
    this.submitted = false;
    this.dismiss();
  }

}
