import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

import { Router, ActivatedRoute } from '@angular/router';

import { StateService } from '../../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';

// import { CategoriesService } from '../../../shared/api/services/categories.service';
import { RatingPerceivedService } from '../../../shared/api/services/rating-perceived.service';


@Component({
  selector: 'ngx-delete-ratings-perceived',
  templateUrl: './delete-ratings-perceived.component.html',
  styleUrls: ['./delete-ratings-perceived.component.scss']
})
export class DeleteRatingsPerceivedComponent implements OnInit {

  @Input() iIDPerceivedValueQualification: String;
  @Input() tComment: String;
  @Input() iIDVersion: String;
  @Input() competitor: any;
  category_data: any = {};
  id_category: any = null;
  current_payload: string = null;
  submitted: Boolean = false;

  // lang_nav: any =  window.navigator.language;
  lang_nav: any = 'en';
  DATA_LANG: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    protected ref: NbDialogRef<DeleteRatingsPerceivedComponent>,
    private ratingPerceivedService: RatingPerceivedService,
  ) { }

  ngOnInit() {
    const self = this;
    
    /********Llamado a la funcion que trae los textos del modulo calificacion por valor percibido*********/
    self.utilitiesService.fnGetLanguage('ratingPerceivedValue', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    // this.iIDPerceivedValueQualification;
    console.log('this.iIDPerceivedValueQualification: ', this.iIDPerceivedValueQualification);
    // this.tComment;
    console.log('this.tComment: ', this.tComment);
    // this.iIDVersion;
    console.log('this.iIDVersion: ', this.iIDVersion);
    console.log('this.competitor: ', this.competitor);

    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        // this.text_modal = this.competitor['tCompetitorName'];
        console.log('this.current_payload: ', this.current_payload);
      }
    });
  }

  fnSetDeleteRatingValuePerceivedById(id_rating_value_perceived) {
    this.submitted = true;
    this.ratingPerceivedService.fnHttpSetDeleteRatingValuePerceivedById(this.current_payload, id_rating_value_perceived).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessDeletePerVS.text);
        this.dismiss();
      }
      if (r.status == 206) {
        this.submitted = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  dismiss() {
    this.ref.close();
  }

  fnCancelRatingValuePerceived() {
    this.dismiss();
  }

}
