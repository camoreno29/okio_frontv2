import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng5SliderModule } from 'ng5-slider';

// import { RatingPerceivedComponent } from './rating-perceived.component';
import { AddRatingPerceivedComponent } from './add-rating-perceived/add-rating-perceived.component';
import { EditRatingPerceivedComponent } from './edit-rating-perceived/edit-rating-perceived.component';
import { EditCategoryScoringComponent } from './edit-category-scoring/edit-category-scoring.component';
import { DeleteRatingsPerceivedComponent } from './delete-ratings-perceived/delete-ratings-perceived.component';
import { DeleteAllRatingsPerceivedComponent } from './delete-all-ratings-perceived/delete-all-ratings-perceived.component';
import { AdvancedSearchRatingPerceivedComponent } from './advanced-search-rating-perceived/advanced-search-rating-perceived.component';

const ENTRY_COMPONENTS = [
  AddRatingPerceivedComponent,
  DeleteRatingsPerceivedComponent,
  EditRatingPerceivedComponent,
  EditCategoryScoringComponent,
  DeleteAllRatingsPerceivedComponent,
  AdvancedSearchRatingPerceivedComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    NgSelectModule,
    FormsModule,
    TooltipModule.forRoot(),
    ModalModule,
    NgxPaginationModule,
    Ng5SliderModule,
  ],
  declarations: [
    // RatingPerceivedComponent,
    AddRatingPerceivedComponent,
    DeleteRatingsPerceivedComponent,
    EditRatingPerceivedComponent,
    EditCategoryScoringComponent,
    DeleteAllRatingsPerceivedComponent,
    AdvancedSearchRatingPerceivedComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class RatingPerceivedModule { }
