import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options, LabelType } from 'ng5-slider';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { RatingPerceivedService } from '../../../shared/api/services/rating-perceived.service';
import { CompetitorsService } from '../../../shared/api/services/competitors.service';

@Component({
  selector: 'ngx-add-rating-perceived',
  templateUrl: './add-rating-perceived.component.html',
  styleUrls: ['./add-rating-perceived.component.scss']
})
export class AddRatingPerceivedComponent implements OnInit {

  submitted: boolean = false;
  current_payload: any = null;
  rating_perceived_data: any = {};
  list_competitors: any = [];
  data_competitor: any = null;

  list_options_show_complementarity_relation: any = [{ 'value': true, 'name': 'Yes' }, { 'value': false, 'name': 'No' }];

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  @Input() id_version: string;
  @Input() data_category: any;
  // @Input() iIDCategory: string;
  // @Input() iIDVersion: string;
  // @Input() tCategoryName: string;
  // @Input() bAppliesComplementarityRelation: string;

  value_weight: number = 1;
  options_weight: Options = {
    floor: 1,
    ceil: 5,
  };

  value_rating: number = 1;
  options_rating: Options = {
    floor: 1,
    ceil: 9,
  };

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private ratingPerceivedService: RatingPerceivedService,
    private competitorsService: CompetitorsService,
    protected ref: NbDialogRef<AddRatingPerceivedComponent>,
  ) { }

  ngOnInit() {
    const self = this;

    /********Llamado a la funcion que trae los textos del modulo calificacion por valor percibido*********/
    self.utilitiesService.fnGetLanguage('ratingPerceivedValue', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
        console.log('id_version======================>: ', this.id_version);
        console.log('data_category======================>: ', this.data_category);
        // this.items_menu = JSON.parse(JSON.stringify(this.menu));
        this.rating_perceived_data['iIDPerceivedValueQualification'] = 0;
        this.rating_perceived_data['iWeightCompetitor'] = 1;
        this.rating_perceived_data['iCompetitorQualification'] = 1;
        this.rating_perceived_data['category'] = this.data_category;
        this.rating_perceived_data['iIDVersion'] = parseInt(this.id_version, 10);
        this.rating_perceived_data ['tLanguage'] = localStorage.getItem('language_app'),
        this.fnGetListCompetitors(this.current_payload, this.id_version);
      }
    });
  }

  fnGetListCompetitors(current_payload, id_version) {
    this.competitorsService.fnHttpGetDataListCompetitorsByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_competitors = JSON.parse(JSON.stringify(r.body));
        // this.rating_perceived_data.competitor = this.list_competitors[0];
        this.submitted = false;
        // console.log('r.status: ', r.status);
        // this.utilitiesService.showToast('top-right', 'success', 'Category has been created successfully!');
        // this.dismiss();
        // this.router.navigate(['/pages/projects/' + id_company]);
        // this.companies = r.body;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  dismiss() {
    this.ref.close();
  }

  fnCancelCreateNewRatingPerceivedValue() {
    this.submitted = false;
    this.dismiss();
  }

  fnCreateNewRatingPerceivedValue(rating_perceived_data) {
    this.submitted = true;

    if (!rating_perceived_data.tComment) {
      rating_perceived_data['tComment'] = '';
    }
    // const data_object = {
    //   'iIDCategory': 0,
    //   'iIDVersion': parseInt(this.id_version, 10),
    //   'tCategoryName': rating_perceived_data.tCategoryName,
    //   'bAppliesComplementarityRelation': rating_perceived_data.bAppliesComplementarityRelation,
    // };
    // console.log('data_object: ', data_object);
    console.log('rating_perceived_data: ', rating_perceived_data);

    this.ratingPerceivedService.fnHttpSetSaveNewPerceivedValueQualification(this.current_payload, rating_perceived_data).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.rating_perceived_data = {};
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessCreatePerVS.text);
        this.dismiss();
        // this.router.navigate(['/pages/projects/' + id_company]);
        // this.companies = r.body;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(null, r.body.codMessage);
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }
}
