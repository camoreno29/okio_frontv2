import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DeleteAllCategoriesComponent } from './delete-all-categories.component';

describe('DeleteAllCategoriesComponent', () => {
  let component: DeleteAllCategoriesComponent;
  let fixture: ComponentFixture<DeleteAllCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteAllCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAllCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
