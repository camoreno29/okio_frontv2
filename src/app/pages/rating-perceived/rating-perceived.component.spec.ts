import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingPerceivedComponent } from './rating-perceived.component';

describe('RatingPerceivedComponent', () => {
  let component: RatingPerceivedComponent;
  let fixture: ComponentFixture<RatingPerceivedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RatingPerceivedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingPerceivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
