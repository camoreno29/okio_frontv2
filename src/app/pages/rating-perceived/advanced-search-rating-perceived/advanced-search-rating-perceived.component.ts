import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options } from 'ng5-slider';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { RatingPerceivedService } from '../../../shared/api/services/rating-perceived.service';
import { CompetitorsService } from '../../../shared/api/services/competitors.service';

@Component({
  selector: 'ngx-advanced-search-rating-perceived',
  templateUrl: './advanced-search-rating-perceived.component.html',
  styleUrls: ['./advanced-search-rating-perceived.component.scss']
})
export class AdvancedSearchRatingPerceivedComponent implements OnInit {

  submitted: boolean = false;
  current_payload: any = null;
  rating_perceived_data: any = {};
  list_competitors: any = [];
  data_competitor: any = null;
  data_competitors_list_filter: any = [];
  state_loading: boolean = true;

  list_options_show_complementarity_relation: any = [{ 'value': true, 'name': 'Yes' }, { 'value': false, 'name': 'No' }];
  
  // lang_nav: any =  window.navigator.language;
  lang_nav: any = 'es';
  DATA_LANG: any = null;

  @Input() id_version: string;
  @Input() data_category: any;
  @Input() object_filter: any;
  id_category: any = null;
  // @Input() iIDCategory: string;
  // @Input() iIDVersion: string;
  // @Input() tCategoryName: string;
  // @Input() bAppliesComplementarityRelation: string;
  min_value_weight: number = 1;
  max_value_weight: number = 5;
  value_weight: number = 1;
  options_weight: Options = {
    floor: 1,
    ceil: 5,
    translate: (value: number): string => {
      return '' + value;
    },
  };
  
  min_value_rating: number = 1;
  max_value_rating: number = 9;
  value_rating: number = 1;
  options_rating: Options = {
    floor: 1,
    ceil: 9,
    translate: (value: number): string => {
      return '' + value;
    },
  };

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private ratingPerceivedService: RatingPerceivedService,
    private competitorsService: CompetitorsService,
    protected ref: NbDialogRef<AdvancedSearchRatingPerceivedComponent>,
  ) { }

  ngOnInit() {
    
    const self = this;

    /********Llamado a la funcion que trae los textos del modulo calificacion por valor percibido*********/
    self.utilitiesService.fnGetLanguage('ratingPerceivedValue', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });

    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
        console.log('id_version======================>: ', this.id_version);
        console.log('data_category======================>: ', this.data_category);
        console.log('object_filter======================>: ', this.object_filter);
        this.id_category = this.data_category['category']['iIDCategory'];
        // this.items_menu = JSON.parse(JSON.stringify(this.menu));

        // this.min_value_weight = 1; // this.object_filter['iMaxWeightCompetitor'];
        // this.max_value_weight = 5; // this.object_filter['iMaxCompetitorQualification'];
        // this.min_value_rating = 1;
        // this.max_value_rating = 9;

        this.rating_perceived_data['iWeightCompetitor'] = (this.object_filter['iMinWeightCompetitor'] && this.object_filter['iMaxWeightCompetitor']) ? [this.object_filter.iMinWeightCompetitor, this.object_filter.iMaxWeightCompetitor] : 1;
        this.rating_perceived_data['iCompetitorQualification'] = (this.object_filter['iMinCompetitorQualification'] && this.object_filter['iMaxCompetitorQualification']) ? [this.object_filter.iMinCompetitorQualification, this.object_filter.iMaxCompetitorQualification] : 1;
        this.rating_perceived_data['category'] = this.data_category;
        this.rating_perceived_data['competitor'] = JSON.parse(JSON.stringify(this.object_filter['competitors']));
        this.data_competitors_list_filter = JSON.parse(JSON.stringify(this.object_filter['competitors']));
        this.fnGetListCompetitors(this.current_payload, this.id_version);
      }
    });
  }

  fnGetListCompetitors(current_payload, id_version) {
    this.competitorsService.fnHttpGetDataListCompetitorsByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_competitors = JSON.parse(JSON.stringify(r.body));
        // this.rating_perceived_data.competitor = this.list_competitors[0];
        this.submitted = false;
        this.state_loading = false;
        // console.log('r.status: ', r.status);
        // this.utilitiesService.showToast('top-right', 'success', 'Category has been created successfully!');
        // this.dismiss();
        // this.router.navigate(['/pages/projects/' + id_company]);
        // this.companies = r.body;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  dismiss(object_response_onclose?) {
    console.log('object_response_onclose: ', object_response_onclose);
    console.log('this.rating_perceived_data: ', this.rating_perceived_data);
    if (object_response_onclose) {
      object_response_onclose['iMinWeightCompetitor'] = this.rating_perceived_data['iWeightCompetitor'][0];
      object_response_onclose['iMaxWeightCompetitor'] = this.rating_perceived_data['iWeightCompetitor'][1];
      object_response_onclose['iMinCompetitorQualification'] = this.rating_perceived_data['iCompetitorQualification'][0];
      object_response_onclose['iMaxCompetitorQualification'] = this.rating_perceived_data['iCompetitorQualification'][1];
      console.log('object_response_onclose: ', object_response_onclose);
      this.ref.close(object_response_onclose);
    } else {
      this.ref.close();
    }
  }

  fnCancelCreateNewRatingPerceivedValue() {
    this.submitted = false;
    this.dismiss();
  }

  fnAdvancedSearchRatingPerceivedValue(rating_perceived_data) {
    this.submitted = true;
    console.log('rating_perceived_data: ', rating_perceived_data);
    const data_object = {
      'competitors': rating_perceived_data.competitor,
      'iMinWeightCompetitor': this.min_value_weight,
      'iMaxWeightCompetitor': this.max_value_weight,
      'iMinCompetitorQualification': this.min_value_rating,
      'iMaxCompetitorQualification': this.max_value_rating,
    };
    console.log('data_object: ', data_object);
    this.dismiss(data_object);
    // this.ratingPerceivedService.fnHttpSetSaveNewPerceivedValueQualification(this.current_payload, rating_perceived_data).subscribe(r => {
    //   console.log('r: ', r);
    //   if (r.status == 200) {
    //     this.rating_perceived_data = {};
    //     this.submitted = false;
    //     console.log('r.status: ', r.status);
    //     this.utilitiesService.showToast('top-right', 'success', 'Rating perceived value has been created successfully!');
    //     this.dismiss();
    //     // this.router.navigate(['/pages/projects/' + id_company]);
    //     // this.companies = r.body;
    //   }
    //   if (r.status == 206) {
    //     this.submitted = false;
    //     let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
    //     console.log('error: ', error);
    //     this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
    //   }
    // }, err => {
    //   this.submitted = false;
    //   console.log('err: ', err);
    // });
  }

  changeSource(event) {
    console.log('event: ', event);
    this.rating_perceived_data['competitor'] = event;
  }

}
