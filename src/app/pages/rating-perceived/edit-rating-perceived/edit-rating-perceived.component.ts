import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options, LabelType } from 'ng5-slider';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { RatingPerceivedService } from '../../../shared/api/services/rating-perceived.service';
import { CompetitorsService } from '../../../shared/api/services/competitors.service';

@Component({
  selector: 'ngx-edit-rating-perceived',
  templateUrl: './edit-rating-perceived.component.html',
  styleUrls: ['./edit-rating-perceived.component.scss']
})
export class EditRatingPerceivedComponent implements OnInit {

  submitted: boolean = false;
  current_payload: any = null;
  rating_perceived_data: any = {};
  list_competitors: any = [];
  data_competitor: any = null;

  list_options_show_complementarity_relation: any = [{ 'value': true, 'name': 'Yes' }, { 'value': false, 'name': 'No' }];

  // lang_nav: any =  window.navigator.language;
  lang_nav: any = 'en';
  DATA_LANG: any = null;

  @Input() id_version: string;
  @Input() data_category: any;
  // @Input() iIDCategory: string;
  // @Input() iIDVersion: string;
  // @Input() tCategoryName: string;
  // @Input() bAppliesComplementarityRelation: string;

  value_weight: number = 1;
  options_weight: Options = {
    floor: 1,
    ceil: 5,
  };

  value_rating: number = 1;
  options_rating: Options = {
    floor: 1,
    ceil: 9,
  };
  competitor_test: any = {};
  data_list_competitors: any = {};
  state_loading: boolean = true;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private ratingPerceivedService: RatingPerceivedService,
    private competitorsService: CompetitorsService,
    protected ref: NbDialogRef<EditRatingPerceivedComponent>,
  ) { }

  ngOnInit() {
    const self = this;

    /********Llamado a la funcion que trae los textos del modulo calificacion por valor percibido*********/
    self.utilitiesService.fnGetLanguage('ratingPerceivedValue', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
        console.log('id_version======================>: ', this.id_version);
        console.log('data_category======================>: ', this.data_category);
        // this.items_menu = JSON.parse(JSON.stringify(this.menu));
        // this.rating_perceived_data['iIDPerceivedValueQualification'] = 0;
        this.data_list_competitors = JSON.parse(JSON.stringify(this.data_category['competitor']));
        console.log('this.data_list_competitors: ', this.data_list_competitors);
        this.rating_perceived_data = {
          'iIDPerceivedValueQualification': this.data_category['iIDPerceivedValueQualification'],
          'iIDVersion': parseInt(this.id_version, 10),
          'category': this.data_category['category'],
          'competitor': this.data_category['competitor'],
          'tComment': this.data_category['tComment'],
          'iWeightCompetitor': this.data_category['iWeightCompetitor'],
          'iCompetitorQualification': this.data_category['iCompetitorQualification'],
          'tLanguage': localStorage.getItem('language_app'),
        };
        console.log('this.rating_perceived_data: ', this.rating_perceived_data);
        // this.rating_perceived_data['iIDPerceivedValueQualification'] = this.data_category['iIDPerceivedValueQualification'];
        // this.rating_perceived_data['iIDVersion'] = parseInt(this.id_version, 10);
        // this.rating_perceived_data['category'] = this.data_category['category'];
        // this.rating_perceived_data['competitor'] = this.data_category['competitor'];
        // this.rating_perceived_data['tComment'] = this.data_category['tComment'];
        // this.rating_perceived_data['iWeightCompetitor'] = this.data_category['iWeightCompetitor'];
        // this.rating_perceived_data['iCompetitorQualification'] = this.data_category['iCompetitorQualification'];
        this.fnGetListCompetitors(this.current_payload, this.id_version);
        
        // console.log('this.data_category["competitor"]: ', this.data_category['competitor']);
      }
    });
  }

  fnGetListCompetitors(current_payload, id_version) {
    this.competitorsService.fnHttpGetDataListCompetitorsByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_competitors = JSON.parse(JSON.stringify(r.body));
        this.submitted = false;
        this.state_loading = false;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  dismiss() {
    this.ref.close();
  }

  fnCancelEditRatingPerceived() {
    this.submitted = false;
    this.dismiss();
  }

  fnUpdateDataRatingPerceived(data_object) {
    console.log('data_object: ', data_object);
    this.submitted = true;
    this.ratingPerceivedService.fnHttpSetEditDataRatingPerceived(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessUpdatePerVS.text);
        this.dismiss();
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(null, r.body.codMessage);
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  changeSource(event) {
    console.log('event: ', event);
    this.rating_perceived_data['competitor'] = event;
    console.log('this.rating_perceived_data: ', this.rating_perceived_data);
  }

  // dismiss() {
  //   this.ref.close();
  // }

  // fnCancelCreateNewRatingPerceivedValue() {
  //   this.submitted = false;
  //   this.dismiss();
  // }

  // fnCreateNewRatingPerceivedValue(rating_perceived_data) {
  //   this.submitted = true;
  //   console.log('rating_perceived_data: ', rating_perceived_data);
  //   // const data_object = {
  //   //   'iIDCategory': 0,
  //   //   'iIDVersion': parseInt(this.id_version, 10),
  //   //   'tCategoryName': rating_perceived_data.tCategoryName,
  //   //   'bAppliesComplementarityRelation': rating_perceived_data.bAppliesComplementarityRelation,
  //   // };
  //   // console.log('data_object: ', data_object);

  //   this.ratingPerceivedService.fnHttpSetSaveNewPerceivedValueQualification(this.current_payload, rating_perceived_data).subscribe(r => {
  //     console.log('r: ', r);
  //     if (r.status == 200) {
  //       this.rating_perceived_data = {};
  //       this.submitted = false;
  //       console.log('r.status: ', r.status);
  //       this.utilitiesService.showToast('top-right', 'success', 'Rating perceived value has been created successfully!');
  //       this.dismiss();
  //       // this.router.navigate(['/pages/projects/' + id_company]);
  //       // this.companies = r.body;
  //     }
  //     if (r.status == 206) {
  //       this.submitted = false;
  //       let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
  //       console.log('error: ', error);
  //       this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
  //     }
  //   }, err => {
  //     this.submitted = false;
  //     console.log('err: ', err);
  //   });
  // }

}
