import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MenuService } from '../shared/api/services/menu.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NbMenuItem, NbMenuService, NbSidebarService } from '@nebular/theme';
import { SharedService } from '../shared/services/shared.service';
import { UtilitiesService } from '../shared/api/services/utilities.service';
import { takeWhile } from 'rxjs/operators';
import { Observable, Observer } from 'rxjs';
import { forEach } from '@angular/router/src/utils/collection';

declare var $: any;
@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  templateUrl: 'pages.component.html',
})
export class PagesComponent implements OnInit, OnDestroy {
  menu: any = [];
  id_company: any = null;
  link_project: any = null;
  data_item: any = null;
  data_company_object: any = {};
  menu_lang: any = {};

  private alive: boolean = true;
  selectedItem: string;

  MENU_ITEMS_CONFIG: NbMenuItem[] = [];
  MENU_ITEMS: NbMenuItem[] = [];

  dataChange: Observable<any>;
  dataChangeObserver: any;


  constructor(
    private menuService: MenuService,
    private sidebarService: NbSidebarService,
    private utilitiesService: UtilitiesService,
    private nbMenuService: NbMenuService,
    private route: ActivatedRoute,
    private router: Router,
    private sharedService: SharedService) {
    this.dataChange = new Observable((observer: Observer<any>) => {
      this.dataChangeObserver = observer;
    });
  }

  ngOnInit() {

    const self = this;
    
    self.dataChange.subscribe((data) => {
      self.fnGetLanguage();
    });


    this.sharedService.getEmittedValue()
      .subscribe(item => {
        this.MENU_ITEMS = item;
      });


    this.nbMenuService.onItemClick().subscribe(response => {
      if (response.tag === 'left-menu') {
        this.data_item = response.item;
        this.data_item['selected'] = true;
        localStorage.setItem('item_menu', JSON.stringify(this.data_item));
        this.MENU_ITEMS.forEach((value, key) => {
          if (value['iIDMenu'] === this.data_item.iIDMenu) {
            this.data_item['selected'] = true;
            value['selected'] = true;
          } else {
            value['selected'] = false;
          }
        });
      }
    });

    this.nbMenuService.getSelectedItem('left-menu')
      .pipe(takeWhile(() => this.alive))
      .subscribe((menuBag) => {
        console.log('menuBag: ', menuBag);
      });
  }

  getItemsMenuCompany(id_company) {
    console.log('>>>>>>>>>>>>>>>>>>>id_company: ', id_company);
    return this.MENU_ITEMS = [{
      title: 'Versions',
      icon: 'fas fa-tasks',
      link: '/pages/versions-company/' + id_company,
      home: false,
    }, {
      title: 'Projects',
      icon: 'fas fa-folder-open',
      link: '/pages/projects/' + id_company,
      home: false,
    }, {
      title: 'Members',
      icon: 'fas fa-users',
      link: '/pages/members/' + id_company,
      home: false,
    }, {
      title: 'Settings',
      icon: 'fas fa-tools',
      link: '/pages/settings-company/' + id_company,
      home: false,
    }];
  }

  getItemsMenuProject(id_project) {
    console.log('>>>>>>>>>>>>>>>>>>>id_project: ', id_project);
    this.MENU_ITEMS = [];
    return this.MENU_ITEMS = [{
      title: 'Versions',
      icon: 'fas fa-tasks',
      link: '/pages/versions-project/' + id_project,
      home: false,
    }, {
      title: 'Profiles',
      icon: 'fas fa-users',
      link: '/pages/groups/' + id_project,
      home: false,
    },
    ];
  }

  getMenu() {
    return this.MENU_ITEMS;
  }

  isAdmin() {
    // your code goes here
    return true;
  }

  fnSetMenuVersion(current_payload, id_version, responseIDMenu, observer) {
    console.log('current_payload: ', current_payload);
    console.log('id_version: ', id_version);

    const self = this;

    const object_data_send = {
      'iIDVersion': id_version,
    };

    let iIDMenu = null;

    if (responseIDMenu == null) {
      iIDMenu = 5;
    } else {
      iIDMenu = responseIDMenu.iIDMenu;
    }

    self.menuService.fnHttpGetMenuVersions(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        const menuVersion = r.body;
        console.log('menuVersion: ', menuVersion);
        const copyMenuVersion = [];
        const cards_access = [];
        const order = 1;
        self.utilitiesService.fnGetLanguage('menu', function (res_lang) {
          self.menu_lang = res_lang.module;
          copyMenuVersion.push({
            'title': self.menu_lang['lbMenuDashboard']['text'],
            'icon': 'fas fa-tachometer-alt',
            'selected': true,
            'link': '/pages/home/' + id_version,
            'home': true,
            'iIDMenu': 5,
            'order': order,
            'cards': [],
          });
          menuVersion.forEach(element => {
            console.log('element: ', element);
            copyMenuVersion.push({
              'title': self.menu_lang[element.tMenuName]['text'],
              'icon': element.tMenuIcon,
              'selected': false,
              'link': element.cards[0].tPathCard ? element.cards[0].tPathCard + id_version : '/pages/home/' + id_version,
              'home': false,
              'iIDMenu': element.iIDMenu,
              'order': (order + 1),
              'cards': element.cards,
            });
            if (element.iIDMenu == iIDMenu) {
              cards_access.push(element.cards);
            }
          });
          const finalMenuVersion: NbMenuItem[] = copyMenuVersion;
          self.MENU_ITEMS = [];
          self.MENU_ITEMS = finalMenuVersion;
          const object_return = {
            'cards': cards_access.length > 0 ? cards_access[0] : [],
            'menu_items': self.MENU_ITEMS,
          };
          observer(object_return);
        });
      }
    }, err => {
      observer(err);
      console.log('err: ', err);
    });
  }

  getSelectedItem() {
    this.nbMenuService.getSelectedItem('left-menu')
      .pipe(takeWhile(() => this.alive))
      .subscribe((menuBag) => {
        console.log('menuBag: ', menuBag);
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }

  setData(data: any) {
    this.dataChangeObserver.next(data);
  }

  fnGetLanguage() {
    const self = this;
    self.utilitiesService.fnGetLanguage('menu', function (res_lang) {
      self.menu_lang = res_lang.module;

      if (self.MENU_ITEMS.length > 0) {
        self.MENU_ITEMS[0].title = self.menu_lang['lbMenuDashboard']['text'];
        self.MENU_ITEMS[1].title = self.menu_lang['lbMenuMarket']['text'];
        self.MENU_ITEMS[2].title = self.menu_lang['lbMenuPortfolio']['text'];
        self.MENU_ITEMS[3].title = self.menu_lang['lbMenuResults']['text'];
        self.MENU_ITEMS[4].title = self.menu_lang['lbMenuSettings']['text'];
      }

    });
  }

}
