import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

import { BaseComponent } from './base.component';
import { CardsOrderComponent } from './cards-order/cards-order.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [ThemeModule, NgSelectModule, FormsModule],
  declarations: [
    BaseComponent,
    CardsOrderComponent,
  ],
})
export class BaseModule { }
