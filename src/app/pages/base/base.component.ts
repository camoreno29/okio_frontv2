import { Component, OnInit } from '@angular/core';
import { UtilitiesService } from '../../shared/api/services/utilities.service';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database'; // Firebase modules for Database, Data list and Single object

declare var $: any;

@Component({
  selector: 'ngx-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss'],
})
export class BaseComponent implements OnInit {

  constructor(
    private utilitiesService: UtilitiesService,
    private db: AngularFireDatabase,
  ) { }

  ngOnInit() { }

}
