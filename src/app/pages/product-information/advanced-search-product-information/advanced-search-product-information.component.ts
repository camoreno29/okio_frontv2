import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options } from 'ng5-slider';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { CompetitorsService } from '../../../shared/api/services/competitors.service';
import { PricesListService } from '../../../shared/api/services/prices-list.service';
import { ProductInformationService } from '../../../shared/api/services/product-information.service';

@Component({
  selector: 'ngx-advanced-search-product-information',
  templateUrl: './advanced-search-product-information.component.html',
  styleUrls: ['./advanced-search-product-information.component.scss'],
})
export class AdvancedSearchProductInformationComponent implements OnInit {

  submitted: boolean = false;
  current_payload: any = null;
  rating_perceived_data: any = {};
  product_information_data: any = {};
  list_competitors: any = [];
  data_competitor: any = null;
  state_loading: boolean = true;

  list_options_show_complementarity_relation: any = [{ 'value': true, 'name': 'Yes' }, { 'value': false, 'name': 'No' }];

  @Input() id_version: string;
  @Input() items_collection_filtered: any;
  id_category: any = null;
  list_categories_collection: any = [];
  list_prices_lists_collection: any = [];
  list_products_collection: any = [];

  config_products: any = null;
  bHideTrendOfVolumeInstalledCapacity: Boolean = false;
  bShowCents: any = null;
  quantiy_decimals: any = 0;

  // lang_nav: any =  window.navigator.language;
  lang_nav: any = 'en';
  DATA_LANG: any = null;

  /* ********** START - Data slider range - VAT ********* */
  min_value_vat: any = null;
  max_value_vat: any = null;
  /* *********** END - Data slider range - VAT ********** */
  /* ********** START - Data slider range - Current units ********* */
  min_value_current_units: any = null;
  max_value_current_units: any = null;
  /* *********** END - Data slider range - Current units ********** */
  /* ********** START - Data slider range - Variable cost ********* */
  min_value_variable_cost: any = null;
  max_value_variable_cost: any = null;
  /* *********** END - Data slider range - Variable cost ********** */
  /* ********** START - Data slider range - Current price ********* */
  min_value_current_price: any = null;
  max_value_current_price: any = null;
  /* *********** END - Data slider range - Current price ********** */
  /* ********** START - Data slider range - Average discount ********* */
  min_value_average_discount: any = null;
  max_value_average_discount: any = null;
  /* *********** END - Data slider range - Average discount ********** */
  /* ********** START - Data slider range - Annual volume trend ********* */
  min_value_annual_value_trend: any = null;
  max_value_annual_value_trend: any = null;
  /* *********** END - Data slider range - Annual volume trend ********** */
  /* *********** START - Data vars class ********** */
  list_data_filters_collection: any = [];
  list_data_filters_original_collection: any = [];
  loading: Boolean = true;
  /* ************ END - Data vars class *********** */

  data_filter_products: any = [];
  data_filter_products_code: any = [];
  data_filter_price_list: Object = {};
  data_filter_categories: Object = {};


  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private competitorsService: CompetitorsService,
    private categoriesService: CategoriesService,
    private pricesListService: PricesListService,
    private productInformationService: ProductInformationService,
    protected ref: NbDialogRef<AdvancedSearchProductInformationComponent>,
  ) { }

  ngOnInit() {
    const self = this;

    /********Llamado a la funcion que trae los textos del modulo informacion de producto*********/
    self.utilitiesService.fnGetLanguage('productInformation', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });

    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('items_collection_filtered======================>: ', this.items_collection_filtered);
        this.data_filter_products_code = this.items_collection_filtered['products'];
        this.data_filter_products = this.items_collection_filtered['products'];
        this.data_filter_price_list = this.items_collection_filtered['priceList'];
        this.data_filter_categories = this.items_collection_filtered['category'];
        this.min_value_vat = (this.items_collection_filtered['dVAT']) ? ((this.items_collection_filtered['dVAT']['dMin'] != null) ? this.items_collection_filtered['dVAT']['dMin'] * 100 : null) : null;
        this.max_value_vat = (this.items_collection_filtered['dVAT']) ? ((this.items_collection_filtered['dVAT']['dMax'] != null) ? this.items_collection_filtered['dVAT']['dMax'] * 100 : null) : null;
        this.min_value_current_units = (this.items_collection_filtered['dCurrentUnits']) ? this.items_collection_filtered['dCurrentUnits']['dMin'] : null;
        this.max_value_current_units = (this.items_collection_filtered['dCurrentUnits']) ? this.items_collection_filtered['dCurrentUnits']['dMax'] : null;
        this.min_value_variable_cost = (this.items_collection_filtered['dVariableCost']) ? this.items_collection_filtered['dVariableCost']['dMin'] : null;
        this.max_value_variable_cost = (this.items_collection_filtered['dVariableCost']) ? this.items_collection_filtered['dVariableCost']['dMax'] : null;
        this.min_value_current_price = (this.items_collection_filtered['dCurrentPrice']) ? this.items_collection_filtered['dCurrentPrice']['dMin'] : null;
        this.max_value_current_price = (this.items_collection_filtered['dCurrentPrice']) ? this.items_collection_filtered['dCurrentPrice']['dMax'] : null;
        this.min_value_average_discount = (this.items_collection_filtered['dAverageDiscount']) ? ((this.items_collection_filtered['dAverageDiscount']['dMin'] != null) ? this.items_collection_filtered['dAverageDiscount']['dMin'] * 100 : null) : null;
        this.max_value_average_discount = (this.items_collection_filtered['dAverageDiscount']) ? ((this.items_collection_filtered['dAverageDiscount']['dMax'] != null) ? this.items_collection_filtered['dAverageDiscount']['dMax'] * 100 : null) : null;
        console.log('min_value_vat: ', this.min_value_vat);
        this.fnGetListCategories(this.current_payload, this.id_version);
        this.fnGetPricesLists(this.current_payload, this.id_version);
        this.fnGetListProducts(this.current_payload, this.id_version);
        this.fnGetConfigProducts(this.current_payload, this.id_version);
      }
    });
  }

  fnGetListCategories(current_payload, id_version) {
    this.categoriesService.fnHttpGetDataListCategoriesByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_categories_collection = JSON.parse(JSON.stringify(r.body));
        this.submitted = false;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnGetPricesLists(current_payload, id_version?) {
    const self = this;
    self.pricesListService.fnGetAllPricesListByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.list_prices_lists_collection = JSON.parse(JSON.stringify(r.body));
        console.log('self.list_prices_lists_collection: ', self.list_prices_lists_collection);
        self.submitted = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        let error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetListProducts(current_payload, id_version, text_search?) {
    const self = this;
    self.productInformationService.fnHttpGetListProducts(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.list_products_collection = JSON.parse(JSON.stringify(r.body));
        self.state_loading = false;
        self.submitted = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        let error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetListCompetitors(current_payload, id_version) {
    this.competitorsService.fnHttpGetDataListCompetitorsByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_competitors = JSON.parse(JSON.stringify(r.body));
        this.submitted = false;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  dismiss(object_response_onclose?) {

    console.log('object_response_onclose: ', object_response_onclose);


    let data_filter_products_end: any = [];

    if (this.data_filter_products_code && this.data_filter_products) {

      const self = this;
      self.data_filter_products_code.forEach(element => {
        self.data_filter_products.forEach(function (value, key) {
          if (element.iIDProduct === value.iIDProduct) {
            self.data_filter_products.splice(key, 1);
          }
        });
      });

      data_filter_products_end = this.data_filter_products.concat(this.data_filter_products_code);
    } else if (this.data_filter_products_code) {
      data_filter_products_end = this.data_filter_products_code;
    } else if (this.data_filter_products) {
      data_filter_products_end = this.data_filter_products;
    }


    if (object_response_onclose) {
      object_response_onclose['products'] = data_filter_products_end;
      object_response_onclose['priceList'] = (this.data_filter_price_list) ? this.data_filter_price_list : [];
      object_response_onclose['category'] = (this.data_filter_categories) ? this.data_filter_categories : [];
      object_response_onclose['dVAT'] = (this.min_value_vat && this.max_value_vat) ? { 'dMin': this.min_value_vat / 100, 'dMax': this.max_value_vat / 100 } : { 'dMin': this.min_value_vat, 'dMax': this.max_value_vat };
      if (this.min_value_vat && this.max_value_vat) {
        console.log('console this.min_value_vat && this.max_value_vat');
        object_response_onclose['dVAT'] = { 'dMin': this.min_value_vat / 100, 'dMax': this.max_value_vat / 100 }
      } else if (!this.min_value_vat && !this.max_value_vat) {
        object_response_onclose['dVAT'] = { 'dMin': null, 'dMax': null }
      } else if (!this.min_value_vat && this.max_value_vat) {
        object_response_onclose['dVAT'] = { 'dMin': null, 'dMax': this.max_value_vat / 100 }
      } else if (!this.max_value_vat && this.min_value_vat) {
        object_response_onclose['dVAT'] = { 'dMin': this.min_value_vat / 100, 'dMax': null }
      }
      object_response_onclose['dCurrentUnits'] = (this.max_value_current_units) ? { 'dMin': this.min_value_current_units, 'dMax': this.max_value_current_units } : { 'dMin': this.min_value_current_units, 'dMax': null };
      object_response_onclose['dVariableCost'] = (this.max_value_variable_cost) ? { 'dMin': this.min_value_variable_cost, 'dMax': this.max_value_variable_cost } : { 'dMin': this.min_value_variable_cost, 'dMax': null };
      object_response_onclose['dCurrentPrice'] = (this.max_value_current_price) ? { 'dMin': this.min_value_current_price, 'dMax': this.max_value_current_price } : { 'dMin': this.min_value_current_price, 'dMax': null };
      // object_response_onclose['dAverageDiscount'] = (this.max_value_average_discount) ? { 'dMin': this.min_value_average_discount, 'dMax': this.max_value_average_discount } : { 'dMin': this.min_value_average_discount, 'dMax': null };
      if (this.min_value_average_discount && this.max_value_average_discount) {
        object_response_onclose['dAverageDiscount'] = { 'dMin': this.min_value_average_discount / 100, 'dMax': this.max_value_average_discount / 100 }
      } else if (!this.min_value_average_discount && !this.max_value_average_discount) {
        object_response_onclose['dAverageDiscount'] = { 'dMin': null, 'dMax': null }
      } else if (!this.min_value_average_discount && this.max_value_average_discount) {
        object_response_onclose['dAverageDiscount'] = { 'dMin': null, 'dMax': this.max_value_average_discount / 100 }
      } else if (!this.max_value_average_discount && this.min_value_average_discount) {
        object_response_onclose['dAverageDiscount'] = { 'dMin': this.min_value_average_discount / 100, 'dMax': null }
      }
      console.log('object_response_onclose: ', object_response_onclose);
      this.ref.close(object_response_onclose);
    } else {
      this.ref.close();
    }
  }

  fnCancelCreateNewProductInformation() {
    this.submitted = false;
    this.dismiss();
  }

  fnAdvancedSearchProductInformation(produc_information_data) {
    this.submitted = true;
    console.log('produc_information_data: ', produc_information_data);
    const data_object = {
      'products': (produc_information_data.products) ? produc_information_data.products : [],
      'priceList': (produc_information_data.priceList) ? produc_information_data.priceList : [],
      'category': (produc_information_data.category) ? produc_information_data.category : [],
      'dVAT': {
        'dMin': this.min_value_vat == null ? '' : this.min_value_vat,
        'dMax': this.max_value_vat == null ? '' : this.max_value_vat,
      },
      'dCurrentUnits': {
        'dMin': this.min_value_current_units == null ? '' : this.min_value_current_units,
        'dMax': this.max_value_current_units == null ? '' : this.max_value_current_units,
      },
      'dVariableCost': {
        'dMin': this.min_value_variable_cost == null ? '' : this.min_value_variable_cost,
        'dMax': this.max_value_variable_cost == null ? '' : this.max_value_variable_cost,
      },
      'dCurrentPrice': {
        'dMin': this.min_value_current_price == null ? '' : this.min_value_current_price,
        'dMax': this.max_value_current_price == null ? '' : this.max_value_current_price,
      },
      'dAverageDiscount': {
        'dMin': this.min_value_average_discount == null ? '' : this.min_value_average_discount,
        'dMax': this.max_value_average_discount == null ? '' : this.max_value_average_discount,
      },
      'page': 1,
      'pageSize': 12,
      'tSearch': '',
    };
    console.log('data_object: ', data_object);
    this.dismiss(data_object);
  }

  fnGetConfigProducts(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);
    this.config_products = null;
    this.productInformationService.fnHttpGetConfigProducts(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.config_products = JSON.parse(JSON.stringify(r.body));
        console.log('this.config_products: ', this.config_products);
        this.bHideTrendOfVolumeInstalledCapacity = r.body['bHideTrendOfVolumeInstalledCapacity'];
        console.log('this.bHideTrendOfVolumeInstalledCapacity: ', this.bHideTrendOfVolumeInstalledCapacity);
        this.bShowCents = r.body['bShowCents'];
        console.log('this.bShowCents: ', this.bShowCents);
        this.quantiy_decimals = (this.bShowCents) ? 2 : 0;
        console.log('this.quantiy_decimals: ', this.quantiy_decimals);
      }
    }, err => {
      console.log('err: ', err);
    });
  }

}
