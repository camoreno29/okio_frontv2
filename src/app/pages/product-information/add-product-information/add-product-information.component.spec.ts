import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRatingPerceivedComponent } from './add-rating-perceived.component';

describe('AddRatingPerceivedComponent', () => {
  let component: AddRatingPerceivedComponent;
  let fixture: ComponentFixture<AddRatingPerceivedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRatingPerceivedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRatingPerceivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
