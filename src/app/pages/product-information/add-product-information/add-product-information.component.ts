import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options, LabelType } from 'ng5-slider';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { PricesListService } from '../../../shared/api/services/prices-list.service';
import { RatingPerceivedService } from '../../../shared/api/services/rating-perceived.service';
import { ProductInformationService } from '../../../shared/api/services/product-information.service';

@Component({
  selector: 'ngx-add-product-information',
  templateUrl: './add-product-information.component.html',
  styleUrls: ['./add-product-information.component.scss'],
})
export class AddProductInformationComponent implements OnInit {

  submitted: boolean = true;
  section_display: Number = 1;
  current_payload: any = null;
  product_data: any = {};
  list_competitors: any = [];
  data_competitor: any = null;

  list_categories_collection: any = [];
  list_prices_lists_collection: any = [];

  list_options_show_complementarity_relation: any = [{ 'value': true, 'name': 'Yes' }, { 'value': false, 'name': 'No' }];
 
  // lang_nav: any =  window.navigator.language;
  lang_nav: any = 'en';
  DATA_LANG: any = null;

  @Input() id_version: string;
  @Input() bHideTrendOfVolumeInstalledCapacity: any;

  modalAction: boolean = true;

  value_weight: number = 1;
  options_weight: Options = {
    floor: 1,
    ceil: 5,
  };

  value_rating: number = 1;
  options_rating: Options = {
    floor: 1,
    ceil: 9,
  };

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private ratingPerceivedService: RatingPerceivedService,
    private categoriesService: CategoriesService,
    private pricesListService: PricesListService,
    private productInformationService: ProductInformationService,
    protected ref: NbDialogRef<AddProductInformationComponent>,
  ) { }

  ngOnInit() {
    const self = this;

    /********Llamado a la funcion que trae los textos del modulo informacion de producto*********/
    self.utilitiesService.fnGetLanguage('productInformation', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
        console.log('id_version======================>: ', this.id_version);
        console.log('bHideTrendOfVolumeInstalledCapacity======================>: ', this.bHideTrendOfVolumeInstalledCapacity);
        this.fnGetListCategories(this.current_payload, this.id_version);
        this.fnGetPricesLists(this.current_payload, this.id_version);
      }
    });
  }

  fnGetListCategories(current_payload, id_version) {
    this.categoriesService.fnHttpGetDataListCategoriesByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_categories_collection = JSON.parse(JSON.stringify(r.body));
        // this.product_data.competitor = this.list_competitors[0];
        this.submitted = false;
        // console.log('r.status: ', r.status);
        // this.utilitiesService.showToast('top-right', 'success', 'Category has been created successfully!');
        // this.dismiss();
        // this.router.navigate(['/pages/projects/' + id_company]);
        // this.companies = r.body;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnGetPricesLists(current_payload, id_version?) {
    const self = this;
    self.pricesListService.fnGetAllPricesListByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.list_prices_lists_collection = JSON.parse(JSON.stringify(r.body));
        console.log('self.list_prices_lists_collection: ', self.list_prices_lists_collection);
        // self.product_data.competitor = self.list_competitors[0];
        self.submitted = false;
        // console.log('r.status: ', r.status);
        // self.utilitiesService.showToast('top-right', 'success', 'Category has been created successfully!');
        // self.dismiss();
        // self.router.navigate(['/pages/projects/' + id_company]);
        // self.companies = r.body;
      }
      if (r.status == 206) {
        self.submitted = false;
        let error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  dismiss(modalAction) {
    this.ref.close(modalAction);
  }

  fnCancelCreateNewRatingPerceivedValue() {
    this.submitted = false;
    this.dismiss(true);
  }

  fnCreateNewProduct(product_data) {
    this.submitted = true;
    console.log('product_data: ', product_data);
    const data_object = {
      'iIDProduct': null,
      'iIDVersion': parseInt(this.id_version, 10),
      'iOrder': null,
      'tProductCode': product_data.tProductCode,
      'tProductName': product_data.tProductName,
      'priceList': product_data.priceList,
      'category': product_data.category,
      'dVAT': (product_data.dVAT) ? (parseFloat(product_data.dVAT) / 100) : 0,
      'dCurrentUnits': (product_data.dCurrentUnits) ? parseFloat(product_data.dCurrentUnits) : 0,
      'dVariableCost': (product_data.dVariableCost) ? parseFloat(product_data.dVariableCost.replace(/,/g, '')) : 0,
      'dCurrentPrice': parseFloat(product_data.dCurrentPrice.replace(/,/g, '')),
      'dAverageDiscount': (product_data.dAverageDiscount) ? (parseFloat(product_data.dAverageDiscount) / 100) : 0,
      'dAnnualVolumeTrend': (product_data.dAnnualVolumeTrend) ? parseFloat(product_data.dAnnualVolumeTrend) : null,
      'dInstalledCapacity': (product_data.dInstalledCapacity) ? parseFloat(product_data.dInstalledCapacity) : null,
      'tLanguage': localStorage.getItem('language_app'),
    };
    console.log('data_object: ', data_object);

    this.productInformationService.fnHttpSetCreateNewProduct(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.product_data = {};
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessCreateProdInfo.text );
        this.dismiss(false);
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(null, r.body.codMessage);
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  /*****(currencyInputChanged)add thousands format*****/
  currencyInputChanged(val) {
    if (val) {
      val = this.format_number(val, '');
    }
    return val;
  }

  format_number(number, prefix) {
    let thousand_separator = ',',
      decimal_separator = '.',
      regex = new RegExp('[^' + decimal_separator + '\\d]', 'g'),
      number_string = number.replace(regex, '').toString(),
      split = number_string.split(decimal_separator),
      rest = split[0].length % 3,
      result = split[0].substr(0, rest),
      thousands = split[0].substr(rest).match(/\d{3}/g);

    if (thousands) {
      let separator = rest ? thousand_separator : '';
      result += separator + thousands.join(thousand_separator);
    }
    result = split[1] != undefined ? result + decimal_separator + split[1] : result;
    return prefix == undefined ? result : (result ? prefix + result : '');
  };

  /*****(numberOnly)allows only numbers and decimals*****/

  numberOnly(evt): boolean {
    const charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 46 && evt.srcElement.value.split('.').length > 1) {
      return false;
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

}
