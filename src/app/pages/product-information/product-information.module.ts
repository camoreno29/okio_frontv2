import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng5SliderModule } from 'ng5-slider';

import { NbStepperModule } from '@nebular/theme';


// import { ProductInformationComponent } from './product-information.component';

import { AddProductInformationComponent } from './add-product-information/add-product-information.component';
import { EditProductInformationComponent } from './edit-product-information/edit-product-information.component';
import { DeleteProductInformationComponent } from './delete-product-information/delete-product-information.component';
import { DeleteAllProductInformationComponent } from './delete-all-product-information/delete-all-product-information.component';
import { AdvancedSearchProductInformationComponent } from './advanced-search-product-information/advanced-search-product-information.component';
import { ThousandPipePipe } from '../../shared/pipes/thousand-pipe.pipe';

const ENTRY_COMPONENTS = [
  AddProductInformationComponent,
  EditProductInformationComponent,
  DeleteProductInformationComponent,
  DeleteAllProductInformationComponent,
  AdvancedSearchProductInformationComponent,
];


@NgModule({
  imports: [
    ThemeModule,
    NgSelectModule,
    FormsModule,
    TooltipModule.forRoot(),
    ModalModule,
    NgxPaginationModule,
    Ng5SliderModule,
    NbStepperModule,
  ],
  declarations: [
    AddProductInformationComponent,
    EditProductInformationComponent,
    DeleteProductInformationComponent,
    DeleteAllProductInformationComponent,
    AdvancedSearchProductInformationComponent,
    ThousandPipePipe,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class ProductInformationModule { }
