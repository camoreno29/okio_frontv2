import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options, LabelType } from 'ng5-slider';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { PricesListService } from '../../../shared/api/services/prices-list.service';
import { ProductInformationService } from '../../../shared/api/services/product-information.service';

@Component({
  selector: 'ngx-edit-product-information',
  templateUrl: './edit-product-information.component.html',
  styleUrls: ['./edit-product-information.component.scss']
})
export class EditProductInformationComponent implements OnInit {
  submitted: boolean = false;
  section_display: Number = 1;
  current_payload: any = null;
  product_data: any = {};
  list_competitors: any = [];
  data_competitor: any = null;

  list_categories_collection: any = [];
  list_prices_lists_collection: any = [];

  list_options_show_complementarity_relation: any = [{ 'value': true, 'name': 'Yes' }, { 'value': false, 'name': 'No' }];

  @Input() id_version: string;
  @Input() data_category: any;
  @Input() bHideTrendOfVolumeInstalledCapacity: any;
   
  // lang_nav: any =  window.navigator.language;
  lang_nav: any = 'en';
  DATA_LANG: any = null;

  value_weight: number = 1;
  options_weight: Options = {
    floor: 1,
    ceil: 5,
  };

  value_rating: number = 1;
  options_rating: Options = {
    floor: 1,
    ceil: 9,
  };

  data_price_list: any = {};
  data_list_category: any = {};
  state_loading: boolean = true;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private categoriesService: CategoriesService,
    private pricesListService: PricesListService,
    private productInformationService: ProductInformationService,
    protected ref: NbDialogRef<EditProductInformationComponent>,
  ) { }

  ngOnInit() {
    const self = this;

    /********Llamado a la funcion que trae los textos del modulo informacion de producto*********/
    self.utilitiesService.fnGetLanguage('productInformation', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    self.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        self.current_payload = token.getValue();
        self.fnGetListCategories(self.current_payload, self.id_version);
        self.fnGetPricesLists(self.current_payload, self.id_version, function (res_price_list) {
          console.log('res_price_list: ', res_price_list);
          console.log('self.current_payload: ', self.current_payload);
          console.log('id_version======================>: ', self.id_version);
          console.log('data_category======================>: ', self.data_category);
          self.data_price_list = JSON.parse(JSON.stringify(self.data_category['data_category']['priceList']));
          self.data_list_category = JSON.parse(JSON.stringify(self.data_category['data_category']['category']));
          console.log('self.data_price_list: ', self.data_price_list);
          self.product_data = {
            'iIDProduct': (self.data_category['data_category'].iIDProduct) ? parseInt(self.data_category['data_category'].iIDProduct, 10) : '',
            'iIDVersion': parseInt(self.id_version, 10),
            'iOrder': (self.data_category['data_category']['iOrder']) ? parseInt(self.data_category['data_category']['iOrder'], 10) : 0,
            'tProductCode': (self.data_category['data_category'].tProductCode) ? self.data_category['data_category'].tProductCode : '',
            'tProductName': (self.data_category['data_category'].tProductName) ? self.data_category['data_category'].tProductName : '',
            'priceList': self.data_category['data_category']['priceList'],
            'category': self.data_category['data_category']['category'],
            'dVAT': (self.data_category['data_category'].dVAT) ? (parseFloat(self.data_category['data_category'].dVAT) * 100) : '',
            'dCurrentUnits': (self.data_category['data_category'].dCurrentUnits) ? parseFloat(self.data_category['data_category'].dCurrentUnits) : '',
            'dVariableCost': (self.data_category['data_category'].dVariableCost) ? parseFloat(self.data_category['data_category'].dVariableCost) : '',
            'dCurrentPrice': (self.data_category['data_category'].dCurrentPrice) ? parseFloat(self.data_category['data_category'].dCurrentPrice.toFixed(2)) : '',
            'dAverageDiscount': (self.data_category['data_category'].dAverageDiscount) ? parseFloat((self.data_category['data_category'].dAverageDiscount * 100).toFixed(2)) : '',
            'dAnnualVolumeTrend': (self.data_category['data_category'].dAnnualVolumeTrend) ? parseFloat(self.data_category['data_category'].dAnnualVolumeTrend) : '',
            'dInstalledCapacity': (self.data_category['data_category'].dInstalledCapacity) ? parseFloat(self.data_category['data_category'].dInstalledCapacity) : '',
          };
          self.state_loading = false;
          console.log('self.product_data: ', self.product_data);
        });
      }
    });
  }

  fnGetListCategories(current_payload, id_version) {
    this.categoriesService.fnHttpGetDataListCategoriesByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_categories_collection = JSON.parse(JSON.stringify(r.body));
        this.submitted = false;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnGetPricesLists(current_payload, id_version, observer) {
    const self = this;
    self.pricesListService.fnGetAllPricesListByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.list_prices_lists_collection = JSON.parse(JSON.stringify(r.body));
        console.log('self.list_prices_lists_collection: ', self.list_prices_lists_collection);
        self.submitted = false;
        observer(self.list_prices_lists_collection);
      }
      if (r.status == 206) {
        observer(null);
        self.submitted = false;
        let error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  dismiss(modalAction) {
    this.ref.close(modalAction);
  }

  fnCancelCreateNewProductInformation() {
    this.submitted = false;
    this.dismiss(true);
  }

  fnEditProduct(product_data) {
    this.submitted = true;
    console.log('product_data: ', product_data);
    const data_object = {
      'iIDProduct': parseInt(product_data.iIDProduct, 10),
      'iIDVersion': parseInt(this.id_version, 10),
      'tProductCode': product_data.tProductCode,
      'tProductName': product_data.tProductName,
      'priceList': product_data.priceList,
      'category': product_data.category,
      'dVAT': (parseFloat(product_data.dVAT) / 100),
      'dCurrentUnits': parseFloat(product_data.dCurrentUnits),
      'dVariableCost': parseFloat(product_data.dVariableCost),
      'dCurrentPrice': parseFloat(product_data.dCurrentPrice),
      'dAverageDiscount': (parseFloat(product_data.dAverageDiscount) / 100),
      'dAnnualVolumeTrend': parseFloat(product_data.dAnnualVolumeTrend),
      'dInstalledCapacity': parseFloat(product_data.dInstalledCapacity),
      'tLanguage': localStorage.getItem('language_app'),
    };
    console.log('data_object: ', data_object);

    this.productInformationService.fnHttpSetEditDataProduct(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.product_data = {};
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessUpdateProdInfo.text);
        this.dismiss(false);
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(null, r.body.codMessage);
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  changeSourcePriceList(event) {
    console.log('event: ', event);
    this.product_data['priceList'] = event;
    console.log('this.product_data: ', this.product_data);
  }

  changeSourceCategory(event) {
    console.log('event: ', event);
    this.product_data['category'] = event;
    console.log('this.product_data: ', this.product_data);
  }

  /*****(currencyInputChanged)add thousands format*****/
  currencyInputChanged(val) {
    if (val) {
      val = this.format_number(val, '');
    }
    return val;
  }

  format_number(number, prefix) {
    let thousand_separator = ',',
      decimal_separator = '.',
      regex = new RegExp('[^' + decimal_separator + '\\d]', 'g'),
      number_string = number.replace(regex, '').toString(),
      split = number_string.split(decimal_separator),
      rest = split[0].length % 3,
      result = split[0].substr(0, rest),
      thousands = split[0].substr(rest).match(/\d{3}/g);

    if (thousands) {
      let separator = rest ? thousand_separator : '';
      result += separator + thousands.join(thousand_separator);
    }
    result = split[1] != undefined ? result + decimal_separator + split[1] : result;
    return prefix == undefined ? result : (result ? prefix + result : '');
  };

  /*****(numberOnly)allows only numbers and decimals*****/

  numberOnly(evt): boolean {
    const charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 46 && evt.srcElement.value.split('.').length > 1) {
      return false;
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

}
