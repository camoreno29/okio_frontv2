import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { StateService } from '../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';
import { environment } from '../../../environments/environment';

import { FormControl, FormGroup, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';

import { UtilitiesService } from '../../shared/api/services/utilities.service';
import { ProductInformationService } from '../../shared/api/services/product-information.service';
import { CategoriesService } from '../../shared/api/services/categories.service';

import { AddProductInformationComponent } from './add-product-information/add-product-information.component';
import { EditProductInformationComponent } from './edit-product-information/edit-product-information.component';
import { DeleteProductInformationComponent } from './delete-product-information/delete-product-information.component';
import { DeleteAllProductInformationComponent } from './delete-all-product-information/delete-all-product-information.component';
import { AdvancedSearchProductInformationComponent } from './advanced-search-product-information/advanced-search-product-information.component';

import { ModalsComponent } from '../../shared/components/modals/modals.component';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  selector: 'ngx-product-information',
  templateUrl: './product-information.component.html',
  styleUrls: ['./product-information.component.scss']
})
export class ProductInformationComponent implements OnInit {

  url_host: any = environment.apiUrl;

  obj_category: any = {};
  current_payload: string = null;
  id_version: any = null;
  id_category: any = null;
  submitted: any = false;
  list_categories_collection: any = null;
  categories_original_collection: any = null;
  list_products_information: any = null;
  list_products_information_original_collection: any = null;

  search_input: any = '';
  sorted: boolean = false;
  field_sort: any = null;
  state_sort_data: any = 'asc';
  fileToUpload: File = null;

  numItemsPage: any = null;
  currentPage: any = null;
  totalItems: any = null;
  file_import_input: any = null;

  object_filter: any = {};

  data_category: any = {};
  data_category_select: any = {};
  list_options_show_cents: any = [{ 'value': true, 'name': 'Yes' }, { 'value': false, 'name': 'No' }];

  layout_component_display: String = 'grid';
  bHideTrendOfVolumeInstalledCapacity: Boolean = false;
  bShowCents: any = null;
  quantiy_decimals: any = 0;
  tCurrencyISO: any = '';

  loading: boolean = true;
  filter_state: boolean = false;
  items_filtered_advance_search: any = [];

  config_products: any = null;
  show_paginate_results: boolean = false;

  loadingChart: boolean = true;

  filterState: boolean = false;
  text_searching: any = '';
  
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;
  DATA_LANG_GENERAL: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    private productInformationService: ProductInformationService,
    private categoriesService: CategoriesService,
  ) { }

  ngOnInit() {
    const self = this;
    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => { 
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    // });   
    
    self.currentPage = 1;
    self.numItemsPage = 12;

    self.route.params.subscribe(params => {
      console.log('params: ', params);
      if (params.id_version) {
        self.id_version = params.id_version;
        self.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          console.log('token: ', token);
          if (token.isValid()) {
            // here we receive a payload from the token and assigne it to our `user` variable
            self.current_payload = token.getValue();
            console.log('self.current_payload: ', self.current_payload);
            if (self.current_payload) {
              self.object_filter['competitors'] = [];
              self.fnGetAllProductsInformationByVersion(self.current_payload, self.id_version, 1);
              self.fnGetConfigProducts(self.current_payload, self.id_version);
              return false;
            }
          }
        });
      }
    });
  }

  fnGetConfigProducts(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);
    this.config_products = null;
    this.productInformationService.fnHttpGetConfigProducts(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.config_products = JSON.parse(JSON.stringify(r.body));
        console.log('this.config_products: ', this.config_products);
        this.bHideTrendOfVolumeInstalledCapacity = r.body['bHideTrendOfVolumeInstalledCapacity'];
        this.tCurrencyISO = r.body['tCurrencyISO'];
        console.log('this.bHideTrendOfVolumeInstalledCapacity: ', this.bHideTrendOfVolumeInstalledCapacity);
        this.bShowCents = r.body['bShowCents'];
        console.log('this.bShowCents: ', this.bShowCents);
        this.quantiy_decimals = (this.bShowCents) ? 2 : 0;
        console.log('this.quantiy_decimals: ', this.quantiy_decimals);
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetListCategoriesByVersion(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);

    const object_data_send = {
      'iIDCompany': id_version,
    };
    this.list_categories_collection = [];
    this.categoriesService.fnHttpGetDataListCategoriesByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_categories_collection = JSON.parse(JSON.stringify(r.body));
        this.categories_original_collection = JSON.parse(JSON.stringify(r.body));
        const id_first_category = this.list_categories_collection[0]['iIDCategory'];
        const id_category = this.list_categories_collection[0]['iIDCategory'];
        this.data_category_select = this.list_categories_collection[0];
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  /* *************** START - Get all products information *************** */
  fnGetAllProductsInformationByVersion(current_payload, id_version, page?, text_search?) {
    console.log('current_payload: ', current_payload);
    let obj_Sort = null;

    if(this.sorted){
      obj_Sort = {
        'field' : this.field_sort,
        'dir': this.state_sort_data
      }
    }

    const object_data_send = {
      'products': [],
      'priceList': [],
      'category': [],
      'dVAT': {
        'dMin': null,
        'dMax': null,
      },
      'dCurrentUnits': {
        'dMin': null,
        'dMax': null,
      },
      'dVariableCost': {
        'dMin': null,
        'dMax': null,
      },
      'dCurrentPrice': {
        'dMin': null,
        'dMax': null,
      },
      'dAverageDiscount': {
        'dMin': null,
        'dMax': null,
      },
      'Sort': obj_Sort,
      'page': (page) ? page : 1,
      'pageSize': 12,
      'tSearch': text_search,
    };

    this.productInformationService.fnHttpGetAllProductsInformationByVersion(current_payload, id_version, object_data_send).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.filter_state = false;
        this.list_products_information = JSON.parse(JSON.stringify(r.body));
        console.log('this.list_products_information: ', this.list_products_information);
        this.list_products_information_original_collection = JSON.parse(JSON.stringify(r.body));
        console.log('this.list_products_information_original_collection: ', this.list_products_information_original_collection);
        this.totalItems = r.body['totalItems'];
        console.log('this.totalItems: ', this.totalItems);
        this.show_paginate_results = (r.body['products'].length > 0) ? true : false;
        this.loadingChart = false;
        this.fnValidStateSearch(this.items_filtered_advance_search);
      }

      if (r.status == 206) {
        this.list_products_information = [];
        this.list_products_information_original_collection = [];
        this.loadingChart = false;
      }
    }, err => {
      console.log('err: ', err);
    });
  }
  /* **************** END - Get all products information **************** */
  /* *************** START - Get all products information *************** */
  fnGetAllProductsInformationAdvanceSearch(current_payload, id_version, data_object, page?) {
    console.log('current_payload: ', current_payload);
    const object_data_send = data_object;

    let obj_Sort = null;

    if(this.sorted){
      obj_Sort = {
        'field' : this.field_sort,
        'dir': this.state_sort_data
      }
    }

    object_data_send['Sort'] = obj_Sort;
    object_data_send['page'] = (page) ? page : 1,

    this.productInformationService.fnHttpGetAllProductsInformationByVersion(current_payload, id_version, object_data_send).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.list_products_information = JSON.parse(JSON.stringify(r.body));
        console.log('this.list_products_information: ', this.list_products_information);
        this.list_products_information_original_collection = JSON.parse(JSON.stringify(r.body));
        console.log('this.list_products_information_original_collection: ', this.list_products_information_original_collection);
        this.totalItems = r.body['totalItems'];
        console.log('this.totalItems: ', this.totalItems);
        this.fnValidStateSearch(this.items_filtered_advance_search);
      }

      if (r.status == 206) {
        this.list_products_information = [];
        this.list_products_information_original_collection = [];
      }
    }, err => {
      console.log('err: ', err);
    });
  }
  /* **************** END - Get all products information **************** */

  fnGetAllProducInformationFilter(current_payload, object_filter, id_category?, page?) {
    console.log('id_category: ', id_category);
    console.log('object_filter: ', object_filter);
    console.log('current_payload: ', current_payload);

    this.list_products_information.products = [];
    this.fnGetAllProductsInformationAdvanceSearch(current_payload, this.id_version, object_filter, page);
  }

  showModalAdvancedSearchProducInformation(obj_data_filter) {
    this.search_input = '';
    obj_data_filter.id_version = this.id_version;
    obj_data_filter.items_collection_filtered = this.items_filtered_advance_search;
    console.log('this.items_filtered_advance_search: ', this.items_filtered_advance_search);
    console.log('obj_data_filter: ', obj_data_filter);
    this.dialogService.open(AdvancedSearchProductInformationComponent, { context: obj_data_filter, hasScroll: true }).onClose.subscribe((res) => {
      console.log('res: ', res);
      if (res) {
        this.loading = true;
        this.filter_state = true;
        const data_object = res;
        console.log('data_object: ', data_object);
        this.items_filtered_advance_search = JSON.parse(JSON.stringify(res));
        this.fnGetAllProductsInformationAdvanceSearch(this.current_payload, this.id_version, data_object);
      }
    });
  }

  showModalAddCategory(obj_data) {
    console.log('this.data_category: ', this.data_category);
    console.log('this.bHideTrendOfVolumeInstalledCapacity: ', this.bHideTrendOfVolumeInstalledCapacity);
    obj_data.id_version = this.id_version;
    obj_data.bHideTrendOfVolumeInstalledCapacity = this.bHideTrendOfVolumeInstalledCapacity;
    console.log('obj_data: ', obj_data);
    this.dialogService.open(AddProductInformationComponent, { context: obj_data }).onClose.subscribe((res) => {
      console.log('res: ', res);
      console.log('obj_data: ', obj_data);
      if(res == false){
        this.fnGetAllProductsInformationByVersion(this.current_payload, this.id_version, 1);
        this.currentPage = 1;
      }
    });
  }

  showModalEditCategory(obj_category, id_category) {
    console.log('obj_category: ', obj_category);
    console.log('id_category: ', id_category);
    obj_category.id_version = this.id_version;
    obj_category.bHideTrendOfVolumeInstalledCapacity = this.bHideTrendOfVolumeInstalledCapacity;
    obj_category.data_category = obj_category;
    this.dialogService.open(EditProductInformationComponent, { context: obj_category }).onClose.subscribe((res) => {
      console.log('res----------------: ', res);
      console.log('obj_category: ', obj_category);
      this.id_category = obj_category['category']['iIDCategory'];
      if(res == false){
        this.fnGetAllProductsInformationByVersion(this.current_payload, this.id_version, this.currentPage);
        // this.currentPage = 1;
      }
    });
  }

  fnDeleteCategories(data_category) {
    data_category.id_version = this.id_version;
    data_category.data_category = data_category;
    data_category.id_product = data_category.iIDProduct;
    console.log('data_category: ', data_category);
    this.dialogService.open(DeleteProductInformationComponent, { context: data_category }).onClose.subscribe((res) => {
      console.log('res: ', res);
      console.log('obj_category: ', data_category);
      this.id_category = data_category['category']['iIDCategory'];
      if(res == false){
        this.fnGetAllProductsInformationByVersion(this.current_payload, this.id_version, 1);
        this.currentPage = 1;
      }    
    });
  }

  fnDeleteProductsInformation(obj_category) {
    console.log('obj_category: ', obj_category);
    obj_category.id_version = this.id_version;
    console.log('obj_category: ', obj_category);
    this.dialogService.open(DeleteAllProductInformationComponent, { context: obj_category }).onClose.subscribe((res) => {
      console.log('res: ', res);
      if(res == false){
        this.fnGetAllProductsInformationByVersion(this.current_payload, this.id_version, 1);
        this.currentPage = 1;
      }
    });
  }

  fnFilterProducts(products, text_typing) {
    console.log('products: ', products);
    console.log('text_typing: ', text_typing);
    console.log('this.filter_state: ', this.filter_state);
    if (text_typing) {
      if (text_typing.length > 2) {
        console.log('Text ok');
        if (this.filter_state) {
          console.log('this.filter_state - 1: ', this.filter_state);
          this.fnGetAllProducInformationFilter(this.current_payload, this.items_filtered_advance_search, this.id_category);
        } else {
          console.log('this.filter_state - 2: ', this.filter_state);
          this.text_searching = text_typing;
          this.fnGetAllProductsInformationByVersion(this.current_payload, this.id_version, 1, text_typing);
        }
      } else {
        console.log('Text very short');
      }
    } else {
      this.fnGetAllProductsInformationByVersion(this.current_payload, this.id_version, 1);
      console.log('Text not exist');
    }
  }

  filterItems(categories, text_typing, field?) {
    const self = this;
    const toSearch = text_typing.toLowerCase();
    if (toSearch) {
      categories = JSON.parse(JSON.stringify(self.categories_original_collection));
      self.utilitiesService.fnGetDataFilter(categories, toSearch, function (data_collection) {
        self.list_products_information = data_collection;
      });
    } else {
      self.list_products_information = JSON.parse(JSON.stringify(self.categories_original_collection));
    }
  }

  fnExportProductsInformation() {
    const lang = localStorage.getItem('language_app');
    window.open(
      this.url_host + '/api/Products/GetExportProducts?iIDVersion=' + this.id_version + '&language=' + lang,
      '_blank');
  }

  sortColumn(field) {
    console.log(`sortColumn(field)`);
    console.log('field: ', field);
    const self = this;
    self.sorted = true;
    self.field_sort = field;
    
    if (self.state_sort_data === 'asc') {
      self.state_sort_data = 'desc';
    } else {
      self.state_sort_data = 'asc';
    }
    if(this.filterState){
      this.fnGetAllProducInformationFilter(this.current_payload, this.items_filtered_advance_search, this.id_category)
    }else{
      this.fnGetAllProductsInformationByVersion(this.current_payload, this.id_version, 1, this.text_searching);
    }
    this.currentPage = 1;
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.uploadFileToActivity(this.fileToUpload);
  }

  uploadFileToActivity(fileToUpload) {
    console.log('fileToUpload: ', fileToUpload);
    const end_point_url = '/api/Products/PostImportProducts';
    const parameter = 'iIDVersion';
    this.loadingChart = true;
    this.utilitiesService.fnHttSetUploadFile(this.current_payload, this.fileToUpload, this.id_version, end_point_url, parameter).subscribe(data => {
      console.log('data: ', data);
      if (data.status == 200) {
        this.loadingChart = false;
        this.file_import_input = null;
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessImport.text);
        this.fnGetAllProductsInformationByVersion(this.current_payload, this.id_version, 1);
      }
      if (data.status == 206) {
        this.loadingChart = false;
        const error = this.utilitiesService.fnSetErrors(null, data.body.message);
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, error => {
      this.loadingChart = false;
      console.log(error);
    });
    this.file_import_input = null;
  }

  fnGetDataListProductInformation(data_category_select) {
    console.log('data_category_select: ', data_category_select);
    this.id_category = data_category_select.iIDCategory;
  }

  fnSetRemoveItemSearchFilteredAdvanced(index, data_collection) {
    console.log('index: ', index);
    console.log('data_collection: ', data_collection);
    data_collection.splice(index, 1);
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    console.log('data_collection: ', data_collection);
    this.object_filter = this.items_filtered_advance_search;

    console.log('this.items_filtered_advance_search: ', this.items_filtered_advance_search);
    this.fnGetAllProducInformationFilter(this.current_payload, this.items_filtered_advance_search, this.id_category);
  }

  fnSetRemoveBadgeAverageDiscount() {
    this.items_filtered_advance_search['dAverageDiscount']['dMin'] = null;
    this.items_filtered_advance_search['dAverageDiscount']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllProducInformationFilter(this.current_payload, this.items_filtered_advance_search, this.id_category);
  }

  fnSetRemoveBadgeCurrentPrice() {
    this.items_filtered_advance_search['dCurrentPrice']['dMin'] = null;
    this.items_filtered_advance_search['dCurrentPrice']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllProducInformationFilter(this.current_payload, this.items_filtered_advance_search, this.id_category);
  }

  fnSetRemoveBadgeCurrentUnits() {
    this.items_filtered_advance_search['dCurrentUnits']['dMin'] = null;
    this.items_filtered_advance_search['dCurrentUnits']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllProducInformationFilter(this.current_payload, this.items_filtered_advance_search, this.id_category);
  }

  fnSetRemoveBadgeVat() {
    this.items_filtered_advance_search['dVAT']['dMin'] = null;
    this.items_filtered_advance_search['dVAT']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllProducInformationFilter(this.current_payload, this.items_filtered_advance_search, this.id_category);
  }

  fnSetRemoveBadgeVariableCost() {
    this.items_filtered_advance_search['dVariableCost']['dMin'] = null;
    this.items_filtered_advance_search['dVariableCost']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllProducInformationFilter(this.current_payload, this.items_filtered_advance_search, this.id_category);
  }

  fnSwitchViewData(state_view) {
    console.log('state_view: ', state_view);
    this.layout_component_display = state_view;
  }

  getPage(page: number) {
    console.log('page: ', page);
    this.loading = true;
    if(this.filterState){
      this.fnGetAllProducInformationFilter(this.current_payload, this.items_filtered_advance_search, this.id_version, page);
    }else{
      this.fnGetAllProductsInformationByVersion(this.current_payload, this.id_version, page, this.text_searching);
    }
    this.currentPage = page;
  }

  fnValidStateSearch(collection) {
    console.log('collection: ', collection);
    if(typeof collection['category'] === "undefined"){
      console.log('indefinida')
    this.filterState = false;
      }
      else{
      console.log('definida')
      if (collection['category'].length < 1 &&
        collection['priceList'].length < 1 &&
        collection['products'].length < 1 &&
        collection['dAverageDiscount']['dMin'] == null &&
        collection['dAverageDiscount']['dMax'] == null &&
        collection['dCurrentPrice']['dMin'] == null &&
        collection['dCurrentPrice']['dMax'] == null &&
        collection['dCurrentUnits']['dMin'] == null &&
        collection['dCurrentUnits']['dMax'] == null &&
        collection['dVAT']['dMin'] == null &&
        collection['dVAT']['dMax'] == null &&
        collection['dVariableCost']['dMin'] == null &&
        collection['dVariableCost']['dMax'] == null) {
        this.filterState = false;
        return false;
      } else {
        this.filterState = true;
        return true;
      }
    } 
  }

  fnClearAllFiltersSearch() {
    this.search_input = '';
    this.filterState = false;
    this.items_filtered_advance_search = [];
    this.fnGetAllProductsInformationByVersion(this.current_payload, this.id_version);
    this.currentPage = 1;
  }

  /*********Show modal parameters function *********/

  open(_title, _body) {
    this.dialogService.open(ModalsComponent, {
      context: {
        title: _title,
        body: _body
      },
    });
  }

  /********Funcion que trae los textos del modulo informacion de producto*********/
  fnGetLanguage(){
    const self = this;
    self.utilitiesService.fnGetLanguage('productInformation', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module,
      self.DATA_LANG_GENERAL = res_lang.general
    });
  }

}
