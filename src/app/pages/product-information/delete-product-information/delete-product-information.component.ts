import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

import { Router, ActivatedRoute } from '@angular/router';

import { StateService } from '../../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { ProductInformationService } from '../../../shared/api/services/product-information.service';


@Component({
  selector: 'ngx-delete-product-information',
  templateUrl: './delete-product-information.component.html',
  styleUrls: ['./delete-product-information.component.scss'],
})
export class DeleteProductInformationComponent implements OnInit {

  @Input() id_product: String;
  @Input() tComment: String;
  @Input() iIDVersion: String;
  @Input() data_category: any;
  category_data: any = {};
  id_category: any = null;
  product: any = null;
  current_payload: string = null;
  submitted: Boolean = false;
 
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    private productInformationService: ProductInformationService,
    protected ref: NbDialogRef<DeleteProductInformationComponent>,
  ) { }

  ngOnInit() {
    const self = this;

    /********Llamado a la funcion que trae los textos del modulo informacion de producto*********/
    self.utilitiesService.fnGetLanguage('productInformation', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    console.log('this.id_product: ', this.id_product);
    console.log('this.tComment: ', this.tComment);
    console.log('this.iIDVersion: ', this.iIDVersion);
    console.log('this.data_category: ', this.data_category);
    console.log('this.category_data: ', this.category_data);
    this.product = this.data_category['tProductCode'] + ' - ' + this.data_category['tProductName']
    console.log('this.product: ', this.product);
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
      }
    });
  }

  fnSetDeleteProductInformationById(id_product) {
    this.submitted = true;
    this.productInformationService.fnHttpDeleteProduct(this.current_payload, id_product).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessDeleteProdInfo.text);
        this.dismiss(false);
      }
      if (r.status == 206) {
        this.submitted = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  dismiss(modalAction) {
    this.ref.close(modalAction);
  }

  fnCancelProductInformation() {
    this.dismiss(true);
  }

}
