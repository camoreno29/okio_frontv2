import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VersionsCompanyComponent } from './versions-company.component';

describe('VersionsCompanyComponent', () => {
  let component: VersionsCompanyComponent;
  let fixture: ComponentFixture<VersionsCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VersionsCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VersionsCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
