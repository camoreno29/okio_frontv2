import { Component, OnInit, Injectable, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';

import { StateService } from '../../../@core/utils';
import { VersionService } from '../../../shared/api/services/version.service';
import { AddVersionComponent } from '../add-version/add-version.component';
declare var $: any;

@Component({
  selector: 'ngx-versions-company',
  templateUrl: './versions-company.component.html',
  styleUrls: ['./versions-company.component.scss']
})
export class VersionsCompanyComponent implements OnInit {

  current_payload: string = null;
  id_project: any = null;
  id_company: any = null;
  versions: any = null;
  company_name: string = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private versionService: VersionService,
  ) { }

  ngOnInit() {
    console.log('this.route.params: ', this.route.params);
    this.route.params.subscribe(params => {
      console.log('params: ', params);
      if (params.id_company) {
        console.log('params: ', params);
        this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          console.log('token: ', token);
          if (token.isValid()) {
            // here we receive a payload from the token and assigne it to our `user` variable
            this.current_payload = token.getValue();
            console.log('this.current_payload: ', this.current_payload);
            if (this.current_payload) {
              let data_id_project = sessionStorage.getItem('id_project');
              let data_id_company = sessionStorage.getItem('id_company');
              this.company_name = sessionStorage.getItem('company_name');
              console.log('data_id_project: ', data_id_project);
              this.id_company = params.id_company;
              this.id_project = data_id_project;
              this.fnGetAllVersionsCompany(this.current_payload, this.id_company);
            } else {
              return false;
            }
          }
        });
      } else {
        console.log('no params ========>');
        this.router.navigate(['/pages/add-company']);
      }
    });
  }

  fnGetAllVersionsCompany(current_payload, id_company?) {
    console.log('current_payload: ', current_payload);

    const object_data_send = {
      'iIDCompany': id_company,
    }

    this.versionService.fnHttpGetAllVersionsCompany(current_payload, id_company).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.versions = r.body;
        console.log('this.versions : ', this.versions);
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetAllVersionsProject(current_payload, id_project?) {
    console.log('current_payload: ', current_payload);

    const object_data_send = {
      'iIDProject': id_project,
    };

    this.versionService.fnHttpGetAllVersionsProject(current_payload, id_project).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.versions = r.body;
        console.log('this.versions : ', this.versions);
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  open(id_project, id_company) {
    console.log('id_project: ', id_project);
    console.log('id_company: ', id_company);
    // this.dialogService.open(AddVersionComponent, {
    //   context: {
    //     title: 'Text title',
    //   },
    // });
    const ddlProyect = true;

    const group_data = {
      'ddlProyect': ddlProyect,
      'iIDCompany': id_company,
    };

    this.dialogService.open(AddVersionComponent, { context: group_data }).onClose.subscribe((res) => {
      // this.fnGetAllVersionsProject(this.current_payload, id_company);
      this.fnGetAllVersionsCompany(this.current_payload, id_company);
    });
  }



  // function for redirect to start

  fnRedirectHomePage(id_versions) {
    console.log('id_versions: ', id_versions);
    this.router.navigate(['/pages/home', id_versions]);
  }

  fnRedirectListCompanies() {
    this.router.navigate(['/pages/list-companies']);
  }


}
