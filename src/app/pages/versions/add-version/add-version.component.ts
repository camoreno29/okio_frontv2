import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { Router } from '@angular/router';
/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { VersionService } from '../../../shared/api/services/version.service';
import { ProjectService } from '../../../shared/api/services/project.service';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { formatDate } from '@angular/common';
@Component({
  selector: 'ngx-add-version',
  templateUrl: './add-version.component.html',
  styleUrls: ['./add-version.component.scss'],
})

export class AddVersionComponent implements OnInit {
  @Input() title: string;
  version: any = {};
  current_payload: any = null;
  list_projects: any = [];
  id_company: any = '';
  id_version: any = '';
  id_project: any = '';
  id_state: any = '';
  submitted: boolean = false;
  time_stamp: any = '';
  DATA_LANG: any = null;

  @Input() iIDCompany: string;
  @Input() iIDProject: string;
  @Input() iIDVersion: string;
  @Input() ddlProyect: boolean;
  // cars = [
  //   { id: 1, name: 'Volvo' },
  //   { id: 2, name: 'Saab', disabled: true },
  //   { id: 3, name: 'Opel' },
  //   { id: 4, name: 'Audi' }];
  // selectedSimpleItem = 'Two';
  // simpleItems = [];
  // disable = true;
  // selectedCarId = 3;
  constructor(
    private versionService: VersionService,
    private projectService: ProjectService,
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    protected ref: NbDialogRef<AddVersionComponent>) {
    console.log('title: ', this.title);
  }

  dismiss() {
    this.ref.close();
  }

  ngOnInit() {

    const self = this;
    self.utilitiesService.fnGetLanguage('versions', function (res_lang) {
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module;
    });
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
        console.log('this.iIDCompany: ', this.iIDCompany);
        console.log('this.iIDProject: ', this.iIDProject);
        this.id_company = this.iIDCompany;
        this.id_project = this.iIDProject;
        this.id_version = this.iIDVersion;
        this.time_stamp = formatDate(new Date(), 'yyyy/MM/dd hh:mm:ss', 'en', 'GMT+12');
        console.log('this.time_stamp: ', this.time_stamp);
        if (this.id_company) {
          this.fnGetListProjectsByCompany(this.current_payload, this.id_company);
        }
      }
    });
  }

  fnAddVersion(version) {
    console.log('version: ', version);
    this.submitted = true;
    this.id_project = this.iIDProject ? this.iIDProject : version.iIDProject;
    const data_object = {
      'iIDVersion': this.id_version,
      'iIDProject': parseInt(this.id_project, 10),
      'tVersionName': version.tVersionName,
      'bDefault': false,
      'dtCreatedDate': null,
      'tLanguage': localStorage.getItem('language_app'),
    };
    console.log('this.current_payload: ', this.current_payload);
    console.log('data_object: ', data_object);
    this.versionService.fnHttpSetSaveNewVersion(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.utilitiesService.showToast(
          'top-right', 'success', this.DATA_LANG.msgSuccessCreateVersion.text,
        );
        this.fnCancelSaveVersion();
        setTimeout(() => {
          this.dismiss();
        }, 3000);
        console.log('r.status: ', r.status);
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(null, r.body.codMessage);
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'danger', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }
  fnGetListProjectsByCompany(current_payload, id_company?) {
    console.log('current_payload: ', current_payload);
    const object_data_send = {
      'iIDCompany': id_company,
    };
    this.projectService.fnHttpGetAllProjectsByCompany(current_payload, id_company).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_projects = r.body;
        console.log('this.list_projects : ', this.list_projects);
      }
    }, err => {
      console.log('err: ', err);
    });
  }
  fnCancelSaveVersion() {
    this.submitted = false;
    this.version['iIDProject'] = '';
    this.version['tVersionName'] = '';
    this.dismiss();
  }
}
