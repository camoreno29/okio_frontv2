import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

import { Router, ActivatedRoute } from '@angular/router';

import { StateService } from '../../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';

import { VersionService } from '../../../shared/api/services/version.service';

@Component({
  selector: 'ngx-delete-versions',
  templateUrl: './delete-versions.component.html',
  styleUrls: ['./delete-versions.component.scss']
})

export class DeleteVersionsComponent implements OnInit {

  @Input() id_version: String;
  @Input() version_name: String;
  

  current_payload: string = null;
  submitted: Boolean = false;

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    protected ref: NbDialogRef<DeleteVersionsComponent>,
    private versionService: VersionService,
    ) { }

  ngOnInit() {

    const self = this;
    
    self.utilitiesService.fnGetLanguage('versions', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });

    console.log('id_version: ', this.id_version);
    console.log('version_name: ', this.version_name);
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
      }
    });
  }

  fnSetDeleteVersion(id_version) {
    this.submitted = true;
    this.versionService.fnHttpDeleteVersion(this.current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessDeleteVersion.text);
        // this.dismiss(false);
        
      }
      if (r.status == 206) {
        this.submitted = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
      this.dismiss();
    }, err => {
      console.log('err: ', err);
    });
  }

  dismiss() {
    this.ref.close();
  }

  fnCancelDeleteProject() {
    this.dismiss();
  }

}
