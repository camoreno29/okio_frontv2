import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteVersionsComponent } from './delete-versions.component';

describe('DeleteVersionsComponent', () => {
  let component: DeleteVersionsComponent;
  let fixture: ComponentFixture<DeleteVersionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteVersionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteVersionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
