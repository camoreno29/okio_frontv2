import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { StateService } from '../../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';

import { BrowserModule } from '@angular/platform-browser';
import { FormControlName, FormGroup, ReactiveFormsModule, FormBuilder, Validators, NgForm } from '@angular/forms';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { ConfigService } from '../../../shared/api/services/config.service';

import { CommonModule } from "@angular/common"
import { ModalsComponent } from '../../../shared/components/modals/modals.component';
import { NbDialogService } from '@nebular/theme';
import { validateConfig } from '@angular/router/src/config';

import { PagesComponent } from '../../../pages/pages.component';
import { element } from '@angular/core/src/render3';

// import { HeaderComponent } from '../../../@theme/components/header/header.component';

declare var $: any;
@Component({
  selector: 'ngx-settings-version',
  templateUrl: './settings-version.component.html',
  styleUrls: ['./settings-version.component.scss']
})
export class SettingsVersionComponent implements OnInit {

  current_payload: string = null;
  id_version: any = null;
  groups: any = null;
  submitted: any = false;

  data_types: any = [];
  data_types_copy: any = [];
  list_currencies: any = [];
  list_product_types: any = [];

  state_form: boolean = true;

  option_menu: Number = 1;

  state: boolean = false;

  objValidate: any = {};
  /* ********** Start - Variables form ********** */
  data_form_settings_version: any = {};
  data_form_settings_version_copy: any = {};
  list_options_show_cents: any = [{ 'value': true, 'name': 'Yes' }, { 'value': false, 'name': 'No' }];
  list_options_show_currency: any = [{ 'value': 1, 'name': 'COP' }];
  list_options_volume_trend_installed_capacity: any = [{ 'value': true, 'name': 'Show' }, { 'value': false, 'name': 'Hide' }];
  list_options_round_psychological_prices: any = [{ 'value': true, 'name': 'Yes' }, { 'value': false, 'name': 'No' }];


  /* *********** End - Variables form *********** */

  lang_nav: any = window.navigator.language;
  DATA_LANG: any = null;

  current_lang: any = null;
  lang_list: any = ['en', 'es'];

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    private configService: ConfigService,
    private formBuilder: FormBuilder,
    private pagesComponent: PagesComponent,
    // private headerComponent: HeaderComponent,
  ) { }

  ngOnInit() {

    /* *** START - JQuery definition *** */
    // JQuery ready
    $(document).ready(function () {
      change_size();
      $('.pgp-item_list').click(function () {
        $('.pgp-item_list').removeClass('pgp-active_item_list');
        var num_item = $(this).attr('item');
        $('#pgp-item_list_' + num_item).addClass('pgp-active_item_list');
      });
    });
    $('#pgp-btn_toogle_side_bar').click(function (){
      change_size();
    });
    $(window).resize(function(){
      change_size();
    });
    // JQuery function to determines size of layout width
    function change_size(){
      var div_height = $("#pgp-approach_width").width();
      if(div_height < 897.5){
        $(".pgp-input_margin").css({marginTop: "0px"});
      }else{
        $(".pgp-input_margin").css({marginTop: "16px"});
      }
      if(div_height < 759){
        $(".pgp-input_margin_2").css({marginTop: "0px"});
      }else{
        $(".pgp-input_margin_2").css({marginTop: "16px"});
      }
      if(div_height > 260){
        $(".pgp-input_margin_3").css({marginTop: "0px"});
      }else{
        $(".pgp-input_margin_3").css({marginTop: "16px"});
      }
      if(div_height < 870.5){
        $(".pgp-input_margin_4").css({marginTop: "0px"});
      }else{
        $(".pgp-input_margin_4").css({marginTop: "16px"});
      }
      if(div_height < 831){
        $(".pgp-input_margin_5").css({marginTop: "0px"});
      }else{
        $(".pgp-input_margin_5").css({marginTop: "16px"});
      }
    };

    /* **** END - JQuery definition **** */
    const self = this;
    self.fnGetLanguageList();
    self.fnGetLanguage();
    
    self.route.params.subscribe(params => {
      console.log('params: ', params);
      if (params.id_version) {
        self.id_version = params.id_version;
        self.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          console.log('token: ', token);
          self.current_payload = token.getValue();
          if (self.current_payload != null && self.current_payload != '' && token.isValid()) {
            console.log('token.isValid(): ', token.isValid());
            // here we receive a payload from the token and assigne it to our `user` variable
            
            console.log('self.current_payload: ', self.current_payload);
            if (self.current_payload) {
              const data_id_company = sessionStorage.getItem('id_company');
              console.log('data_id_company: ', data_id_company);
              self.fnGetCurrencies(self.current_payload, self.id_version);
              self.fnGetListProductTypesVersion(self.current_payload, self.id_version);
              self.fnGetAllDataConfigByVersion(self.current_payload, self.id_version);
              return false;
            }
          } else {
            self.utilitiesService.fnDestroySession();
            this.router.navigateByUrl('auth/login');
          }
        });
      }
    });
  }

  fnGetAllDataConfigByVersion(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);

    const object_data_send = {
      'iIDVersion': id_version,
    };

    this.configService.fnHttpGetAllDataConfigByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        let data_config = r.body;
        console.log('data_config : ', data_config);

        const object_data = {
          'iIDConfig': parseInt(data_config.iIDConfig, 10),
          'iIDVersion': parseInt(data_config.iIDVersion, 10),
          'bShowCents': data_config.bShowCents,
          'iIDCurrency': parseInt(data_config.iIDCurrency, 10),
          'bHideTrendOfVolumeInstalledCapacity': data_config.bHideTrendOfVolumeInstalledCapacity,
          'bRoundPsychologicalPrices': data_config.bRoundPsychologicalPrices,
          'iIDProductHookUpTP': parseInt(data_config.iIDProductHookUpTP, 10),
          'iIDComplementaryProductsFromTP': data_config.iIDComplementaryProductsFromTP,
          'dXX1nZerosRight': parseInt(data_config.dXX1nZerosRight, 10),
          'dXX5AndXX9nZerosRight': parseInt(data_config.dXX5AndXX9nZerosRight, 10),
          'dXX9nZerosRight': parseInt(data_config.dXX9nZerosRight, 10),
          'dX49AndX99nZerosRight': parseInt(data_config.dX49AndX99nZerosRight, 10),
          'dX99nZerosRightHereafter': parseInt(data_config.dX99nZerosRightHereafter, 10),
          'inZerosRight': parseInt(data_config.inZerosRight, 10),
          'bMaximizeSuggestedPrice': data_config.bMaximizeSuggestedPrice,
          'iIDFromTP': (data_config.iIDFromTP) ? parseInt(data_config.iIDFromTP, 10) : null,
          'bForceTheoreticalPriceSuggested': data_config.bForceTheoreticalPriceSuggested,
          'iIDUntilTP': (data_config.iIDUntilTP) ? parseInt(data_config.iIDUntilTP, 10) : null,
          'bRoundOffProductPricesHook': data_config.bRoundOffProductPricesHook,
          'dMaximunIncreasePriceIE': (data_config.dMaximunIncreasePriceIE) || (data_config.dMaximunIncreasePriceIE == 0) ? parseFloat(data_config.dMaximunIncreasePriceIE) : null,
          'dMaximunIncreaseSalesIE': (data_config.dMaximunIncreaseSalesIE) || (data_config.dMaximunIncreaseSalesIE == 0) ? parseFloat(data_config.dMaximunIncreaseSalesIE) : null,
          'dMinimunElasticityIE': (data_config.dMinimunElasticityIE) || (data_config.dMinimunElasticityIE == 0) ? parseFloat(data_config.dMinimunElasticityIE) : null,
          'dRelationshipBrandElasticityMarketElasticityIE': (data_config.dRelationshipBrandElasticityMarketElasticityIE) ? parseFloat(data_config.dRelationshipBrandElasticityMarketElasticityIE) : null,
        };

        console.log('object_data: ', object_data);


        //this.test = object_data.dXX1nZerosRight;

        this.data_form_settings_version = object_data;
        this.data_form_settings_version_copy = JSON.parse(JSON.stringify(object_data));
        console.log('this.data_form_settings_version_copy: ', this.data_form_settings_version_copy);
        this.data_types = data_config.productTypesVersion;
        this.data_types_copy = JSON.parse(JSON.stringify(data_config.productTypesVersion));
        console.log('this.data_form_settings_version: ', this.data_form_settings_version);
        console.log('this.data_types: ', this.data_types);
        // this.fnValidateFormNull();
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetCurrencies(current_payload, id_version?) {
    // this.list_currencies = [];
    this.configService.fnHttpGetCurrencies(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_currencies = r.body;
      }
    });
  }

  fnGetListProductTypesVersion(current_payload, id_version?) {
    this.configService.fnHttpGetListProductTypesVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_product_types = r.body;
      }
    });
  }
  fnSaveConfigVersion(data_form_settings_version) {
    console.log('data_form_settings_version: ', data_form_settings_version);
    console.log('data_form_settings_version.dXX1nZerosRight: ', data_form_settings_version.dXX1nZerosRight);
    console.log('this.data_types: ', this.data_types);
    console.log('this.data_types_copy: ', this.data_types_copy);
    const data_types_sent = [];
    let object_data_types: any = null;

    this.data_types.forEach(item_menu => {
      object_data_types = {
        'dMaximumPriceIncrease': parseFloat(item_menu.dMaximumPriceIncrease),
        'dMaximumPriceReduction': parseFloat(item_menu.dMaximumPriceReduction),
        'dMinimumPriceReduction': parseFloat(item_menu.dMinimumPriceReduction),
        'dPriceWeight': parseFloat(item_menu.dPriceWeight),
        'iIDProductTypeVersion': parseFloat(item_menu.iIDProductTypeVersion),
        'iPriceLevelFloor': parseFloat(item_menu.iPriceLevelFloor),
        'tProductTypeName': item_menu.tProductTypeName,
      };
      data_types_sent.push(object_data_types);
    });

    console.log('data_types_sent ===========================>>>>>>>>>', data_types_sent);
    this.submitted = true;
    console.log('data_form_settings_version: ', data_form_settings_version);
    const object_data = {
      'iIDConfig': parseInt(data_form_settings_version.iIDConfig, 10),
      'iIDVersion': parseInt(data_form_settings_version.iIDVersion, 10),
      'bShowCents': data_form_settings_version.bShowCents,
      'iIDCurrency': parseInt(data_form_settings_version.iIDCurrency, 10),
      'bHideTrendOfVolumeInstalledCapacity': data_form_settings_version.bHideTrendOfVolumeInstalledCapacity,
      'bRoundPsychologicalPrices': data_form_settings_version.bRoundPsychologicalPrices,
      'iIDProductHookUpTP': parseInt(data_form_settings_version.iIDProductHookUpTP, 10),
      'iIDComplementaryProductsFromTP': data_form_settings_version.iIDComplementaryProductsFromTP,
      'dXX1nZerosRight': parseInt(data_form_settings_version.dXX1nZerosRight, 10),
      'dXX5AndXX9nZerosRight': parseInt(data_form_settings_version.dXX5AndXX9nZerosRight, 10),
      'dXX9nZerosRight': parseInt(data_form_settings_version.dXX9nZerosRight, 10),
      'dX49AndX99nZerosRight': parseInt(data_form_settings_version.dX49AndX99nZerosRight, 10),
      'dX99nZerosRightHereafter': parseInt(data_form_settings_version.dX99nZerosRightHereafter, 10),
      'inZerosRight': parseInt(data_form_settings_version.inZerosRight, 10),
      'bMaximizeSuggestedPrice': data_form_settings_version.bMaximizeSuggestedPrice,
      'iIDFromTP': (data_form_settings_version.iIDFromTP) ? parseInt(data_form_settings_version.iIDFromTP, 10) : null,
      'bForceTheoreticalPriceSuggested': data_form_settings_version.bForceTheoreticalPriceSuggested,
      'iIDUntilTP': (data_form_settings_version.iIDUntilTP) ? parseInt(data_form_settings_version.iIDUntilTP, 10) : null,
      'bRoundOffProductPricesHook': data_form_settings_version.bRoundOffProductPricesHook,
      'productTypesVersion': data_types_sent,
      'dMaximunIncreasePriceIE': (data_form_settings_version.dMaximunIncreasePriceIE) ? parseFloat(data_form_settings_version.dMaximunIncreasePriceIE) : null,
      'dMaximunIncreaseSalesIE': (data_form_settings_version.dMaximunIncreaseSalesIE) ? parseFloat(data_form_settings_version.dMaximunIncreaseSalesIE) : null,
      'dMinimunElasticityIE': (data_form_settings_version.dMinimunElasticityIE) ? parseFloat(data_form_settings_version.dMinimunElasticityIE) : null,
      'dRelationshipBrandElasticityMarketElasticityIE': (data_form_settings_version.dRelationshipBrandElasticityMarketElasticityIE) ? parseFloat(data_form_settings_version.dRelationshipBrandElasticityMarketElasticityIE) : null,
    };
    console.log('object_data: ', object_data);
    this.configService.fnHttpSetEditDataConfigVersion(this.current_payload, object_data).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSucessUpdate.text);
        this.submitted = false;

        this.fnGetCurrencies(this.current_payload, this.id_version);
        this.fnGetListProductTypesVersion(this.current_payload, this.id_version);
        this.fnGetAllDataConfigByVersion(this.current_payload, this.id_version);
      }

      if (r.status == 206) {
        this.submitted = false;
        this.utilitiesService.showToast('top-right', 'warning', 'Error!');
      }
    });
  }

  onKeydown(event) {
    const key = event ? event.which : event.keyCode;
    if (!(key >= 48 && key <= 57) && !(key >= 96 && key <= 105) &&
      (key != 8 && key != 190 && key != 188 && key != 9 && key != 13 && key != 110 && key != 46 && key)) {
      event.preventDefault();
    }
  }

  numberOnly(e): boolean {
    const t = e.keyCode ? e.keyCode : e.which;
    if ((t > 47 && t < 58)) {
      return true;
    } else {
      if (t === 8 || t === 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  numberOnlyDot(evt): boolean {
    const charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 46 && evt.srcElement.value.split('.').length > 1) {
      return false;
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57) && charCode == 189)
      return false;
    return true;
  }


  /**Funcion para validar**/

  fnValidate(caseValidate, value, validator, valueReference?) {
    //**** Case 1: value min 1 max 5
    //**** Case 2: value min 0% max 100%
    //**** Case 3: value dont min than reference
    //**** Case 4: value should be negative and dont max than reference
    if (value === '' || value === '%') {
      this.objValidate[validator] = '.';
      this.state_form = false;
    } else {
      switch (caseValidate) {
        case 0:
          this.objValidate[validator] = false;
          this.state_form = true;
          break;
        case 1:
          if (value > 5 || value < 1) {
            this.objValidate[validator] = true;
            this.state_form = false;
          } else {
            this.objValidate[validator] = false;
            this.state_form = true;
          }
          break;
        case 2:
          if (value > 1 || value < 0) {
            this.objValidate[validator] = true;
            this.state_form = false;
          } else {
            this.objValidate[validator] = false;
            this.state_form = true;
          }
          break;
        case 3:
          if (value <= valueReference) {
            this.objValidate[validator] = true;
            this.state_form = false;
          } else {
            this.objValidate[validator] = false;
            this.state_form = true;
          }
          break;
        case 4:
          if (value < -1 || value > 0) {
            this.objValidate[validator] = 'range';
            this.state_form = false;
          } else {
            if (value <= valueReference) {
              this.objValidate[validator] = false;
              this.state_form = true;
            } else {
              this.objValidate[validator] = true;
              this.state_form = false;
            }
          }
          break;
      }
    }
  }


  onBlurMethod(id, property, event) {
    console.log('id: ', id);
    console.log('property: ', property);
    console.log('event: ', event);
    let valor = event.target.value;
    console.log('valor: ', valor);
    valor = valor.replace('%', '');
    console.log('valor: ', valor);
    if (valor) {
      event.target.value = (valor + '%');
      this.data_types[id][property] = Number(valor) / 100;
      console.log('this.data_types[id][property]: ', this.data_types[id][property]);
    }
  }

  fnPrueba(data_form_settings_version) {
    console.log('data_form_settings_version:===========================(blur) ', data_form_settings_version);
  }

  fnChangeSourceCheck(event) {
    console.log('event: ', event);
  }

  /*********Show modal parameters function *********/

  open(_title, _body) {

    let modal_params = {};
    modal_params['title'] = _title;
    modal_params['body'] = _body;
    this.dialogService.open(ModalsComponent, { context: modal_params });
  }

  /**********START - Settings select language***********/

  fnGetLanguageList() {
    const self = this;
    self.utilitiesService.fnGetLanguageList().valueChanges().subscribe(data => {
      console.log('data: ', data);
      let collection = Object.values(data);
      let collection_list = Object.keys(data);
      self.lang_list = collection;
      let lang_selected = localStorage.getItem('language_app').toString();
      //let indexofdata = collection.map(function(e) { return e.value; }).indexOf(lang_selected);
      let indexofdata = collection_list.map(e => e).indexOf(lang_selected);
      console.log('indexofdata: ', indexofdata);
    
      // let indexofdata = collection.indexOf(lang_selected);
      self.current_lang = self.lang_list[indexofdata]['text'];
      console.log('self.lang_list[indexofdata]: ', self.lang_list[indexofdata]);
      console.log('self.lang_list===================: ', self.lang_list);
    });
  }

  fnChangeLanguage(select_lang) {
    console.log('select_lang: ', typeof select_lang);
    console.log('select_lang: ', JSON.parse(select_lang));
    let lang_select = JSON.parse(select_lang);
    this.current_lang = lang_select.text;
    localStorage.setItem('language_app', lang_select.value);
    console.log('language_app:', localStorage.getItem('language_app'));
    this.utilitiesService.setData({ lang: lang_select.value });
    this.fnChangeLanguageMenu(lang_select.value);
    this.fnGetLanguage();
  }

  fnGetLanguage(){
    const self = this;
    self.utilitiesService.fnGetLanguage('settingsVersion', function (res_lang) {
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
  }

  fnChangeLanguageMenu(select_lang) {
    this.pagesComponent.setData({ lang: select_lang });
    $('#Language').text(select_lang);
  }

  // fnChangeLanguageVersionsMenu(select_lang) {
  //   this.themeSettingsComponent.setData({ lang: select_lang });
  // }

  /**********END - Settings select language***********/

}
