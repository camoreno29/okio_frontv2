import { NgModule } from '@angular/core';
import { ThemeModule } from '../../../@theme/theme.module';
// import { SettingsVersionComponent } from './settings-version.component';

import { NgSelectModule } from '@ng-select/ng-select';
import { ReactiveFormsModule } from '@angular/forms';


const ENTRY_COMPONENTS = [
  // SettingsVersionComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    NgSelectModule,
    ReactiveFormsModule,
  ],
  declarations: [
    // SettingsVersionComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class SettingsVersionModule { }
