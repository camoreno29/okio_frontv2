import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

// import { VersionsComponent } from './versions.component';
import { AddVersionComponent } from './add-version/add-version.component';
import { VersionsProjectComponent } from './versions-project/versions-project.component';
import { VersionsCompanyComponent } from './versions-company/versions-company.component';
import { DeleteVersionsComponent } from './delete-versions/delete-versions.component';

const ENTRY_COMPONENTS = [
  AddVersionComponent,
  VersionsProjectComponent,
  VersionsCompanyComponent,
];

@NgModule({
  imports: [ThemeModule],
  declarations: [
    // VersionsComponent,
    AddVersionComponent,
    VersionsProjectComponent,
    VersionsCompanyComponent,
    DeleteVersionsComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
    DeleteVersionsComponent,
  ],
})
export class VersionsModule { }
