import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VersionsProjectComponent } from './versions-project.component';

describe('VersionsProjectComponent', () => {
  let component: VersionsProjectComponent;
  let fixture: ComponentFixture<VersionsProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VersionsProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VersionsProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
