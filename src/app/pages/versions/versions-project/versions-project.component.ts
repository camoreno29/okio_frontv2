import { Component, OnInit, Injectable, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Observable, from } from 'rxjs';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbMenuItem, NbMenuService, NbSidebarService, NbDialogService } from '@nebular/theme';

import { StateService, LayoutService } from '../../../@core/utils';
import { VersionService } from '../../../shared/api/services/version.service';
import { AddVersionComponent } from '../add-version/add-version.component';
import { PagesComponent } from '../../pages.component';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { DeleteVersionsComponent } from '../delete-versions/delete-versions.component'

declare var $: any;

@Component({
  selector: 'ngx-versions-project',
  templateUrl: './versions-project.component.html',
  styleUrls: ['./versions-project.component.scss']
})
export class VersionsProjectComponent implements OnInit {

  current_payload: string = null;
  project_name: string = null;
  company_name: string = null;
  id_project: any = null;
  id_company: any = null;
  versions: any = null;
  list_versions: any = null;
  versions_original_collection: any = null;
  search_input: any = '';
  id_version: any = null;
  data_versions_object: any = {};

  lang_nav: any = window.navigator.language;
  DATA_LANG: any = '';

  constructor(
    protected stateService: StateService,
    protected layoutService: LayoutService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private versionService: VersionService,
    private pagesComponent: PagesComponent,
    private sidebarService: NbSidebarService,
    private nbMenuService: NbMenuService,
    private utilitiesService: UtilitiesService,
  ) { }

  ngOnInit() {

    const self = this;
    self.fnGetLanguage();

    self.toggleSidebar();

    // self.utilitiesService.dataChange.subscribe((data) => {
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    // });

    $(document).ready(function () {
      $('.menu-sidebar').removeClass('d-none').addClass('d-block');
      const currentElement = 'Versions';
      // $('#pgp-main_content_menu_sidebar > .menu-items > li > a[ title=' + currentElement + ']').css('background-color', 'red');
      $('#pgp-main_content_menu_sidebar > .menu-items > li > a[ title=' + currentElement + ']').addClass('active');

    });

    console.log('this.route.params: ', this.route.params);
    this.route.params.subscribe(params => {
      console.log('params: ', params);
      if (params.id_project) {
        this.id_project = params.id_project;
        console.log('params: ', params);
        this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          console.log('token: ', token);
          if (token.isValid()) {
            // here we receive a payload from the token and assigne it to our `user` variable
            this.current_payload = token.getValue();
            console.log('this.current_payload: ', this.current_payload);
            if (this.current_payload) {
              this.project_name = sessionStorage.getItem('project_name');
              let data_id_project = sessionStorage.getItem('id_project');
              let data_id_company = sessionStorage.getItem('id_company');
              console.log('data_id_project: ', data_id_project);
              this.id_company = data_id_company;
              this.company_name = sessionStorage.getItem('company_name');
              this.pagesComponent.getItemsMenuProject(this.id_project);
              this.fnGetAllVersionsProject(this.current_payload, this.id_project);
            } else {
              return false;
            }
          }
        });
      } else {
        console.log('no params ========>');
        this.router.navigate(['/pages/add-company']);
      }
    });
  }

  fnGetAllVersionsCompany(current_payload, id_company?) {
    console.log('current_payload: ', current_payload);

    const object_data_send = {
      'iIDCompany': id_company,
    };
    this.list_versions = [];
    this.versionService.fnHttpGetAllVersionsCompany(current_payload, id_company).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.versions = r.body;
        this.list_versions = JSON.parse(JSON.stringify(r.body));
        this.versions_original_collection = JSON.parse(JSON.stringify(r.body));
        console.log('this.versions : ', this.versions);
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetAllVersionsProject(current_payload, id_project?) {
    console.log('current_payload: ', current_payload);

    const object_data_send = {
      'iIDProject': id_project,
    }
    this.list_versions = [];
    this.versionService.fnHttpGetAllVersionsProject(current_payload, id_project).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.versions = r.body;
        this.list_versions = JSON.parse(JSON.stringify(r.body));
        this.versions_original_collection = JSON.parse(JSON.stringify(r.body));
        console.log('this.versions : ', this.versions);
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  open(id_project, id_company) {
    console.log('id_project: ', id_project);
    console.log('id_company: ', id_company);
    const ddlProyect = false;
    let iIDVersion = '';
    this.versions.forEach(element => {
      if (element.bDefault) {
        iIDVersion = element.iIDVersion;
      }
    });

    const group_data = {
      'ddlProyect': ddlProyect,
      'iIDProject': id_project,
      'iIDVersion': iIDVersion,
    };

    console.log('group_data: ', group_data);

    this.dialogService.open(AddVersionComponent, { context: group_data }).onClose.subscribe((res) => {
      this.fnGetAllVersionsProject(this.current_payload, id_project);
    });
  }

  fnRedirectHomePage(id_versions) {
    console.log('id_versions: ', id_versions);
    this.utilitiesService.setData({ version: id_versions });
    this.router.navigate(['/pages/home', id_versions]);
  }

  fnRedirectListProjects() {
    this.router.navigate(['/pages/projects']);
  }

  fnRedirectListCompanies() {
    this.router.navigate(['/pages/list-companies']);
  }

  toggleSidebar() {
    console.log('toggleSidebar: ');
    const self = this;
    self.sidebarService.toggle(true, 'menu-sidebar');
    self.layoutService.changeLayoutSize();
  }

  fnSetDefineVersionDefault(version_data) {
    console.log('version_data: ', version_data);
    this.fnHttpSetVersionDefault(this.current_payload, parseInt(version_data.iIDVersion, 10));
  }

  fnHttpSetVersionDefault(current_payload, id_version) {
    console.log('id_version: ', id_version);
    console.log('current_payload: ', current_payload);

    this.versionService.fnHttpSetVersionDefault(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.fnGetAllVersionsProject(this.current_payload, this.id_project);
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnShowModalDeleteProjects(version_data) {
    //version_data.iIDVersion = this.id_version
    this.data_versions_object.id_version = version_data.iIDVersion;
    this.data_versions_object.version_name = version_data.tVersionName;
    console.log('this.data_versions_object: ', this.data_versions_object);
    console.log('version_data: ', version_data);
    this.dialogService.open(DeleteVersionsComponent, { context: this.data_versions_object }).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllVersionsProject(this.current_payload, this.id_project);
    });
  }


  fnFilterVersions(versions, text_typing) {
    console.log('text_typing: ', text_typing);
    console.log('versions: ', versions);
    this.filterItems(versions, text_typing);
  }

  filterItems(versions, text_typing, field?) {
    const self = this;
    const toSearch = text_typing.toLowerCase();
    if (toSearch && toSearch.length > 2) {
      versions = JSON.parse(JSON.stringify(self.versions_original_collection));
      self.utilitiesService.fnGetDataFilter(versions, toSearch, function (data_collection) {
        console.log('data_collection: ', data_collection);
        // self.list_versions = data_collection;
        self.versions = data_collection;
      });
    } else {
      // self.list_versions = JSON.parse(JSON.stringify(self.versions_original_collection));
      self.versions = JSON.parse(JSON.stringify(self.versions_original_collection));
    }
  }

  fnGetLanguage() {
    const self = this;
    self.utilitiesService.fnGetLanguage('versions', function (res_lang) {
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
  }

}
