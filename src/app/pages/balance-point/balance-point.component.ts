import { Component, OnInit, ModuleWithComponentFactories, Pipe, PipeTransform } from '@angular/core';

import { environment } from '../../../environments/environment';
import { ActivatedRoute } from '@angular/router';

import { NbDialogService } from '@nebular/theme';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { BalancePointService } from '../../shared/api/services/balance-point.service';


import { UtilitiesService } from '../../shared/api/services/utilities.service';
import { ProductInformationService } from '../../shared/api/services/product-information.service';
import { ContentType } from '@angular/http/src/enums';

import { ModalsComponent } from '../../shared/components/modals/modals.component';

@Component({
  selector: 'ngx-balance-point',
  templateUrl: './balance-point.component.html',
  styleUrls: ['./balance-point.component.scss'],
})
export class BalancePointComponent implements OnInit {

  url_host: any = environment.apiUrl;
  id_version: any = null;

/************************ */

  config_balance: any = null;
  quantiy_decimals: any = 0;
  bShowCents: any = null;

  bHideTrendOfVolumeInstalledCapacity: Boolean = false;

/********************* */

layout_state: string = 'show_loading_layout';

list_balance_point: any = null;
obj_data: any = null;
balance_point_original_collection: any = null;

/** */

  current_payload: string = null;
  object_filter: any = {};

  submitted: boolean = false;
  IVA: string = '';
  PSVPI: string = '0';
  PSVPF: string = '0';
  PSVPV: string = '0';

  dPriceI: string = '0';
  dPriceF: string = '0';
  dPrecioV: string = '0';

  CVFI: string = '0';
  CVFF: string = '0';
  CVFV: string = '0';

  MCPI: string = '0';
  MCPF: string = '0';
  MCPV: string = '0';

  MCMI: string = '0';
  MCMF: string = '0';
  MCMV: string = '0';

  PNI: string = '0';
  PNF: string = '0';
  PNV: string = '0';

  MBI: string = '0';
  MBF: string = '0';
  MBV: string = '0';

  RMACD: string = '0';

  VEV: string = '0.0';

  VI: string = '1';

  CFI: string = '0';

  display = false;

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private route: ActivatedRoute,
    private authService: NbAuthService,
    private balancePointService: BalancePointService,
    private utilitiesService: UtilitiesService,
    private dialogService: NbDialogService,
    private productInformationService: ProductInformationService,
  ) {}

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('balancePoint', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.route.params.subscribe(params => {
      console.log('params: ', params);
      if (params.id_version) {
        this.id_version = params.id_version;
        this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          console.log('token: ', token);
          if (token.isValid()) {
            // here we receive a payload from the token and assigne it to our `user` variable
            this.current_payload = token.getValue();
            console.log('this.current_payload: ', this.current_payload);
            if (this.current_payload) {
              this.fnGetConfigBalancePoint(this.current_payload, this.id_version);
              return false;
            }
          }
        });
      }
    });

  }

  fnGetConfigBalancePoint(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);
    this.config_balance = null;
    this.productInformationService.fnHttpGetConfigProducts(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.config_balance = JSON.parse(JSON.stringify(r.body));
        console.log('this.config_balance: ', this.config_balance);
        this.bShowCents = r.body['bShowCents'];
        console.log('this.bShowCents: ', this.bShowCents);
        this.quantiy_decimals = (this.bShowCents) ? 2 : 0;
        console.log('this.quantiy_decimals: ', this.quantiy_decimals);
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetFormulasGeneral() {
     this.submitted = true;
     const data_object = {

        'vat': this.IVA  != '' ? parseFloat(this.IVA) : 0,
        'PSVPI': this.PSVPI != '' ? parseFloat(this.PSVPI.replace(/,/g,'')) : 0,
        'PSVPF': this.PSVPF != '' ? parseFloat(this.PSVPF.replace(/,/g,'')) : 0,
        'CVFI': this.CVFI != '' ? parseFloat(this.CVFI.replace(/,/g,'')) : 0,
        'CVFF' : this.CVFF != '' ? parseFloat(this.CVFF.replace(/,/g,'')) : 0,
        'MCPI': this.MCPI != '' ? parseFloat(this.MCPI) : 0,
        'MCPF': this.MCPF != '' ? parseFloat(this.MCPF) : 0,
        'VI': parseFloat(this.VI.replace(/,/g,'')),
        'CFI': this.CFI != '' ? parseFloat(this.CFI) : 0,
    };


  this.balancePointService.fnHttpGetFormulasGeneral(this.current_payload, data_object).subscribe(r => {
      if (r.status == 200) {
        this.PSVPV = r.body.dPSVPV;
        this.dPriceI = r.body.dPriceI;
        this.dPriceF = r.body.dPriceI;
        this.dPrecioV = r.body.dPrecioV;
        this.CVFV = r.body.dCVFV;
        this.MCPV = r.body.dMCPV;
        this.MCMI = r.body.dMCMI;
        this.MCMF = r.body.dMCMF;
        this.MCMV = r.body.dMCMV;
        this.PNI = r.body.dPNI;
        this.PNF = r.body.dPNF;
        this.PNV = r.body.dPNV;
        this.MBI = r.body.dMBI;
        this.MBF = r.body.dMBF;
        this.MBV = r.body.dMBV;
        this.RMACD = r.body.dRMACD;

        this.submitted = false;
      }
      if (r.status == 206) {
        this.submitted = false;
      }
    }, err => {
      this.submitted = false;
    });
  }

  /*****(showBalancePointResult)Show balance point result div*****/
  showBalancePointResult(){
    this.display = true;
  }

  /*****(currencyInputChanged)add thousands format*****/

  currencyInputChanged(val) {
    if(val){
    val = this.format_number(val, '');
    }
    return val;
  }

  format_number(number, prefix) {
    let thousand_separator = ',',
      decimal_separator = '.',
      regex = new RegExp('[^' + decimal_separator + '\\d]', 'g'),
      number_string = number.replace(regex, '').toString(),
      split = number_string.split(decimal_separator),
      rest = split[0].length % 3,
      result = split[0].substr(0, rest),
      thousands = split[0].substr(rest).match(/\d{3}/g);

    if (thousands) {
      let separator = rest ? thousand_separator : '';
      result += separator + thousands.join(thousand_separator);
    }
    result = split[1] != undefined ? result + decimal_separator + split[1] : result;
    return prefix == undefined ? result : (result ? prefix + result : '');
  };

  /*****(numberOnly)allows only numbers and decimals*****/

  numberOnly(evt): boolean {
    const charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 46 && evt.srcElement.value.split('.').length>1) {
        return false;
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
  }

  fnGetBalancePointResult(){
    this.submitted = true;
     const data_object = {
      'CFI': this.CFI != '' ? parseFloat(this.CFI) : 0,
      'CVFF': this.CVFF != '' ? parseFloat(this.CVFF.replace(/,/g,'')) : 0,
      'CVFI': this.CVFI != '' ? parseFloat(this.CVFI.replace(/,/g,'')) : 0,
      'VAT': this.IVA != '' ? parseFloat(this.IVA) : 0,
      'MCMF': parseFloat(this.MCMF),
      'MCMI': parseFloat(this.MCMI),
      'PSVPF': this.PSVPF != '' ? parseFloat(this.PSVPF.replace(/,/g,'')) : 0,
      'PSVPI': this.PSVPI != '' ? parseFloat(this.PSVPI.replace(/,/g,'')) : 0,
      'PSVPV': parseFloat(this.PSVPV),
      'VEV': this.VEV != '' ? parseFloat(this.VEV) : 0,
      'VI': this.VI != '' ? parseFloat(this.VI.replace(/,/g,'')) : 0,
      'tlanguage': localStorage.getItem('language_app'),
     };

     this.list_balance_point = [];
     this.balancePointService.fnHttpGetBalancePointResult(this.current_payload, data_object).subscribe(r => {
      if (r.status == 200) {

         this.list_balance_point = JSON.parse(JSON.stringify(r.body));
         this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgResults.text);

        this.submitted = false;
      }
      if (r.status == 206) {
        this.submitted = false;
      }
    }, err => {
      this.submitted = false;
    });
  }

  /*****(selectAllContent)select all the text in an input*****/

  selectAllContent($event) {
    $event.target.select();
  }

  fnCargarExportar() {

    let data_obj = [];
    let list_sent = [];

    this.list_balance_point.forEach(datos => {
      list_sent.push(datos);
      console.log('datos',datos);
      });

    const data_object = {
      'VAT': this.IVA  != '' ? parseFloat(this.IVA) : 0,
      'PSVPI': this.PSVPI != '' ?  parseFloat(this.PSVPI.replace(/,/g,'')) : 0,
      'PSVPF': this.PSVPF != '' ?  parseFloat(this.PSVPF.replace(/,/g,'')) : 0,
      'PSVPV': parseFloat(this.PSVPV),
      'priceI': parseFloat(this.dPriceI),
      'priceF': parseFloat(this.dPriceF),
      'priceV': parseFloat(this.dPrecioV),
      'CVFF': this.CVFF != '' ? parseFloat(this.CVFF.replace(/,/g,'')) : 0,
      'CVFI': this.CVFI != '' ? parseFloat(this.CVFI.replace(/,/g,'')) : 0,
      'CVFV': this.CVFV != '' ? parseFloat(this.CVFV) : 0.0,
      'MCPI': this.MCPI != '' ? parseFloat(this.MCPI.replace(/,/g,'')) : 0.0,
      'MCPF': this.MCPF != '' ? parseFloat(this.MCPF.replace(/,/g,'')) : 0.0,
      'MCPV': parseFloat(this.MCPV),
      'MCMI': parseFloat(this.MCMI),
      'MCMF': parseFloat(this.MCMF),
      'MCMV': this.MCMV != '' ? parseFloat(this.MCMV) : 0.0,
      'PNI': parseFloat(this.PNI),
      'PNF': parseFloat(this.PNF),
      'PNV': parseFloat(this.PNV),
      'MBI': parseFloat(this.MBI),
      'MBF': parseFloat(this.MBF),
      'MBV': this.MBV != '' ? parseFloat(this.MBV) : 0.0,
      'RMACD': parseFloat(this.RMACD),
      'VEV': parseFloat(this.VEV),
      'VI': parseFloat(this.VI.replace(/,/g,'')),
      'CFI': parseFloat(this.CFI),
      'balancePointResult': list_sent,
      'tlanguage': localStorage.getItem('language_app'),
    };
   // data_obj.push(data_object);

    

    this.balancePointService.fnHttpExportBalancePointResult(this.current_payload, data_object).subscribe(r => {
      if (r.status == 200) {
        // var data = JSON.parse(JSON.stringify(r.body));
          var blob = new Blob([r.body], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });

           if (window.navigator.msSaveOrOpenBlob) {
               window.navigator.msSaveBlob(blob);
           }
           else {
               var downloadLink = window.document.createElement('a');
               downloadLink.href = window.URL.createObjectURL(blob);
               downloadLink.download = "Punto de equilibrio.xlsx";
               document.body.appendChild(downloadLink);
               downloadLink.click();
               document.body.removeChild(downloadLink);
           }
          this.submitted = false;
      }

    });
  }

  /*********Show modal parameters function *********/

  open(_title,_body) {
    this.dialogService.open(ModalsComponent, {
      context: {
        title: _title,
        body:_body
      },
    });
  }

}
