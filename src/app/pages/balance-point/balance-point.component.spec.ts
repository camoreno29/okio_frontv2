import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BalancePointComponent } from './balance-point.component';

describe('BalancePointComponent', () => {
  let component: BalancePointComponent;
  let fixture: ComponentFixture<BalancePointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BalancePointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BalancePointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
