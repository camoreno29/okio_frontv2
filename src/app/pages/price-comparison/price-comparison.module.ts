import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng5SliderModule } from 'ng5-slider';

import { AdvancedSearchPriceComparisonComponent } from './advanced-search-price-comparison/advanced-search-price-comparison.component';

const ENTRY_COMPONENTS = [
  AdvancedSearchPriceComparisonComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    TooltipModule.forRoot(),
    ModalModule,
    NgSelectModule,
    FormsModule,
    NgxPaginationModule,
    Ng5SliderModule,
  ],
  declarations: [
    AdvancedSearchPriceComparisonComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class PriceComparisonModule { }
