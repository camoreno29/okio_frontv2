import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// import * as $ from 'jquery';// import Jquery here

import { StateService } from '../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';
import { environment } from '../../../environments/environment';

import { UtilitiesService } from '../../shared/api/services/utilities.service';

import { PriceComparisonService } from '../../shared/api/services/price-comparison.service';
import { CategoriesService } from '../../shared/api/services/categories.service';
import { ProductInformationService } from '../../shared/api/services/product-information.service';

import { AdvancedSearchPriceComparisonComponent } from './advanced-search-price-comparison/advanced-search-price-comparison.component';
import { constants } from 'os';

import { ModalsComponent } from '../../shared/components/modals/modals.component';

declare var $: any;
@Component({
  selector: 'ngx-price-comparison',
  templateUrl: './price-comparison.component.html',
  styleUrls: ['./price-comparison.component.scss'],
})
export class PriceComparisonComponent implements OnInit {
  state: boolean = false;
  suggestedStage: boolean = false;
  Opc1: boolean = true;
  Opc2: boolean = false;
  Opc3: boolean = false;
  iIDOpc: any = 1;

  url_host: any = environment.apiUrl;
  list_price_comparison: any = null;
  ddl_price_comparison: any = [];
  layoud_component_display: String = 'grid';
  numItemsPage: any = null;
  currentPage: any = null;
  totalItems: any = null;

  sorted: boolean = false;
  field_sort: any = null;
  state_sort_data: any = 'asc';
  
  filterState: boolean = false;

  text_searching: any = '';

  loading: boolean = true;
  filter_state: boolean = false;
  items_filtered_advance_search: any = [];
  obj: any = {};
  current_payload: string = null;
  id_version: any = null;
  submitted: any = false;
  fileToUpload: File = null;
  file_import_input: any = null;
  object_filter: any = {};
  data: any = {};
  quantiy_decimals: any = 0;
  bShowCents: any = null;
  tCurrencyISO: any = '';
  config: any = null;
  search_input: any = '';
  charttest: any = [];

  type = 'ColumnChart';
  data_chart = [];
  collection_graph = [];
  columnnames = ['name', 'Price', { 'role': 'style' }];

  legend: 'none';
  options = {
    legend: 'none',
    hAxis: {
      baselineColor: '#fff',
      gridlineColor: '#fff',
      textPosition: 'none',
    },
    vAxis: {
      baselineColor: '#fff',
      gridlineColor: '#fff',
      textPosition: 'none',
    },
    chartArea: {
      left: 0,
      right: 0, // !!! works !!!
      bottom: 0,  // !!! works !!!
      top: 0,
      width: '60',
      height: '20',
    },
    tooltip: {
      trigger: 'none',
    },
  };
  width = '60';
  height = '20';
  /* ********** START - Options graphic card ********** */
  optionsCard = {
    legend: 'none',
    hAxis: {
      baselineColor: '#fff',
      gridlineColor: '#fff',
      textPosition: 'none',
    },
    vAxis: {
      baselineColor: '#fff',
      gridlineColor: '#fff',
      textPosition: 'none',
    },
    chartArea: {
      left: 0,
      right: 0, // !!! works !!!
      bottom: 0,  // !!! works !!!
      top: 0,
      width: '60',
      height: '20',
    },
    tooltip: {
      trigger: 'none',
    },
  };
  widthCard = '150';
  heightCard = '40';
  /* ********** END - Options graphic card ********** */


  data_price_base: any = [];
  data_price_index_company: any = [];
  data_price_index_maxim: any = [];
  data_price_index_average: any = [];
  
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;
  DATA_LANG_GENERAL: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    private priceComparisonService: PriceComparisonService,
    private categoriesService: CategoriesService,
    private productInformationService: ProductInformationService,
  ) { }

  ngOnInit() {
    const self = this;
    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => { 
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    // });
    this.currentPage = 1;
    this.numItemsPage = 12;
    this.state = false;
    this.collection_graph = this.data_price_base;
    this.route.params.subscribe(params => {
      // console.log('params: ', params);
      if (params.id_version) {
        this.id_version = params.id_version;
        this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          // console.log('token: ', token);
          if (token.isValid()) {
            // here we receive a payload from the token and assigne it to our `user` variable
            this.current_payload = token.getValue();
            // console.log('this.current_payload: ', this.current_payload);
            if (this.current_payload) {
              this.object_filter = [];
              this.fnGetAllPriceComparison(this.current_payload, this.id_version, 1);
              this.fnGetConfigProducts(this.current_payload, this.id_version);
              this.fnGetConfigStage(this.current_payload, this.id_version);
              // this.fnGetAllddlBase();
              return false;
            }
          }
        });
      }
    });
  }

  /** Funcion para cambiar de grid a cards **/
  fnSwitchViewData(state_view) {
    // console.log('state_view: ', state_view);
    this.layoud_component_display = state_view;
  }
  /** **/

  /** Funciones para cargar los datos y Ordenar por columna **/
  fnGetAllddlBase() {
    this.ddl_price_comparison = [
      { 'iIDOpc': 1, 'tNombre': this.DATA_LANG.ddlCompany.text },
      { 'iIDOpc': 2, 'tNombre': this.DATA_LANG.ddlMaxCompetition.text },
      { 'iIDOpc': 3, 'tNombre': this.DATA_LANG.ddlAverageComp.text },
    ];
  }

  fnGetConfigStage(current_payload, id_version?) {
    // console.log('current_payload: ', current_payload);
    // fnHttpGetConfigProducts(guid_user, id_version)
    this.config = null;
    this.loading = true;
    this.submitted = true;
    this.productInformationService.fnHttpGetSuggestedPrice(current_payload, id_version).subscribe(r => {
      // console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.submitted = false;
        this.suggestedStage = JSON.parse(JSON.stringify(r.body));
        // console.log('suggestedStage: ', this.suggestedStage);
      }
    }, err => {
      this.loading = false;
      this.submitted = false;
      // console.log('err: ', err);
    });
  }

  fnChangeState(event) {

    if (this.state) {
      this.collection_graph = [];
      this.collection_graph = this.data_price_index_company;
    } else if (!this.state) {
      this.collection_graph = [];
      this.collection_graph = this.data_price_base;
    }

  }


  fnChangeOpc(event) {
    // console.log('event: ', event);

    if (event.iIDOpc == 1) {
      this.Opc1 = true;
      this.Opc2 = false;
      this.Opc3 = false;
      this.collection_graph = [];
      this.collection_graph = this.data_price_index_company;
      console.log('collection_graph: ', this.collection_graph);
    } else if (event.iIDOpc == 2) {
      this.Opc1 = false;
      this.Opc2 = true;
      this.Opc3 = false;
      this.collection_graph = [];
      this.collection_graph = this.data_price_index_maxim;
      console.log('this.collection_graph: ', this.collection_graph);
    } else if (event.iIDOpc == 3) {
      this.Opc1 = false;
      this.Opc2 = false;
      this.Opc3 = true;
      this.collection_graph = [];
      this.collection_graph = this.data_price_index_average;
      console.log('this.collection_graph: ', this.collection_graph);
    }
    // this.fnGetAllPriceComparison(this.current_payload, this.id_version, 1);
  }

  fnGetAllPriceComparison(current_payload, id_version?, page?, text_search?) {
    let obj_Sort = null;

    if (this.sorted) {
      obj_Sort = {
        'field': this.field_sort,
        'dir': this.state_sort_data,
      };
    }

    const object_data_send = {
      'products': [],
      'priceList': [],
      'category': [],
      'competitors': [],
      'tProductsType': null,
      'dPrices': {
        'dMin': null,
        'dMax': null,
      },
      'dIndex': {
        'dMin': null,
        'dMax': null,
      },
      'dIndex1User': {
        'dMin': null,
        'dMax': null,
      },
      'dIndex1Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dCurrentPrice': {
        'dMin': null,
        'dMax': null,
      },
      'dCurrentIndex': {
        'dMin': null,
        'dMax': null,
      },
      'dSuggestedPrice': {
        'dMin': null,
        'dMax': null,
      },
      'dSuggestedIndex': {
        'dMin': null,
        'dMax': null,
      },
      'dUserPrice': {
        'dMin': null,
        'dMax': null,
      },
      'dUserIndex': {
        'dMin': null,
        'dMax': null,
      },
      'Sort': obj_Sort,
      'indexType': this.iIDOpc,
      'page': (page) ? page : 1,
      'pageSize': 12,
      'tSearch': text_search,
    };

    console.log('object_data_send: ', object_data_send);

    this.loading = true;
    this.submitted = true;
    this.list_price_comparison = [];

    this.priceComparisonService.fnHttpGetAllPriceComparison(current_payload, id_version, object_data_send).subscribe(r => {
      // console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.filter_state = false;
        this.submitted = false;
        this.list_price_comparison = JSON.parse(JSON.stringify(r.body.priceComparison));

        this.list_price_comparison.forEach((value, key) => {

          this.data_price_base[key] = [
            ['dCurrentPriceWithVAT', parseInt((value['dCurrentPriceWithVAT']) ? value['dCurrentPriceWithVAT'] : 0), '#F79420'],
            ['dPriceCompetitor1', parseInt((value['dPriceCompetitor1']) ? value['dPriceCompetitor1'] : 0), '#06A7E2'],
            ['dPriceCompetitor2', parseInt((value['dPriceCompetitor2']) ? value['dPriceCompetitor2'] : 0), '#06A7E2'],
            ['dPriceCompetitor3', parseInt((value['dPriceCompetitor3']) ? value['dPriceCompetitor3'] : 0), '#06A7E2'],
            ['dPriceCompetitor4', parseInt((value['dPriceCompetitor4']) ? value['dPriceCompetitor4'] : 0), '#06A7E2'],
            ['dPriceCompetitor5', parseInt((value['dPriceCompetitor5']) ? value['dPriceCompetitor5'] : 0), '#06A7E2'],
            this.suggestedStage ? ['dPriceWithVAT_User', parseInt((value['dPriceWithVAT_User']) ? value['dPriceWithVAT_User'] : 0), '#F79420'] : ['dPriceWithVAT_Suggested', parseInt((value['dPriceWithVAT_Suggested']) ? value['dPriceWithVAT_Suggested'] : 0), '#F79420'],
          ]

          this.data_price_index_company[key] = [

            ['dPriceIndexCurrent1Company', parseInt((value['dPriceIndexCurrent1Company']) ? value['dPriceIndexCurrent1Company'] : 0), '#F79420'],
            this.suggestedStage ? ['dPriceIndex1Competitor1_User', parseInt((value['dPriceIndex1Competitor1_User']) ? value['dPriceIndex1Competitor1_User'] : 0), '#06A7E2'] : ['dPriceIndex1Competitor1_Suggested', parseInt((value['dPriceIndex1Competitor1_Suggested']) ? value['dPriceIndex1Competitor1_Suggested'] : 0), '#06A7E2'],
            this.suggestedStage ? ['dPriceIndex1Competitor2_User', parseInt((value['dPriceIndex1Competitor2_User']) ? value['dPriceIndex1Competitor2_User'] : 0), '#06A7E2'] : ['dPriceIndex1Competitor2_Suggested', parseInt((value['dPriceIndex1Competitor2_Suggested']) ? value['dPriceIndex1Competitor2_Suggested'] : 0), '#06A7E2'],
            this.suggestedStage ? ['dPriceIndex1Competitor3_User', parseInt((value['dPriceIndex1Competitor3_User']) ? value['dPriceIndex1Competitor3_User'] : 0), '#06A7E2'] : ['dPriceIndex1Competitor3_Suggested', parseInt((value['dPriceIndex1Competitor3_Suggested']) ? value['dPriceIndex1Competitor3_Suggested'] : 0), '#06A7E2'],
            this.suggestedStage ? ['dPriceIndex1Competitor4_User', parseInt((value['dPriceIndex1Competitor4_User']) ? value['dPriceIndex1Competitor4_User'] : 0), '#06A7E2'] : ['dPriceIndex1Competitor4_Suggested', parseInt((value['dPriceIndex1Competitor4_Suggested']) ? value['dPriceIndex1Competitor4_Suggested'] : 0), '#06A7E2'],
            this.suggestedStage ? ['dPriceIndex1Competitor5_User', parseInt((value['dPriceIndex1Competitor5_User']) ? value['dPriceIndex1Competitor5_User'] : 0), '#06A7E2'] : ['dPriceIndex1Competitor5_Suggested', parseInt((value['dPriceIndex1Competitor5_Suggested']) ? value['dPriceIndex1Competitor5_Suggested'] : 0), '#06A7E2'],
            this.suggestedStage ? ['dPriceIndex1Company_User', parseInt((value['dPriceIndex1Company_User']) ? value['dPriceIndex1Company_User'] : 0), '#F79420'] : ['dPriceIndex1Company_Suggested', parseInt((value['dPriceIndex1Company_Suggested']) ? value['dPriceIndex1Company_Suggested'] : 0), '#F79420'],

          ]

          this.data_price_index_maxim[key] = [
            ['dPriceIndexCurrent2Company', parseInt((value['dPriceIndexCurrent2Company']) ? value['dPriceIndexCurrent2Company'] : 0), '#F79420'],
            ['dPriceIndex2Competitor1', parseInt((value['dPriceIndex2Competitor1']) ? value['dPriceIndex2Competitor1'] : 0), '#06A7E2'],
            ['dPriceIndex2Competitor2', parseInt((value['dPriceIndex2Competitor2']) ? value['dPriceIndex2Competitor2'] : 0), '#06A7E2'],
            ['dPriceIndex2Competitor3', parseInt((value['dPriceIndex2Competitor3']) ? value['dPriceIndex2Competitor3'] : 0), '#06A7E2'],
            ['dPriceIndex2Competitor4', parseInt((value['dPriceIndex2Competitor4']) ? value['dPriceIndex2Competitor4'] : 0), '#06A7E2'],
            ['dPriceIndex2Competitor5', parseInt((value['dPriceIndex2Competitor5']) ? value['dPriceIndex2Competitor5'] : 0), '#06A7E2'],
            this.suggestedStage ? ['dPriceIndex2Company_User', parseInt((value['dPriceIndex2Company_User']) ? value['dPriceIndex2Company_User'] : 0), '#F79420'] : ['dPriceIndex2Company_Suggested', parseInt((value['dPriceIndex2Company_Suggested']) ? value['dPriceIndex2Company_Suggested'] : 0), '#F79420'],
          ]

          this.data_price_index_average[key] = [
            ['dPriceIndexCurrent3Company', parseInt((value['dPriceIndexCurrent3Company']) ? value['dPriceIndexCurrent3Company'] : 0), '#F79420'],
            ['dPriceIndex3Competitor1', parseInt((value['dPriceIndex3Competitor1']) ? value['dPriceIndex3Competitor1'] : 0), '#06A7E2'],
            ['dPriceIndex3Competitor2', parseInt((value['dPriceIndex3Competitor2']) ? value['dPriceIndex3Competitor2'] : 0), '#06A7E2'],
            ['dPriceIndex3Competitor3', parseInt((value['dPriceIndex3Competitor3']) ? value['dPriceIndex3Competitor3'] : 0), '#06A7E2'],
            ['dPriceIndex3Competitor4', parseInt((value['dPriceIndex3Competitor4']) ? value['dPriceIndex3Competitor4'] : 0), '#06A7E2'],
            ['dPriceIndex3Competitor5', parseInt((value['dPriceIndex3Competitor5']) ? value['dPriceIndex3Competitor5'] : 0), '#06A7E2'],
            this.suggestedStage ? ['dPriceIndex3Company_User', parseInt((value['dPriceIndex3Company_User']) ? value['dPriceIndex3Company_User'] : 0), '#F79420'] : ['dPriceIndex3Company_Suggested', parseInt((value['dPriceIndex3Company_Suggested']) ? value['dPriceIndex3Company_Suggested'] : 0), '#F79420'],
          ]

        });

        // console.log('data_price_base: ', this.data_price_base);
        // console.log('data_price_index_company: ', this.data_price_index_company);
        // console.log('data_price_index_maxim: ', this.data_price_index_maxim);
        // console.log('data_price_index_average: ', this.data_price_index_average);
        this.totalItems = r.body['totalItems'];
        console.log('this.list_price_comparison: ', this.list_price_comparison);
        this.fnValidStateSearch(this.items_filtered_advance_search);
      }
      if (r.status == 206) {
        this.loading = false;
        this.filter_state = false;
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.loading = false;
      this.filter_state = false;
      this.submitted = false;
      console.log('err: ', err);
      this.utilitiesService.showToast('top-right', '', this.DATA_LANG.msgNoExist.text);
    });
  }

  fnGetConfigProducts(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);
    // fnHttpGetConfigProducts(guid_user, id_version)
    this.config = null;
    this.loading = true;
    this.submitted = true;
    this.productInformationService.fnHttpGetConfigProducts(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.submitted = false;
        this.config = JSON.parse(JSON.stringify(r.body));
        console.log('this.config_products: ', this.config);
        this.bShowCents = r.body['bShowCents'];
        this.tCurrencyISO = r.body['tCurrencyISO'];
        console.log('this.bShowCents: ', this.bShowCents);
        this.quantiy_decimals = (this.bShowCents) ? 2 : 0;
        console.log('this.quantiy_decimals: ', this.quantiy_decimals);
      }
    }, err => {
      this.loading = false;
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  sortColumn(field) {

    console.log(`sortColumn(field)`);
    console.log('field: ', field);
    const self = this;

    // 'indexType': this.iIDOpc,
    self.sorted = true;
    self.field_sort = field;
    if (self.state_sort_data === 'asc') {
      self.state_sort_data = 'desc';
    } else {
      self.state_sort_data = 'asc';
    }

    if(this.filterState){
      this.fnGetAllPriceComparisonFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    }else{
      this.fnGetAllPriceComparison(this.current_payload, this.id_version, 1, this.text_searching);
    }
    this.currentPage = 1;

  }
  /** **/

  /** Funciones para filtrar y remover filtros **/
  fnFilteSearch(list_price_comparison, text_typing) {

    console.log('list_price_comparison: ', list_price_comparison);
    console.log('text_typing: ', text_typing);
    console.log('this.filter_state: ', this.filter_state);

    if (text_typing) {
      if (text_typing.length > 2) {
        console.log('Text ok');
        if (this.filter_state) {
          console.log('this.filter_state - 1: ', this.filter_state);
          this.fnGetAllPriceComparisonFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
        } else {
          console.log('this.filter_state - 2: ', this.filter_state);         
          this.fnGetAllPriceComparison(this.current_payload, this.id_version, 1, text_typing);
          this.text_searching = text_typing;
        }
      }
    } else {
      this.fnGetAllPriceComparison(this.current_payload, this.id_version, 1);
      console.log('Text not exist');
    }
  }

  showModalAdvancedSearch(obj_data_filter) {
    console.log('obj: ', obj_data_filter);
    console.log('this.items_filtered_advance_search: ', this.items_filtered_advance_search);
    obj_data_filter.id_version = this.id_version;
    obj_data_filter.items_collection_filtered = this.items_filtered_advance_search;
    obj_data_filter.State = this.state;
    obj_data_filter.suggestedStage = this.suggestedStage;
    obj_data_filter.iIDOpc = this.iIDOpc;
    this.dialogService.open(AdvancedSearchPriceComparisonComponent, { context: obj_data_filter, hasScroll: true }).onClose.subscribe((res) => {
      console.log('res: ', res);
      if (res) {
        this.loading = true;
        this.filter_state = true;
        const data_object = res;
        console.log('data_object: ', data_object);
        this.items_filtered_advance_search = JSON.parse(JSON.stringify(res));

        this.fnGetAllPriceComparisonAdvanceSearch(this.current_payload, this.id_version, data_object);
      }
    });
  }

  fnGetAllPriceComparisonFilter(current_payload, object_filter, id_version?, page?) {
    console.log('id_version: ', id_version);
    console.log('object_filter: ', object_filter);
    console.log('current_payload: ', current_payload);

    this.list_price_comparison = [];
    this.fnGetAllPriceComparisonAdvanceSearch(current_payload, this.id_version, object_filter, page);
  }

  fnGetAllPriceComparisonAdvanceSearch(current_payload, id_version, data_object, page?) {
    console.log('current_payload: ', current_payload);
    const object_data_send = data_object;

    let obj_Sort = null;

    if(this.sorted){
      obj_Sort = {
        'field' : this.field_sort,
        'dir': this.state_sort_data
      }
    }

    object_data_send['Sort'] = obj_Sort;
    object_data_send['page'] = (page) ? page : 1,

    this.submitted = true;
    this.loading = true;

    this.priceComparisonService.fnHttpGetAllPriceComparison(current_payload, id_version, object_data_send).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        this.loading = false;
        this.list_price_comparison = JSON.parse(JSON.stringify(r.body.priceComparison));
        console.log('this.list_price_comparison: ', this.list_price_comparison);
        this.totalItems = r.body['totalItems'];
        console.log('this.totalItems: ', this.totalItems);
        this.fnValidStateSearch(this.items_filtered_advance_search);
      }

      if (r.status == 206) {
        this.submitted = false;
        this.loading = false;
        this.list_price_comparison = [];
      }
    }, err => {
      this.submitted = false;
      this.loading = false;
      console.log('err: ', err);
    });
  }


  fnSetRemoveItemSearchFilteredAdvanced(index, data_collection) {

    console.log('index: ', index);
    console.log('data_collection: ', data_collection);
    data_collection.splice(index, 1);
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    console.log('data_collection: ', data_collection);

    this.object_filter = this.items_filtered_advance_search;
    console.log('this.items_filtered_advance_search: ', this.items_filtered_advance_search);
    this.fnGetAllPriceComparisonFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
  }
  fnSetRemoveBadgetProductType(){
    this.items_filtered_advance_search['tProductsType'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllPriceComparisonFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
  }

  fnSetRemoveBadgetPrice() {
    this.items_filtered_advance_search['dPrices']['dMin'] = null;
    this.items_filtered_advance_search['dPrices']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllPriceComparisonFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
  }
  fnSetRemoveBadgetdIndex() {
    this.items_filtered_advance_search['dIndex']['dMin'] = null;
    this.items_filtered_advance_search['dIndex']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllPriceComparisonFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
  }
  fnSetRemoveBadgetdIndex1User() {
    this.items_filtered_advance_search['dIndex1User']['dMin'] = null;
    this.items_filtered_advance_search['dIndex1User']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllPriceComparisonFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
  }
  fnSetRemoveBadgetdIndex1Suggested() {
    this.items_filtered_advance_search['dIndex1Suggested']['dMin'] = null;
    this.items_filtered_advance_search['dIndex1Suggested']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllPriceComparisonFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
  }
  fnSetRemoveBadgetdCurrentPrice() {
    this.items_filtered_advance_search['dCurrentPrice']['dMin'] = null;
    this.items_filtered_advance_search['dCurrentPrice']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllPriceComparisonFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
  }
  fnSetRemoveBadgetdCurrentIndex() {
    this.items_filtered_advance_search['dCurrentIndex']['dMin'] = null;
    this.items_filtered_advance_search['dCurrentIndex']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllPriceComparisonFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
  }
  fnSetRemoveBadgetdSuggestedPrice() {
    this.items_filtered_advance_search['dSuggestedPrice']['dMin'] = null;
    this.items_filtered_advance_search['dSuggestedPrice']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllPriceComparisonFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
  }
  fnSetRemoveBadgetdUserPrice() {
    this.items_filtered_advance_search['dUserPrice']['dMin'] = null;
    this.items_filtered_advance_search['dUserPrice']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllPriceComparisonFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
  }
  fnSetRemoveBadgetdSuggestedIndex() {
    this.items_filtered_advance_search['dSuggestedIndex']['dMin'] = null;
    this.items_filtered_advance_search['dSuggestedIndex']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllPriceComparisonFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
  }
  fnSetRemoveBadgetdUserIndex() {
    this.items_filtered_advance_search['dUserIndex']['dMin'] = null;
    this.items_filtered_advance_search['dUserIndex']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllPriceComparisonFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
  }

  fnClearAllFiltersSearch() {
    this.search_input = '';
    this.filterState = false;
    this.items_filtered_advance_search['products'] = [];
    this.items_filtered_advance_search['priceList'] = [];
    this.items_filtered_advance_search['category'] = [];
    this.items_filtered_advance_search['competitors'] = [];
    this.items_filtered_advance_search['tProductsType'] = null;
    this.items_filtered_advance_search['dPrices'] = null;
    this.items_filtered_advance_search['dIndex'] = null;
    this.items_filtered_advance_search['dIndex1User'] = null;
    this.items_filtered_advance_search['dIndex1Suggested'] = null;
    this.items_filtered_advance_search['dCurrentPrice'] = null;
    this.items_filtered_advance_search['dCurrentIndex'] = null;
    this.items_filtered_advance_search['dSuggestedPrice'] = null;
    this.items_filtered_advance_search['dUserPrice'] = null;
    this.items_filtered_advance_search['dSuggestedIndex'] = null;
    this.items_filtered_advance_search['dUserIndex'] = null;
    this.fnGetAllPriceComparison(this.current_payload, this.id_version);
  }

  fnValidStateSearch(collection) {
    console.log('collection: ', collection);
    if(typeof collection['category'] === "undefined"){
      console.log('indefinida')
      this.filterState = false;
      }else{
      console.log('definida')
        if (collection['products'].length < 1 &&
          collection['priceList'].length < 1 &&
          collection['category'].length < 1 &&
          collection['competitors'].length < 1 &&
          collection['dPrices']['dMin'] == null &&
          collection['dPrices']['dMax'] == null) {
          this.filterState = false;
          return false;
        } else {
          this.filterState = true;
          return true;
        }
      }
  }
  /****/

  /** Funciones para exportar e importar **/
  fnExport() {
    const index = this.Opc1 ? 1 : this.Opc2 ? 2 : this.Opc3 ? 3 : '';
    const lang = localStorage.getItem('language_app');
    window.open(
      this.url_host + '/api/PriceComparison/ExportPriceComparisons?iIDVersion=' + this.id_version + '&indexType=' + index + '&language=' + lang,
      '_blank');
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.uploadFileToActivity(this.fileToUpload);
  }

  uploadFileToActivity(fileToUpload) {
    this.id_version;
    console.log('fileToUpload: ', fileToUpload);
    const end_point_url = '/api/ForecastResult/PostImportForecastResult';
    const parameter = 'iIDVersion';
    this.submitted = true;
    this.loading = true;
    this.utilitiesService.fnHttSetUploadFile(this.current_payload, this.fileToUpload, this.id_version, end_point_url, parameter).subscribe(data => {
      console.log('data: ', data);
      // do something, if upload success

      if (data.status == 200) {
        this.submitted = false;
        this.loading = false;
        this.file_import_input = null;
        this.utilitiesService.showToast('top-right', 'success', 'Price comparision values has been import successfully!');
        this.fnGetAllPriceComparison(this.current_payload, this.id_version);
      }
      if (data.status == 206) {
        this.submitted = false;
        this.loading = false;
        const error = this.utilitiesService.fnSetErrors(null, data.body.message);
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, error => {
      this.submitted = false;
      this.loading = false;
      console.log(error);
    });
  }
  /****/

  /** Funciones para Pagindo **/
  getPage(page: number) {
    console.log('page: ', page);
    page;
    this.loading = true;
    if(this.filterState){
      this.fnGetAllPriceComparisonFilter(this.current_payload, this.items_filtered_advance_search, this.id_version, page);
    }else{
      this.fnGetAllPriceComparison(this.current_payload, this.id_version, page, this.text_searching);
    }
    this.currentPage = page;
  }
  /****/

  /*********Show modal parameters function *********/

  open(_title, _body) {
    this.dialogService.open(ModalsComponent, {
      context: {
        title: _title,
        body: _body
      },
    });
  }

  fnGetLanguage(){
    const self = this;
    self.utilitiesService.fnGetLanguage('priceComparision', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module;
      self.DATA_LANG_GENERAL = res_lang.general;
      self.fnGetAllddlBase();
    });
  }

}
