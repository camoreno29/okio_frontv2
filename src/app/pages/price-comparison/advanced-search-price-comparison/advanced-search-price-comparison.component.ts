import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options } from 'ng5-slider';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { PricesListService } from '../../../shared/api/services/prices-list.service';
import { ProductInformationService } from '../../../shared/api/services/product-information.service';
import { PriceComparisonService } from './../../../shared/api/services/price-comparison.service';
import { CompetitorsService } from '../../../shared/api/services/competitors.service';

@Component({
  selector: 'ngx-advanced-search-price-comparison',
  templateUrl: './advanced-search-price-comparison.component.html',
  styleUrls: ['./advanced-search-price-comparison.component.scss'],
})
export class AdvancedSearchPriceComparisonComponent implements OnInit {

  submitted: boolean = false;
  current_payload: any = null;
  price_comparison_results_data: any = {};
  state_loading: boolean = true;

  @Input() id_version: string;
  @Input() items_collection_filtered: any;
  @Input() State: boolean;
  @Input() suggestedStage: boolean;
  @Input() iIDOpc: string;


  list_categories_collection: any = [];
  list_prices_lists_collection: any = [];
  list_products_collection: any = [];
  list_competitors: any = [];


  data_filter_products: Object = {};
  data_filter_price_list: Object = {};
  data_filter_categories: Object = {};
  data_filter_competitors: Object = {};
  data_filter_products_type: Object = {};

  min_value_price: number = 0;
  max_value_price: number = 0;
  min_value_index: number = 0;
  max_value_index: number = 0;
  min_value_index_user: number = 0;
  max_value_index_user: number = 0;
  min_value_index_suggested: number = 0;
  max_value_index_suggested: number = 0;
  min_value_current_price: number = 0;
  max_value_current_price: number = 0;
  min_value_current_index: number = 0;
  max_value_current_index: number = 0;
  min_value_suggested_price: number = 0;
  max_value_suggested_price: number = 0;
  min_value_user_price: number = 0;
  max_value_user_price: number = 0;
  min_value_suggested_index: number = 0;
  max_value_suggested_index: number = 0;
  min_value_user_index: number = 0;
  max_value_user_index: number = 0;

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private categoriesService: CategoriesService,
    private pricesListService: PricesListService,
    private productInformationService: ProductInformationService,
    private priceComparisonService: PriceComparisonService,
    private competitorsService: CompetitorsService,
    protected ref: NbDialogRef<AdvancedSearchPriceComparisonComponent>,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('priceComparision', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();

        this.suggestedStage;

        console.log('items_collection_filtered======================>: ', this.items_collection_filtered);
        this.data_filter_products = this.items_collection_filtered['products'];
        this.data_filter_price_list = this.items_collection_filtered['priceList'];
        this.data_filter_categories = this.items_collection_filtered['category'];
        this.data_filter_competitors = this.items_collection_filtered['competitors'];
        this.data_filter_products_type = this.items_collection_filtered['tProductsType'];
        this.min_value_price = (this.items_collection_filtered['dPrices']) ? this.items_collection_filtered['dPrices']['dMin'] : null;
        this.max_value_price = (this.items_collection_filtered['dPrices']) ? this.items_collection_filtered['dPrices']['dMax'] : null;
        this.min_value_index = (this.items_collection_filtered['dIndex']) ? this.items_collection_filtered['dIndex']['dMin'] : null;
        this.max_value_index = (this.items_collection_filtered['dIndex']) ? this.items_collection_filtered['dIndex']['dMax'] : null;
        this.min_value_index_user = (this.items_collection_filtered['dIndex1User']) ? this.items_collection_filtered['dIndex1User']['dMin'] : null;
        this.max_value_index_user = (this.items_collection_filtered['dIndex1User']) ? this.items_collection_filtered['dIndex1User']['dMax'] : null;
        this.min_value_index_suggested = (this.items_collection_filtered['dIndex1Suggested']) ? this.items_collection_filtered['dIndex1Suggested']['dMin'] : null;
        this.max_value_index_suggested = (this.items_collection_filtered['dIndex1Suggested']) ? this.items_collection_filtered['dIndex1Suggested']['dMax'] : null;
        this.min_value_current_price = (this.items_collection_filtered['dCurrentPrice']) ? this.items_collection_filtered['dCurrentPrice']['dMin'] : null;
        this.max_value_current_price = (this.items_collection_filtered['dCurrentPrice']) ? this.items_collection_filtered['dCurrentPrice']['dMax'] : null;
        this.min_value_current_index = (this.items_collection_filtered['dCurrentIndex']) ? this.items_collection_filtered['dCurrentIndex']['dMin'] : null;
        this.max_value_current_index = (this.items_collection_filtered['dCurrentIndex']) ? this.items_collection_filtered['dCurrentIndex']['dMax'] : null;
        this.min_value_suggested_price = (this.items_collection_filtered['dSuggestedPrice']) ? this.items_collection_filtered['dSuggestedPrice']['dMin'] : null;
        this.max_value_suggested_price = (this.items_collection_filtered['dSuggestedPrice']) ? this.items_collection_filtered['dSuggestedPrice']['dMax'] : null;
        this.min_value_user_price = (this.items_collection_filtered['dUserPrice']) ? this.items_collection_filtered['dUserPrice']['dMin'] : null;
        this.max_value_user_price = (this.items_collection_filtered['dUserPrice']) ? this.items_collection_filtered['dUserPrice']['dMax'] : null;
        this.min_value_suggested_index = (this.items_collection_filtered['dSuggestedIndex']) ? this.items_collection_filtered['dSuggestedIndex']['dMin'] : null;
        this.max_value_suggested_index = (this.items_collection_filtered['dSuggestedIndex']) ? this.items_collection_filtered['dSuggestedIndex']['dMax'] : null;
        this.min_value_user_index = (this.items_collection_filtered['dUserIndex']) ? this.items_collection_filtered['dUserIndex']['dMin'] : null;
        this.max_value_user_index = (this.items_collection_filtered['dUserIndex']) ? this.items_collection_filtered['dUserIndex']['dMax'] : null;

        this.fnGetListProducts(this.current_payload, this.id_version);
        this.fnGetPricesLists(this.current_payload, this.id_version);
        this.fnGetListCategories(this.current_payload, this.id_version);
        this.fnGetListCompetitors(this.current_payload, this.id_version);
      }
    });
  }

  fnGetListProducts(current_payload, id_version, text_search?) {
    const self = this;
    self.productInformationService.fnHttpGetListProducts(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.list_products_collection = JSON.parse(JSON.stringify(r.body));
        self.state_loading = false;
        self.submitted = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        let error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetPricesLists(current_payload, id_version?) {
    const self = this;
    self.pricesListService.fnGetAllPricesListByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.list_prices_lists_collection = JSON.parse(JSON.stringify(r.body));
        console.log('self.list_prices_lists_collection: ', self.list_prices_lists_collection);
        self.submitted = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        let error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetListCategories(current_payload, id_version) {
    this.categoriesService.fnHttpGetDataListCategoriesByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_categories_collection = JSON.parse(JSON.stringify(r.body));
        this.submitted = false;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnGetListCompetitors(current_payload, id_version) {
    this.competitorsService.fnHttpGetDataListCompetitorsByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_competitors = JSON.parse(JSON.stringify(r.body));
        // this.rating_perceived_data.competitor = this.list_competitors[0];
        this.submitted = false;
        this.state_loading = false;
        // console.log('r.status: ', r.status);
        // this.utilitiesService.showToast('top-right', 'success', 'Category has been created successfully!');
        // this.dismiss();
        // this.router.navigate(['/pages/projects/' + id_company]);
        // this.companies = r.body;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnAdvancedSearchValue(price_comparison_results_data) {
    this.submitted = true;
    console.log('price_comparison_results_data: ', price_comparison_results_data);
    const data_object = {
      'products': (this.data_filter_products) ? this.data_filter_products : [],
      'priceList': (this.data_filter_price_list) ? this.data_filter_price_list : [],
      'category': (this.data_filter_categories) ? this.data_filter_categories : [],
      'competitors': (this.data_filter_competitors) ? this.data_filter_competitors : [],
      'tProductsType': (this.data_filter_products_type) ? this.data_filter_products_type : null,
      'dPrices': {
        'dMin': this.min_value_price,
        'dMax': this.max_value_price == 0 ? '' : this.max_value_price,
      },
      'dIndex': {
        'dMin': this.min_value_index,
        'dMax': this.max_value_index == 0 ? '' : this.max_value_index,
      },
      'dIndex1User': {
        'dMin': this.min_value_index_user,
        'dMax': this.max_value_index_user == 0 ? '' : this.max_value_index_user,
      },
      'dIndex1Suggested': {
        'dMin': this.min_value_index_suggested,
        'dMax': this.max_value_index_suggested == 0 ? '' : this.max_value_index_suggested,
      },
      'dCurrentPrice': {
        'dMin': this.min_value_current_price,
        'dMax': this.max_value_current_price == 0 ? '' : this.max_value_current_price,
      },
      'dCurrentIndex': {
        'dMin': this.min_value_current_index,
        'dMax': this.max_value_current_index == 0 ? '' : this.max_value_current_index,
      },
      'dSuggestedPrice': {
        'dMin': this.min_value_suggested_price,
        'dMax': this.max_value_suggested_price == 0 ? '' : this.max_value_suggested_price,
      },
      'dUserPrice': {
        'dMin': this.min_value_user_price,
        'dMax': this.max_value_user_price == 0 ? '' : this.max_value_user_price,
      },
      'dSuggestedIndex': {
        'dMin': this.min_value_suggested_index,
        'dMax': this.max_value_suggested_index == 0 ? '' : this.max_value_suggested_index,
      },
      'dUserIndex': {
        'dMin': this.min_value_user_index,
        'dMax': this.max_value_user_index == 0 ? '' : this.max_value_user_index,
      },
      'indexType': this.iIDOpc,
      'page': 1,
      'pageSize': 12,
      'tSearch': '',
    };
    console.log('data_object: ', data_object);
    this.dismiss(data_object);
  }

  dismiss(object_response_onclose?) {
    console.log('object_response_onclose: ', object_response_onclose);
    if (object_response_onclose) {
      object_response_onclose['products'] = (this.data_filter_products) ? this.data_filter_products : [];
      object_response_onclose['priceList'] = (this.data_filter_price_list) ? this.data_filter_price_list : [];
      object_response_onclose['category'] = (this.data_filter_categories) ? this.data_filter_categories : [];
      object_response_onclose['competitors'] = (this.data_filter_competitors) ? this.data_filter_competitors : [];
      object_response_onclose['tProductsType'] = (this.data_filter_products_type) ? this.data_filter_products_type : [];
      object_response_onclose['dPrices'] = (this.max_value_price) ? { 'dMin': this.min_value_price, 'dMax': this.max_value_price } : { 'dMin': this.min_value_price, 'dMax': null };
      object_response_onclose['dIndex'] = (this.max_value_index) ? { 'dMin': this.min_value_index, 'dMax': this.max_value_index } : { 'dMin': this.min_value_index, 'dMax': null };
      object_response_onclose['dIndex1User'] = (this.max_value_index_user) ? { 'dMin': this.min_value_index_user, 'dMax': this.max_value_index_user } : { 'dMin': this.min_value_index_user, 'dMax': null };
      object_response_onclose['dIndex1Suggested'] = (this.max_value_index_suggested) ? { 'dMin': this.min_value_index_suggested, 'dMax': this.max_value_index_suggested } : { 'dMin': this.min_value_index_suggested, 'dMax': null };
      object_response_onclose['dCurrentPrice'] = (this.max_value_current_price) ? { 'dMin': this.min_value_current_price, 'dMax': this.max_value_current_price } : { 'dMin': this.min_value_current_price, 'dMax': null };
      object_response_onclose['dCurrentIndex'] = (this.max_value_current_index) ? { 'dMin': this.min_value_current_index, 'dMax': this.max_value_current_index } : { 'dMin': this.min_value_current_index, 'dMax': null };
      object_response_onclose['dSuggestedPrice'] = (this.max_value_suggested_price) ? { 'dMin': this.min_value_suggested_price, 'dMax': this.max_value_suggested_price } : { 'dMin': this.min_value_suggested_price, 'dMax': null };
      object_response_onclose['dUserPrice'] = (this.max_value_user_price) ? { 'dMin': this.min_value_user_price, 'dMax': this.max_value_user_price } : { 'dMin': this.min_value_user_price, 'dMax': null };
      object_response_onclose['dSuggestedIndex'] = (this.max_value_suggested_index) ? { 'dMin': this.min_value_suggested_index, 'dMax': this.max_value_suggested_index } : { 'dMin': this.min_value_suggested_index, 'dMax': null };
      object_response_onclose['dUserIndex'] = (this.max_value_user_index) ? { 'dMin': this.min_value_user_index, 'dMax': this.max_value_user_index } : { 'dMin': this.min_value_user_index, 'dMax': null };
      this.ref.close(object_response_onclose);
      console.log('object_response_onclose: ', object_response_onclose);
    } else {
      this.ref.close();
    }
  }

  fnCancelFilter() {
    this.submitted = false;
    this.dismiss();
  }

}
