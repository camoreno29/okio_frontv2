import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedSearchForecastResultsComponent } from './advanced-search-forecast-results.component';

describe('AdvancedSearchForecastResultsComponent', () => {
  let component: AdvancedSearchForecastResultsComponent;
  let fixture: ComponentFixture<AdvancedSearchForecastResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvancedSearchForecastResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedSearchForecastResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
