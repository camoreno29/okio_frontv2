import { NgModule } from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';

import { NgxPaginationModule } from 'ngx-pagination';

import { WebDataRocksPivot } from '../../shared/components/webdatarocks/webdatarocks.angular4';

import { ThemeModule } from '../../@theme/theme.module';
import { HomeComponent } from './home.component';
import { CardsComponent } from '../../shared/components/cards/cards.component';
import { SettingsVersionComponent } from '../versions/settings-version/settings-version.component';
import { GoogleChartsModule } from 'angular-google-charts';

import { CategoriesComponent } from '../categories/categories.component';
import { CompetitorsComponent } from '../competitors/competitors.component';
import { PricesListComponent } from '../prices-list/prices-list.component';
import { RatingPerceivedComponent } from '../rating-perceived/rating-perceived.component';

import { ProductInformationComponent } from '../product-information/product-information.component';
import { MarketPriceComponent } from '../market-price/market-price.component';
import { RatingProductTypeComponent } from '../rating-product-type/rating-product-type.component';
import { ElasticityProductComponent } from '../elasticity-product/elasticity-product.component';
import { PriceRelationshipsComponent } from '../price-relationships/price-relationships.component';
import { PriceCalculationSummaryComponent } from '../price-calculation-summary/price-calculation-summary.component';
import { ForecastResultsComponent } from '../forecast-results/forecast-results.component';
import { PriceComparisonComponent } from '../price-comparison/price-comparison.component';
import { PivotTableComponent } from '../pivot-table/pivot-table.component';
import { BalancePointComponent } from '../balance-point/balance-point.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { TermsConditionsComponent } from '../terms-conditions/terms-conditions.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

const ENTRY_COMPONENTS = [
  HomeComponent,
  SettingsVersionComponent,
  CategoriesComponent,
  CompetitorsComponent,
  PricesListComponent,
  RatingPerceivedComponent,
  ProductInformationComponent,
  MarketPriceComponent,
  RatingProductTypeComponent,
  ElasticityProductComponent,
  PriceRelationshipsComponent,
  PriceCalculationSummaryComponent,
  ForecastResultsComponent,
  PriceComparisonComponent,
  PivotTableComponent,
  BalancePointComponent,
  DashboardComponent,
  TermsConditionsComponent,
  CardsComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    NgSelectModule,
    FormsModule,
    NgxPaginationModule,
    GoogleChartsModule.forRoot(),
    TooltipModule.forRoot(),
  ],
  declarations: [
    HomeComponent,
    SettingsVersionComponent,
    CardsComponent,
    CategoriesComponent,
    CompetitorsComponent,
    PricesListComponent,
    RatingPerceivedComponent,
    ProductInformationComponent,
    MarketPriceComponent,
    RatingProductTypeComponent,
    ElasticityProductComponent,
    PriceRelationshipsComponent,
    PriceCalculationSummaryComponent,
    ForecastResultsComponent,
    PriceComparisonComponent,
    PivotTableComponent,
    BalancePointComponent,
    DashboardComponent,
    WebDataRocksPivot,
    TermsConditionsComponent,
  ],
  exports: [
    CardsComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})

export class HomeModule { }
