import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { MenuService } from '../../shared/api/services/menu.service';
import { PagesComponent } from '../pages.component';
import { SharedService } from '../../shared/services/shared.service';
import { NbMenuItem, NbMenuService, NbSidebarService } from '@nebular/theme';
import { setTimeout } from 'timers';
import { UtilitiesService } from '../../shared/api/services/utilities.service';

import { ConfigService } from '../../shared/api/services/config.service';
declare var $: any;


@Component({
  selector: 'ngx-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  current_payload: string = null;
  id_version: any = null;
  id_company: any = null;
  home: any = null;
  content_main_option: any = [];
  data_cards: any = [];
  index_card: any = 1;
  position_selected: any = 0;
  message: any = null;
  menu_select: any = null;
  objmenu_select: any = null;
  iIDindex_card: any = null;
  state_button_next: Boolean = true;
  MENU_ITEMS: NbMenuItem[] = [];
  data_item: any = null;
  menu_card_lang: any = {};

  cards_state: any = [];

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private authService: NbAuthService,
    private menuService: MenuService,
    private pagesComponent: PagesComponent,
    private utilitiesService: UtilitiesService,
    private nbMenuService: NbMenuService,
    private sidebarService: NbSidebarService,
    private sharedService: SharedService,
    private configService: ConfigService,
  ) { }

  ngOnInit() {

    console.log('ngOnInit: ');
    $(document).ready(function () {
      const data_item_menu = $('.menu-sidebar').attr('class');
      console.log('data_item_menu: ', data_item_menu);
      $('#toggle-settings').removeClass('d-none').addClass('d-block');
    });
    const self = this;

    // self.utilitiesService.dataChange.subscribe((data) => {
    //   console.log('data cristian==================>>: ', data);
    //   // self.fnGetLanguage();
    // });
    self.nbMenuService.onItemClick().subscribe(response => {
      console.log('onItemClick: ', response);
      if (response.tag === 'left-menu') {
        this.fnSelectItem(response.item);
      }
    });

    self.nbMenuService.getSelectedItem().subscribe(response => {
      console.log('response: ', response);
      console.log('self.content_main_option: ', self.content_main_option);
      console.log('self.data_cards: ', self.data_cards);
    });

    self.route.params.subscribe(params => {
      if (params.id_version) {
        self.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          if (token.isValid()) {
            // here we receive a payload from the token and assigne it to our `user` variable
            self.current_payload = token.getValue();
            if (self.current_payload) {
              const data_id_company = sessionStorage.getItem('id_company');
              self.id_company = data_id_company;
              self.id_version = params.id_version;
              self.fnStartConfig(self.current_payload, self.id_version);
            } else {
              return false;
            }
          }
        });
      } else {
        const data_id_company = sessionStorage.getItem('id_company');
        self.router.navigate(['/pages/versions-company', data_id_company]);
      }
    });

  }

  fnStartConfig(current_payload, id_version) {
    const self = this;
    self.fnGetMenuVersions(current_payload, id_version, null, function (resp) {
      console.log('resp: ', resp);

      self.objmenu_select = JSON.parse(sessionStorage.getItem('objmenu_select'));
      self.iIDindex_card = sessionStorage.getItem('iIDindex_card') === 'null' ? null : sessionStorage.getItem('iIDindex_card');

      if (self.objmenu_select && self.iIDindex_card && self.iIDindex_card !== '-1') {

        resp.menu_items[0].home = true;
        resp.menu_items[0].selected = false;

        resp.menu_items[self.objmenu_select.iIDMenu].home = false;
        resp.menu_items[self.objmenu_select.iIDMenu].selected = true;

        self.MENU_ITEMS = resp.menu_items;
        self.index_card = self.iIDindex_card;
        self.content_main_option = self.objmenu_select['cards'];
        self.fnBuildCollectionCardsRefresh(self.objmenu_select['cards'], self.index_card);

        self.menu_select = {
          'title': self.objmenu_select.title,
          'icon': self.objmenu_select.icon,
          'selected': true,
          'link': self.objmenu_select.link,
          'home': self.objmenu_select.home,
          'iIDMenu': self.objmenu_select.iIDMenu,
          'cards': self.objmenu_select.cards,
        };

      } else {

        let CategoriesRedirect: any = '';
        CategoriesRedirect = sessionStorage.getItem('CategoriesRedirect');

        if (CategoriesRedirect === 'true') {

          resp.menu_items[0]['home'] = true;
          resp.menu_items[0]['selected'] = false;

          resp.menu_items[1]['home'] = false;
          resp.menu_items[1]['selected'] = true;

          self.MENU_ITEMS = resp.menu_items;

          self.content_main_option = resp.menu_items[1]['cards'];
          self.fnBuildCollectionCards(resp.menu_items[1]['cards'], 0);


          self.menu_select = {
            'title': resp.menu_items[1]['title'],
            'icon': resp.menu_items[1]['icon'],
            'selected': true,
            'link': resp.menu_items[1]['link'],
            'home': resp.menu_items[1]['home'],
            'iIDMenu': resp.menu_items[1]['iIDMenu'],
            'cards': resp.menu_items[1]['cards'],
          };

          sessionStorage.setItem('CategoriesRedirect', 'false');

        } else {

          self.MENU_ITEMS = resp.menu_items;
          console.log('self.data_cards: ', self.data_cards);
          self.content_main_option = resp.cards;
          self.fnBuildCollectionCards(resp.cards, 0);

          self.menu_select = {
            'title': resp.menu_items[0]['title'],
            'icon': resp.menu_items[0]['icon'],
            'selected': true,
            'link': resp.menu_items[0]['link'],
            'home': resp.menu_items[0]['home'],
            'iIDMenu': resp.menu_items[0]['iIDMenu'],
            'cards': self.data_cards,
          };
        }

      }
      console.log('self.menu_select: ', self.menu_select);
      sessionStorage.setItem('objmenu_select', JSON.stringify(self.menu_select));
      $('.menu-sidebar').removeClass('d-none').addClass('d-block compacted');
    });
  }

  fnBuildCollectionCardsRefresh(data_cards, position_selected) {
    const collection_build = [];
    this.position_selected = parseFloat(position_selected);
    data_cards.forEach((value, key) => {
      const obj = {
        'iIDCard': value.iIDCard,
        'tCardName': value.tCardName,
        'tCardIcon': value.tCardIcon,
      };
      if (value.iIDCard === this.position_selected) {
        obj['selected'] = true;
      } else {
        obj['selected'] = false;
      }
      collection_build.push(obj);
    });

    this.configService.fnHttpGetCartsNotificationsByVersion(this.current_payload, this.id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        r.body.forEach((element, key)=> {
          this.cards_state.push(element);
        });
        console.log('this.cards_state: ', this.cards_state);
        for(var i=0; i < collection_build.length; i++){
          for(var j=0; j < this.cards_state.length; j++){
            if (this.cards_state[j].iIDCard == collection_build[i].iIDCard) {
              collection_build[i].iIDStateMachine = this.cards_state[j].iIDStateMachine;
            }
          }
        }
        this.data_cards = collection_build;
      }
    });
  }

  fnGetMenuVersions(current_payload, id_version?, responseIDMenu?, observer?) {
    const self = this;
    self.pagesComponent.fnSetMenuVersion(current_payload, id_version, responseIDMenu, function (resp_menu) {
      console.log('resp_menu: ', resp_menu);
      observer(resp_menu);
    });
  }

  fnSelectItem(item) {
    const self = this;
    self.data_cards = [];
    self.index_card = item.cards.length > 0 ? item['cards'][0]['iIDCard'] : -1;
    sessionStorage.setItem('iIDindex_card', self.index_card);
    self.state_button_next = true;
    console.log('self.index_card: ', self.index_card);
    console.log('response.item["iIDMenu"]: ', item['iIDMenu']);
    self.menu_select = item;
    sessionStorage.setItem('objmenu_select', JSON.stringify(self.menu_select));
    console.log('self.menu_select: ', self.menu_select);
    if (item['title'] == 'Settings') {
      $('#pgp-content_btn_next_step').css('display', 'none');
    }
    else {
      $('#pgp-content_btn_next_step').css('display', 'block');
    }
    self.fnBuildCollectionCards(item['cards'], 0);
  }

  fnBuildCollectionCards(data_cards, position_selected) {
    const collection_build = [];
    this.position_selected = position_selected;  
  
    data_cards.forEach((value, key) => {
      const obj = {
        'iIDCard': value.iIDCard,
        'tCardName': value.tCardName,
        'tCardIcon': value.tCardIcon,
      };
      if (key === position_selected) {
        obj['selected'] = true;
      } else {
        obj['selected'] = false;
      }
      collection_build.push(obj);
    });

    this.configService.fnHttpGetCartsNotificationsByVersion(this.current_payload, this.id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        r.body.forEach((element, key)=> {
          this.cards_state.push(element);
        });
        console.log('this.cards_state: ', this.cards_state);

        for(var i=0; i < collection_build.length; i++){
          for(var j=0; j < this.cards_state.length; j++){
            if (this.cards_state[j].iIDCard == collection_build[i].iIDCard) {
              collection_build[i].iIDStateMachine = this.cards_state[j].iIDStateMachine;
            }
          }
        }
        this.data_cards = collection_build;
      }
    });

    // this.data_cards = collection_build;
  }

  receiveMessage($event) {
    const length_collection_card = this.content_main_option.length;
    console.log('length_collection_card: ', length_collection_card);
    this.index_card = $event.item_card['iIDCard'];
    console.log('this.index_card: ', this.index_card);
    sessionStorage.setItem('iIDindex_card', this.index_card);
    console.log('$event: ', $event);
    this.position_selected = $event['index_card'];
    console.log('this.position_selected: ', this.position_selected);
    this.message = $event;
    if (this.menu_select.cards.length - 1 == this.position_selected) {
      this.state_button_next = false;
    } else {
      this.state_button_next = true;
    }
  }

  fnChangeNexCard($event, index_card) {
    const index = this.MENU_ITEMS.findIndex(m => m['iIDMenu'] == this.menu_select['iIDMenu']);

    const size_list_cards = this.menu_select.cards.length;
    console.log('size_list_cards: ', size_list_cards);

    if (this.objmenu_select && this.iIDindex_card && this.iIDindex_card !== '-1') {
      this.objmenu_select.cards.forEach((value, key) => {
        if (value.iIDCard === this.position_selected) {
          this.iIDindex_card = '-1';
          this.position_selected = key;
        }
      });
    }

    if (this.position_selected < (size_list_cards - 1)) {
      this.position_selected = this.position_selected + 1;
      console.log('this.position_selected: ', this.position_selected);
      this.index_card = (parseFloat(this.index_card) + 1).toString();
      console.log('this.index_card: ', this.index_card);
      sessionStorage.setItem('iIDindex_card', this.index_card);

      switch (this.menu_select.iIDMenu) {
        case (1):
          if (size_list_cards == this.position_selected) {
            this.index_card = 1;
            this.position_selected = 0;
          }
          break;
        case (2):
          if (size_list_cards == this.position_selected) {
            this.index_card = 5;
            this.position_selected = 0;
          }
          break;
        case (3):
          if (size_list_cards == this.position_selected) {
            this.index_card = 10;
            this.position_selected = 0;
          }
          break;
        case (4):
          if (size_list_cards == this.position_selected) {
            this.index_card = 15;
            this.position_selected = 0;
          }
          break;
      }
      this.fnBuildCollectionCards(this.data_cards, this.position_selected);
    } else {
      const collection_build = [];

      if (this.MENU_ITEMS[this.MENU_ITEMS.length - 2]['iIDMenu'] != this.menu_select['iIDMenu']) {
        this.menu_select = this.MENU_ITEMS[index + 1];
      } else {
        this.menu_select = this.MENU_ITEMS[0];
      }

      this.MENU_ITEMS.forEach((value, key) => {
        const obj = {
          'cards': value['cards'],
          'home': value.home,
          'iIDMenu': value['iIDMenu'],
          'icon': value.icon,
          'link': value.link,
          'selected': value.selected,
          'title': value.title,
        };

        if (this.MENU_ITEMS[this.MENU_ITEMS.length - 1]['iIDMenu'] != this.menu_select['iIDMenu']) {
          if (value['iIDMenu'] === this.menu_select['iIDMenu']) {
            obj['selected'] = true;
          } else {
            obj['selected'] = false;
          }
        } else {
          obj['selected'] = false;
        }

        collection_build.push(obj);
      });

      this.MENU_ITEMS = collection_build;

      if (this.MENU_ITEMS[this.MENU_ITEMS.length - 1]['iIDMenu'] != this.menu_select['iIDMenu']) {
        this.fnSelectItem(this.menu_select);
      } else {
        this.MENU_ITEMS[0].selected = true;
        this.fnSelectItem(this.MENU_ITEMS[0]);
      }

      this.sharedService.change(this.MENU_ITEMS);
    }
    let id_version = sessionStorage.getItem('id_version');
    this.utilitiesService.setData({ version: id_version });
  }

  fnChangeDashboardMenu() {
    this.menu_select = this.MENU_ITEMS[3];
    sessionStorage.setItem('objmenu_select', JSON.stringify(this.menu_select));
    const collection_build = [];

    this.MENU_ITEMS.forEach((value, key) => {
      const obj = {
        'cards': value['cards'],
        'home': value.home,
        'iIDMenu': value['iIDMenu'],
        'icon': value.icon,
        'link': value.link,
        'selected': value.selected,
        'title': value.title,
      };

      if (this.MENU_ITEMS[this.MENU_ITEMS.length - 1]['iIDMenu'] != this.menu_select['iIDMenu']) {
        if (value['iIDMenu'] === this.menu_select['iIDMenu']) {
          obj['selected'] = true;
        } else {
          obj['selected'] = false;
        }
      } else {
        obj['selected'] = false;
      }

      collection_build.push(obj);
    });

    this.MENU_ITEMS = collection_build;

    if (this.MENU_ITEMS[this.MENU_ITEMS.length - 1]['iIDMenu'] != this.menu_select['iIDMenu']) {
      this.fnSelectItemForecast(this.menu_select);
    }
    this.sharedService.change(this.MENU_ITEMS);
  }

  fnSelectItemCategories(item) {
    const self = this;
    self.data_cards = [];
    self.index_card = item.cards.length > 0 ? item['cards'][0]['iIDCard'] : -1;
    console.log('this.index_card: ', this.index_card);
    sessionStorage.setItem('iIDindex_card', this.index_card);
    self.state_button_next = true;
    console.log('self.index_card: ', self.index_card);
    console.log('response.item["iIDMenu"]: ', item['iIDMenu']);
    self.menu_select = item;
    console.log('self.menu_select: ', self.menu_select);
    self.fnBuildCollectionCards(item['cards'], 1);
  }

  fnSelectItemForecast(item) {
    const self = this;
    self.data_cards = [];
    self.index_card = item.cards.length > 0 ? item['cards'][1]['iIDCard'] : -1;
    console.log('this.index_card: ', this.index_card);
    sessionStorage.setItem('iIDindex_card', this.index_card);
    self.state_button_next = true;
    console.log('self.index_card: ', self.index_card);
    console.log('response.item["iIDMenu"]: ', item['iIDMenu']);
    self.menu_select = item;
    console.log('self.menu_select: ', self.menu_select);
    self.fnBuildCollectionCards(item['cards'], 1);
  }

}
