import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { BaseComponent } from './base/base.component';
import { MembersComponent } from './members/members.component';
import { ProjectsComponent } from './projects/projects.component';
import { VersionsCompanyComponent } from './versions/versions-company/versions-company.component';
import { VersionsProjectComponent } from './versions/versions-project/versions-project.component';
// import { AddCompanyComponent } from './company/add-company/add-company.component';
import { MyAccountComponent } from './user/my-account/my-account.component';
import { GroupsComponent } from './groups/groups.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { HomeComponent } from './home/home.component';
import { SettingsVersionComponent } from './versions/settings-version/settings-version.component';
// import { SettingsCompanyComponent } from './company/settings-company/settings-company.component';
// import { ListCompaniesComponent } from './company/list-companies/list-companies.component';
import { SettingsProjectComponent } from './projects/settings-project/settings-project.component';
import { CategoriesComponent } from './categories/categories.component';
import { CompetitorsComponent } from './competitors/competitors.component';
import { PricesListComponent } from './prices-list/prices-list.component';
import { RatingPerceivedComponent } from './rating-perceived/rating-perceived.component';
import { ProductInformationComponent } from './product-information/product-information.component';
import { MarketPriceComponent } from './market-price/market-price.component';
import { RatingProductTypeComponent } from './rating-product-type/rating-product-type.component';
import { ElasticityProductComponent } from './elasticity-product/elasticity-product.component';
import { PriceRelationshipsComponent } from './price-relationships/price-relationships.component';
import { ForecastResultsComponent } from './forecast-results/forecast-results.component';
import { PriceComparisonComponent } from './price-comparison/price-comparison.component';
import { PivotTableComponent } from './pivot-table/pivot-table.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { CPanelComponent } from './c-panel/c-panel.component';
import { NotificationsComponent } from './notifications/notifications.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'members/:id_company',
    component: MembersComponent,
  }, {
    path: 'projects',
    component: ProjectsComponent,
  }, {
    path: 'versions-company/:id_company',
    component: VersionsCompanyComponent,
  }, {
    path: 'versions-project/:id_project',
    component: VersionsProjectComponent,
  }, {
    path: 'groups/:id_project',
    component: GroupsComponent,
  }, {
    path: 'home/:id_version',
    component: HomeComponent,
  }, {
    path: 'settings-project/:id_project',
    component: SettingsProjectComponent,
  }, {
    path: 'settings-version/:id_version',
    component: SettingsVersionComponent,
  }, {
    path: 'categories/:id_version',
    component: CategoriesComponent,
  }, {
    path: 'competitors/:id_version',
    component: CompetitorsComponent,
  }, {
    path: 'prices-list/:id_version',
    component: PricesListComponent,
  }, {
    path: 'rating-value-perceived/:id_version',
    component: RatingPerceivedComponent,
  }, {
    path: 'product-information/:id_version',
    component: ProductInformationComponent,
  }, {
    path: 'market-price/:id_version',
    component: MarketPriceComponent,
  }, {
    path: 'rating-product-type/:id_version',
    component: RatingProductTypeComponent,
  }, {
    path: 'elasticity-product/:id_version',
    component: ElasticityProductComponent,
  }, {
    path: 'price-relationships/:id_version',
    component: PriceRelationshipsComponent,
  }, {
    path: 'forecast-results/:id_version',
    component: ForecastResultsComponent,
  }, {
    path: 'price-comparison/:id_version',
    component: PriceComparisonComponent,
  }, {
    path: 'pivot-table/:id_version',
    component: PivotTableComponent,
  }, {
    path: 'dashboard/:id_version',
    component: DashboardComponent,
  }, {
    path: 'terms-conditions',
    component: TermsConditionsComponent,
  }, {
    path: 'c-panel',
    component: CPanelComponent,
  }, {
    path: 'base',
    component: BaseComponent,
  }, {
    path: 'notifications',
    component: NotificationsComponent,
  }, {
    path: 'my-account',
    component: MyAccountComponent,
  }, {
    path: 'modal-overlays',
    loadChildren: './modal-overlays/modal-overlays.module#ModalOverlaysModule',
  }, {
    path: '**',
    component: NotFoundComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
