import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options, LabelType } from 'ng5-slider';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { RatingPerceivedService } from '../../../shared/api/services/rating-perceived.service';
import { RatingProductTypeService } from '../../../shared/api/services/rating-product-type.service';
import { ConfigService } from '../../../shared/api/services/config.service';

import { ForecastResultsService } from '../../../shared/api/services/forecast-results.service';

declare var $: any;

@Component({
  selector: 'ngx-edit-forecast-results',
  templateUrl: './edit-forecast-results.component.html',
  styleUrls: ['./edit-forecast-results.component.scss']
})

export class EditForecastResultsComponent implements OnInit {

  submitted: boolean = false;
  state_btn_next: boolean = false;
  state_btn_preview: boolean = true;
  current_payload: any = null;

  id_version: string;
  id_Product: string;

  // list_rating_produc_type_collection: any = [];
  // list_competitors: any = [];
  obj_form: any = {};

  select_competitor: any = {};
  input_product: any = '';
  input_prices: any = '';

  index_ant: number = null;

  competitor_collection: any = [];
  
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  @Input() data: any;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private forecastResultsService: ForecastResultsService,
    private configService: ConfigService,
    protected ref: NbDialogRef<EditForecastResultsComponent>,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('forecastResults', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();

        this.id_version = this.data['id_version'];
        this.id_Product = this.data['iIDProduct'];
        console.log('this.current_payload: ', this.current_payload);
        console.log('id_version======================>: ', this.id_version);
        console.log('data======================>: ', this.data);

        this.obj_form = {
          "iIDForecastResult": this.data['iIDForecastResult'],
          "iIDVersion": parseInt(this.id_version, 10),
          "iIDProduct": this.data['iIDProduct'],
          "iOrder": this.data['iIDProduct'],
          "tProductCode": this.data['tProductCode'],
          "tProductName": this.data['tProductName'],
          "tPriceListName": this.data['tPriceListName'],
          "tProductType": this.data['tProductType'],
          "category": {
            "iIDCategory": this.data['iIDCategory'],
            "tCategoryName": this.data['tCategoryName'],
          },
          "dCurrentPriceWithVAT": this.data['dCurrentPriceWithVAT'],
          "dSuggestedPriceWithVAT": this.data['dSuggestedPriceWithVAT'],
          "dPriceWithVATVar": this.data['dPriceWithVATVar'],
          "dUnitsCurrentYear": this.data['dUnitsCurrentYear'],
          "dUnitsYearSuggested": this.data['dUnitsYearSuggested'],
          "dUnitsYearVar": this.data['dUnitsYearVar'],
          "dIncomeYearWithVATCurrent": this.data['dIncomeYearWithVATCurrent'],
          "dIncomeYearWithVATSuggested": this.data['dIncomeYearWithVATSuggested'],
          "dIncomeYearVar": this.data['dIncomeYearVar'],
          "dGrossContributionCurrentYear": this.data['dGrossContributionCurrentYear'],
          "dGrossContributionYearSuggested": this.data['dGrossContributionYearSuggested'],
          "dGrossContributionYearVar": this.data['dGrossContributionYearVar'],
          "dCurrentPriceWithVAT_User": this.data['dCurrentPriceWithVAT_User'],
          "dPriceWithVAT_User": this.data['dPriceWithVAT_User'],
          "dPriceWithVATVar_User": this.data['dPriceWithVATVar_User'],
          "dUnitsCurrentYear_User": this.data['dUnitsCurrentYear_User'],
          "dUnitsYearSuggested_User": this.data['dUnitsYearSuggested_User'],
          "dUnitsYearVar_User": this.data['dUnitsYearVar_User'],
          "dIncomeYearWithVATCurrent_User": this.data['dIncomeYearWithVATCurrent_User'],
          "dIncomeYearWithVATSuggested_User": this.data['dIncomeYearWithVATSuggested_User'],
          "dIncomeYearVar_User": this.data['dIncomeYearVar_User'],
          "dGrossContributionCurrentYear_User": this.data['dGrossContributionCurrentYear_User'],
          "dGrossContributionYearSuggested_User": this.data['dGrossContributionYearSuggested_User'],
          "dGrossContributionYearVar_User": this.data['dGrossContributionYearVar_User']
        };

        // this.fnGetRatingProducType(this.current_payload, this.id_version);
      }
    });
  }

  /** Funcion para cargar la DDl de competidores**/
  // fnGetRatingProducType(current_payload, id_version?) {
  //   const self = this;
  //   self.configService.fnHttpGetListProductTypesVersion(current_payload, id_version).subscribe(r => {
  //     console.log('r: ', r);
  //     if (r.status == 200) {
  //       self.list_rating_produc_type_collection = JSON.parse(JSON.stringify(r.body));
  //       console.log('self.list_rating_produc_type_collection: ', self.list_rating_produc_type_collection);
  //       self.submitted = false;
  //     }
  //     if (r.status == 206) {
  //       self.submitted = false;
  //       const error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
  //       console.log('error: ', error);
  //       self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
  //     }
  //   }, err => {
  //     console.log('err: ', err);
  //   });
  // }
  /** **/

  /** Funcion que envia los datos al servicio de actalizar **/
  fnUpdateData(obj) {
    this.submitted = true;

    this.forecastResultsService.fnHttpSetEditDataForecastResult(this.current_payload, this.obj_form).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessUpdate.text);
        this.dismiss();
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }
  /** **/


  /** Funciones para cancelar y cerrar **/
  fnCancelEdit() {
    this.submitted = false;
    this.dismiss();
  }

  dismiss() {
    this.ref.close();
  }
  /** **/

}
