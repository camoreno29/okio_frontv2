import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditForecastResultsComponent } from './edit-forecast-results.component';

describe('EditForecastResultsComponent', () => {
  let component: EditForecastResultsComponent;
  let fixture: ComponentFixture<EditForecastResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditForecastResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditForecastResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
