import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as $ from 'jquery'; // import Jquery here

import { StateService } from '../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';
import { environment } from '../../../environments/environment';

import { UtilitiesService } from '../../shared/api/services/utilities.service';

import { ForecastResultsService } from '../../shared/api/services/forecast-results.service';
import { CategoriesService } from '../../shared/api/services/categories.service';
import { ProductInformationService } from '../../shared/api/services/product-information.service';

import { AdvancedSearchForecastResultsComponent } from './advanced-search-forecast-results/advanced-search-forecast-results.component';
import { EditForecastResultsComponent } from './edit-forecast-results/edit-forecast-results.component';
import { constants } from 'os';

import { ModalsComponent } from '../../shared/components/modals/modals.component';

@Component({
  selector: 'ngx-forecast-results',
  templateUrl: './forecast-results.component.html',
  styleUrls: ['./forecast-results.component.scss'],
})

export class ForecastResultsComponent implements OnInit {

  userStage: boolean = false;

  url_host: any = environment.apiUrl;
  list_forecast_results: any = null;
  layoud_component_display: String = 'grid';
  numItemsPage: any = null;
  currentPage: any = null;
  totalItems: any = null;
  chartForecastResult: any = [];
  objLimits: any = [];

  loading: boolean = true;
  loadingChart: boolean = true;
  filter_state: boolean = false;
  items_filtered_advance_search: any = [];
  obj: any = {};
  data_object: any = {};
  current_payload: string = null;
  id_version: any = null;
  submitted: any = false;
  fileToUpload: File = null;
  file_import_input: any = null;
  object_filter: any = {};
  data: any = {};
  quantiy_decimals: any = 0;
  bShowCents: any = null;
  config: any = null;
  bHideTrendOfVolumeInstalledCapacity: Boolean = false;
  search_input: any = '';
  tCurrencyISO: any = '';

  showChart: boolean = false;
  type: string = 'ScatterChart';
  dataChart = [];
  columnNames = ['Current', 'Suggested', { 'type': 'string', 'role': 'tooltip', 'p': { 'html': true } }, { 'role': 'style' }];
  options = {};

  sorted: boolean = false;
  field_sort: any = null;
  state_sort_data: any = 'asc';

  filterState: boolean = false;

  state_view: any = null;
  state_chart: boolean = true;

  text_searching: any = '';
  
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;
  DATA_LANG_GENERAL: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    private forecastResultsService: ForecastResultsService,
    private categoriesService: CategoriesService,
    private productInformationService: ProductInformationService,
  ) { }

  ngOnInit() {
    const self = this;
    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => { 
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    // });
    
    this.currentPage = 1;
    this.numItemsPage = 12;

    this.route.params.subscribe(params => {
      console.log('params: ', params);
      if (params.id_version) {
        this.id_version = params.id_version;
        this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          console.log('token: ', token);
          if (token.isValid()) {
            // here we receive a payload from the token and assigne it to our `user` variable
            this.current_payload = token.getValue();
            console.log('this.current_payload: ', this.current_payload);
            if (this.current_payload) {
              this.object_filter = [];
              this.fnGetAllForecastResults(this.current_payload, this.id_version, 1);
              this.fnGetConfigProducts(this.current_payload, this.id_version);
              this.fnGetConfigStage(this.current_payload, this.id_version);
              return false;
            }
          }
        });
      }
    });
  }

  fnSwitchStage(stage) {
    this.fnGetDataCharts(this.current_payload, this.id_version, {});
    this.fnUpdateConfigStage(this.current_payload, this.id_version, !this.userStage);
  }

  /** Funcion para cambiar de grid a cards **/
  fnSwitchViewData(state_view) {
    this.state_view = state_view;
    console.log('state_view: ', state_view);
    this.layoud_component_display = state_view;

    if (this.data_object) {
      this.fnGetDataCharts(this.current_payload, this.id_version, this.data_object);

    } else {
      this.fnGetDataCharts(this.current_payload, this.id_version, {});
    }
  }
  /** **/

  /** Funciones para cargar los datos y Ordenar por columna **/
  fnGetAllForecastResults(current_payload, id_version?, page?, text_search?) {

    let obj_Sort = null;

    if (this.sorted) {
      obj_Sort = {
        'field': this.field_sort,
        'dir': this.state_sort_data,
      };
    }

    const object_data_send = {
      'products': [],
      'priceList': [],
      'category': [],
      'tProductsType': null,
      'dCurrentPriceWithVAT': {
        'dMin': null,
        'dMax': null,
      },
      'dPriceWithVAT_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dPriceWithVAT_User': {
        'dMin': null,
        'dMax': null,
      },
      'dPriceWithVATVar_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dPriceWithVATVar_User': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsCurrentYear': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsYear_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsYear_User': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsYearVar_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsYearVar_User': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearWithVATCurrent': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearWithVAT_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearWithVAT_User': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearVar_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearVar_User': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionCurrentYear': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionYear_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionYear_User': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionYearVar_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionYearVar_User': {
        'dMin': null,
        'dMax': null,
      },
      'dCurrentGrossMargin': {
        'dMin': null,
        'dMax': null,
      },
      'dSuggestedGrossMargin': {
        'dMin': null,
        'dMax': null,
      },
      'dUserGrossMargin': {
        'dMin': null,
        'dMax': null,
      },
      'Sort': obj_Sort,
      'page': (page) ? page : 1,
      'pageSize': 12,
      'tSearch': text_search,
    };

    this.loading = true;
    this.submitted = true;
    this.list_forecast_results = [];

    this.forecastResultsService.fnHttpGetAllForecastResults(current_payload, id_version, object_data_send).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.filter_state = false;
        this.submitted = false;

        // if (!this.showChart) {
        //   this.fnGetDataCharts(current_payload, id_version);
        // }

        this.list_forecast_results = JSON.parse(JSON.stringify(r.body.forecastResult));
        this.totalItems = r.body['totalItems'];
        console.log('this.list_forecast_results: ', this.list_forecast_results);
        this.fnValidStateSearch(this.items_filtered_advance_search);
      }
      if (r.status == 206) {
        this.loading = false;
        this.filter_state = false;
        this.submitted = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.loading = false;
      this.filter_state = false;
      this.submitted = false;
      console.log('err: ', err);
      this.utilitiesService.showToast('top-right', '', this.DATA_LANG.msgNoExist.text);
    });
  }

  fnGetConfigProducts(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);
    this.config = null;
    this.loading = true;
    this.submitted = true;
    this.productInformationService.fnHttpGetConfigProducts(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.submitted = false;
        this.config = JSON.parse(JSON.stringify(r.body));
        console.log('this.config_products: ', this.config);
        this.bHideTrendOfVolumeInstalledCapacity = r.body['bHideTrendOfVolumeInstalledCapacity'];
        this.tCurrencyISO = r.body['tCurrencyISO'];
        console.log('this.tCurrencyISO: ', this.tCurrencyISO);
        console.log('this.bHideTrendOfVolumeInstalledCapacity: ', this.bHideTrendOfVolumeInstalledCapacity);
        this.bShowCents = r.body['bShowCents'];
        console.log('this.bShowCents: ', this.bShowCents);
        this.quantiy_decimals = (this.bShowCents) ? 2 : 0;
        console.log('this.quantiy_decimals: ', this.quantiy_decimals);
      }
    }, err => {
      this.loading = false;
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnGetConfigStage(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);
    this.config = null;
    this.loading = true;
    this.submitted = true;
    this.productInformationService.fnHttpGetSuggestedPrice(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.submitted = false;
        this.userStage = JSON.parse(JSON.stringify(!r.body));
        console.log('userStage: ', this.userStage);
      }
    }, err => {
      this.loading = false;
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnUpdateConfigStage(current_payload, id_version, Stage) {
    this.config = null;
    this.loading = true;
    this.submitted = true;
    this.productInformationService.fnHttpUpdateSuggestedPrice(current_payload, id_version, Stage).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.submitted = false;
      }
    }, err => {
      this.loading = false;
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnGetDataCharts(current_payload, id_version?, object_filter?, page?, text_search?) {
    console.log('object_filter: ', object_filter);
    const self = this;
    const object_data_send = {
      'products': (object_filter['products']) ? object_filter['products'] : [],
      'priceList': (object_filter['priceList']) ? object_filter['priceList'] : [],
      'category': (object_filter['category']) ? object_filter['category'] : [],
      'dCurrentPriceWithVAT': {
        'dMin': (object_filter['dCurrentPriceWithVAT']) ? object_filter['dCurrentPriceWithVAT']['dMin'] : null,
        'dMax': (object_filter['dCurrentPriceWithVAT']) ? object_filter['dCurrentPriceWithVAT']['dMax'] : null,
      },
      'dPriceWithVAT_Suggested': {
        'dMin': (object_filter['dPriceWithVAT_Suggested']) ? object_filter['dPriceWithVAT_Suggested']['dMin'] : null,
        'dMax': (object_filter['dPriceWithVAT_Suggested']) ? object_filter['dPriceWithVAT_Suggested']['dMax'] : null,
      },
      'dPriceWithVAT_User': {
        'dMin': null,
        'dMax': null,
      },
      'dPriceWithVATVar_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dPriceWithVATVar_User': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsCurrentYear': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsYear_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsYear_User': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsYearVar_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dUnitsYearVar_User': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearWithVATCurrent': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearWithVAT_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearWithVAT_User': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearVar_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dIncomeYearVar_User': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionCurrentYear': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionYear_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionYear_User': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionYearVar_Suggested': {
        'dMin': null,
        'dMax': null,
      },
      'dGrossContributionYearVar_User': {
        'dMin': null,
        'dMax': null,
      },
      'dCurrentGrossMargin': {
        'dMin': null,
        'dMax': null,
      },
      'dSuggestedGrossMargin': {
        'dMin': null,
        'dMax': null,
      },
      'dUserGrossMargin': {
        'dMin': null,
        'dMax': null,
      },
      'page': 1,
      'pageSize': 12,
      'tSearch': text_search,
    };
    self.loadingChart = true;
    self.submitted = true;

    self.forecastResultsService.fnHttpGetGraphFiltersForecastResult(current_payload, id_version, object_data_send).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.filter_state = false;
        self.dataChart = [];

        self.chartForecastResult = JSON.parse(JSON.stringify(r.body.resultforecastResult.forecastResult));
        self.objLimits = JSON.parse(JSON.stringify(r.body.objLimits));
        self.chartForecastResult.forEach((value, key) => {
          const obj_ = [value.dSuggestedPrice, value.dCurrentPrice, self.createCustomHTMLContent(value), '#156282'];
          self.dataChart.push(obj_);
        });
        self.fnGetConfigCharts();

        console.log('self.chartForecastResult: ', self.chartForecastResult);
        console.log('self.objLimits: ', self.objLimits);
        console.log('self.dataChart>>>>>>>>>>>>>>: ', self.dataChart);

        self.loadingChart = false;
        self.loading = false;
        self.submitted = false;
        self.showChart = true;
      }
      if (r.status == 206) {
        self.submitted = false;
        self.loadingChart = false;
        self.showChart = false;
        const error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      self.submitted = false;
      self.loadingChart = false;
      self.showChart = false;
      console.log('err: ', err);
      self.utilitiesService.showToast('top-right', '', this.DATA_LANG.msgGraphError.text);
    });
  }

  fnGetConfigCharts() {
    const xminValue: number = this.userStage ? this.objLimits.dX_User.dMin : this.objLimits.dX_Suggest.dMin;
    const xmaxValue: number = this.userStage ? this.objLimits.dX_User.dMax : this.objLimits.dX_Suggest.dMax;
    const yminValue: number = this.userStage ? this.objLimits.dY_User.dMin : this.objLimits.dY_Suggest.dMin;
    const ymaxValue: number = this.userStage ? this.objLimits.dY_User.dMax : this.objLimits.dY_Suggest.dMax;

    this.options = {
      title: '',
      hAxis: { title: this.userStage ? 'PVP User' : this.DATA_LANG.forResCharxAxisSuggested.text, minValue: yminValue, maxValue: ymaxValue },
      vAxis: { title: this.DATA_LANG.forResCharyAxisCurrent.text, minValue: xminValue, maxValue: xmaxValue },
      legend: 'none',
      pointSize: 10,
      // Draw a trendline for data series 0.
      trendlines: {
        0: {
          color: '#F79420',
          lineWidth: 5,
          tooltip: false,
        },
      },
      fontName: 'Roboto',
      width: $(window).width(),
      height: $(window).height() * 0.60,
      // tooltip: { isHtml: true },
      tooltip: { isHtml: true },
      chartArea: { left: 70, top: 20, bottom: 70, width: '80%' },
    };
    console.log('this.options: ', this.options);
  }

  createCustomHTMLContent(objLimits: any) {
    const nameStage = this.userStage ? 'User:' : this.DATA_LANG.forResCharSuggested.text;
    return '<div class="p-2">' +
      '<table class="table table-bordered pgp-table">' +
      '<tr><td><b>' + objLimits.tProductCode + ' </b></td></tr>' +
      '<tr><td><b><small>' + this.DATA_LANG.forResCharCurrent.text + ' </small>' + parseFloat(objLimits.dCurrentPrice).toFixed(this.quantiy_decimals) + '</b></td></tr>' +
      '<tr><td><b><small> ' + nameStage + ' </small>' + parseFloat(objLimits.dSuggestedPrice).toFixed(this.quantiy_decimals) + '</b></td></tr>' +
      '</table>' + '</div>';
  }

  sortColumn(field) {
    console.log(`sortColumn(field)`);
    console.log('field: ', field);
    const self = this;

    self.sorted = true;
    self.field_sort = field;

    if (self.state_sort_data === 'asc') {
      self.state_sort_data = 'desc';
    } else {
      self.state_sort_data = 'asc';
    }
    if (this.filterState) {
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    } else {
      this.fnGetAllForecastResults(this.current_payload, this.id_version, 1, this.text_searching);
    }
    this.currentPage = 1;
  }
  /** **/

  /** Funciones para filtrar y remover filtros **/
  fnFilteSearch(list_forecast_results, text_typing) {
    console.log('list_forecast_results: ', list_forecast_results);
    console.log('text_typing: ', text_typing);
    console.log('this.filter_state: ', this.filter_state);
    if (text_typing) {
      if (text_typing.length > 2) {
        console.log('Text ok');
        if (this.filter_state) {
          console.log('this.filter_state - 1: ', this.filter_state);
          this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
        } else {
          this.text_searching = text_typing;
          console.log('this.filter_state - 2: ', this.filter_state);
          this.fnGetAllForecastResults(this.current_payload, this.id_version, 1, text_typing);
          this.fnGetDataCharts(this.current_payload, this.id_version, {}, 1, text_typing);
        }
      }
    } else {
      this.fnGetAllForecastResults(this.current_payload, this.id_version, 1);
      this.fnGetDataCharts(this.current_payload, this.id_version, {}, 1);
      console.log('Text not exist');
    }

    // if (this.list_forecast_results.length > 0) {
    //   this.state_chart = true;

    // } else {

    //   this.state_chart = false;
    // }
  }

  showModalAdvancedSearch(obj_data_filter) {
    console.log('obj: ', obj_data_filter);
    console.log('this.items_filtered_advance_search: ', this.items_filtered_advance_search);
    obj_data_filter.id_version = this.id_version;
    obj_data_filter.items_collection_filtered = this.items_filtered_advance_search;
    obj_data_filter.userStage = this.userStage;
    obj_data_filter.state_view = this.state_view;
    this.dialogService.open(AdvancedSearchForecastResultsComponent, { context: obj_data_filter, hasScroll: true }).onClose.subscribe((res) => {
      console.log('res: ', res);
      if (res) {
        this.loading = true;
        this.filter_state = true;
        this.data_object = res;
        console.log('this.data_object: ', this.data_object);
        this.items_filtered_advance_search = JSON.parse(JSON.stringify(res));
        this.fnGetAllForecastResultsAdvanceSearch(this.current_payload, this.id_version, this.data_object);
        this.fnGetDataCharts(this.current_payload, this.id_version, this.data_object);
      }
    });
  }

  fnGetAllForecastResultsFilter(current_payload, object_filter, id_version?, page?) {
    console.log('id_version: ', id_version);
    console.log('object_filter: ', object_filter);
    console.log('current_payload: ', current_payload);

    this.list_forecast_results = [];
    this.fnGetAllForecastResultsAdvanceSearch(current_payload, this.id_version, object_filter, page);
  }

  fnGetAllForecastResultsAdvanceSearch(current_payload, id_version, data_object, page?) {
    console.log('current_payload: ', current_payload);

    const object_data_send = data_object;

    let obj_Sort = null;

    if (this.sorted) {
      obj_Sort = {
        'field': this.field_sort,
        'dir': this.state_sort_data,
      };
    }

    object_data_send['Sort'] = obj_Sort;
    object_data_send['page'] = (page) ? page : 1;

    this.submitted = true;
    this.loading = true;

    this.forecastResultsService.fnHttpGetAllForecastResults(current_payload, id_version, object_data_send).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.filter_state = false;
        this.submitted = false;
        this.list_forecast_results = JSON.parse(JSON.stringify(r.body.forecastResult));
        this.totalItems = r.body['totalItems'];
        console.log('this.list_forecast_results: ', this.list_forecast_results);
        this.fnValidStateSearch(this.items_filtered_advance_search);
      }
      if (r.status == 206) {
        this.loading = false;
        this.filter_state = false;
        this.submitted = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }

    }, err => {
      this.submitted = false;
      this.loading = false;
      console.log('err: ', err);
    });
  }

  fnSetRemoveItemSearchFilteredAdvanced(index, data_collection) {
    console.log('index: ', index);
    console.log('data_collection: ', data_collection);
    data_collection.splice(index, 1);
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    console.log('data_collection: ', data_collection);

    this.object_filter = this.items_filtered_advance_search;
    console.log('this.items_filtered_advance_search: ', this.items_filtered_advance_search);
    this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    this.fnGetDataCharts(this.current_payload, this.id_version, {});
  }

  fnSetRemoveProductsType() {
    this.items_filtered_advance_search['tProductsType'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    this.fnGetDataCharts(this.current_payload, this.id_version, {});
  }

  fnSetRemoveBadgetCurrentPriceWithVAT() {
    this.items_filtered_advance_search['dCurrentPriceWithVAT']['dMin'] = null;
    this.items_filtered_advance_search['dCurrentPriceWithVAT']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    this.fnGetDataCharts(this.current_payload, this.id_version, {});
  }

  fnSetRemoveBadgetPriceWithVAT() {
    if (!this.userStage) {
      this.items_filtered_advance_search['dPriceWithVAT_Suggested']['dMin'] = null;
      this.items_filtered_advance_search['dPriceWithVAT_Suggested']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    } else {
      this.items_filtered_advance_search['dPriceWithVAT_User']['dMin'] = null;
      this.items_filtered_advance_search['dPriceWithVAT_User']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    }
    this.fnGetDataCharts(this.current_payload, this.id_version, {});
  }

  fnSetRemoveBadgetPriceWithVATVar() {
    if (!this.userStage) {
      this.items_filtered_advance_search['dPriceWithVATVar_Suggested']['dMin'] = null;
      this.items_filtered_advance_search['dPriceWithVATVar_Suggested']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    } else {
      this.items_filtered_advance_search['dPriceWithVATVar_User']['dMin'] = null;
      this.items_filtered_advance_search['dPriceWithVATVar_User']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    }
    this.fnGetDataCharts(this.current_payload, this.id_version, {});
  }

  fnSetRemoveBadgetUnitsCurrentYear() {
    this.items_filtered_advance_search['dUnitsCurrentYear']['dMin'] = null;
    this.items_filtered_advance_search['dUnitsCurrentYear']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    this.fnGetDataCharts(this.current_payload, this.id_version, {});
  }

  fnSetRemoveBadgetUnitsYear() {
    if (!this.userStage) {
      this.items_filtered_advance_search['dUnitsYear_Suggested']['dMin'] = null;
      this.items_filtered_advance_search['dUnitsYear_Suggested']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    } else {
      this.items_filtered_advance_search['dUnitsYear_User']['dMin'] = null;
      this.items_filtered_advance_search['dUnitsYear_User']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    }
    this.fnGetDataCharts(this.current_payload, this.id_version, {});
  }

  fnSetRemoveBadgetUnitsYearVar() {
    if (!this.userStage) {
      this.items_filtered_advance_search['dUnitsYearVar_Suggested']['dMin'] = null;
      this.items_filtered_advance_search['dUnitsYearVar_Suggested']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    } else {
      this.items_filtered_advance_search['dUnitsYearVar_User']['dMin'] = null;
      this.items_filtered_advance_search['dUnitsYearVar_User']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    }
    this.fnGetDataCharts(this.current_payload, this.id_version, {});
  }

  fnSetRemoveBadgetIncomeYearWithVATCurrent() {
    this.items_filtered_advance_search['dIncomeYearWithVATCurrent']['dMin'] = null;
    this.items_filtered_advance_search['dIncomeYearWithVATCurrent']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    this.fnGetDataCharts(this.current_payload, this.id_version, {});
  }

  fnSetRemoveBadgetIncomeYearWithVAT() {
    if (!this.userStage) {
      this.items_filtered_advance_search['dIncomeYearWithVAT_Suggested']['dMin'] = null;
      this.items_filtered_advance_search['dIncomeYearWithVAT_Suggested']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    } else {
      this.items_filtered_advance_search['dIncomeYearWithVAT_User']['dMin'] = null;
      this.items_filtered_advance_search['dIncomeYearWithVAT_User']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    }
    this.fnGetDataCharts(this.current_payload, this.id_version, {});
  }

  fnSetRemoveBadgetIncomeYearVar() {
    if (!this.userStage) {
      this.items_filtered_advance_search['dIncomeYearVar_Suggested']['dMin'] = null;
      this.items_filtered_advance_search['dIncomeYearVar_Suggested']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    } else {
      this.items_filtered_advance_search['dIncomeYearVar_User']['dMin'] = null;
      this.items_filtered_advance_search['dIncomeYearVar_User']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    }
    this.fnGetDataCharts(this.current_payload, this.id_version, {});
  }

  fnSetRemoveBadgetGrossContributionCurrentYear() {
    this.items_filtered_advance_search['dGrossContributionCurrentYear']['dMin'] = null;
    this.items_filtered_advance_search['dGrossContributionCurrentYear']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    this.fnGetDataCharts(this.current_payload, this.id_version, {});
  }

  fnSetRemoveBadgetGrossContributionYear() {
    if (!this.userStage) {
      this.items_filtered_advance_search['dGrossContributionYear_Suggested']['dMin'] = null;
      this.items_filtered_advance_search['dGrossContributionYear_Suggested']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    } else {
      this.items_filtered_advance_search['dGrossContributionYear_User']['dMin'] = null;
      this.items_filtered_advance_search['dGrossContributionYear_User']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    }
    this.fnGetDataCharts(this.current_payload, this.id_version, {});
  }

  fnSetRemoveBadgetGrossContributionYearVar() {
    if (!this.userStage) {
      this.items_filtered_advance_search['dGrossContributionYearVar_Suggested']['dMin'] = null;
      this.items_filtered_advance_search['dGrossContributionYearVar_Suggested']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    } else {
      this.items_filtered_advance_search['dGrossContributionYearVar_User']['dMin'] = null;
      this.items_filtered_advance_search['dGrossContributionYearVar_User']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    }
    this.fnGetDataCharts(this.current_payload, this.id_version, {});
  }

  fnSetRemoveBadgetCurrentGrossMargin() {
    this.items_filtered_advance_search['dCurrentGrossMargin']['dMin'] = null;
    this.items_filtered_advance_search['dCurrentGrossMargin']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    this.fnGetDataCharts(this.current_payload, this.id_version, {});
  }

  fnSetRemoveBadgetGrossMargin() {
    if (!this.userStage) {
      this.items_filtered_advance_search['dSuggestedGrossMargin']['dMin'] = null;
      this.items_filtered_advance_search['dSuggestedGrossMargin']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    } else {
      this.items_filtered_advance_search['dUserGrossMargin']['dMin'] = null;
      this.items_filtered_advance_search['dUserGrossMargin']['dMax'] = null;
      this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
      console.log('this.filter_state: ', this.filter_state);
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
      this.fnGetDataCharts(this.current_payload, this.id_version, {});
    }
  }

  fnClearAllFiltersSearch() {
    this.search_input = '';
    this.filterState = false;
    this.items_filtered_advance_search = [];
    this.fnGetAllForecastResults(this.current_payload, this.id_version);
    this.fnGetDataCharts(this.current_payload, this.id_version, {});
  }

  fnValidStateSearch(collection) {
    console.log('collection: ', collection);
    if (typeof collection['category'] === "undefined") {
      console.log('indefinida')
      this.filterState = false;
    } else {
      if (collection['products'].length < 1 &&
        collection['priceList'].length < 1 &&
        collection['category'].length < 1 &&
        collection['tProductsType'] == null &&
        collection['dCurrentPriceWithVAT']['dMin'] == null &&
        collection['dCurrentPriceWithVAT']['dMax'] == null &&
        collection['dPriceWithVAT_Suggested']['dMin'] == null &&
        collection['dPriceWithVAT_Suggested']['dMax'] == null &&
        collection['dPriceWithVATVar_Suggested']['dMin'] == null &&
        collection['dPriceWithVATVar_Suggested']['dMax'] == null &&
        collection['dUnitsCurrentYear']['dMin'] == null &&
        collection['dUnitsCurrentYear']['dMax'] == null &&
        collection['dUnitsYear_Suggested']['dMin'] == null &&
        collection['dUnitsYear_Suggested']['dMax'] == null &&
        collection['dUnitsYearVar_Suggested']['dMin'] == null &&
        collection['dUnitsYearVar_Suggested']['dMax'] == null &&
        collection['dIncomeYearWithVATCurrent']['dMin'] == null &&
        collection['dIncomeYearWithVATCurrent']['dMax'] == null &&
        collection['dIncomeYearWithVAT_Suggested']['dMin'] == null &&
        collection['dIncomeYearWithVAT_Suggested']['dMax'] == null &&
        collection['dIncomeYearVar_Suggested']['dMin'] == null &&
        collection['dIncomeYearVar_Suggested']['dMax'] == null &&
        collection['dGrossContributionCurrentYear']['dMin'] == null &&
        collection['dGrossContributionCurrentYear']['dMax'] == null &&
        collection['dGrossContributionYear_Suggested']['dMin'] == null &&
        collection['dGrossContributionYear_Suggested']['dMax'] == null &&
        collection['dGrossContributionYearVar_Suggested']['dMin'] == null &&
        collection['dGrossContributionYearVar_Suggested']['dMax'] == null &&
        collection['dCurrentGrossMargin']['dMin'] == null &&
        collection['dCurrentGrossMargin']['dMax'] == null &&
        collection['dSuggestedGrossMargin']['dMin'] == null &&
        collection['dSuggestedGrossMargin']['dMax'] == null &&
        collection['dPriceWithVAT_User']['dMin'] == null &&
        collection['dPriceWithVAT_User']['dMax'] == null &&
        collection['dPriceWithVATVar_User']['dMin'] == null &&
        collection['dPriceWithVATVar_User']['dMax'] == null &&
        collection['dUnitsYear_User']['dMin'] == null &&
        collection['dUnitsYear_User']['dMax'] == null &&
        collection['dUnitsYearVar_User']['dMin'] == null &&
        collection['dUnitsYearVar_User']['dMax'] == null &&
        collection['dIncomeYearWithVAT_User']['dMin'] == null &&
        collection['dIncomeYearWithVAT_User']['dMax'] == null &&
        collection['dIncomeYearVar_User']['dMin'] == null &&
        collection['dIncomeYearVar_User']['dMax'] == null &&
        collection['dGrossContributionYear_User']['dMin'] == null &&
        collection['dGrossContributionYear_User']['dMax'] == null &&
        collection['dGrossContributionYearVar_User']['dMin'] == null &&
        collection['dGrossContributionYearVar_User']['dMax'] == null &&
        collection['dUserGrossMargin']['dMin'] == null &&
        collection['dUserGrossMargin']['dMax'] == null) {
        this.filterState = false;
        return false;
      } else {
        this.filterState = true;
        return true;
      }
    }
  }
  /****/

  /** Funciones para editar y borrar **/
  showModalEdit(data) {
    console.log('data: ', data);
    // console.log('id_version: ', this.id_version);
    data.id_version = this.id_version;
    console.log('data: ', data);

    this.dialogService.open(EditForecastResultsComponent, { context: { data } }).onClose.subscribe((res) => {
      console.log('res: ', res);
      console.log('data: ', data);
      this.id_version = data['id_version'];
      this.fnGetAllForecastResults(this.current_payload, this.id_version, this.currentPage);
    });
  }
  /****/

  /** Funciones para exportar aplicar reglas e importar **/
  fnExport() {
    const lang = localStorage.getItem('language_app');
    window.open(
      this.url_host + '/api/ForecastResult/GetExportForecastResult?iIDVersion=' + this.id_version + '&language=' + lang,
      '_blank');
  }

  /****/

  /** Funciones para Pagindo **/
  getPage(page: number) {
    console.log('page: ', page);
    this.loading = true;
    if (this.filterState) {
      this.fnGetAllForecastResultsFilter(this.current_payload, this.items_filtered_advance_search, this.id_version, page);
    } else {
      this.fnGetAllForecastResults(this.current_payload, this.id_version, page, this.text_searching);
    }
    this.currentPage = page;

  }
  /****/

  /*********Show modal parameters function *********/

  open(_title, _body) {
    this.dialogService.open(ModalsComponent, {
      context: {
        title: _title,
        body: _body,
      },
    });
  }

  fnGetLanguage(){
    const self = this;
    self.utilitiesService.fnGetLanguage('forecastResults', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module,
      self.DATA_LANG_GENERAL = res_lang.general
    });
  }

}