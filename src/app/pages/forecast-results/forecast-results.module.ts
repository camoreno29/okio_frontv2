import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng5SliderModule } from 'ng5-slider';
import { EditForecastResultsComponent } from './edit-forecast-results/edit-forecast-results.component';
import { AdvancedSearchForecastResultsComponent } from './advanced-search-forecast-results/advanced-search-forecast-results.component';

const ENTRY_COMPONENTS = [
  EditForecastResultsComponent,
  AdvancedSearchForecastResultsComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    TooltipModule.forRoot(),
    ModalModule,
    NgSelectModule,
    FormsModule,
    NgxPaginationModule,
    Ng5SliderModule,
  ],
  declarations: [
    EditForecastResultsComponent,
    AdvancedSearchForecastResultsComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class ForecastResultsModule { }
