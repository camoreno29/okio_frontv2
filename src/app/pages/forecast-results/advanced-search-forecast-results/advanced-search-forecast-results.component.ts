import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options } from 'ng5-slider';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { PricesListService } from '../../../shared/api/services/prices-list.service';
import { ProductInformationService } from '../../../shared/api/services/product-information.service';
import { ForecastResultsService } from './../../../shared/api/services/forecast-results.service';
// import { PriceCalculationSummaryService } from '../../../shared/api/services/price-calculation-summary.service';

@Component({
  selector: 'ngx-advanced-search-forecast-results',
  templateUrl: './advanced-search-forecast-results.component.html',
  styleUrls: ['./advanced-search-forecast-results.component.scss']
})
export class AdvancedSearchForecastResultsComponent implements OnInit {

  submitted: boolean = false;
  current_payload: any = null;
  forecast_results_data: any = {};
  state_loading: boolean = true;

  @Input() id_version: string;
  @Input() items_collection_filtered: any;
  @Input() userStage: boolean;
  @Input() state_view: string;


  id_category: any = null;
  list_categories_collection: any = [];
  list_prices_lists_collection: any = [];
  list_products_collection: any = [];

  /* ********** START - Data slider range - CurrentPriceWithVAT ********* */
  min_value_CurrentPriceWithVAT: number = 0;
  max_value_CurrentPriceWithVAT: number = 0;
  /* *********** END - Data slider range - CurrentPriceWithVAT ********** */
  /* ********** START - Data slider range - SuggestedPriceWithVAT ********* */
  min_value_SuggestedPriceWithVAT: number = 0;
  max_value_SuggestedPriceWithVAT: number = 0;
  /* *********** END - Data slider range - SuggestedPriceWithVAT ********** */
  /* ********** START - Data slider range - SuggestedPriceWithVAT_User ********* */
  min_value_SuggestedPriceWithVAT_User: number = 0;
  max_value_SuggestedPriceWithVAT_User: number = 0;
  /* *********** END - Data slider range - SuggestedPriceWithVAT_User ********** */
  /* ********** START - Data slider range - PriceWithVATVar ********* */
  min_value_PriceWithVATVar: number = null;
  max_value_PriceWithVATVar: number = null;
  /* *********** END - Data slider range - PriceWithVATVar ********** */
  /* ********** START - Data slider range - PriceWithVATVar_User ********* */
  min_value_PriceWithVATVar_User: number = null;
  max_value_PriceWithVATVar_User: number = null;
  /* *********** END - Data slider range - PriceWithVATVar_User ********** */
  /* ********** START - Data slider range - UnitsCurrentYear ********* */
  min_value_UnitsCurrentYear: number = 0;
  max_value_UnitsCurrentYear: number = 0;
  /* *********** END - Data slider range - UnitsCurrentYear ********** */
  /* ********** START - Data slider range - UnitsYearSuggested ********* */
  min_value_UnitsYearSuggested: number = 0;
  max_value_UnitsYearSuggested: number = 0;
  /* *********** END - Data slider range - UnitsYearSuggested ********** */
  /* ********** START - Data slider range - UnitsYearSuggested_User ********* */
  min_value_UnitsYearSuggested_User: number = 0;
  max_value_UnitsYearSuggested_User: number = 0;
  /* *********** END - Data slider range - UnitsYearSuggested_User ********** */
  /* ********** START - Data slider range - UnitsYearVar ********* */
  min_value_UnitsYearVar: number = null;
  max_value_UnitsYearVar: number = null;
  /* *********** END - Data slider range - UnitsYearVar ********** */
  /* ********** START - Data slider range - UnitsYearVar_User ********* */
  min_value_UnitsYearVar_User: number = null;
  max_value_UnitsYearVar_User: number = null;
  /* *********** END - Data slider range - UnitsYearVar_User ********** */
  /* ********** START - Data slider range - IncomeYearWithVATCurrent ********* */
  min_value_IncomeYearWithVATCurrent: number = 0;
  max_value_IncomeYearWithVATCurrent: number = 0;
  /* *********** END - Data slider range - IncomeYearWithVATCurrent ********** */
  /* ********** START - Data slider range - IncomeYearWithVATSuggested ********* */
  min_value_IncomeYearWithVATSuggested: number = 0;
  max_value_IncomeYearWithVATSuggested: number = 0;
  /* *********** END - Data slider range - IncomeYearWithVATSuggested ********** */
  /* ********** START - Data slider range - IncomeYearWithVATSuggested_User ********* */
  min_value_IncomeYearWithVATSuggested_User: number = 0;
  max_value_IncomeYearWithVATSuggested_User: number = 0;
  /* *********** END - Data slider range - IncomeYearWithVATSuggested_User ********** */
  /* ********** START - Data slider range - IncomeYearVar ********* */
  min_value_IncomeYearVar: number = null;
  max_value_IncomeYearVar: number = null;
  /* *********** END - Data slider range - IncomeYearVar ********** */
  /* ********** START - Data slider range - IncomeYearVar_User ********* */
  min_value_IncomeYearVar_User: number = null;
  max_value_IncomeYearVar_User: number = null;
  /* *********** END - Data slider range - IncomeYearVar_User ********** */
  /* ********** START - Data slider range - GrossContributionCurrentYear ********* */
  min_value_GrossContributionCurrentYear: number = 0;
  max_value_GrossContributionCurrentYear: number = 0;
  /* *********** END - Data slider range - GrossContributionCurrentYear ********** */
  /* ********** START - Data slider range - GrossContributionYearSuggested ********* */
  min_value_GrossContributionYearSuggested: number = 0;
  max_value_GrossContributionYearSuggested: number = 0;
  /* *********** END - Data slider range - GrossContributionYearSuggested ********** */
  /* ********** START - Data slider range - GrossContributionYearSuggested_User ********* */
  min_value_GrossContributionYearSuggested_User: number = 0;
  max_value_GrossContributionYearSuggested_User: number = 0;
  /* *********** END - Data slider range - GrossContributionYearSuggested_User ********** */
  /* ********** START - Data slider range - GrossContributionYearVar ********* */
  min_value_GrossContributionYearVar: number = null;
  max_value_GrossContributionYearVar: number = null;
  /* *********** END - Data slider range - GrossContributionYearVar ********** */
  /* ********** START - Data slider range - GrossContributionYearVar_User ********* */
  min_value_GrossContributionYearVar_User: number = null;
  max_value_GrossContributionYearVar_User: number = null;
  /* *********** END - Data slider range - GrossContributionYearVar_User ********** */
  /* ********** START - Data slider range - GrossContributionCurrentYear ********* */
  min_value_CurrentGrossGrossMargin: number = 0;
  max_value_CurrentGrossGrossMargin: number = 0;
  /* *********** END - Data slider range - GrossContributionCurrentYear ********** */
  /* ********** START - Data slider range - GrossContributionYearSuggested ********* */
  min_value_GrossGrossMargin: number = 0;
  max_value_GrossGrossMargin: number = 0;
  /* *********** END - Data slider range - GrossContributionYearSuggested ********** */
  /* ********** START - Data slider range - GrossContributionYearSuggested_User ********* */
  min_value_GrossGrossMargin_User: number = 0;
  max_value_GrossGrossMargin_User: number = 0;
  /* *********** END - Data slider range - GrossContributionYearSuggested_User ********** */

  /* *********** START - Data vars class ********** */
  list_data_filters_collection: any = [];
  list_data_filters_original_collection: any = [];
  /* ************ END - Data vars class *********** */

  data_filter_products: Object = {};
  data_filter_price_list: Object = {};
  data_filter_categories: Object = {};
  data_filter_products_type: Object = {};
  
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;


  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private categoriesService: CategoriesService,
    private pricesListService: PricesListService,
    private productInformationService: ProductInformationService,
    private forecastResultsService: ForecastResultsService,
    protected ref: NbDialogRef<AdvancedSearchForecastResultsComponent>,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('forecastResults', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('items_collection_filtered======================>: ', this.items_collection_filtered);
        this.data_filter_products = this.items_collection_filtered['products'];
        this.data_filter_price_list = this.items_collection_filtered['priceList'];
        this.data_filter_categories = this.items_collection_filtered['category'];
        this.data_filter_products_type = this.items_collection_filtered['tProductsType'];
        this.min_value_CurrentPriceWithVAT = (this.items_collection_filtered['dCurrentPriceWithVAT']) ? this.items_collection_filtered['dCurrentPriceWithVAT']['dMin'] : null;
        this.max_value_CurrentPriceWithVAT = (this.items_collection_filtered['dCurrentPriceWithVAT']) ? this.items_collection_filtered['dCurrentPriceWithVAT']['dMax'] : null;
        this.min_value_SuggestedPriceWithVAT = (this.items_collection_filtered['dPriceWithVAT_Suggested']) ? this.items_collection_filtered['dPriceWithVAT_Suggested']['dMin'] : null;
        this.max_value_SuggestedPriceWithVAT = (this.items_collection_filtered['dPriceWithVAT_Suggested']) ? this.items_collection_filtered['dPriceWithVAT_Suggested']['dMax'] : null;
        this.min_value_SuggestedPriceWithVAT_User = (this.items_collection_filtered['dPriceWithVAT_User']) ? this.items_collection_filtered['dPriceWithVAT_User']['dMin'] : null;
        this.max_value_SuggestedPriceWithVAT_User = (this.items_collection_filtered['dPriceWithVAT_User']) ? this.items_collection_filtered['dPriceWithVAT_User']['dMax'] : null;
        this.min_value_PriceWithVATVar = (this.items_collection_filtered['dPriceWithVATVar_Suggested']) ? (( this.items_collection_filtered['dPriceWithVATVar_Suggested']['dMin'] != null) ? this.items_collection_filtered['dPriceWithVATVar_Suggested']['dMin'] * 100: null): null;
        this.max_value_PriceWithVATVar = (this.items_collection_filtered['dPriceWithVATVar_Suggested']) ? (( this.items_collection_filtered['dPriceWithVATVar_Suggested']['dMax'] != null) ? this.items_collection_filtered['dPriceWithVATVar_Suggested']['dMax'] * 100: null): null;
        this.min_value_PriceWithVATVar_User = (this.items_collection_filtered['dPriceWithVATVar_User']) ? ((this.items_collection_filtered['dPriceWithVATVar_User']['dMin'] != null) ? this.items_collection_filtered['dPriceWithVATVar_User']['dMin'] * 100: null): null;
        this.max_value_PriceWithVATVar_User = (this.items_collection_filtered['dPriceWithVATVar_User']) ? ((this.items_collection_filtered['dPriceWithVATVar_User']['dMax'] != null) ? this.items_collection_filtered['dPriceWithVATVar_User']['dMax'] * 100: null): null;
        this.min_value_UnitsCurrentYear = (this.items_collection_filtered['dUnitsCurrentYear']) ? this.items_collection_filtered['dUnitsCurrentYear']['dMin'] : null;
        this.max_value_UnitsCurrentYear = (this.items_collection_filtered['dUnitsCurrentYear']) ? this.items_collection_filtered['dUnitsCurrentYear']['dMax'] : null;
        this.min_value_UnitsYearSuggested = (this.items_collection_filtered['dUnitsYear_Suggested']) ? this.items_collection_filtered['dUnitsYear_Suggested']['dMin'] : null;
        this.max_value_UnitsYearSuggested = (this.items_collection_filtered['dUnitsYear_Suggested']) ? this.items_collection_filtered['dUnitsYear_Suggested']['dMax'] : null;
        this.min_value_UnitsYearSuggested_User = (this.items_collection_filtered['dUnitsYear_User']) ? this.items_collection_filtered['dUnitsYear_User']['dMin'] : null;
        this.max_value_UnitsYearSuggested_User = (this.items_collection_filtered['dUnitsYear_User']) ? this.items_collection_filtered['dUnitsYear_User']['dMax'] : null;
        this.min_value_UnitsYearVar = (this.items_collection_filtered['dUnitsYearVar_Suggested']) ? ((this.items_collection_filtered['dUnitsYearVar_Suggested']['dMin'] != null) ? this.items_collection_filtered['dUnitsYearVar_Suggested']['dMin'] * 100: null) : null;
        this.max_value_UnitsYearVar = (this.items_collection_filtered['dUnitsYearVar_Suggested']) ? ((this.items_collection_filtered['dUnitsYearVar_Suggested']['dMax'] != null) ? this.items_collection_filtered['dUnitsYearVar_Suggested']['dMax'] * 100: null) : null;
        this.min_value_UnitsYearVar_User = (this.items_collection_filtered['dUnitsYearVar_User']) ? ((this.items_collection_filtered['dUnitsYearVar_User']['dMin'] != null) ? this.items_collection_filtered['dUnitsYearVar_User']['dMin'] * 100: null) : null;
        this.max_value_UnitsYearVar_User = (this.items_collection_filtered['dUnitsYearVar_User']) ? ((this.items_collection_filtered['dUnitsYearVar_User']['dMax'] != null) ? this.items_collection_filtered['dUnitsYearVar_User']['dMax'] * 100: null) : null;
        this.min_value_IncomeYearWithVATCurrent = (this.items_collection_filtered['dIncomeYearWithVATCurrent']) ? this.items_collection_filtered['dIncomeYearWithVATCurrent']['dMin'] : null;
        this.max_value_IncomeYearWithVATCurrent = (this.items_collection_filtered['dIncomeYearWithVATCurrent']) ? this.items_collection_filtered['dIncomeYearWithVATCurrent']['dMax'] : null;
        this.min_value_IncomeYearWithVATSuggested = (this.items_collection_filtered['dIncomeYearWithVAT_Suggested']) ? this.items_collection_filtered['dIncomeYearWithVAT_Suggested']['dMin'] : null;
        this.max_value_IncomeYearWithVATSuggested = (this.items_collection_filtered['dIncomeYearWithVAT_Suggested']) ? this.items_collection_filtered['dIncomeYearWithVAT_Suggested']['dMax'] : null;
        this.min_value_IncomeYearWithVATSuggested_User = (this.items_collection_filtered['dIncomeYearWithVAT_User']) ? this.items_collection_filtered['dIncomeYearWithVAT_User']['dMin'] : null;
        this.max_value_IncomeYearWithVATSuggested_User = (this.items_collection_filtered['dIncomeYearWithVAT_User']) ? this.items_collection_filtered['dIncomeYearWithVAT_User']['dMax'] : null;
        this.min_value_IncomeYearVar = (this.items_collection_filtered['dIncomeYearVar_Suggested']) ? ((this.items_collection_filtered['dIncomeYearVar_Suggested']['dMin'] != null) ? this.items_collection_filtered['dIncomeYearVar_Suggested']['dMin'] * 100: null): null;
        this.max_value_IncomeYearVar = (this.items_collection_filtered['dIncomeYearVar_Suggested']) ? ((this.items_collection_filtered['dIncomeYearVar_Suggested']['dMax'] != null) ? this.items_collection_filtered['dIncomeYearVar_Suggested']['dMax'] * 100: null): null;
        this.min_value_IncomeYearVar_User = (this.items_collection_filtered['dIncomeYearVar_User']) ? ((this.items_collection_filtered['dIncomeYearVar_User']['dMin'] != null) ? this.items_collection_filtered['dIncomeYearVar_User']['dMin'] * 100: null): null;
        this.max_value_IncomeYearVar_User = (this.items_collection_filtered['dIncomeYearVar_User']) ? ((this.items_collection_filtered['dIncomeYearVar_User']['dMax'] != null) ? this.items_collection_filtered['dIncomeYearVar_User']['dMax'] * 100: null): null;
        this.min_value_GrossContributionCurrentYear = (this.items_collection_filtered['dGrossContributionCurrentYear']) ? this.items_collection_filtered['dGrossContributionCurrentYear']['dMin'] : null;
        this.max_value_GrossContributionCurrentYear = (this.items_collection_filtered['dGrossContributionCurrentYear']) ? this.items_collection_filtered['dGrossContributionCurrentYear']['dMax'] : null;
        this.min_value_GrossContributionYearSuggested = (this.items_collection_filtered['dGrossContributionYear_Suggested']) ? this.items_collection_filtered['dGrossContributionYear_Suggested']['dMin'] : null;
        this.max_value_GrossContributionYearSuggested = (this.items_collection_filtered['dGrossContributionYear_Suggested']) ? this.items_collection_filtered['dGrossContributionYear_Suggested']['dMax'] : null;
        this.min_value_GrossContributionYearSuggested_User = (this.items_collection_filtered['dGrossContributionYear_User']) ? this.items_collection_filtered['dGrossContributionYear_User']['dMin'] : null;
        this.max_value_GrossContributionYearSuggested_User = (this.items_collection_filtered['dGrossContributionYear_User']) ? this.items_collection_filtered['dGrossContributionYear_User']['dMax'] : null;
        this.min_value_GrossContributionYearVar = (this.items_collection_filtered['dGrossContributionYearVar_Suggested']) ? ((this.items_collection_filtered['dGrossContributionYearVar_Suggested']['dMin'] != null) ? this.items_collection_filtered['dGrossContributionYearVar_Suggested']['dMin'] * 100: null): null;
        this.max_value_GrossContributionYearVar = (this.items_collection_filtered['dGrossContributionYearVar_Suggested']) ? ((this.items_collection_filtered['dGrossContributionYearVar_Suggested']['dMax'] != null) ? this.items_collection_filtered['dGrossContributionYearVar_Suggested']['dMax'] * 100: null): null;
        this.min_value_GrossContributionYearVar_User = (this.items_collection_filtered['dGrossContributionYearVar_User']) ? ((this.items_collection_filtered['dGrossContributionYearVar_User']['dMin'] != null) ? this.items_collection_filtered['dGrossContributionYearVar_User']['dMin'] * 100: null): null;
        this.max_value_GrossContributionYearVar_User = (this.items_collection_filtered['dGrossContributionYearVar_User']) ? ((this.items_collection_filtered['dGrossContributionYearVar_User']['dMax'] != null) ? this.items_collection_filtered['dGrossContributionYearVar_User']['dMax'] * 100: null): null;
        this.min_value_CurrentGrossGrossMargin = (this.items_collection_filtered['dCurrentGrossMargin']) ? ((this.items_collection_filtered['dCurrentGrossMargin']['dMin'] != null) ? this.items_collection_filtered['dCurrentGrossMargin']['dMin'] * 100: null): null;
        this.max_value_CurrentGrossGrossMargin = (this.items_collection_filtered['dCurrentGrossMargin']) ? ((this.items_collection_filtered['dCurrentGrossMargin']['dMax'] != null) ? this.items_collection_filtered['dCurrentGrossMargin']['dMax'] * 100: null): null;
        this.min_value_GrossGrossMargin = (this.items_collection_filtered['dSuggestedGrossMargin']) ? ((this.items_collection_filtered['dSuggestedGrossMargin']['dMin'] != null) ? this.items_collection_filtered['dSuggestedGrossMargin']['dMin'] * 100: null): null;
        this.max_value_GrossGrossMargin = (this.items_collection_filtered['dSuggestedGrossMargin']) ? ((this.items_collection_filtered['dSuggestedGrossMargin']['dMax'] != null) ? this.items_collection_filtered['dSuggestedGrossMargin']['dMax'] * 100: null): null;
        this.min_value_GrossGrossMargin_User = (this.items_collection_filtered['dUserGrossMargin']) ? ((this.items_collection_filtered['dUserGrossMargin']['dMin'] != null) ? this.items_collection_filtered['dUserGrossMargin']['dMin'] * 100: null): null;
        this.max_value_GrossGrossMargin_User = (this.items_collection_filtered['dUserGrossMargin']) ? ((this.items_collection_filtered['dUserGrossMargin']['dMax'] != null) ? this.items_collection_filtered['dUserGrossMargin']['dMax'] * 100: null): null;

        this.fnGetListCategories(this.current_payload, this.id_version);
        this.fnGetPricesLists(this.current_payload, this.id_version);
        this.fnGetListProducts(this.current_payload, this.id_version);
      }
    });
  }

  fnGetListCategories(current_payload, id_version) {
    this.categoriesService.fnHttpGetDataListCategoriesByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_categories_collection = JSON.parse(JSON.stringify(r.body));
        this.submitted = false;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnGetPricesLists(current_payload, id_version?) {
    const self = this;
    self.pricesListService.fnGetAllPricesListByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.list_prices_lists_collection = JSON.parse(JSON.stringify(r.body));
        console.log('self.list_prices_lists_collection: ', self.list_prices_lists_collection);
        self.submitted = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        let error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetListProducts(current_payload, id_version, text_search?) {
    const self = this;
    self.productInformationService.fnHttpGetListProducts(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.list_products_collection = JSON.parse(JSON.stringify(r.body));
        self.state_loading = false;
        self.submitted = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        let error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnAdvancedSearchValue(forecast_results_data) {
    this.submitted = true;
    console.log('forecast_results_data: ', forecast_results_data);
    const data_object = {
      'products': (this.data_filter_products) ? this.data_filter_products : [],
      'priceList': (this.data_filter_price_list) ? this.data_filter_price_list : [],
      'category': (this.data_filter_categories) ? this.data_filter_categories : [],
      'tProductsType': (this.data_filter_products_type) ? this.data_filter_products_type : [],
      'dCurrentPriceWithVAT': {  
        'dMin': this.min_value_CurrentPriceWithVAT == null ? '' : this.min_value_CurrentPriceWithVAT,
        'dMax': this.max_value_CurrentPriceWithVAT == null ? '' : this.max_value_CurrentPriceWithVAT,
      },
      'dPriceWithVAT_Suggested': {
        'dMin': this.min_value_SuggestedPriceWithVAT == null ? '' : this.min_value_SuggestedPriceWithVAT,
        'dMax': this.max_value_SuggestedPriceWithVAT == null ? '' : this.max_value_SuggestedPriceWithVAT,
      },
      'dPriceWithVATVar_Suggested': {
        'dMin': this.min_value_PriceWithVATVar == null ? '' : this.min_value_PriceWithVATVar,
        'dMax': this.max_value_PriceWithVATVar == null ? '' : this.max_value_PriceWithVATVar,
      },
      'dUnitsCurrentYear': {
        'dMin': this.min_value_UnitsCurrentYear == null ? '' : this.min_value_UnitsCurrentYear,
        'dMax': this.max_value_UnitsCurrentYear == null ? '' : this.max_value_UnitsCurrentYear,
      },
      'dUnitsYear_Suggested': {
        'dMin': this.min_value_UnitsYearSuggested == null ? '' : this.min_value_UnitsYearSuggested,
        'dMax': this.max_value_UnitsYearSuggested == null ? '' : this.max_value_UnitsYearSuggested,
      },
      'dUnitsYearVar_Suggested': {
        'dMin': this.min_value_UnitsYearVar == null ? '' : this.min_value_UnitsYearVar,
        'dMax': this.max_value_UnitsYearVar == null ? '' : this.max_value_UnitsYearVar,
      },
      'dIncomeYearWithVATCurrent': {
        'dMin': this.min_value_IncomeYearWithVATCurrent == null ? '' : this.min_value_IncomeYearWithVATCurrent,
        'dMax': this.max_value_IncomeYearWithVATCurrent == null ? '' : this.max_value_IncomeYearWithVATCurrent,
      },
      'dIncomeYearWithVAT_Suggested': {
        'dMin': this.min_value_IncomeYearWithVATSuggested == null ? '' : this.min_value_IncomeYearWithVATSuggested,
        'dMax': this.max_value_IncomeYearWithVATSuggested == null ? '' : this.max_value_IncomeYearWithVATSuggested,
      },
      'dIncomeYearVar_Suggested': {
        'dMin': this.min_value_IncomeYearVar == null ? '' : this.min_value_IncomeYearVar,
        'dMax': this.max_value_IncomeYearVar == null ? '' : this.max_value_IncomeYearVar,
      },
      'dGrossContributionCurrentYear': {
        'dMin': this.min_value_GrossContributionCurrentYear == null ? '' : this.min_value_GrossContributionCurrentYear,
        'dMax': this.max_value_GrossContributionCurrentYear == null ? '' : this.max_value_GrossContributionCurrentYear,
      },
      'dGrossContributionYear_Suggested': {
        'dMin': this.min_value_GrossContributionYearSuggested == null ? '' : this.min_value_GrossContributionYearSuggested,
        'dMax': this.max_value_GrossContributionYearSuggested == null ? '' : this.max_value_GrossContributionYearSuggested,
      },
      'dGrossContributionYearVar_Suggested': {
        'dMin': this.min_value_GrossContributionCurrentYear == null ? '' : this.min_value_GrossContributionCurrentYear,
        'dMax': this.max_value_GrossContributionCurrentYear == null ? '' : this.max_value_GrossContributionCurrentYear,
      },
      'dCurrentGrossMargin': {
        'dMin': this.min_value_CurrentGrossGrossMargin == null ? '' : this.min_value_CurrentGrossGrossMargin,
        'dMax': this.max_value_CurrentGrossGrossMargin == null ? '' : this.max_value_CurrentGrossGrossMargin,
      },
      'dSuggestedGrossMargin': {
        'dMin': this.min_value_GrossGrossMargin == null ? '' : this.min_value_GrossGrossMargin,
        'dMax': this.max_value_GrossGrossMargin == null ? '' : this.max_value_GrossGrossMargin,
      },
      'dPriceWithVAT_User': {
        'dMin': this.min_value_SuggestedPriceWithVAT_User == null ? '' : this.min_value_SuggestedPriceWithVAT_User,
        'dMax': this.max_value_SuggestedPriceWithVAT_User == null ? '' : this.max_value_SuggestedPriceWithVAT_User,
      },
      'dPriceWithVATVar_User': {
        'dMin': this.min_value_PriceWithVATVar_User == null ? '' : this.min_value_PriceWithVATVar_User,
        'dMax': this.max_value_PriceWithVATVar_User == null ? '' : this.max_value_PriceWithVATVar_User,
      },
      'dUnitsYear_User': {
        'dMin': this.min_value_UnitsYearSuggested_User == null ? '' : this.min_value_UnitsYearSuggested_User,
        'dMax': this.max_value_UnitsYearSuggested_User == null ? '' : this.max_value_UnitsYearSuggested_User,
      },
      'dUnitsYearVar_User': {
        'dMin': this.min_value_UnitsYearVar_User == null ? '' : this.min_value_UnitsYearVar_User,
        'dMax': this.max_value_UnitsYearVar_User == null ? '' : this.max_value_UnitsYearVar_User,
      },
      'dIncomeYearWithVAT_User': {
        'dMin': this.min_value_IncomeYearWithVATSuggested_User == null ? '' : this.min_value_IncomeYearWithVATSuggested_User,
        'dMax': this.max_value_IncomeYearWithVATSuggested_User == null ? '' : this.max_value_IncomeYearWithVATSuggested_User,
      },
      'dIncomeYearVar_User': {
        'dMin': this.min_value_IncomeYearVar_User == null ? '' : this.min_value_IncomeYearVar_User,
        'dMax': this.max_value_IncomeYearVar_User == null ? '' : this.max_value_IncomeYearVar_User,
      },
      'dGrossContributionYear_User': {
        'dMin': this.min_value_GrossContributionYearSuggested_User == null ? '' : this.min_value_GrossContributionYearSuggested_User,
        'dMax': this.max_value_GrossContributionYearSuggested_User == null ? '' : this.max_value_GrossContributionYearSuggested_User,
      },
      'dGrossContributionYearVar_User': {
        'dMin': this.min_value_GrossContributionYearVar == null ? '' : this.min_value_GrossContributionYearVar,
        'dMax': this.max_value_GrossContributionYearVar == null ? '' : this.max_value_GrossContributionYearVar,
      },
      'dUserGrossMargin': {
        'dMin': this.min_value_GrossGrossMargin_User == null ? '' : this.min_value_GrossGrossMargin_User,
        'dMax': this.max_value_GrossGrossMargin_User == null ? '' : this.max_value_GrossGrossMargin_User,
      },
      
      'page': 1,
      'pageSize': 12,
      'tSearch': '',
    };
    console.log('data_object: ', data_object);
    this.dismiss(data_object);
  }

  dismiss(object_response_onclose?) {
    console.log('object_response_onclose: ', object_response_onclose);
    if (object_response_onclose) {
      object_response_onclose['products'] = (this.data_filter_products) ? this.data_filter_products : [];
      object_response_onclose['priceList'] = (this.data_filter_price_list) ? this.data_filter_price_list : [];
      object_response_onclose['category'] = (this.data_filter_categories) ? this.data_filter_categories : [];
      object_response_onclose['tProductsType'] = (this.data_filter_products_type) ? this.data_filter_products_type : [];
      object_response_onclose['dCurrentPriceWithVAT'] = (this.max_value_CurrentPriceWithVAT) ? { 'dMin': this.min_value_CurrentPriceWithVAT, 'dMax': this.max_value_CurrentPriceWithVAT } : { 'dMin': this.min_value_CurrentPriceWithVAT, 'dMax': null };
      object_response_onclose['dPriceWithVAT_Suggested'] = (this.max_value_SuggestedPriceWithVAT) ? { 'dMin': this.min_value_SuggestedPriceWithVAT, 'dMax': this.max_value_SuggestedPriceWithVAT } : { 'dMin': this.min_value_SuggestedPriceWithVAT, 'dMax': null };
      object_response_onclose['dPriceWithVAT_User'] = (this.max_value_SuggestedPriceWithVAT_User) ? { 'dMin': this.min_value_SuggestedPriceWithVAT_User, 'dMax': this.max_value_SuggestedPriceWithVAT_User } : { 'dMin': this.min_value_SuggestedPriceWithVAT_User, 'dMax': null };
      //object_response_onclose['dPriceWithVATVar_Suggested'] = (this.max_value_PriceWithVATVar) ? { 'dMin': this.min_value_PriceWithVATVar / 100, 'dMax': this.max_value_PriceWithVATVar / 100 } : { 'dMin': this.min_value_PriceWithVATVar, 'dMax': null };
      if(this.min_value_PriceWithVATVar && this.max_value_PriceWithVATVar){
        object_response_onclose['dPriceWithVATVar_Suggested'] = { 'dMin': this.min_value_PriceWithVATVar / 100, 'dMax': this.max_value_PriceWithVATVar / 100 }
        }else if(!this.min_value_PriceWithVATVar && !this.max_value_PriceWithVATVar){
          object_response_onclose['dPriceWithVATVar_Suggested'] = { 'dMin': null, 'dMax': null }
        }else if(!this.min_value_PriceWithVATVar && this.max_value_PriceWithVATVar){
          object_response_onclose['dPriceWithVATVar_Suggested'] = { 'dMin': null, 'dMax': this.max_value_PriceWithVATVar / 100 }
        }else if(!this.max_value_PriceWithVATVar && this.min_value_PriceWithVATVar){
          object_response_onclose['dPriceWithVATVar_Suggested'] = { 'dMin': this.min_value_PriceWithVATVar / 100, 'dMax': null }
        }
      // object_response_onclose['dPriceWithVATVar_User'] = (this.max_value_PriceWithVATVar_User) ? { 'dMin': this.min_value_PriceWithVATVar_User / 100, 'dMax': this.max_value_PriceWithVATVar_User / 100 } : { 'dMin': this.min_value_PriceWithVATVar_User, 'dMax': null };
      if(this.min_value_PriceWithVATVar_User && this.max_value_PriceWithVATVar_User){
        object_response_onclose['dPriceWithVATVar_User'] = { 'dMin': this.min_value_PriceWithVATVar_User / 100, 'dMax': this.max_value_PriceWithVATVar_User / 100 }
        }else if(!this.min_value_PriceWithVATVar_User && !this.max_value_PriceWithVATVar_User){
          object_response_onclose['dPriceWithVATVar_User'] = { 'dMin': null, 'dMax': null }
        }else if(!this.min_value_PriceWithVATVar_User && this.max_value_PriceWithVATVar_User){
          object_response_onclose['dPriceWithVATVar_User'] = { 'dMin': null, 'dMax': this.max_value_PriceWithVATVar_User / 100 }
        }else if(!this.max_value_PriceWithVATVar_User && this.min_value_PriceWithVATVar_User){
          object_response_onclose['dPriceWithVATVar_User'] = { 'dMin': this.min_value_PriceWithVATVar_User / 100, 'dMax': null }
        }
      object_response_onclose['dUnitsCurrentYear'] = (this.max_value_UnitsCurrentYear) ? { 'dMin': this.min_value_UnitsCurrentYear, 'dMax': this.max_value_UnitsCurrentYear } : { 'dMin': this.min_value_UnitsCurrentYear, 'dMax': null };
      object_response_onclose['dUnitsYear_Suggested'] = (this.max_value_UnitsYearSuggested) ? { 'dMin': this.min_value_UnitsYearSuggested, 'dMax': this.max_value_UnitsYearSuggested } : { 'dMin': this.min_value_UnitsYearSuggested, 'dMax': null };
      object_response_onclose['dUnitsYear_User'] = (this.max_value_UnitsYearSuggested_User) ? { 'dMin': this.min_value_UnitsYearSuggested_User, 'dMax': this.max_value_UnitsYearSuggested_User } : { 'dMin': this.min_value_UnitsYearSuggested_User, 'dMax': null };
      // object_response_onclose['dUnitsYearVar_Suggested'] = (this.max_value_UnitsYearVar) ? { 'dMin': this.min_value_UnitsYearVar / 100, 'dMax': this.max_value_UnitsYearVar / 100 } : { 'dMin': this.min_value_UnitsYearVar, 'dMax': null };
      if(this.min_value_UnitsYearVar && this.max_value_UnitsYearVar){
        object_response_onclose['dUnitsYearVar_Suggested'] = { 'dMin': this.min_value_UnitsYearVar / 100, 'dMax': this.max_value_UnitsYearVar / 100 }
        }else if(!this.min_value_UnitsYearVar && !this.max_value_UnitsYearVar){
          object_response_onclose['dUnitsYearVar_Suggested'] = { 'dMin': null, 'dMax': null }
        }else if(!this.min_value_UnitsYearVar && this.max_value_UnitsYearVar){
          object_response_onclose['dUnitsYearVar_Suggested'] = { 'dMin': null, 'dMax': this.max_value_UnitsYearVar / 100 }
        }else if(!this.max_value_UnitsYearVar && this.min_value_UnitsYearVar){
          object_response_onclose['dUnitsYearVar_Suggested'] = { 'dMin': this.min_value_UnitsYearVar / 100, 'dMax': null }
        }
      // object_response_onclose['dUnitsYearVar_User'] = (this.max_value_UnitsYearVar_User) ? { 'dMin': this.min_value_UnitsYearVar_User / 100, 'dMax': this.max_value_UnitsYearVar_User / 100 } : { 'dMin': this.min_value_UnitsYearVar_User, 'dMax': null };
      if(this.min_value_UnitsYearVar_User && this.max_value_UnitsYearVar_User){
        object_response_onclose['dUnitsYearVar_User'] = { 'dMin': this.min_value_UnitsYearVar_User / 100, 'dMax': this.max_value_UnitsYearVar_User / 100 }
        }else if(!this.min_value_UnitsYearVar_User && !this.max_value_UnitsYearVar_User){
          object_response_onclose['dUnitsYearVar_User'] = { 'dMin': null, 'dMax': null }
        }else if(!this.min_value_UnitsYearVar_User && this.max_value_UnitsYearVar_User){
          object_response_onclose['dUnitsYearVar_User'] = { 'dMin': null, 'dMax': this.max_value_UnitsYearVar_User / 100 }
        }else if(!this.max_value_UnitsYearVar_User && this.min_value_UnitsYearVar_User){
          object_response_onclose['dUnitsYearVar_User'] = { 'dMin': this.min_value_UnitsYearVar_User / 100, 'dMax': null }
        }
      object_response_onclose['dIncomeYearWithVATCurrent'] = (this.max_value_IncomeYearWithVATCurrent) ? { 'dMin': this.min_value_IncomeYearWithVATCurrent, 'dMax': this.max_value_IncomeYearWithVATCurrent } : { 'dMin': this.min_value_IncomeYearWithVATCurrent, 'dMax': null };
      object_response_onclose['dIncomeYearWithVAT_Suggested'] = (this.max_value_IncomeYearWithVATSuggested) ? { 'dMin': this.min_value_IncomeYearWithVATSuggested, 'dMax': this.max_value_IncomeYearWithVATSuggested } : { 'dMin': this.min_value_IncomeYearWithVATSuggested, 'dMax': null };
      object_response_onclose['dIncomeYearWithVAT_User'] = (this.max_value_IncomeYearWithVATSuggested_User) ? { 'dMin': this.min_value_IncomeYearWithVATSuggested_User, 'dMax': this.max_value_IncomeYearWithVATSuggested_User } : { 'dMin': this.min_value_IncomeYearWithVATSuggested_User, 'dMax': null };
      // object_response_onclose['dIncomeYearVar_Suggested'] = (this.max_value_IncomeYearVar) ? { 'dMin': this.min_value_IncomeYearVar / 100, 'dMax': this.max_value_IncomeYearVar / 100 } : { 'dMin': this.min_value_IncomeYearVar, 'dMax': null };
      if(this.min_value_IncomeYearVar && this.max_value_IncomeYearVar){
        object_response_onclose['dIncomeYearVar_Suggested'] = { 'dMin': this.min_value_IncomeYearVar / 100, 'dMax': this.max_value_IncomeYearVar / 100 }
        }else if(!this.min_value_IncomeYearVar && !this.max_value_IncomeYearVar){
          object_response_onclose['dIncomeYearVar_Suggested'] = { 'dMin': null, 'dMax': null }
        }else if(!this.min_value_IncomeYearVar && this.max_value_IncomeYearVar){
          object_response_onclose['dIncomeYearVar_Suggested'] = { 'dMin': null, 'dMax': this.max_value_IncomeYearVar / 100 }
        }else if(!this.max_value_IncomeYearVar && this.min_value_IncomeYearVar){
          object_response_onclose['dIncomeYearVar_Suggested'] = { 'dMin': this.min_value_IncomeYearVar / 100, 'dMax': null }
        }
      // object_response_onclose['dIncomeYearVar_User'] = (this.max_value_IncomeYearVar_User) ? { 'dMin': this.min_value_IncomeYearVar_User / 100, 'dMax': this.max_value_IncomeYearVar_User / 100 } : { 'dMin': this.min_value_IncomeYearVar_User, 'dMax': null };
      if(this.min_value_IncomeYearVar_User && this.max_value_IncomeYearVar_User){
        object_response_onclose['dIncomeYearVar_User'] = { 'dMin': this.min_value_IncomeYearVar_User / 100, 'dMax': this.max_value_IncomeYearVar_User / 100 }
        }else if(!this.min_value_IncomeYearVar_User && !this.max_value_IncomeYearVar_User){
          object_response_onclose['dIncomeYearVar_User'] = { 'dMin': null, 'dMax': null }
        }else if(!this.min_value_IncomeYearVar_User && this.max_value_IncomeYearVar_User){
          object_response_onclose['dIncomeYearVar_User'] = { 'dMin': null, 'dMax': this.max_value_IncomeYearVar_User / 100 }
        }else if(!this.max_value_IncomeYearVar_User && this.min_value_IncomeYearVar_User){
          object_response_onclose['dIncomeYearVar_User'] = { 'dMin': this.min_value_IncomeYearVar_User / 100, 'dMax': null }
        }
      object_response_onclose['dGrossContributionCurrentYear'] = (this.max_value_GrossContributionCurrentYear) ? { 'dMin': this.min_value_GrossContributionCurrentYear, 'dMax': this.max_value_GrossContributionCurrentYear } : { 'dMin': this.min_value_GrossContributionCurrentYear, 'dMax': null };
      object_response_onclose['dGrossContributionYear_Suggested'] = (this.max_value_GrossContributionYearSuggested) ? { 'dMin': this.min_value_GrossContributionYearSuggested, 'dMax': this.max_value_GrossContributionYearSuggested } : { 'dMin': this.min_value_GrossContributionYearSuggested, 'dMax': null };
      object_response_onclose['dGrossContributionYear_User'] = (this.max_value_GrossContributionYearSuggested_User) ? { 'dMin': this.min_value_GrossContributionYearSuggested_User, 'dMax': this.max_value_GrossContributionYearSuggested_User } : { 'dMin': this.min_value_GrossContributionYearSuggested_User, 'dMax': null };
      // object_response_onclose['dGrossContributionYearVar_Suggested'] = (this.max_value_GrossContributionYearVar) ? { 'dMin': this.min_value_GrossContributionYearVar / 100, 'dMax': this.max_value_GrossContributionYearVar / 100 } : { 'dMin': this.min_value_GrossContributionYearVar, 'dMax': null };
      if(this.min_value_GrossContributionYearVar && this.max_value_GrossContributionYearVar){
        object_response_onclose['dGrossContributionYearVar_Suggested'] = { 'dMin': this.min_value_GrossContributionYearVar / 100, 'dMax': this.max_value_GrossContributionYearVar / 100 }
        }else if(!this.min_value_GrossContributionYearVar && !this.max_value_GrossContributionYearVar){
          object_response_onclose['dGrossContributionYearVar_Suggested'] = { 'dMin': null, 'dMax': null }
        }else if(!this.min_value_GrossContributionYearVar && this.max_value_GrossContributionYearVar){
          object_response_onclose['dGrossContributionYearVar_Suggested'] = { 'dMin': null, 'dMax': this.max_value_GrossContributionYearVar / 100 }
        }else if(!this.max_value_GrossContributionYearVar && this.min_value_GrossContributionYearVar){
          object_response_onclose['dGrossContributionYearVar_Suggested'] = { 'dMin': this.min_value_GrossContributionYearVar / 100, 'dMax': null }
        }
      // object_response_onclose['dGrossContributionYearVar_User'] = (this.max_value_GrossContributionYearVar_User) ? { 'dMin': this.min_value_GrossContributionYearVar_User / 100, 'dMax': this.max_value_GrossContributionYearVar_User / 100 } : { 'dMin': this.min_value_GrossContributionYearVar_User, 'dMax': null };
      if(this.min_value_GrossContributionYearVar_User && this.max_value_GrossContributionYearVar_User){
        object_response_onclose['dGrossContributionYearVar_User'] = { 'dMin': this.min_value_GrossContributionYearVar_User / 100, 'dMax': this.max_value_GrossContributionYearVar_User / 100 }
        }else if(!this.min_value_GrossContributionYearVar_User && !this.max_value_GrossContributionYearVar_User){
          object_response_onclose['dGrossContributionYearVar_User'] = { 'dMin': null, 'dMax': null }
        }else if(!this.min_value_GrossContributionYearVar_User && this.max_value_GrossContributionYearVar_User){
          object_response_onclose['dGrossContributionYearVar_User'] = { 'dMin': null, 'dMax': this.max_value_GrossContributionYearVar_User / 100 }
        }else if(!this.max_value_GrossContributionYearVar_User && this.min_value_GrossContributionYearVar_User){
          object_response_onclose['dGrossContributionYearVar_User'] = { 'dMin': this.min_value_GrossContributionYearVar_User / 100, 'dMax': null }
        }
      // object_response_onclose['dCurrentGrossMargin'] = (this.max_value_CurrentGrossGrossMargin) ? { 'dMin': this.min_value_CurrentGrossGrossMargin / 100, 'dMax': this.max_value_CurrentGrossGrossMargin / 100 } : { 'dMin': this.min_value_CurrentGrossGrossMargin, 'dMax': null };
      if(this.min_value_CurrentGrossGrossMargin && this.max_value_CurrentGrossGrossMargin){
        object_response_onclose['dCurrentGrossMargin'] = { 'dMin': this.min_value_CurrentGrossGrossMargin / 100, 'dMax': this.max_value_CurrentGrossGrossMargin / 100 }
        }else if(!this.min_value_CurrentGrossGrossMargin && !this.max_value_CurrentGrossGrossMargin){
          object_response_onclose['dCurrentGrossMargin'] = { 'dMin': null, 'dMax': null }
        }else if(!this.min_value_CurrentGrossGrossMargin && this.max_value_CurrentGrossGrossMargin){
          object_response_onclose['dCurrentGrossMargin'] = { 'dMin': null, 'dMax': this.max_value_CurrentGrossGrossMargin / 100 }
        }else if(!this.max_value_CurrentGrossGrossMargin && this.min_value_CurrentGrossGrossMargin){
          object_response_onclose['dCurrentGrossMargin'] = { 'dMin': this.min_value_CurrentGrossGrossMargin / 100, 'dMax': null }
        }
      // object_response_onclose['dSuggestedGrossMargin'] = (this.max_value_GrossGrossMargin) ? { 'dMin': this.min_value_GrossGrossMargin / 100, 'dMax': this.max_value_GrossGrossMargin / 100 } : { 'dMin': this.min_value_GrossGrossMargin, 'dMax': null };
      if(this.min_value_GrossGrossMargin && this.max_value_GrossGrossMargin){
        object_response_onclose['dSuggestedGrossMargin'] = { 'dMin': this.min_value_GrossGrossMargin / 100, 'dMax': this.max_value_GrossGrossMargin / 100 }
        }else if(!this.min_value_GrossGrossMargin && !this.max_value_GrossGrossMargin){
          object_response_onclose['dSuggestedGrossMargin'] = { 'dMin': null, 'dMax': null }
        }else if(!this.min_value_GrossGrossMargin && this.max_value_GrossGrossMargin){
          object_response_onclose['dSuggestedGrossMargin'] = { 'dMin': null, 'dMax': this.max_value_GrossGrossMargin / 100 }
        }else if(!this.max_value_GrossGrossMargin && this.min_value_GrossGrossMargin){
          object_response_onclose['dSuggestedGrossMargin'] = { 'dMin': this.min_value_GrossGrossMargin / 100, 'dMax': null }
        }
      // object_response_onclose['dUserGrossMargin'] = (this.max_value_GrossGrossMargin_User) ? { 'dMin': this.min_value_GrossGrossMargin_User / 100, 'dMax': this.max_value_GrossGrossMargin_User / 100 } : { 'dMin': this.min_value_GrossGrossMargin_User, 'dMax': null };
      if(this.min_value_GrossGrossMargin_User && this.max_value_GrossGrossMargin_User){
        object_response_onclose['dUserGrossMargin'] = { 'dMin': this.min_value_GrossGrossMargin_User / 100, 'dMax': this.max_value_GrossGrossMargin_User / 100 }
        }else if(!this.min_value_GrossGrossMargin_User && !this.max_value_GrossGrossMargin_User){
          object_response_onclose['dUserGrossMargin'] = { 'dMin': null, 'dMax': null }
        }else if(!this.min_value_GrossGrossMargin_User && this.max_value_GrossGrossMargin_User){
          object_response_onclose['dUserGrossMargin'] = { 'dMin': null, 'dMax': this.max_value_GrossGrossMargin_User / 100 }
        }else if(!this.max_value_GrossGrossMargin_User && this.min_value_GrossGrossMargin_User){
          object_response_onclose['dUserGrossMargin'] = { 'dMin': this.min_value_GrossGrossMargin_User / 100, 'dMax': null }
        }
      this.ref.close(object_response_onclose);
    } else {
      this.ref.close();
    }
  }

  fnCancelFilter() {
    this.submitted = false;
    this.dismiss();
  }

}
