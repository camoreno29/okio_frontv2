import { Component, OnInit, Injectable, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';

import { MyAccountService } from '../../../shared/api/services/my-account.service';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { SignOutService } from '../../../shared/api/services/sign-out.service';
import { HeaderComponent } from '../../../@theme/components/header/header.component';
declare var $: any;

@Component({
  selector: 'ngx-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss']
})
export class MyAccountComponent implements OnInit {
  option_menu: any = 1;
  current_payload: string = null;
  state: boolean = true;
  state_password: boolean = false;
  submited_state: boolean = false;
  submited_state_password: boolean = false;
  personal_info: any = {};
  change_password: any = {};
  personal_info_original: any = {};
  messages_success: any = [];
  messages_error: any = [];
  
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private myAccountService: MyAccountService,
    // private headerComponent: HeaderComponent,
    private signOutService: SignOutService,
    private utilitiesService: UtilitiesService,
  ) { }

  ngOnInit() {

    const self = this;
    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => { 
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    // });

    $(document).ready(function() {
      $('.menu-sidebar').addClass('d-none');
      // $('.menu-sidebar').removeClass('expanded').addClass('compacted');
    });

    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
        if (this.current_payload) {
          let data_id_company = sessionStorage.getItem('id_company');
          console.log('data_id_company: ', data_id_company);
          this.fnGetPersonalInformation(this.current_payload);
        } else {
          return false;
        }
      }
    });
  }

  fnGetPersonalInformation(current_payload?) {
    const self = this;
    console.log('current_payload: ', current_payload);

    self.myAccountService.fnHttpGetPersonalIformation(current_payload).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        const data_personal_info = JSON.parse(JSON.stringify(r.body));
        this.personal_info_original = JSON.parse(JSON.stringify(r.body));
        this.personal_info = r.body;
        console.log('this.projects : ', this.personal_info );
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnActivateEditState(state?) {
    this.state = (this.state === true) ? false : true;
  }
  
  fnActivateEditStatePassword(state_password?) {
    this.state_password = (this.state_password === true) ? false : true;
  }

  fnUpdateDataPersonalInformation(personal_info) {
    const self = this;
    self.submited_state = true;
    console.log('personal_info: ', personal_info);

    const objectForm = {
      'tFirstName': personal_info.tFirstName,
      'tLastName': personal_info.tLastName,
      'tEmail': personal_info.tEmail,
    };
    console.log('objectForm: ', objectForm);
    self.myAccountService.fnHttpSetUpdateMyAccountDataUser(objectForm).subscribe(r => {
      console.log('r: ', r);
      if (r.status === 200) {
        console.log('r.status: ', r.status);
        // self.headerComponent.fnSetDataUser(personal_info.tFirstName + ' ' + personal_info.tLastName);
        // HeaderComponent.prototype.fnSetDataUser(personal_info.tFirstName + ' ' + personal_info.tLastName);
        self.submited_state = false;
        self.fnActivateEditState();
        self.fnGetPersonalInformation(this.current_payload);
        self.messages_success = [{
          'state': 'Success',
          'color': 'success',
          'description': 'Your password has been reset successfully',
        }];
      } else {
        self.submited_state = false;
        self.messages_error = [{
          'state': 'Error',
          'color': 'danger',
          'description': 'New error',
        }];
      }
    }, err => {
      self.submited_state = false;
      self.messages_error = [{
        'state': 'Error',
        'color': 'danger',
        'description': 'New error',
      }];
      console.log('err: ', err);
    });
  }

  fnCancelEditPersonalInformation() {
    this.personal_info = JSON.parse(JSON.stringify(this.personal_info_original));
    this.state = true;
  }

  fnUpdatePasswordUser(change_password) {
    const self = this;
    self.submited_state_password = true;
    self.state_password = true;
    console.log('change_password: ', change_password);

    const objectForm = {
      'tCurrentPassword': change_password.tCurrentPassword,
      'tPassword': change_password.tPassword,
      'tConfirmPassword': change_password.tConfirmPassword,
      'tLanguage': localStorage.getItem('language_app'),
    };
    console.log('objectForm: ', objectForm);
    self.myAccountService.fnHttpSetUpdatePasswordUser(objectForm).subscribe(r => {
      console.log('r: ', r);
      if (r.status === 200) {
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessUpdatePass.text, 'nb-checkmark');
        this.utilitiesService.fnDestroySessionData( function(res_clean_session) {
          console.log('res_clean_session: ', res_clean_session);
        });
        setTimeout(() => {
          this.router.navigate(['auth/login']);
        }, 3000);  // 5s

        console.log('r.status: ', r.status);
        self.change_password = {};
        self.submited_state_password = false;
        self.state_password = false;
        // self.fnActivateEditState();
        // self.fnGetPersonalInformation(this.current_payload);

        self.messages_success = [{
          'state': 'Success',
          'color': 'success',
          'description': 'Your password has been reset successfully',
        }];
      }

      if (r.status == 206) {
        let error = this.utilitiesService.fnSetErrors(null, r.body.codMessage);
        console.log('error: ', error);
        this.change_password = {};
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
        self.submited_state_password = false;
        self.state_password = false;
        self.messages_error = [{
          'state': 'Error',
          'color': 'danger',
          'description': 'New error',
        }];
      }
      // else {
      //   self.submited_state_password = false;
      //   self.state_password = false;
      //   self.messages_error = [{
      //     'state': 'Error',
      //     'color': 'danger',
      //     'description': 'New error',
      //   }];
      // }
    }, err => {
      self.submited_state_password = false;
      self.state_password = false;
      self.messages_error = [{
        'state': 'Error',
        'color': 'danger',
        'description': 'New error',
      }];
      console.log('err: ', err);
    });
  }

  fnCancelEditChangePassword() {
    this.change_password = {};
    // this.state_password = true;
  }

  fnGetLanguage(){
    const self = this;
    self.utilitiesService.fnGetLanguage('myAccount', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
  }

}
