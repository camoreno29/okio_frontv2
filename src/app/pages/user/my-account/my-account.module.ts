import { NgModule } from '@angular/core';
import { ThemeModule } from '../../../@theme/theme.module';

import { MyAccountComponent } from './my-account.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

@NgModule({
  imports: [ThemeModule, TooltipModule.forRoot()],
  declarations: [
    MyAccountComponent,
  ],
})
export class MyAccountModule { }
