import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { MemberService } from '../../../shared/api/services/member.service';

@Component({
  selector: 'ngx-add-member',
  templateUrl: './add-member.component.html',
  styleUrls: ['./add-member.component.scss']
})
export class AddMemberComponent implements OnInit {

  // @Input() title: string;
  user_invite: any = {};
  current_payload: any = null;
  title: string = null;
  submitted: boolean = false;
  id_company: any = '';

  constructor(
    private memberService: MemberService,
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    protected ref: NbDialogRef<AddMemberComponent>) {
      console.log('title: ', this.title);
    }

  dismiss() {
    this.ref.close();
  }

  ngOnInit() {

    console.log('this.route.params: ', this.route.params);
    this.route.params.subscribe(params => {
      console.log('params: ', params);
    });

    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
      }
    });
  }

  fnSendInviteNewMember(user_invite) {
    console.log('user_invite: ', user_invite);

    // let data_object = {
    //   'iIDCompany': 0,
    //   'tCompanyLogo': 'url_company_logo_',
    //   'tCompanyName': company.tCompanyName,
    // };
    const id_company = sessionStorage.getItem('id_company');
    this.id_company = sessionStorage.getItem('id_company');
    this.submitted = true;
    let data_object = {
      'iIDCompany': id_company,
      'tEmail': user_invite.tEmail,
      'subject': 'You have been invited to PGP Prexus',
      'tlanguage': 'en',
    }

    console.log('this.current_payload: ', this.current_payload);
    console.log('data_object: ', data_object);

    this.memberService.fnHttpSetInviteNewMember(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.user_invite = {};
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', 'Invitation send successfully!', 'nb-checkmark');
        setTimeout(() => {
          this.dismiss();
        }, 2000);
        // this.router.navigate(['/pages/base']);
        // this.companies = r.body;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnCancelSendInvitation() {
    this.submitted = false;
    this.user_invite['tEmail'] = '';
    // this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then( () => this.router.navigate(['/pages/members/', this.id_company]) );
    this.dismiss();
  }

}
