import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
/* ************+ Import module auth ************ */
import { StateService } from '../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { MemberService } from '../../shared/api/services/member.service';
import { UtilitiesService } from '../../shared/api/services/utilities.service';
import { AddMemberComponent } from './add-member/add-member.component';
declare var $: any;

@Component({
  selector: 'ngx-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit {

  bsModalRef: BsModalRef;
  current_payload: string = null;
  id_company: any = null;
  members: any = null;
  guest_members: any = null;
  data_company_object: any = {};
  items: any = {};
  submitted: any = false;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private modalService: BsModalService,
    private utilitiesService: UtilitiesService,
    private memberService: MemberService) {}

  ngOnInit() {

    this.items = [{ title: 'Profile' }, { title: 'Log out' }];

    this.id_company = null;
    this.route.params.subscribe(params => {
    if (params.id_company) {
      this.id_company = params.id_company;
      console.log('this.id_company: ', this.id_company);
      this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
        console.log('token: ', token);
        if (token.isValid()) {
          // here we receive a payload from the token and assigne it to our `user` variable
          this.current_payload = token.getValue();
          console.log('this.current_payload: ', this.current_payload);
          if (this.current_payload) {
            const data_id_company = sessionStorage.getItem('id_company');
            // this.id_company = this.id_company;
            this.fnGetAllMembers(this.current_payload, data_id_company);
            this.fnGetAllGuestMembers(this.current_payload, data_id_company);
            return false;
          }
        }
      });
    } else {
      console.log('no params ========>');
      this.router.navigate(['/pages/add-company']);
    }
    });

  }

  fnGetAllMembers(current_payload, id_company?) {
    console.log('current_payload: ', current_payload);

    const object_data_send = {
      'iIDCompany': id_company,
    }
    this.members = [];
    this.memberService.fnHttpGetAllMembersByCompany(current_payload, id_company).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.members = r.body;
        console.log('this.members : ', this.members );
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetAllGuestMembers(current_payload, id_company?) {
    console.log('current_payload: ', current_payload);

    const object_data_send = {
      'iIDCompany': id_company,
    }
    this.guest_members = [];
    this.memberService.fnHttpGetAllGuestMembersByCompany(current_payload, id_company).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.guest_members = r.body;
        console.log('this.guest_members : ', this.guest_members );
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnSetRevokeInvitationMember(id_company_member, index) {
    this.memberService.fnHttpSetRevokeInvitationMemberByCompany(this.current_payload, id_company_member).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.utilitiesService.showToast('top-right', 'success', 'Member revoke successfully!', 'nb-checkmark');
        this.fnGetAllMembers(this.current_payload, this.id_company);
        // this.guest_members = r.body;
        // console.log('this.guest_members : ', this.guest_members );
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnSetRevokeInvitationMemberGuest(id_company_guest_member, index) {
    console.log('id_company_guest_member: ', id_company_guest_member);
    console.log('index: ', index);
    this.memberService.fnHttpSetRevokeInvitationMemberGuestByCompany(this.current_payload, id_company_guest_member).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.utilitiesService.showToast('top-right', 'success', 'User invitation revoke successfully!', 'nb-checkmark');
        this.fnGetAllGuestMembers(this.current_payload, this.id_company);
        // this.guest_members = r.body;
        // console.log('this.guest_members : ', this.guest_members );
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnSendInviteUser(id_company_member, index) {

    // let data_object = {
    //   'iIDCompany': 0,
    //   'tCompanyLogo': 'url_company_logo_',
    //   'tCompanyName': company.tCompanyName,
    // };
    const id_company = sessionStorage.getItem('id_company');
    this.submitted = true;
    let data_object = {
      'iIDGuestCompany': id_company_member,
      'subject': 'New subject',
      'tlanguage': 'en',
    }

    console.log('this.current_payload: ', this.current_payload);
    console.log('data_object: ', data_object);

    this.memberService.fnHttpSetReSendInvitationUser(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', 'Invitation re send successfully!');
        setTimeout(() => {
          // this.dismiss();
        }, 2000);
        // this.router.navigate(['/pages/base']);
        // this.companies = r.body;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  open(id_company) {
    console.log('id_company: ', id_company);

    this.dialogService.open(AddMemberComponent).onClose.subscribe((res) => {
      this.fnGetAllMembers(this.current_payload, id_company);
      this.fnGetAllGuestMembers(this.current_payload, id_company);
    });

    // const modal = this.dialogService.open(AddMemberComponent, {
    //   context: {
    //     title: 'sefeffefeferergergregergregr',
    //   },
    // });
    // modal.close.bind(() => { console.log('When user closes'); }, () => { console.log('Backdrop click')})
    // modal.close((res: any) => {
    //   console.log('res: ', res);
    // });
    // modal.onClose
  }

  fnRevokeInvitationMember(id_company_member, index) {
    console.log('index: ', index);
    console.log('id_company_member: ', id_company_member);
    this.fnSetRevokeInvitationMember(id_company_member, index);
  }

  fnRevokeInvitationMemberGuest(id_company_guest_member, index) {
    console.log('index: ', index);
    console.log('id_company_guest_member: ', id_company_guest_member);
    this.fnSetRevokeInvitationMemberGuest(id_company_guest_member, index);
  }

  fnReSendInvitationUser(id_company_member, index) {
    console.log('index: ', index);
    console.log('id_company_member: ', id_company_member);
    this.fnSendInviteUser(id_company_member, index);
  }

}
