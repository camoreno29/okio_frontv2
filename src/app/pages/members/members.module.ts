import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { MembersComponent } from './members.component';
import { AddMemberComponent } from './add-member/add-member.component';

const ENTRY_COMPONENTS = [
  AddMemberComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    TooltipModule.forRoot(),
    ModalModule,
  ],
  declarations: [
    MembersComponent,
    AddMemberComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class MembersModule { }
