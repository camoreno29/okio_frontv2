import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { ActivateAccountService } from '../../../shared/api/services/activate-account.service';
import { environment } from '../../../../environments/environment.prod';
import * as firebase from 'firebase/app';
import 'firebase/database';

@Component({
  selector: 'ngx-activate-account',
  templateUrl: './activate-account.component.html',
  styleUrls: ['./activate-account.component.scss']
})
export class ActivateAccountComponent implements OnInit {
  state_active_account: any = null;
  submitted: Boolean = false;
  DATA_LANG: any = null;
  LANG_NAME: any = '';
  lang_nav: any =  window.navigator.language;

  constructor(
    private activatedRoute: ActivatedRoute,
    private activateAccountService: ActivateAccountService,
    private router: Router,
    ) { }

  ngOnInit() {
    const self = this;
    if (!firebase.apps.length){
      firebase.initializeApp(environment.firebaseConfig);
    }
    self.fnGetDefaultLanguage(function (lang_callback) {
      if (lang_callback) {
        firebase.database().ref('languages/' + lang_callback + '/activateAccount').on('value', function (snapshot) {
          self.DATA_LANG = snapshot.val();
        });
      }
    });
    // Note: Below 'queryParams' can be replaced with 'params' depending on your requirements
    this.activatedRoute.queryParams.subscribe(params => {
      const guid_user_active = params['param'];

      console.log('guid_user_active: ', guid_user_active);


      self.activateAccountService.fnHttpSetActivateAccountUser(guid_user_active).subscribe(r => {
        console.log('r: ', r);
        self.state_active_account = {
          'color': 'success',
          'message': 'success text',
        };
      }, err => {
        self.state_active_account = {
          'color': 'danger',
          'message': 'danger text',
        };
        console.log('err: ', err);
      });
    });
  }

  fnGetDefaultLanguage(callback){
    const self = this;
    let language_start = localStorage.getItem('language_app');
    if(language_start == null){
      firebase.database().ref('languages').on('value', function(snapshot) {
        let collection = Object.keys(snapshot.val());
        let indexofdata = collection.indexOf(self.lang_nav);
        if(indexofdata > -1){
          self.LANG_NAME = self.lang_nav;
          localStorage.setItem('language_app', self.LANG_NAME);
          callback(self.lang_nav);
        }else{
          firebase.database().ref('defaultLanguage').on('value', function(snapshot) {
            self.LANG_NAME = snapshot.val();
            localStorage.setItem('language_app', self.LANG_NAME);
            callback(self.LANG_NAME);
          });
        }
      });
    }else{
      self.LANG_NAME = language_start.toLowerCase();
      callback(self.LANG_NAME);
    }
  }

  fnRedirectPage() {
    this.submitted = true;
    setTimeout(() => {
      this.router.navigate(['auth/login']);
    }, 5000);  // 5s
  }

}
