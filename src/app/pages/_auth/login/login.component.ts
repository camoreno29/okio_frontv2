import { Component, OnInit } from '@angular/core';
import { NbLoginComponent, NbAuthJWTToken, NbAuthResult } from '@nebular/auth';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { environment } from '../../../../environments/environment.prod';
import * as firebase from 'firebase/app';
import 'firebase/database';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class NgxLoginComponent extends NbLoginComponent implements OnInit {

  // submitted = false;
  // errors: string[] = [];
  // text_errors: any = [];
  // messages: string[] = [];
  // user: any = {};
  // private index: number = 0;

  required: Boolean = true;
  minlength: Number = 8;
  maxlength: Number = 50;
  aria_invalid: Boolean = true;


  redirectDelay: number = 0;
  showMessages: any = {};
  strategy: string = '';

  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  submitted: boolean = false;
  rememberMe = false;

  session_state: any = '';
  token_valid: any = null;

  message_error_api: string = null;
  utilities: UtilitiesService;

  DATA_LANG: any = null;
  LANG_NAME: any = '';
  lang_nav: any =  (window.navigator.language.substring(0, 2)).toLowerCase();

  firebase_start: any = false;

  ngOnInit() {
    const self = this;

    // let fb_status = JSON.parse(localStorage.getItem('fb_status'));
    let fb_status = localStorage.getItem('fb_status');
    console.log('fb_status: ', fb_status);
    
    if (!firebase.apps.length){
      console.log('hola!!!!');
      firebase.initializeApp(environment.firebaseConfig);
      localStorage.setItem('fb_status', 'true');
    }

    self.fnGetDefaultLanguage(function (lang_callback) {
      if (lang_callback) {
        firebase.database().ref('languages/' + lang_callback + '/login').on('value', function (snapshot) {
          self.DATA_LANG = snapshot.val();
        });
      }
    });

    self.session_state = localStorage.getItem('rememberMe');
    self.token_valid = localStorage.getItem('auth_app_token')
    console.log('self.session_state: ', self.session_state);

    if(self.session_state == 'true' && self.token_valid != ''){
      self.router.navigateByUrl('pages/projects');
    }

    self.user.rememberMe = false;

    self.redirectDelay = self.getConfigValue('forms.login.redirectDelay');
    self.showMessages = self.getConfigValue('forms.login.showMessages');
    self.strategy = self.getConfigValue('forms.login.strategy');
    self.socialLinks = self.getConfigValue('forms.login.socialLinks');
    self.rememberMe = self.getConfigValue('forms.login.rememberMe');
    self.user.tLanguage = localStorage.getItem('language_app');
    console.log('self.lang_nav: ', self.lang_nav);

  }

  fnGetDefaultLanguage(callback){
    const self = this;
    let language_start = localStorage.getItem('language_app');
    if(language_start == null){
      firebase.database().ref('languages').on('value', function(snapshot) {
        let collection = Object.keys(snapshot.val());
        console.log(self.lang_nav.substring(0,2));
        let indexofdata = collection.indexOf(self.lang_nav);
        
        if(indexofdata > -1){
          self.LANG_NAME = self.lang_nav;
          localStorage.setItem('language_app', self.LANG_NAME);
          callback(self.lang_nav);
        }else{
          firebase.database().ref('defaultLanguage').on('value', function(snapshot) {
            self.LANG_NAME = snapshot.val();
            localStorage.setItem('language_app', self.LANG_NAME);
            callback(self.LANG_NAME);
          });
        }
      });
    }else{
      self.LANG_NAME = language_start.toLowerCase();
      callback(self.LANG_NAME);
    }
  }

  fnCheckRemember(){
    if (this.user.rememberMe) 
    {
      localStorage.setItem('rememberMe', 'true');
    } else {
      localStorage.setItem('rememberMe', 'false');
    }
  }

  login(): void {
    this.submitted = true;
    console.log('this.user: ', this.user);
    this.service.authenticate(this.strategy, this.user).subscribe((result: NbAuthResult) => {
      console.log('this.strategy: ', this.strategy);
      console.log('result: ', result);
      console.log('result.getMessages()[0]: ', result.getMessages()[0]['status']);
      
      if (result.getMessages()[0]['status'] === 200){
        console.log('result.getMessages()[0]: ', result.getMessages()[0]);
        const redirect = result.getRedirect();
        if (redirect) {
          this.submitted = false;
          setTimeout(() => {
            return this.router.navigateByUrl(redirect);
          }, this.redirectDelay);
        }
      } else if (result.getMessages()[0]['status'] === 206) {
        this.message_error_api = null;
        console.log('(206)==result.getMessages()[0]: ', result.getMessages()[0]);
        this.message_error_api = result.getMessages()[0]['body']['codMessage'];
        console.log('this.message_error_api: ', this.message_error_api);
        this.submitted = false;
      }
      
      this.cd.detectChanges();
    
    });

  }

  // getConfigValue(key: string): any {
  //   return getDeepFromObject(this.options, key, null);
  // }
}
