import { Component, OnInit, Injector } from '@angular/core';
import { NbRegisterComponent } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';
import { NbAuthService } from '@nebular/auth';
import { NbAuthResult } from '@nebular/auth';
import { getDeepFromObject } from '@nebular/auth/helpers';
import { environment } from '../../../../environments/environment.prod';
import * as firebase from 'firebase/app';
import 'firebase/database';


declare var grecaptcha: any;
@Component({
  selector: 'ngx-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})


export class NgxRegisterComponent extends NbRegisterComponent implements OnInit {
  siteKeyCaptcha: string = '6LefepcUAAAAAEhBKcNZje758JXbCyzvlowD7hHl';
  // siteKeyCaptcha: string = '6LdT0JgUAAAAAPvuxbHZL5PXMAf5HMyRzXugUxVl';
  // siteKeyCaptcha: string = '6LefepcUAAAAACk_aFG24Wtg6mUPPWC7baG3va8Q';
  terms_and_conditions: Boolean = true;
  captcha_valid: Boolean = null;
  display='none';

  redirectDelay: number = 2000;
  showMessages: any = {};
  strategy: string = '';

  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  user: any = {};

  DATA_LANG: any = null;
  LANG_NAME: any = '';
  lang_nav: any =  window.navigator.language;

  message_error_api: string = null;
  message_success: boolean = false;

  ngOnInit() {
    const self = this;
    if (!firebase.apps.length){
      firebase.initializeApp(environment.firebaseConfig);
    }
    self.fnGetDefaultLanguage(function (lang_callback) {
      if (lang_callback) {
        firebase.database().ref('languages/' + lang_callback + '/signUp').on('value', function (snapshot) {
          self.DATA_LANG = snapshot.val();
        });
      }
    });

    this.user.tlanguage = localStorage.getItem('language_app');
    this.redirectDelay = this.getConfigValue('forms.requestPassword.redirectDelay');
    this.showMessages = this.getConfigValue('forms.requestPassword.showMessages');
    this.strategy = this.getConfigValue('forms.requestPassword.strategy');
    grecaptcha.render('capcha_element', {
      'sitekey': this.siteKeyCaptcha,
    });
    window['getResponceCapcha'] = this.getResponceCapcha.bind(this);
  }

  fnGetDefaultLanguage(callback){
    const self = this;
    let language_start = localStorage.getItem('language_app');
    if(language_start == null){
      firebase.database().ref('languages').on('value', function(snapshot) {
        let collection = Object.keys(snapshot.val());
        let indexofdata = collection.indexOf(self.lang_nav);
        if(indexofdata > -1){
          self.LANG_NAME = self.lang_nav;
          localStorage.setItem('language_app', self.LANG_NAME);
          callback(self.lang_nav);
        }else{
          firebase.database().ref('defaultLanguage').on('value', function(snapshot) {
            self.LANG_NAME = snapshot.val();
            localStorage.setItem('language_app', self.LANG_NAME);
            callback(self.LANG_NAME);
          });
        }
      });
    }else{
      self.LANG_NAME = language_start.toLowerCase();
      callback(self.LANG_NAME);
    }
  }

  getResponceCapcha(captchaResponse: string) {
    console.log('captchaResponse: ', captchaResponse);
    this.verifyCaptcha(captchaResponse);
  }

  verifyCaptcha(captchaResponse: string) {
    console.log('captchaResponse: ', captchaResponse);
    if (captchaResponse) {
      this.captcha_valid = true;
    } else {
      this.captcha_valid = false;
      return false;
    }
  }

  fnValidateRequiredName(tFirstName, touched) {
    if (tFirstName === undefined && touched) {
      return true;
    }
  }

  fnValidateLengthName(lengthFirstName) {
    if (lengthFirstName > 0) {
      if (lengthFirstName < 3 || lengthFirstName > 50) {
        return true;
      } else {
        return false;
      }
    }
  }

  register(): void {
    this.submitted = true;
    this.service.register(this.strategy, this.user).subscribe((result: NbAuthResult) => {

      if (result.getMessages()[0]['status'] === 200){
        this.message_success = true;
        console.log('result.getMessages()[0]: ', result.getMessages()[0]);
        const redirect = result.getRedirect();
        if (redirect) {
          this.submitted = false;
          setTimeout(() => {
            return this.router.navigateByUrl(redirect);
          }, this.redirectDelay);
        }
      } else if (result.getMessages()[0]['status'] === 206) {
        this.message_error_api = null;
        console.log('(206)==result.getMessages()[0]: ', result.getMessages()[0]);
        this.message_error_api = result.getMessages()[0]['body']['codMessage'];
        console.log('this.message_error_api: ', this.message_error_api);
        this.submitted = false;
      }
      this.cd.detectChanges();
    });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }

  openModalDialog(){
    this.display='block';
  }

  closeModalDialog(){
  this.display='none';
  }

}
