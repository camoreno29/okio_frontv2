import { Component, OnInit } from '@angular/core';
import { NbRequestPasswordComponent, NbAuthResult } from '@nebular/auth';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { environment } from '../../../../environments/environment.prod';
import * as firebase from 'firebase/app';
import 'firebase/database';
@Component({
  selector: 'ngx-request-password',
  templateUrl: './request-password.component.html',
  styleUrls: ['./request-password.component.scss'],
})
export class NgxRequestPasswordComponent extends NbRequestPasswordComponent implements OnInit {

  required: Boolean = true;
  minlength: Number = 8;
  maxlength: Number = 50;
  aria_invalid: Boolean = true;
  message_error_api: string = null;

  redirectDelay: number = 6000;
  showMessages: any = {};
  strategy: string = '';

  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  submitted: boolean = false;
  rememberMe = false;

  code_error_api: string = null;
  message_success: boolean = false;
  DATA_LANG: any = null;
  LANG_NAME: any = '';
  lang_nav: any =  window.navigator.language;

  ngOnInit() {
    const self = this;
    if (!firebase.apps.length){
      firebase.initializeApp(environment.firebaseConfig);
    }

    self.fnGetDefaultLanguage(function (lang_callback) {
      if (lang_callback) {
        firebase.database().ref('languages/' + lang_callback + '/requestPass').on('value', function (snapshot) {
          self.DATA_LANG = snapshot.val();
        });
      }
    });

    this.user.tlanguage = localStorage.getItem('language_app');
    // this.redirectDelay = this.getConfigValue('forms.requestPassword.redirectDelay');
    this.showMessages = this.getConfigValue('forms.requestPassword.showMessages');
    this.strategy = this.getConfigValue('forms.requestPassword.strategy');
  }

  fnGetDefaultLanguage(callback){
    const self = this;
    let language_start = localStorage.getItem('language_app');
    if(language_start == null){
      firebase.database().ref('languages').on('value', function(snapshot) {
        let collection = Object.keys(snapshot.val());
        let indexofdata = collection.indexOf(self.lang_nav);
        if(indexofdata > -1){
          self.LANG_NAME = self.lang_nav;
          localStorage.setItem('language_app', self.LANG_NAME);
          callback(self.lang_nav);
        }else{
          firebase.database().ref('defaultLanguage').on('value', function(snapshot) {
            self.LANG_NAME = snapshot.val();
            localStorage.setItem('language_app', self.LANG_NAME);
            callback(self.LANG_NAME);
          });
        }
      });
    }else{
      self.LANG_NAME = language_start.toLowerCase();
      callback(self.LANG_NAME);
    }
  }

  requestPass(): void{
    this.submitted = true;
    this.service.requestPassword(this.strategy, this.user).subscribe((result: NbAuthResult) => {
      console.log('this.strategy: ', this.strategy);
      console.log('result: ', result);
      console.log('result.getMessages()[0]: ', result.getMessages()[0]['status']);
      if (result.getMessages()[0]['status'] === 200){
        this.message_success = true;
        console.log('result.getMessages()[0]: ', result.getMessages()[0]);
        const redirect = result.getRedirect();
        if (redirect) {
          this.submitted = false;
          setTimeout(() => {
            return this.router.navigateByUrl(redirect);
          }, this.redirectDelay);
        }
      } else if (result.getMessages()[0]['status'] === 206) {
        this.message_error_api = null;
        console.log('(206)==result.getMessages()[0]: ', result.getMessages()[0]);
        this.message_error_api = result.getMessages()[0]['body']['codMessage'];
        console.log('this.message_error_api: ', this.message_error_api);
        this.submitted = false;
      }
      
      this.cd.detectChanges();
      
    });
    
  }
}

/** */
