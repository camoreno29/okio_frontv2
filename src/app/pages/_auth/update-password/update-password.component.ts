import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { UpdatePasswordService } from '../../../shared/api/services/update-password.service';
import { NbTooltipModule } from '@nebular/theme';
import { environment } from '../../../../environments/environment.prod';
import * as firebase from 'firebase/app';
import 'firebase/database';

@Component({
  selector: 'ngx-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent implements OnInit {
  state_update_password: any = null;
  unamePattern: any = null;
  user: any = {};
  submitted: Boolean = false;
  messages_error: any = [];
  messages_success: any = [];

  DATA_LANG: any = null;
  LANG_NAME: any = '';
  lang_nav: any =  window.navigator.language;

  constructor(
    private activatedRoute: ActivatedRoute,
    private updatePasswordService: UpdatePasswordService,
    private router: Router,
  ) {}

  ngOnInit() {
    const self = this;
    if (!firebase.apps.length){
      firebase.initializeApp(environment.firebaseConfig);
    }
    self.fnGetDefaultLanguage(function (lang_callback) {
      if (lang_callback) {
        firebase.database().ref('languages/' + lang_callback + '/updatePassword').on('value', function (snapshot) {
          self.DATA_LANG = snapshot.val();
        });
      }
    });
    self.user['new_password'] = '';
    self.user['confirm_new_password'] = '';
    // Note: Below 'queryParams' can be replaced with 'params' depending on your requirements
    this.activatedRoute.queryParams.subscribe(params => {
      const guid_user_active = params['param'];

      console.log('guid_user_active: ', guid_user_active);


      self.updatePasswordService.fnHttpSetValidateTokenPasswordUser(guid_user_active).subscribe(r => {
        console.log('r: ', r);
        if (r.status !== 200) {
          self.messages_error = [{
            'state': 'Error',
            'color': 'danger',
            'description': 'Unauthorized to this action',
          }];
          this.fnRedirectPage(3000);
        }
      }, err => {
        self.messages_error = [{
          'state': 'Error',
          'color': 'danger',
          'description': 'New error',
        }];
        this.fnRedirectPage(3000);
        console.log('err: ', err);
      });
    });
  }

  updatePass(new_password: any, confirm_new_password: any) {
    console.log('confirm_new_password: ', confirm_new_password);
    console.log('new_password: ', new_password);
    const self = this;
    self.submitted = true;
    if ((new_password && confirm_new_password) && (new_password === confirm_new_password)) {
      const objectForm = {
        'tPassword': new_password,
        'tConfirmPassword': confirm_new_password,
      };
      console.log('objectForm: ', objectForm);
      self.updatePasswordService.fnHttpSetUpdatePasswordUser(objectForm).subscribe(r => {
        console.log('r: ', r);
        if (r.status === 200) {
          console.log('r.status: ', r.status);
          self.messages_success = [{
            'state': 'Success',
            'color': 'success',
            'description': 'Your password has been reset successfully',
          }];
          self.fnRedirectPage(3000);
        } else {
          self.messages_error = [{
            'state': 'Error',
            'color': 'danger',
            'description': 'New error',
          }];
          self.fnRedirectPage(3000);
        }
      }, err => {
        self.messages_error = [{
          'state': 'Error',
          'color': 'danger',
          'description': 'New error',
        }];
        console.log('err: ', err);
        self.fnRedirectPage(100);
      });
    } else {
      self.messages_error = [{
        'state': 'Error',
        'color': 'danger',
        'description': 'Passwords no concide',
      }];
      self.fnRedirectPage(3000);
      return false;
    }

  }

  fnRedirectPage(time: number) {
    this.submitted = true;
    setTimeout(() => {
      this.router.navigate(['auth/login']);
    }, time);  // 5s
  }

  fnGetDefaultLanguage(callback){
    const self = this;
    let language_start = localStorage.getItem('language_app');
    if(language_start == null){
      firebase.database().ref('languages').on('value', function(snapshot) {
        let collection = Object.keys(snapshot.val());
        let indexofdata = collection.indexOf(self.lang_nav);
        if(indexofdata > -1){
          self.LANG_NAME = self.lang_nav;
          localStorage.setItem('language_app', self.LANG_NAME);
          callback(self.lang_nav);
        }else{
          firebase.database().ref('defaultLanguage').on('value', function(snapshot) {
            self.LANG_NAME = snapshot.val();
            localStorage.setItem('language_app', self.LANG_NAME);
            callback(self.LANG_NAME);
          });
        }
      });
    }else{
      self.LANG_NAME = language_start.toLowerCase();
      callback(self.LANG_NAME);
    }
  }

}
