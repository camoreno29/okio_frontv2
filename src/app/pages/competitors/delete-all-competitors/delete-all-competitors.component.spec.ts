import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAllCompetitorsComponent } from './delete-all-competitors.component';

describe('DeleteAllCompetitorsComponent', () => {
  let component: DeleteAllCompetitorsComponent;
  let fixture: ComponentFixture<DeleteAllCompetitorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteAllCompetitorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAllCompetitorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
