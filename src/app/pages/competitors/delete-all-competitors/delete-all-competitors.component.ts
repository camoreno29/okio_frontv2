import { Component, OnInit, Input } from '@angular/core';

import { NbDialogRef } from '@nebular/theme';

import { Router, ActivatedRoute } from '@angular/router';

import { StateService } from '../../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';

import { CompetitorsService } from '../../../shared/api/services/competitors.service';

@Component({
  selector: 'ngx-delete-all-competitors',
  templateUrl: './delete-all-competitors.component.html',
  styleUrls: ['./delete-all-competitors.component.scss'],
})
export class DeleteAllCompetitorsComponent implements OnInit {

  @Input() id_version: String;
  Competitor_data: any = {};
  current_payload: string = null;
  submitted: Boolean = false;
  
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    protected ref: NbDialogRef<DeleteAllCompetitorsComponent>,
    private competitorsService: CompetitorsService,
  ) { }

  dismiss() {
    this.ref.close();
  }


  ngOnInit() {
    const self = this;

    self.utilitiesService.fnGetLanguage('competitors', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.Competitor_data.id_version = this.id_version;
    console.log('this.Competitor_data: ', this.Competitor_data);

    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
      }
    });
  }

  fnDeleteAllCompetitors(Competitor_data) {
    this.submitted = true;
    console.log('Competitor_data: ', Competitor_data);
    this.competitorsService.fnDeleteAllCompetitors(this.current_payload, Competitor_data.id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessDeleteAllComp.text );
        this.dismiss();
      }
      if (r.status == 206) {
        this.submitted = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }


  fnCancelDeleteCompetitor() {
    this.dismiss();
  }

}
