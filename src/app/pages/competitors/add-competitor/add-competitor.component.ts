import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CompetitorsService } from '../../../shared/api/services/competitors.service';


@Component({
  selector: 'ngx-add-competitor',
  templateUrl: './add-competitor.component.html',
  styleUrls: ['./add-competitor.component.scss']
})
export class AddCompetitorComponent implements OnInit {

  submitted: boolean = false;
  current_payload: any = null;
  competitor_data: any = {};

  @Input() id_version: string;
    
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private competitorsService: CompetitorsService,
    protected ref: NbDialogRef<AddCompetitorComponent>,
  ) { }

  ngOnInit() {
    const self = this;

    self.utilitiesService.fnGetLanguage('competitors', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
        console.log('id_version: ', this.id_version);
      }
    });
  }

  dismiss() {
    this.ref.close();
  }

  fnCreateNewCompetitor(competitor_data) {
    this.submitted = true;
    const data_object = {
      'iIDCompetitor': 0,
      'iIDVersion': parseInt(this.id_version, 10),
      'tCompetitorName': competitor_data.tCompetitorName,
      'tLanguage': localStorage.getItem('language_app'),
    };
    console.log('data_object: ', data_object);

    this.competitorsService.fnSetCreateNewCompetitor(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.competitor_data = {};
        this.submitted = false;
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessCreateComp.text , 'fas fa-window-close');
        this.dismiss();
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(null, r.body.codMessage);
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }


  fnCancelCreateNewCompetitor() {
    this.submitted = false;
    this.dismiss();
  }

}
