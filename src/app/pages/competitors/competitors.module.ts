import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxPaginationModule } from 'ngx-pagination';

// import { CompetitorsComponent } from './competitors.component';
import { AddCompetitorComponent } from './add-competitor/add-competitor.component';
import { DeleteCompetitorsComponent } from './delete-competitors/delete-competitors.component';
import { EditCompetitorsComponent } from './edit-competitors/edit-competitors.component';
import { DeleteAllCompetitorsComponent } from './delete-all-competitors/delete-all-competitors.component';

const ENTRY_COMPONENTS = [
  AddCompetitorComponent,
  EditCompetitorsComponent,
  DeleteCompetitorsComponent,
  DeleteAllCompetitorsComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    TooltipModule.forRoot(),
    ModalModule,
    NgxPaginationModule,
  ],
  declarations: [
    // CompetitorsComponent,
    AddCompetitorComponent,
    EditCompetitorsComponent,
    DeleteCompetitorsComponent,
    DeleteAllCompetitorsComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})

export class CompetitorsModule { }
