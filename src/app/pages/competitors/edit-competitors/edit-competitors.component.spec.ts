import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCompetitorsComponent } from './edit-competitors.component';

describe('EditCompetitorsComponent', () => {
  let component: EditCompetitorsComponent;
  let fixture: ComponentFixture<EditCompetitorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCompetitorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCompetitorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
