import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CompetitorsService } from '../../../shared/api/services/competitors.service';

@Component({
  selector: 'ngx-edit-competitors',
  templateUrl: './edit-competitors.component.html',
  styleUrls: ['./edit-competitors.component.scss'],
})
export class EditCompetitorsComponent implements OnInit {

  submitted: boolean = false;
  current_payload: any = null;
  competitor_data: any = {};

  @Input() iIDCompetitor: String;
  @Input() tCompetitorName: String;
  @Input() id_version: String;
  
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private competitorsService: CompetitorsService,
    protected ref: NbDialogRef<EditCompetitorsComponent>,
  ) { }

  ngOnInit() {
    const self = this;

    self.utilitiesService.fnGetLanguage('competitors', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        const competitor_data = {
          'iIDCompetitor': this.iIDCompetitor,
          'id_version': this.id_version,
          'tCompetitorName': this.tCompetitorName,         
        };
        this.competitor_data = competitor_data;
      }
    });
  }

  dismiss() {
    this.ref.close();
  }

  fnUpdateCompetitor(competitor_data) {
    this.submitted = true;

    const data_object = {
      'iIDCompetitor': competitor_data.iIDCompetitor,
      'id_version': competitor_data.id_version,
      'tCompetitorName': competitor_data.tCompetitorName,
      'tLanguage': localStorage.getItem('language_app'),
    };
    console.log('data_object: ', data_object);

    this.competitorsService.fnHttpSetEditDataCompetitor(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.competitor_data = {};
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessUpdateComp.text );
        this.dismiss();
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(null, r.body.codMessage);
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnCancelEditCompetitor() {
    this.submitted = false;
    this.dismiss();
  }

}
