import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteCompetitorsComponent } from './delete-competitors.component';

describe('DeleteCompetitorsComponent', () => {
  let component: DeleteCompetitorsComponent;
  let fixture: ComponentFixture<DeleteCompetitorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteCompetitorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteCompetitorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
