import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { StateService } from '../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';

import { UtilitiesService } from '../../shared/api/services/utilities.service';
import { AddCompetitorComponent } from './add-competitor/add-competitor.component';
import { EditCompetitorsComponent } from './edit-competitors/edit-competitors.component';
import { DeleteCompetitorsComponent } from './delete-competitors/delete-competitors.component';
import { DeleteAllCompetitorsComponent } from './delete-all-competitors/delete-all-competitors.component';

import { environment } from '../../../environments/environment';
import { CompetitorsService } from '../../shared/api/services/competitors.service';

import { ModalsComponent } from '../../shared/components/modals/modals.component';

@Component({
  selector: 'ngx-competitors',
  templateUrl: './competitors.component.html',
  styleUrls: ['./competitors.component.scss'],
})
export class CompetitorsComponent implements OnInit {

  current_payload: string = null;
  id_version: any = null;
  competitors: any = null;
  competitors_sent: any = null;
  list_competitors: any = null;
  competitors_original_collection: any = null;
  state_sort_data: string = 'ASC';
  url_host: any = environment.apiUrl;
  numItemsPage: any = null;
  currentPage: any = null;
  fileToUpload: File = null;
  file_import_input: any = null;
  search_input: any = '';

  loadingChart: boolean = true;

  layout_state: string = 'show_loading_layout'; // State: 'show_loading_layout' => Display layout pulse loading, 'show_table_layout' => Display table results, 'show_empty_data_layout' => Display layout empty data results
    
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;
  DATA_LANG_GENERAL: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    private competitorsService: CompetitorsService,
  ) { }

  ngOnInit() {
    const self = this;

    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => { 
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    // });

    self.currentPage = 1;
    self.numItemsPage = 12;

    self.route.params.subscribe(params => {
      console.log('params: ', params);
      if (params.id_version) {
        self.id_version = params.id_version;
        self.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          console.log('token: ', token);
          if (token.isValid()) {
            // here we receive a payload from the token and assigne it to our `user` variable
            self.current_payload = token.getValue();
            console.log('self.current_payload: ', self.current_payload);
            if (self.current_payload) {
              self.fnGetAllCompetitors(self.current_payload, self.id_version);
              return false;
            }
          }
        });
      }
    });

  }

  fnGetAllCompetitors(current_payload, id_version?) {
    const self = this;
    self.layout_state = 'show_loading_layout';
    self.list_competitors = [];
    self.competitorsService.fnGetAllCompetitorsByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.competitors = r.body;
        self.competitors_sent = r.body;
        self.list_competitors = JSON.parse(JSON.stringify(r.body));
        self.competitors_original_collection = JSON.parse(JSON.stringify(r.body));     
        this.loadingChart = false;
        console.log('this.projects : ', self.competitors);
        if(self.competitors.length > 0) {
          self.layout_state = 'show_table_layout';
        } else {
          self.layout_state = 'show_empty_data_layout';
          // alert('No existen catergorias registradas para esta version!');
        }
      }
    }, err => {
      console.log('err: ', err);
    });
  }


  fnFilterCompetitors(list_competitors, text_typing) {    
    console.log('text_typing: ', text_typing);
    console.log('Competitors: ', list_competitors);
    this.filterItems(list_competitors, text_typing);
  }

  filterItems(competitors, text_typing, field?) {
    const self = this;
    const toSearch = text_typing.toLowerCase();
    if (toSearch) {
      competitors = JSON.parse(JSON.stringify(self.competitors_original_collection));
      self.utilitiesService.fnGetDataFilter(competitors, toSearch, function (data_collection) {
        self.list_competitors = data_collection;
        console.log('data_collection: ', data_collection);
      });
    } else {
      self.list_competitors = JSON.parse(JSON.stringify(self.competitors_original_collection));
    }
  }


  showModalAddCompetitors() {
    const data_competitor = {
      'id_version': this.id_version,
    };
    console.log('data_competitor: ', data_competitor);
    this.dialogService.open(AddCompetitorComponent, { context: data_competitor }).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllCompetitors(this.current_payload, this.id_version);
    });
  }


  fnUpdateCompetitors(data_competitor) {
    data_competitor.id_version = this.id_version;
    console.log('data_competitor: ', data_competitor);
    this.dialogService.open(EditCompetitorsComponent, { context: data_competitor }).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllCompetitors(this.current_payload, this.id_version);
    });
  }

  fnDeleteCompetitors(data_competitor) {
    data_competitor.id_version = this.id_version;
    console.log('data_competitor: ', data_competitor);
    this.dialogService.open(DeleteCompetitorsComponent, { context: data_competitor }).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllCompetitors(this.current_payload, this.id_version);
    });
  }

  fnDeleteAllCompetitors() {
    const data_competitor = {
      'id_version': this.id_version,
    };
    console.log('data_competitor: ', data_competitor);
    this.dialogService.open(DeleteAllCompetitorsComponent, { context: data_competitor }).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllCompetitors(this.current_payload, this.id_version);
    });
  }

  fnExportCompetitors() {
    const lang = localStorage.getItem('language_app');
    window.open(
      this.url_host + '/api/Competitors/GetExportCompetitors?iIDVersion=' + this.id_version + '&language=' + lang,
      '_blank');
  }


  sortColumn(field) {
    console.log(`sortColumn(field)`);
    console.log('field: ', field);
    const self = this;
    switch (field) {
      case 'Competitor_name':
        if (self.state_sort_data === 'ASC') {
          self.list_competitors.sort(function (a, b) { return (a.tCompetitorName > b.tCompetitorName) ? 1 : ((b.tCompetitorName > a.tCompetitorName) ? -1 : 0); });
          self.state_sort_data = 'DESC';
        } else {
          self.list_competitors.sort(function (a, b) { return (b.tCompetitorName > a.tCompetitorName) ? 1 : ((a.tCompetitorName > b.tCompetitorName) ? -1 : 0); });
          self.state_sort_data = 'ASC';
        }
        break;
    }
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.uploadFileToActivity(this.fileToUpload);
  }

  uploadFileToActivity(fileToUpload) {
    console.log('fileToUpload: ', fileToUpload);
    if (fileToUpload) {
      this.loadingChart = true;
      const end_point_url = '/api/Competitors/PostImportCompetitors';
      const parameter = 'iIDVersion';
      this.utilitiesService.fnHttSetUploadFile(this.current_payload, this.fileToUpload, this.id_version, end_point_url, parameter).subscribe(data => {
        console.log('data: ', data);
        if (data.status == 200) {
          this.loadingChart = false;
          this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessImport.text, 'fas fa-window-close');
          this.file_import_input = null;
          this.fnGetAllCompetitors(this.current_payload, this.id_version);
        }
        if (data.status == 206) {
          this.loadingChart = false;
          console.log('data.body.codMessage', data.body.message);
          const error = this.utilitiesService.fnSetErrors(null, data.body.message);
          this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
        }
      }, error => {
        this.loadingChart = false;
        console.log(error);
      });
    }
    this.file_import_input = null;
  }

  /*********Show modal parameters function *********/

  open(_title,_body) {
    this.dialogService.open(ModalsComponent, {
      context: {
        title: _title,
        body:_body
      },
    });
  }

  /********Funcion que trae los textos del modulo competidores*********/
  fnGetLanguage(){
    const self = this;
    self.utilitiesService.fnGetLanguage('competitors', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module,
      self.DATA_LANG_GENERAL = res_lang.general
    });
  }

}
