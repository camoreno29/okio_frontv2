import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletePricesListComponent } from './delete-prices-list.component';

describe('DeletePricesListComponent', () => {
  let component: DeletePricesListComponent;
  let fixture: ComponentFixture<DeletePricesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeletePricesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletePricesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
