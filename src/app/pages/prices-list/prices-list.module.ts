import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxPaginationModule } from 'ngx-pagination';

// import { PricesListComponent } from './prices-list.component';
import { AddPriceListComponent } from './add-price-list/add-price-list.component';
import { DeletePricesListComponent } from './delete-prices-list/delete-prices-list.component';
import { DeleteAllPricesListComponent } from './delete-all-prices-list/delete-all-prices-list.component';
import { EditPricesListComponent } from './edit-prices-list/edit-prices-list.component';

const ENTRY_COMPONENTS = [
  AddPriceListComponent,
  EditPricesListComponent,
  DeletePricesListComponent,
  DeleteAllPricesListComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    TooltipModule.forRoot(),
    ModalModule,
    NgxPaginationModule,
  ],
  declarations: [
    // PricesListComponent,
    AddPriceListComponent,
    EditPricesListComponent,
    DeletePricesListComponent,
    DeleteAllPricesListComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})

export class PricesListModule { }
