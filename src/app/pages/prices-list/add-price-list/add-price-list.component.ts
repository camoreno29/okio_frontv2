import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { PricesListService } from '../../../shared/api/services/prices-list.service';

@Component({
  selector: 'ngx-add-price-list',
  templateUrl: './add-price-list.component.html',
  styleUrls: ['./add-price-list.component.scss'],
})
export class AddPriceListComponent implements OnInit {

  submitted: boolean = false;
  current_payload: any = null;
  Pricelist_data: any = {};

  @Input() id_version: string;
  
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private pricesListService: PricesListService,
    protected ref: NbDialogRef<AddPriceListComponent>,
  ) { }

  ngOnInit() {

    const self = this;

    /********Funcion que trae los textos del modulo lista de precios*********/
    self.utilitiesService.fnGetLanguage('priceList', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
        console.log('id_version: ', this.id_version);
      }
    });
  }

  dismiss() {
    this.ref.close();
  }

  fnCreateNewPricelist(Pricelist_data) {
    this.submitted = true;
    const data_object = {
      'iIDPriceList': 0,
      'iIDVersion': parseInt(this.id_version, 10),
      'tPriceListName': Pricelist_data.tPriceListName,
      'tLanguage': localStorage.getItem('language_app'),
    };
    console.log('data_object: ', data_object);

    this.pricesListService.fnSetCreateNewPricesList(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.Pricelist_data = {};
        this.submitted = false;
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessCreatePriceL.text);
        this.dismiss();
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(null, r.body.codMessage);
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }


  fnCancelCreateNewPricesList() {
    this.submitted = false;
    this.dismiss();
  }


}
