import { Component, OnInit, Input } from '@angular/core';

import { NbDialogRef } from '@nebular/theme';
import { Router, ActivatedRoute } from '@angular/router';

import { StateService } from '../../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';

import { PricesListService } from '../../../shared/api/services/prices-list.service';

@Component({
  selector: 'ngx-delete-all-prices-list',
  templateUrl: './delete-all-prices-list.component.html',
  styleUrls: ['./delete-all-prices-list.component.scss']
})
export class DeleteAllPricesListComponent implements OnInit {

  @Input() id_version: String;
  priceslist_data: any = {};
  current_payload: string = null;
  submitted: Boolean = false;
  search_input: any = '';

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    protected ref: NbDialogRef<DeleteAllPricesListComponent>,
    private pricesListService: PricesListService,
  ) { }

  dismiss() {
    this.ref.close();
  }

  ngOnInit() {
    
    const self = this;

    /********Funcion que trae los textos del modulo lista de precios*********/
    self.utilitiesService.fnGetLanguage('priceList', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.priceslist_data.id_version = this.id_version;
    console.log('this.priceslist_data: ', this.priceslist_data);

    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
      }
    });
  }

  fnDeleteAllPricesList(priceslist_data) {
    this.submitted = true;
    console.log('priceslist_data: ', priceslist_data);
    this.pricesListService.fnDeleteAllPricesList(this.current_payload, priceslist_data.id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessDeleteAllPriceL.text);
        this.dismiss();
      }
      if (r.status == 206) {
        this.submitted = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }


  fnCancelDeletePricesList() {
    this.dismiss();
  }

}
