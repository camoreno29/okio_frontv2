import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAllPricesListComponent } from './delete-all-prices-list.component';

describe('DeleteAllPricesListComponent', () => {
  let component: DeleteAllPricesListComponent;
  let fixture: ComponentFixture<DeleteAllPricesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteAllPricesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAllPricesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
