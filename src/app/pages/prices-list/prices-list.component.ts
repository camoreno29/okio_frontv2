import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { StateService } from '../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';

import { UtilitiesService } from '../../shared/api/services/utilities.service';
import { AddPriceListComponent } from './add-price-list/add-price-list.component';
import { EditPricesListComponent } from './edit-prices-list/edit-prices-list.component';
import { DeletePricesListComponent } from './delete-prices-list/delete-prices-list.component';
import { DeleteAllPricesListComponent } from './delete-all-prices-list/delete-all-prices-list.component';

import { environment } from '../../../environments/environment';
import { PricesListService } from '../../shared/api/services/prices-list.service';

import { ModalsComponent } from '../../shared/components/modals/modals.component';

@Component({
  selector: 'ngx-prices-list',
  templateUrl: './prices-list.component.html',
  styleUrls: ['./prices-list.component.scss'],
})
export class PricesListComponent implements OnInit {

  current_payload: string = null;
  id_version: any = null;
  prices_list: any = null;
  price_list_sent: any = null;
  obj_prices_list: any = null;
  price_list_original_collection: any = null;
  url_host: any = environment.apiUrl;
  state_sort_data: string = 'ASC';
  numItemsPage: any = null;
  currentPage: any = null;
  fileToUpload: File = null;
  file_import_input: any = null;
  submitted: Boolean = false;
  search_input: any = '';

  loadingChart: boolean = true;

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;
  DATA_LANG_GENERAL: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    private pricesListService: PricesListService,
  ) { }

  ngOnInit() {

    const self = this;
    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => { 
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    // });

    self.currentPage = 1;
    self.numItemsPage = 12;

    self.route.params.subscribe(params => {
      console.log('params: ', params);
      if (params.id_version) {
        self.id_version = params.id_version;
        self.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          console.log('token: ', token);
          if (token.isValid()) {
            // here we receive a payload from the token and assigne it to our `user` variable
            self.current_payload = token.getValue();
            console.log('self.current_payload: ', self.current_payload);
            if (self.current_payload) {
              self.fnGetAllPricesListByVersion(self.current_payload, self.id_version);
              return false;
            }
          }
        });
      }
    });
  }

  fnGetAllPricesListByVersion(current_payload, id_version?) {
    const self = this;
    self.obj_prices_list = [];
    self.pricesListService.fnGetAllPricesListByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.prices_list = r.body;
        self.price_list_sent = r.body;
        self.obj_prices_list = JSON.parse(JSON.stringify(r.body));
        self.price_list_original_collection = JSON.parse(JSON.stringify(r.body));
        this.loadingChart = false;
        // self.projects_original_collection = JSON.parse(JSON.stringify(r.body));
        // console.log('self.obj_prices_list : ', self.obj_prices_list);

        // self.obj_prices_list = [
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        //   {iIDPriceList: 59, iIDVersion: 4, tPriceListName: "Price list Roojo"},
        // ];
      }
    }, err => {
      console.log('err: ', err);
    });
  }


  fnFilterPrices_list(obj_prices_list, text_typing) {
    console.log('text_typing: ', text_typing);
    console.log('Prices list: ', obj_prices_list);
    this.filterItems(obj_prices_list, text_typing);
  }

  filterItems(obj_prices_list, text_typing, field?) {
    const self = this;
    const toSearch = text_typing.toLowerCase();
    if (toSearch) {
      obj_prices_list = JSON.parse(JSON.stringify(self.price_list_original_collection));
      self.utilitiesService.fnGetDataFilter(obj_prices_list, toSearch, function (data_collection) {
        self.obj_prices_list = data_collection;
        console.log('self.prices_list: ', self.obj_prices_list);
        console.log('data_collection: ', data_collection);
      });
    } else {
      self.obj_prices_list = JSON.parse(JSON.stringify(self.price_list_original_collection));
    }
  }


  showModalAddprices_list() {
    const data_prices_list = {
      'id_version': this.id_version,
    };
    console.log('data_prices_list: ', data_prices_list);
    this.dialogService.open(AddPriceListComponent, { context: data_prices_list }).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllPricesListByVersion(this.current_payload, this.id_version);
    });
  }


  fnUpdateprices_list(data_prices_list) {
    data_prices_list.id_version = this.id_version;
    console.log('data_prices_list: ', data_prices_list);
    this.dialogService.open(EditPricesListComponent, { context: data_prices_list }).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllPricesListByVersion(this.current_payload, this.id_version);
    });
  }

  fnDeleteprices_list(data_prices_list) {
    data_prices_list.id_version = this.id_version;
    console.log('data_prices_list: ', data_prices_list);
    this.dialogService.open(DeletePricesListComponent, { context: data_prices_list }).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllPricesListByVersion(this.current_payload, this.id_version);
    });
  }

  fnDeleteAllprices_list() {
    const data_prices_list = {
      'id_version': this.id_version,
    };
    console.log('data_prices_list: ', data_prices_list);
    this.dialogService.open(DeleteAllPricesListComponent, { context: data_prices_list }).onClose.subscribe((res) => {
      this.fnGetAllPricesListByVersion(this.current_payload, this.id_version);
    });
  }

  fnExportprices_list() {
    const lang = localStorage.getItem('language_app');
    window.open(
      this.url_host + '/api/PriceLists/GetExportPriceLists?iIDVersion=' + this.id_version + '&language=' + lang,
      '_blank');
  }

  sortColumn(field) {
    console.log(`sortColumn(field)`);
    console.log('field: ', field);
    const self = this;
    switch (field) {
      case 'prices_list_name':
        if (self.state_sort_data === 'ASC') {
          self.obj_prices_list.sort(function (a, b) { return (a.tPriceListName > b.tPriceListName) ? 1 : ((b.tPriceListName > a.tPriceListName) ? -1 : 0); });
          self.state_sort_data = 'DESC';
        } else {
          self.obj_prices_list.sort(function (a, b) { return (b.tPriceListName > a.tPriceListName) ? 1 : ((a.tPriceListName > b.tPriceListName) ? -1 : 0); });
          self.state_sort_data = 'ASC';
        }
        break;
    }
  }


  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.uploadFileToActivity(this.fileToUpload);
  }

  uploadFileToActivity(fileToUpload) {
    console.log('fileToUpload: ', fileToUpload);
    if (fileToUpload) {
      this.loadingChart = true;
      const end_point_url = '/api/PriceLists/PostImportPriceLists';
      const parameter = 'iIDVersion';
      this.utilitiesService.fnHttSetUploadFile(this.current_payload, this.fileToUpload, this.id_version, end_point_url, parameter).subscribe(data => {
        console.log('data: ', data);
        if (data.status == 200) {
          this.loadingChart = false;
          this.file_import_input = null;
          this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessImport.text);
          this.fnGetAllPricesListByVersion(this.current_payload, this.id_version);
        }
        if (data.status == 206) {
          this.loadingChart = false;
          const error = this.utilitiesService.fnSetErrors(null, data.body.message);
          console.log('error: ', error);
          this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
        }
      }, error => {
        this.loadingChart = false;
        console.log(error);
      });
      this.file_import_input = null;
    }
  }

  /*********Show modal parameters function *********/

  open(_title, _body) {
    this.dialogService.open(ModalsComponent, {
      context: {
        title: _title,
        body: _body
      },
    });
  }

  /********Funcion que trae los textos del modulo lista de precios*********/
  fnGetLanguage(){
    const self = this;
    self.utilitiesService.fnGetLanguage('priceList', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module,
      self.DATA_LANG_GENERAL = res_lang.general
    });
  }

}
