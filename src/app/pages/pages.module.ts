import { NgModule } from '@angular/core';

import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { ModalOverlaysModule } from './modal-overlays/modal-overlays.module';
import { NbTooltipModule } from '@nebular/theme';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { GoogleChartsModule } from 'angular-google-charts';

import { ModalsComponent } from '../shared/components/modals/modals.component';

// Custom pages modules
import { BaseModule } from './base/base.module';
import { MembersModule } from './members/members.module';
import { ProjectsModule } from './projects/projects.module';
import { VersionsModule } from './versions/versions.module';
import { GroupsModule } from './groups/groups.module';
import { AddCompanyModule } from './company/add-company/add-company.module';
import { MyAccountModule } from './user/my-account/my-account.module';
import { HomeModule } from './home/home.module';
import { SettingsVersionModule } from './versions/settings-version/settings-version.module';
import { SettingsCompanyModule } from './company/settings-company/settings-company.module';
import { ListCompaniesModule } from './company/list-companies/list-companies.module';
import { SettingsProjectModule } from './projects/settings-project/settings-project.module';
import { CategoriesModule } from './categories/categories.module';
import { CompetitorsModule } from './competitors/competitors.module';
import { PricesListModule } from './prices-list/prices-list.module';
import { RatingPerceivedModule } from './rating-perceived/rating-perceived.module';
import { ProductInformationModule } from './product-information/product-information.module';
import { MarketPriceModule } from './market-price/market-price.module';
import { RatingProductTypeModule } from './rating-product-type/rating-product-type.module';
import { ElasticityProductModule } from './elasticity-product/elasticity-product.module';
import { PriceRelationshipseModule } from './price-relationships/price-relationships.module';
import { PriceCalculationSummaryModule } from './price-calculation-summary/price-calculation-summary.module';
import { ForecastResultsModule } from './forecast-results/forecast-results.module';
import { PriceComparisonModule } from './price-comparison/price-comparison.module';
import { PivotTableModule } from './pivot-table/pivot-table.module';
import { BalancePointModule } from './balance-point/balance-point.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { TermsConditionsModule } from './terms-conditions/terms-conditions.module';
import { CPanelModule } from './c-panel/c-panel.module';
import { CardsModule } from '../shared/components/cards/cards.module';
import { NotificationsModule } from './notifications/notifications.module';

const PAGES_COMPONENTS = [
  PagesComponent,
  ModalsComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    MiscellaneousModule,
    ThemeModule,
    NbTooltipModule,
    TooltipModule.forRoot(),
    GoogleChartsModule.forRoot(),
    // Custome pages modules
    BaseModule,
    MembersModule,
    ProjectsModule,
    VersionsModule,
    GroupsModule,
    AddCompanyModule,
    MyAccountModule,
    ModalOverlaysModule,
    HomeModule,
    SettingsVersionModule,
    SettingsCompanyModule,
    ListCompaniesModule,
    SettingsProjectModule,
    CategoriesModule,
    CompetitorsModule,
    PricesListModule,
    RatingPerceivedModule,
    ProductInformationModule,
    MarketPriceModule,
    RatingProductTypeModule,
    ElasticityProductModule,
    PriceRelationshipseModule,
    PriceCalculationSummaryModule,
    ForecastResultsModule,
    PriceComparisonModule,
    PivotTableModule,
    BalancePointModule,
    DashboardModule,
    TermsConditionsModule,
    CPanelModule,
    CardsModule,
    NotificationsModule,
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ],
  entryComponents: [
    ...PAGES_COMPONENTS,
  ],
})
export class PagesModule {
}
