import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { NotificationsComponent } from './notifications.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [NotificationsComponent],
  imports: [ThemeModule, NgSelectModule, FormsModule, NgxPaginationModule]
})
export class NotificationsModule { }
