import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

import { StateService } from '../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';
import { environment } from '../../../environments/environment';
import { ConfigService } from '../../shared/api/services/config.service';

import { UtilitiesService } from '../../shared/api/services/utilities.service';

@Component({
  selector: 'ngx-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  current_payload: string = null;
  id_version: any = null;

  currentPage: number = 1;
  numItemsPage: number = 10;

  notifications_collection: any = null;
  notifications_original_collection: any = null;

  loadingChart: boolean = true;

  DATA_LANG: any = null;
  DATA_LANG_GENERAL: any = null;

  search_input: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    private configService: ConfigService,
  ) { }

  ngOnInit() {

    const self = this;
    self.fnGetLanguage();

    self.currentPage = 1;
    self.numItemsPage = 10;

    const id_version = sessionStorage.getItem('id_version');

    if (id_version) {
      self.id_version = id_version;
      self.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
        console.log('token: ', token);
        if (token.isValid()) {
          // here we receive a payload from the token and assigne it to our `user` variable
          self.current_payload = token.getValue();
          console.log('self.current_payload: ', self.current_payload);
          if (self.current_payload) {
            self.fnGetAllNotificationsByVersion(self.current_payload, self.id_version);
            return false;
          }
        }
      });
    }

  }


  fnGetAllNotificationsByVersion(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);

    this.configService.fnHttpGetAllNotificationsByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.notifications_collection = JSON.parse(JSON.stringify(r.body.reverse()));
        this.notifications_original_collection = JSON.parse(JSON.stringify(r.body.reverse()));
        this.loadingChart = false;
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnFilterNotifications(notifications, text_typing) {
    console.log('text_typing: ', text_typing);
    console.log('categories: ', notifications);
    this.filterItems(notifications, text_typing);
  }

  filterItems(notifications, text_typing, field?) {
    const self = this;
    const toSearch = text_typing.toLowerCase();
    if (toSearch) {
      notifications = JSON.parse(JSON.stringify(self.notifications_original_collection));
      self.utilitiesService.fnGetDataFilter(notifications, toSearch, function (data_collection) {
        self.notifications_collection = data_collection;
        console.log('data_collection: ', data_collection);
        console.log('self.notifications_collection: ', self.notifications_collection);
      });
    } else {
      self.notifications_collection = JSON.parse(JSON.stringify(self.notifications_original_collection));
    }
  }


  fnGetLanguage() {
    const self = this;
    self.utilitiesService.fnGetLanguage('categories', function (res_lang) {
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module;
      self.DATA_LANG_GENERAL = res_lang.general;
    });
  }

}
