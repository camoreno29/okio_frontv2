import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { GroupService } from '../../../shared/api/services/group.service';
import { MenuService } from '../../../shared/api/services/menu.service';

@Component({
  selector: 'ngx-add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.scss']
})
export class AddGroupComponent implements OnInit {

  group_data: any = {};
  current_payload: any = null;
  title: string = null;
  submitted: boolean = false;
  id_company: any = '';
  id_project: any = '';

  option_menu: any = 1;
  items_menu: any = [];
  items_menu_original_list: any = [];
  show_cards: any = 0;
  items_access_collection: any = [];

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;
  DATA_LANG_MENU: any = null;
  DATA_LANG_CARDS: any = null;

  constructor(
    private groupService: GroupService,
    private menuService: MenuService,
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    protected ref: NbDialogRef<AddGroupComponent>) {
      console.log('title: ', this.title);
      this.id_project = sessionStorage.getItem('id_project');
    }

  dismiss() {
    this.ref.close();
  }

  ngOnInit() {

    const self = this;

    self.utilitiesService.fnGetLanguage('profiles', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    self.utilitiesService.fnGetLanguage('menu', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG_MENU = res_lang.module
    });
    self.utilitiesService.fnGetLanguage('cardsMenu', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG_CARDS = res_lang.module
    });

    console.log('this.route.params: ', this.route.params);
    this.route.params.subscribe(params => {
      console.log('params: ', params);
    });

    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
        this.fnGetAllItemsGroup(this.current_payload);
      }
    });
  }

  fnCreateNewGroup(group_data, id_project?) {
    console.log('group_data: ', group_data);

    // let data_object = {
    //   'iIDCompany': 0,
    //   'tCompanyLogo': 'url_company_logo_',
    //   'tCompanyName': company.tCompanyName,
    // };
    // const id_company = sessionStorage.getItem('id_company');
    // this.id_company = sessionStorage.getItem('id_company');
    this.submitted = true;
    // let data_object = {
    //   'iIDProject': parseInt(this.id_project),
    //   'tProjectGroupName': group_data.tProjectGroupName,
    //   'bAdmin': false,
    //   'dtCreatedDate': null,
    //   'projectGroupUsers': [],
    // };
    const data_object = {
      'iIDProjectGroup': 0,
      'iIDProject': parseInt(this.id_project),
      'tProjectGroupName': group_data.tProjectGroupName,
      'menu': this.items_menu,
    }

    console.log('this.current_payload: ', this.current_payload);
    console.log('data_object: ', data_object);

    this.groupService.fnHttpSetCreateNewGroup(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.group_data = {};
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', 'Profile created successfully!', 'nb-checkmark');
        setTimeout(() => {
          this.dismiss();
        }, 2000);

        // this.router.navigate(['/pages/base']);
        // this.companies = r.body;
      }
      if (r.status == 206) {
        // this.submitted = false;
        // let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        let error = r.body.codMessage;
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnCancelCreateNewGroup() {
    this.submitted = false;
    this.group_data['tProjectGroupName'] = '';
    // this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then( () => this.router.navigate(['/pages/members/', this.id_company]) );
    this.dismiss();
  }

  fnGetAllItemsGroup(current_payload?) {
    const self = this;
    console.log('current_payload: ', current_payload);

    self.menuService.fnHttpGetAllItemsTreeMenu(current_payload).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        console.log('r.status: ', r.status);
        self.items_menu = JSON.parse(JSON.stringify(r.body));
        console.log('self.items_menu: ', self.items_menu);
        self.items_menu_original_list = JSON.parse(JSON.stringify(r.body));
        console.log('self.items_menu_original_list: ', self.items_menu_original_list);
        
        self.items_access_collection = JSON.parse(JSON.stringify(r.body));
        console.log('self.items_access_collection: ', self.items_access_collection);
        
        self.items_access_collection.forEach(item_menu => {
          console.log('item_menu: ', item_menu);
          item_menu.cards = [];
        });

        console.log('self.items_access_collection: ', self.items_access_collection);

      }

      if (r.status == 206) {
        console.log('r.status: ', r.status);
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnSelectItemAccess(index_menu, item_menu, index_card, item_card) {
    console.log('index_menu: ', index_menu);
    console.log('item_menu: ', item_menu);
    console.log('index_card: ', index_card);
    console.log('item_card: ', item_card);
    // this.items_access_collection.push(item_menu);
    // this.items_access_collection
    const collection_menu = JSON.parse(JSON.stringify(this.items_menu));
    console.log('collection_menu: ', collection_menu);
    // this.items_menu[index_menu]['cards'][index_card].bChecked = false;
    if (this.items_menu[index_menu]['cards'][index_card].bChecked == false) {
      this.items_access_collection[index_menu]['cards'][index_card] = item_card;
      this.items_access_collection[index_menu]['cards'][index_card].bChecked = true;
      this.items_menu[index_menu]['cards'][index_card].bChecked = true;
    } else {
      this.items_menu[index_menu]['cards'][index_card].bChecked = false;
      // this.items_access_collection[index_menu]['cards'][index_card] = {};
    }

    console.log('this.items_access_collection: ', this.items_access_collection);
    // collection_menu.forEach(item_menu => {
    //   console.log('item_menu: ', item_menu);
    //   item_menu.cards = [];
    // });


    // collection_menu.cards.push(item_card);
    // console.log('collection_menu: ', collection_menu);
    // console.log('this.items_access_collection: ', this.items_access_collection);

  }

}
