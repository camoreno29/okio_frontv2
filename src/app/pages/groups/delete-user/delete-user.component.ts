import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { Router, ActivatedRoute } from '@angular/router';
import { StateService } from '../../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';

import { ProjectService } from '../../../shared/api/services/project.service';

@Component({
  selector: 'ngx-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.scss']
})
export class DeleteUserComponent implements OnInit {
  
  @Input() id_user: String;

  current_payload: string = null;
  submitted: Boolean = false;
  
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    protected ref: NbDialogRef<DeleteUserComponent>,
    private projectService: ProjectService,
    ) { }

  ngOnInit() {

    const self = this;

    self.utilitiesService.fnGetLanguage('profiles', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    console.log('id_user: ', this.id_user);
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
      }
    });
  }

  fnSetDeleteProjectGroupUser(id_user) {
    this.submitted = true;
    this.projectService.fnHttpDeleteProjectGroupUser(this.current_payload, id_user).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', 'The user has been removed to the group successfully!');
        // this.dismiss(false);
        
      }
      if (r.status == 206) {
        this.submitted = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
      this.dismiss();
    }, err => {
      console.log('err: ', err);
    });
  }

  dismiss() {
    this.ref.close();
  }

  fnCancelDeleteProjectGroupUser() {
    this.dismiss();
  }

}