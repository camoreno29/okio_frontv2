import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
/* ************+ Import module auth ************ */
import { StateService } from '../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';

import { GroupService } from '../../shared/api/services/group.service';
import { PagesComponent } from '../pages.component';
import { UtilitiesService } from '../../shared/api/services/utilities.service';
import { AddGroupComponent } from './add-group/add-group.component';
import { InviteUserComponent } from './invite-user/invite-user.component';
import { EditGroupComponent } from './edit-group/edit-group.component';
import { DeleteUserComponent } from './delete-user/delete-user.component';
import { DeleteGroupComponent } from './delete-group/delete-group.component'
declare var $: any;

@Component({
  selector: 'ngx-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {

  current_payload: string = null;
  id_project: any = null;
  groups: any = null;
  submitted: any = false;
  project_name: string = null;
  data_user_object: any = {};
  data_groups_object: any = {};

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    private groupService: GroupService,
    private pagesComponent: PagesComponent,
  ) { }

  ngOnInit() {

    const self = this;
    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => { 
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    // });
    
    // this.pagesComponent.fnSetMenuCommonNavigation();
    this.route.params.subscribe(params => {
      console.log('params: ', params);
      if (params.id_project) {
        this.id_project = params.id_project;
        this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          console.log('token: ', token);
          if (token.isValid()) {
            // here we receive a payload from the token and assigne it to our `user` variable
            this.current_payload = token.getValue();
            console.log('this.current_payload: ', this.current_payload);
            if (this.current_payload) {
              this.project_name = sessionStorage.getItem('project_name');
              const data_id_company = sessionStorage.getItem('id_company');
              console.log('data_id_company: ', data_id_company);
              // this.id_company = this.id_company;
              this.fnGetAllGroupsByProject(this.current_payload, this.id_project);
              // this.fnGetAllGuestMembers(this.current_payload, data_id_company);
              return false;
            }
          }
        });
      }
    });
  }

  fnGetAllGroupsByProject(current_payload, id_project?) {
    console.log('current_payload: ', current_payload);

    const object_data_send = {
      'iIDCompany': id_project,
    }
    this.groups = [];
    this.groupService.fnHttpGetAllGroupsByProject(current_payload, id_project).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.groups = r.body;
        // console.log('this.members : ', this.members );
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  openCreateGroup(id_project, hasScroll: boolean) {
    console.log('id_project: ', id_project);
    sessionStorage.setItem('id_project', id_project);
    this.dialogService.open(AddGroupComponent, { hasScroll }).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllGroupsByProject(this.current_payload, id_project);
    });
  }
  
  openInviteUser(id_project) {
    console.log('id_project: ', id_project);
    sessionStorage.setItem('id_project', id_project);
    this.dialogService.open(InviteUserComponent).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllGroupsByProject(this.current_payload, id_project);
    });
  }

  openEditGroup(group_data){
    console.log('group_data: ', group_data);
    sessionStorage.setItem('id_project', group_data.iIDProject);
    this.dialogService.open(EditGroupComponent, {context: group_data}).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllGroupsByProject(this.current_payload, group_data.iIDProject);
    });
  }

  fnShowModalDeleteProjectGroupUser(id_user) {
    this.data_user_object.id_user = id_user;
    this.dialogService.open(DeleteUserComponent, { context: this.data_user_object }).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllGroupsByProject(this.current_payload, this.id_project);
    });
  }

  fnShowModalDeleteProjectGroup(group_data) {

    this.data_groups_object.id_group = group_data.iIDProjectGroup;
    this.data_groups_object.name_group = group_data.tProjectGroupName;
    if(group_data.projectGroupUsers.length < 1){
      console.log('group_data.length: ', group_data.projectGroupUsers.length);
      this.data_groups_object.state_group = true;
    }else{
      console.log('group_data.length: ', group_data.projectGroupUsers.length);
      this.data_groups_object.state_group = false;
    }

    console.log('this.data_groups_object.id_group: ', this.data_groups_object.id_group);
    console.log('this.data_groups_object.name_group: ', this.data_groups_object.name_group);
    console.log('this.data_groups_object.state_group: ', this.data_groups_object.state_group);

    this.dialogService.open(DeleteGroupComponent, { context: this.data_groups_object }).onClose.subscribe((res) => {
      console.log('res: ', res);
      this.fnGetAllGroupsByProject(this.current_payload, this.id_project);
    });
  }

  fnRedirectListProjects() {
    this.router.navigate(['/pages/projects']);
  }

  fnGetLanguage(){
    const self = this;
    self.utilitiesService.fnGetLanguage('profiles', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
  }

}
