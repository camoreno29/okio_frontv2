import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { GroupsComponent } from './groups.component';
import { AddGroupComponent } from './add-group/add-group.component';
import { InviteUserComponent } from './invite-user/invite-user.component';
import { EditGroupComponent } from './edit-group/edit-group.component';
import { DeleteUserComponent } from './delete-user/delete-user.component';
import { DeleteGroupComponent } from './delete-group/delete-group.component';

const ENTRY_COMPONENTS = [
  AddGroupComponent,
  InviteUserComponent,
  EditGroupComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    TooltipModule.forRoot(),
    ModalModule,
  ],
  declarations: [
    GroupsComponent,
    AddGroupComponent,
    InviteUserComponent,
    EditGroupComponent,
    DeleteUserComponent,
    DeleteGroupComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
    DeleteUserComponent,
    DeleteGroupComponent,
  ],
})
export class GroupsModule { }
