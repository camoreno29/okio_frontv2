import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { GroupService } from '../../../shared/api/services/group.service';

@Component({
  selector: 'ngx-invite-user',
  templateUrl: './invite-user.component.html',
  styleUrls: ['./invite-user.component.scss']
})
export class InviteUserComponent implements OnInit {

  user_invite_data: any = {};
  current_payload: any = null;
  title: string = null;
  submitted: boolean = false;
  list_projects: any = [];
  id_company: any = '';
  id_project: any = '';

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private groupService: GroupService,
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    protected ref: NbDialogRef<InviteUserComponent>) {
      console.log('title: ', this.title);
      this.id_project = sessionStorage.getItem('id_project');
  }

  ngOnInit() {

    const self = this;

    self.utilitiesService.fnGetLanguage('profiles', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    console.log('this.route.params: ', this.route.params);
    this.route.params.subscribe(params => {
      console.log('params: ', params);
    });

    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
      }
    });

    this.fnGetListProjectGroupByProject(this.current_payload, this.id_project);
  }

  fnGetListProjectGroupByProject(current_payload, id_project?) {
    console.log('current_payload: ', current_payload);

    const object_data_send = {
      'iIDProject': id_project,
    }

    this.groupService.fnHttpGetListProjectGroupsByProject(current_payload, id_project).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_projects = r.body;
        this.submitted = false;
        console.log('r.status: ', r.status);
        // this.utilitiesService.showToast('top-right', 'success', 'Invite send successfully!', 'nb-checkmark');
        // setTimeout(() => {
        //   this.dismiss();
        // }, 2000);
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnSendInviteUserToGroup(user_invite_data, id_project?) {
    console.log('user_invite_data: ', user_invite_data);

    // let data_object = {
    //   'iIDCompany': 0,
    //   'tCompanyLogo': 'url_company_logo_',
    //   'tCompanyName': company.tCompanyName,
    // };
    // const id_company = sessionStorage.getItem('id_company');
    // this.id_company = sessionStorage.getItem('id_company');
    user_invite_data.subject = 'You have been invited to project group PGP Prexus';

    this.submitted = true;
    let data_object = {
      'iIDProjectGroup': user_invite_data.iIDProjectGroup,
      'tEmail': user_invite_data.tEmail,
      'subject': user_invite_data.subject,
      'tlanguage': 'en',
    };

    console.log('this.current_payload: ', this.current_payload);
    console.log('data_object: ', data_object);

    this.groupService.fnHttpSetSendInviteUserToGroup(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.user_invite_data = {};
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', 'Group created successfully!', 'nb-checkmark');
        setTimeout(() => {
          this.dismiss();
        }, 2000);

        // this.router.navigate(['/pages/base']);
        // this.companies = r.body;
      }
      if (r.status == 206) {
        // this.submitted = false;
        // let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        let error = r.body.codMessage;
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnCancelSendInvitationGroup() {
    this.submitted = false;
    this.user_invite_data['tProjectGroupName'] = '';
    // this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then( () => this.router.navigate(['/pages/members/', this.id_company]) );
    this.dismiss();
  }

  dismiss() {
    this.ref.close();
  }

}
