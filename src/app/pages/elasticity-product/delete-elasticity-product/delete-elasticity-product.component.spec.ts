import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteMarketPriceComponent } from './delete-market-price.component';

describe('DeleteMarketPriceComponent', () => {
  let component: DeleteMarketPriceComponent;
  let fixture: ComponentFixture<DeleteMarketPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteMarketPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteMarketPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
