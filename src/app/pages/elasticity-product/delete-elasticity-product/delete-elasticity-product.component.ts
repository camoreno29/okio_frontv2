import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

import { Router, ActivatedRoute } from '@angular/router';

import { StateService } from '../../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';

import { ElasticityProductService } from '../../../shared/api/services/elasticity-product.service';

@Component({
  selector: 'ngx-delete-elasticity-product',
  templateUrl: './delete-elasticity-product.component.html',
  styleUrls: ['./delete-elasticity-product.component.scss']
})
export class DeleteElasticityProductComponent implements OnInit {

  @Input() iIDVersion: String;
  @Input() iIDProduct: String;
  @Input() tProductCode: String;
  @Input() tProductName: String;
  tProduct: String;
  data_elasticity_product: any = {};
  id_category: any = null;
  current_payload: string = null;
  submitted: Boolean = false;

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    protected ref: NbDialogRef<DeleteElasticityProductComponent>,
    private elasticityProductService: ElasticityProductService,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('elasticityProduct', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    // this.iIDVersion;
    console.log('this.iIDVersion: ', this.iIDVersion);
    console.log('this.iIDVersion: ', this.iIDProduct);
    console.log('this.data_elasticity_product: ', this.data_elasticity_product);
    this.tProduct = this.tProductCode + '-' + this.tProductName;

    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
      }
    });
  }


  fnSetDeleteElasticityProductById(iIDProduct) {
    this.submitted = true;
    this.utilitiesService.showToast('top-right', 'success', 'Restoring...');
    this.elasticityProductService.fnHttpSetRestoreElasticityProductById(this.current_payload, iIDProduct).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessDelete.text);
        this.dismiss(false);
      }
      if (r.status == 206) {
        this.submitted = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  dismiss(modalAction) {
    this.ref.close(modalAction);
  }

  fnCancelElasticityProduct() {
    this.dismiss(true);
  }

}
