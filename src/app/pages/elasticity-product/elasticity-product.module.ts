import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng5SliderModule } from 'ng5-slider';


import { AdvancedSearchElasticityProductComponent } from './advanced-search-elasticity-product/advanced-search-elasticity-product.component';
import { DeleteAllElasticityProductComponent } from './delete-all-elasticity-product/delete-all-elasticity-product.component';
import { DeleteElasticityProductComponent } from './delete-elasticity-product/delete-elasticity-product.component';
import { EditElasticityProductComponent } from './edit-elasticity-product/edit-elasticity-product.component';

const ENTRY_COMPONENTS = [
  AdvancedSearchElasticityProductComponent,
  DeleteAllElasticityProductComponent,
  DeleteElasticityProductComponent,
  EditElasticityProductComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    TooltipModule.forRoot(),
    ModalModule,
    NgSelectModule,
    FormsModule,
    NgxPaginationModule,
    Ng5SliderModule,
  ],
  declarations: [
    AdvancedSearchElasticityProductComponent,
    DeleteAllElasticityProductComponent,
    DeleteElasticityProductComponent,
    EditElasticityProductComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})

export class ElasticityProductModule { }
