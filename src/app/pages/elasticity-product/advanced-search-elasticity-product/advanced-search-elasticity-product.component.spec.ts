import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedSearchMarketPriceComponent } from './advanced-search-market-price.component';

describe('AdvancedSearchMarketPriceComponent', () => {
  let component: AdvancedSearchMarketPriceComponent;
  let fixture: ComponentFixture<AdvancedSearchMarketPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvancedSearchMarketPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedSearchMarketPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
