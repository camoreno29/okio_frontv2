import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options } from 'ng5-slider';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CompetitorsService } from '../../../shared/api/services/competitors.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { PricesListService } from '../../../shared/api/services/prices-list.service';
import { ProductInformationService } from '../../../shared/api/services/product-information.service';
import { ConfigService } from '../../../shared/api/services/config.service';

@Component({
  selector: 'ngx-advanced-search-elasticity-product',
  templateUrl: './advanced-search-elasticity-product.component.html',
  styleUrls: ['./advanced-search-elasticity-product.component.scss'],
})
export class AdvancedSearchElasticityProductComponent implements OnInit {

  @Input() id_version: string;
  @Input() items_collection_filtered: any;

  submitted: boolean = false;
  current_payload: any = null;
  elasticity_product_data: any = {};
  list_products_collection: any = [];
  list_categories_collection: any = [];
  list_prices_lists_collection: any = [];
  state_loading: boolean = true;

  data_filter_products: Object = {};
  data_filter_price_list: Object = {};
  data_filter_categories: Object = {};
  data_filter_products_type: string = '';
  min_value_mark_elasticity: number = null;
  max_value_mark_elasticity: number = null;

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private competitorsService: CompetitorsService,
    private categoriesService: CategoriesService,
    private pricesListService: PricesListService,
    private productInformationService: ProductInformationService,
    private configService: ConfigService,
    protected ref: NbDialogRef<AdvancedSearchElasticityProductComponent>,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('elasticityProduct', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
        console.log('id_version======================>: ', this.id_version);

        this.data_filter_products = this.items_collection_filtered['products'];
        this.data_filter_price_list = this.items_collection_filtered['priceList'];
        this.data_filter_categories = this.items_collection_filtered['category'];
        this.data_filter_products_type = this.items_collection_filtered['tProductsType'];
        this.min_value_mark_elasticity = (this.items_collection_filtered['dMarkElasticity']) ? this.items_collection_filtered['dMarkElasticity']['dMin'] : null;
        this.max_value_mark_elasticity = (this.items_collection_filtered['dMarkElasticity']) ? this.items_collection_filtered['dMarkElasticity']['dMax'] : null;

        this.fnGetListCategories(this.current_payload, this.id_version);
        this.fnGetListProducts(this.current_payload, this.id_version);
        this.fnGetPricesLists(this.current_payload, this.id_version);
      }
    });
  }

  /** Funciones para cargar los datos **/
  fnGetListCategories(current_payload, id_version) {
    this.categoriesService.fnHttpGetDataListCategoriesByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_categories_collection = JSON.parse(JSON.stringify(r.body));
        this.submitted = false;
      }
      if (r.status == 206) {
        this.submitted = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnGetListProducts(current_payload, id_version) {
    const self = this;
    self.productInformationService.fnHttpGetListProducts(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_products_collection = JSON.parse(JSON.stringify(r.body));
        self.submitted = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        const error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetPricesLists(current_payload, id_version?) {
    const self = this;
    self.pricesListService.fnGetAllPricesListByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.list_prices_lists_collection = JSON.parse(JSON.stringify(r.body));
        console.log('self.list_prices_lists_collection: ', self.list_prices_lists_collection);
        self.submitted = false;
        this.state_loading = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        const error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }
  /** **/


  /** Funciones para enviar Datos al servicio cuando se cierrar el componente o cancelar **/
  fnAdvancedSearchElasticityProduct(elasticity_product_data) {
    this.submitted = true;
    console.log('elasticity_product_data: ', elasticity_product_data);
    const data_object = {
      'products': (elasticity_product_data.products) ? elasticity_product_data.products : [],
      'priceList': (elasticity_product_data.priceList) ? elasticity_product_data.priceList : [],
      'category': (elasticity_product_data.categories) ? elasticity_product_data.categories : [],
      'tProductsType': (elasticity_product_data.tProductsType) ? elasticity_product_data.tProductsType : null,
      'dMarkElasticity': {
        'dMin': this.min_value_mark_elasticity,
        'dMax': this.max_value_mark_elasticity == 0 ? '' : this.max_value_mark_elasticity,
      },
      'page': 1,
      'pageSize': 12,
      'tSearch': '',
    };
    console.log('data_object: ', data_object);
    this.dismiss(data_object);
  }

  fnCancelCreateNewElasticityProduct() {
    this.submitted = false;
    this.dismiss();

  }

  dismiss(object_response_onclose?) {

    console.log('object_response_onclose: ', object_response_onclose);
    if (object_response_onclose) {
      object_response_onclose['products'] = (this.data_filter_products) ? this.data_filter_products : [];
      object_response_onclose['priceList'] = (this.data_filter_price_list) ? this.data_filter_price_list : [];
      object_response_onclose['category'] = (this.data_filter_categories) ? this.data_filter_categories : [];
      object_response_onclose['tProductsType'] = (this.data_filter_products_type) ? this.data_filter_products_type : null;
      object_response_onclose['dMarkElasticity'] = (this.max_value_mark_elasticity) ? { 'dMin': this.min_value_mark_elasticity, 'dMax': this.max_value_mark_elasticity } : { 'dMin': this.min_value_mark_elasticity, 'dMax': null };
      this.ref.close(object_response_onclose);
    } else {
      this.ref.close();
    }
  }
  /** **/

}

