import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { StateService } from '../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';
import { environment } from '../../../environments/environment';

import { FormControl, FormGroup, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';

import { UtilitiesService } from '../../shared/api/services/utilities.service';

import { ElasticityProductService } from '../../shared/api/services/elasticity-product.service';
import { CategoriesService } from '../../shared/api/services/categories.service';

import { EditElasticityProductComponent } from './edit-elasticity-product/edit-elasticity-product.component';
import { DeleteElasticityProductComponent } from './delete-elasticity-product/delete-elasticity-product.component';
import { DeleteAllElasticityProductComponent } from './delete-all-elasticity-product/delete-all-elasticity-product.component';
import { AdvancedSearchElasticityProductComponent } from './advanced-search-elasticity-product/advanced-search-elasticity-product.component';


import { ProductInformationService } from '../../shared/api/services/product-information.service';

import { ModalsComponent } from '../../shared/components/modals/modals.component';

@Component({
  selector: 'ngx-elasticity-product',
  templateUrl: './elasticity-product.component.html',
  styleUrls: ['./elasticity-product.component.scss'],
})
export class ElasticityProductComponent implements OnInit {

  url_host: any = environment.apiUrl;
  list_elasticity_product: any = null;
  layoud_component_display: String = 'grid';
  elasticity_product_original_collection: any = null;
  numItemsPage: any = null;
  currentPage: any = null;
  totalItems: any = null;
  loading: boolean = true;
  filter_state: boolean = false;
  items_filtered_advance_search: any = [];
  obj_elasticity_product: any = {};
  current_payload: string = null;
  id_version: any = null;
  submitted: any = false;
  fileToUpload: File = null;
  file_import_input: any = null;
  object_filter: any = {};
  data_elasticity_product: any = {};
  search_input: any = '';
  bShowCents: any = null;
  quantiy_decimals: any = 0;
  loadingChart: boolean = true;

  sorted: boolean = false;
  field_sort: any = null;
  state_sort_data: any = 'asc';

  filterState: boolean = false;

  text_searching: any = '';

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;
  DATA_LANG_GENERAL: any = null;
  
  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    private elasticityProductService: ElasticityProductService,
    private categoriesService: CategoriesService,
    private productInformationService: ProductInformationService,
  ) { }


  ngOnInit() {
    const self = this;
    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => { 
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    // });
    
    this.currentPage = 1;
    this.numItemsPage = 12;
    //
    this.route.params.subscribe(params => {
      console.log('params: ', params);
      if (params.id_version) {
        this.id_version = params.id_version;
        this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          console.log('token: ', token);
          if (token.isValid()) {
            // here we receive a payload from the token and assigne it to our `user` variable
            this.current_payload = token.getValue();
            console.log('this.current_payload: ', this.current_payload);
            if (this.current_payload) {
              this.object_filter['marketPrices'] = [];
              this.fnGetAllElasticityProduct(this.current_payload, this.id_version, 1);
              this.fnGetConfigElasticityProduct(this.current_payload, this.id_version);
              return false;
            }
          }
        });
      }
    });
  }


  /** Funciones para cargar los datos y Ordenar por columna **/
  fnGetAllElasticityProduct(current_payload, id_version?, page?, text_search?) {

    let obj_Sort = null;

    if(this.sorted){
      obj_Sort = {
        'field' : this.field_sort,
        'dir': this.state_sort_data
      }
    }

    const object_data_send = {
      'products': [],
      'priceList': [],
      'category': [],
      'tProductsType': null,
      'dMarkElasticity': {
        'dMin': null,
        'dMax': null,
      },
      'Sort': obj_Sort,
      'page': (page) ? page : 1,
      'pageSize': 12,
      'tSearch': text_search,
    };

    this.list_elasticity_product = [];

    this.elasticityProductService.fnHttpGetAllElasticityProduct(current_payload, id_version, object_data_send).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.filter_state = false;
        this.list_elasticity_product = JSON.parse(JSON.stringify(r.body.elasticityProducts));
        this.elasticity_product_original_collection = JSON.parse(JSON.stringify(r.body.elasticityProducts));
        this.totalItems = r.body['totalItems'];
        console.log('this.list_elasticity_product: ', this.list_elasticity_product);
        this.data_elasticity_product = this.list_elasticity_product;
        this.loadingChart = false;
        this.fnValidStateSearch(this.items_filtered_advance_search);
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  sortColumn(field) {
    console.log(`sortColumn(field)`);
    console.log('field: ', field);
    const self = this;

    self.sorted = true;
    self.field_sort = field;

    if (self.state_sort_data === 'asc') {
      self.state_sort_data = 'desc';
    } else {
      self.state_sort_data = 'asc';
    }

    if(this.filterState){
      this.fnGetAllElasticityProductFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
    }else{
      this.fnGetAllElasticityProduct(this.current_payload, this.id_version, 1, this.text_searching);
    }
    this.currentPage = 1;
  }
  /** **/


  /** Funciones para filtrar y remover filtros **/
  fnFilterElasticityProduct(elasticity_product, text_typing) {
    console.log('elasticity_product: ', elasticity_product);
    console.log('text_typing: ', text_typing);
    console.log('this.filter_state: ', this.filter_state);

    if (text_typing) {
      if (text_typing.length > 2) {
        console.log('Text ok');
        if (this.filter_state) {
          console.log('this.filter_state - 1: ', this.filter_state);
          this.fnGetAllElasticityProductFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
        } else {
          this.text_searching = text_typing;
          console.log('this.filter_state - 2: ', this.filter_state);
          this.fnGetAllElasticityProduct(this.current_payload, this.id_version, 1, text_typing);
        }
      }
    } else {
      this.fnGetAllElasticityProduct(this.current_payload, this.id_version, 1);
      console.log('Text not exist');
    }
  }

  fnGetAllElasticityProductFilter(current_payload, object_filter, id_version?, page?) {
    console.log('id_version: ', id_version);
    console.log('object_filter: ', object_filter);
    console.log('current_payload: ', current_payload);

    this.list_elasticity_product = [];
    this.fnGetAllElasticityProductAdvanceSearch(current_payload, this.id_version, object_filter, page);
  }

  fnGetAllElasticityProductAdvanceSearch(current_payload, id_version, data_object, page?) {
    console.log('current_payload: ', current_payload);

    const object_data_send = data_object;

    let obj_Sort = null;

    if(this.sorted){
      obj_Sort = {
        'field' : this.field_sort,
        'dir': this.state_sort_data
      }
    }

    object_data_send['Sort'] = obj_Sort;
    object_data_send['page'] = (page) ? page : 1,

    this.elasticityProductService.fnHttpGetAllElasticityProduct(current_payload, id_version, object_data_send).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.list_elasticity_product = JSON.parse(JSON.stringify(r.body.elasticityProducts));
        console.log('this.list_elasticity_product: ', this.list_elasticity_product);
        this.list_elasticity_product = JSON.parse(JSON.stringify(r.body.elasticityProducts));
        console.log('this.elasticity_product_original_collection: ', this.elasticity_product_original_collection);
        this.totalItems = r.body['totalItems'];
        console.log('this.totalItems: ', this.totalItems);
        this.fnValidStateSearch(this.items_filtered_advance_search);
      }

      if (r.status == 206) {
        this.list_elasticity_product = [];
        this.elasticity_product_original_collection = [];
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  showModalAdvancedSearchElasticityProduct(obj_data_filter) {
    this.search_input = '';
    obj_data_filter.id_version = this.id_version;
    obj_data_filter.items_collection_filtered = this.items_filtered_advance_search;
    console.log('this.items_filtered_advance_search: ', this.items_filtered_advance_search);
    console.log('obj_data_filter: ', obj_data_filter);

    this.dialogService.open(AdvancedSearchElasticityProductComponent, { context: obj_data_filter }).onClose.subscribe((res) => {
      console.log('res: ', res);
      if (res) {
        this.loading = true;
        this.filter_state = true;
        const data_object = res;
        console.log('data_object: ', data_object);
        this.items_filtered_advance_search = JSON.parse(JSON.stringify(res));
        this.fnGetAllElasticityProductAdvanceSearch(this.current_payload, this.id_version, data_object);
      }
    });
  }

  fnGetConfigElasticityProduct(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);
    this.productInformationService.fnHttpGetConfigProducts(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.bShowCents = r.body['bShowCents'];
        console.log('this.bShowCents: ', this.bShowCents);
        this.quantiy_decimals = (this.bShowCents) ? 2 : 0;
        console.log('this.quantiy_decimals: ', this.quantiy_decimals);
      }
    }, err => {
      console.log('err: ', err);
    });
  }


  fnSetRemoveItemSearchFilteredAdvanced(index, data_collection) {
    console.log('index: ', index);
    console.log('data_collection: ', data_collection);
    data_collection.splice(index, 1);
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    console.log('data_collection: ', data_collection);

    this.object_filter = this.items_filtered_advance_search;
    console.log('this.items_filtered_advance_search: ', this.items_filtered_advance_search);
    this.fnGetAllElasticityProductFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
  }

  fnSetRemoveProductsType() {
    this.items_filtered_advance_search['tProductsType'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllElasticityProductFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
  }

  fnSetRemoveBadgeMarkElasticity() {
    this.items_filtered_advance_search['dMarkElasticity']['dMin'] = null;
    this.items_filtered_advance_search['dMarkElasticity']['dMax'] = null;
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    this.fnGetAllElasticityProductFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
  }

  fnClearAllFiltersSearch() {
    this.search_input = '';
    this.items_filtered_advance_search = [];
    this.fnGetAllElasticityProduct(this.current_payload, this.id_version);
  }

  fnValidStateSearch(collection) {
    console.log('collection: ', collection);
    if(typeof collection['category'] === "undefined"){
      console.log('indefinida')
      this.filterState = false;
      }else{
      if (collection['products'].length < 1 &&
        collection['priceList'].length < 1 &&
        collection['category'].length < 1 &&
        collection['tProductsType'] == null &&
        collection['dMarkElasticity']['dMin'] == null &&
        collection['dMarkElasticity']['dMax'] == null) {
        this.filterState = false;
        return false;
      } else {
        this.filterState = true;
        return true;
      }
    }
  }
  /****/


  /** Funciones para editar y borrar **/
  showModalEditElasticityProduct(obj_elasticity_product) {
    console.log('obj_elasticity_product: ', obj_elasticity_product);
    // console.log('id_version: ', this.id_version);
    obj_elasticity_product.id_version = this.id_version;
    // obj_elasticity_product.iIDMarketPrice = obj_elasticity_product.iIDMarketPrice;
    console.log('obj_elasticity_product: ', obj_elasticity_product);

    this.dialogService.open(EditElasticityProductComponent, { context: { obj_elasticity_product } }).onClose.subscribe((res) => {
      console.log('res: ', res);
      console.log('obj_elasticity_product: ', obj_elasticity_product);
      if(res == false){
      this.id_version = obj_elasticity_product['id_version'];
      this.fnGetAllElasticityProduct(this.current_payload, this.id_version, this.currentPage);
      }
    });
  }

  fnDeleteElasticityProduct(data_elasticity_product) {
    data_elasticity_product.id_version = this.id_version;
    console.log('data_elasticity_product: ', data_elasticity_product);

    this.dialogService.open(DeleteElasticityProductComponent, { context: data_elasticity_product }).onClose.subscribe((res) => {
      console.log('res: ', res);
      console.log('obj_elasticity_product: ', data_elasticity_product);
      if(res == false){
      this.id_version = data_elasticity_product['id_version'];
      this.fnGetAllElasticityProduct(this.current_payload, this.id_version);
      this.currentPage = 1;
      }
    });
  }

  fnDeleteAllElasticityProduct(obj_elasticity_product) {
    console.log('obj_elasticity_product: ', obj_elasticity_product);
    obj_elasticity_product.id_version = this.id_version;
    console.log('obj_elasticity_product: ', obj_elasticity_product);
    this.dialogService.open(DeleteAllElasticityProductComponent, { context: obj_elasticity_product }).onClose.subscribe((res) => {
      console.log('res: ', res);
      if(res == false){
      this.fnGetAllElasticityProduct(this.current_payload, this.id_version);
      this.currentPage = 1;
      }
    });
  }
  /****/


  /** Funciones para exportar e importar **/
  fnExportElasticityProduct() {
    const lang = localStorage.getItem('language_app');
    window.open(
      this.url_host + '/api/ElasticityProduct/GetExportElasticityProducts?iIDVersion=' + this.id_version + '&language=' + lang,
      '_blank');
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.uploadFileToActivity(this.fileToUpload);
  }

  uploadFileToActivity(fileToUpload) {
    console.log('fileToUpload: ', fileToUpload);
    const end_point_url = '/api/ElasticityProduct/PostImportElasticityProducts';
    const parameter = 'iIDVersion';
    this.loadingChart = true;
    this.utilitiesService.fnHttSetUploadFile(this.current_payload, this.fileToUpload, this.id_version, end_point_url, parameter).subscribe(data => {
      console.log('data: ', data);
      // do something, if upload success

      if (data.status == 200) {
        this.loadingChart = false;
        this.file_import_input = null;
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessImport.text);
        this.fnGetAllElasticityProduct(this.current_payload, this.id_version);
      }
      if (data.status == 206) {
        this.loadingChart = false;
        const error = this.utilitiesService.fnSetErrors(null, data.body.message);
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, error => {
      this.loadingChart = false;
      console.log(error);
    });
    this.file_import_input = null;
  }
  /****/


  /** Funciones para Pagindo **/
  getPage(page: number) {
    console.log('page: ', page);
    this.loading = true;
    if(this.filterState){
      this.fnGetAllElasticityProductFilter(this.current_payload, this.items_filtered_advance_search, this.id_version, page);
    }else{
      this.fnGetAllElasticityProduct(this.current_payload, this.id_version, page, this.text_searching);
    }
    this.currentPage = page;
  }

  /*********Show modal parameters function *********/

  open(_title, _body) {
    this.dialogService.open(ModalsComponent, {
      context: {
        title: _title,
        body: _body
      },
    });
  }

  fnGetLanguage(){
    const self = this;
    self.utilitiesService.fnGetLanguage('elasticityProduct', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module,
      self.DATA_LANG_GENERAL = res_lang.general
    });
  }

}
