import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options, LabelType } from 'ng5-slider';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { RatingPerceivedService } from '../../../shared/api/services/rating-perceived.service';
import { ElasticityProductService } from '../../../shared/api/services/elasticity-product.service';
import { ConfigService } from '../../../shared/api/services/config.service';

declare var $: any;

@Component({
  selector: 'ngx-edit-elasticity-product',
  templateUrl: './edit-elasticity-product.component.html',
  styleUrls: ['./edit-elasticity-product.component.scss'],
})

export class EditElasticityProductComponent implements OnInit {

  submitted: boolean = false;
  state_btn_next: boolean = false;
  state_btn_preview: boolean = true;
  current_payload: any = null;

  id_version: string;
  id_Product: string;

  list_rating_produc_type_collection: any = [];
  list_competitors: any = [];
  obj_form_elasticity_product: any = {};

  select_competitor: any = {};
  input_product: any = '';
  input_prices: any = '';

  index_ant: number = null;

  competitor_collection: any = [];

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  @Input() obj_elasticity_product: any;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private elasticityProductService: ElasticityProductService,
    private configService: ConfigService,
    protected ref: NbDialogRef<EditElasticityProductComponent>,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('elasticityProduct', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();

        this.id_version = this.obj_elasticity_product['id_version'];
        this.id_Product = this.obj_elasticity_product['iIDProduct'];
        console.log('this.current_payload: ', this.current_payload);
        console.log('id_version======================>: ', this.id_version);
        console.log('obj_elasticity_product======================>: ', this.obj_elasticity_product);

        this.obj_form_elasticity_product = {
          'iIDVersion': parseInt(this.id_version, 10),
          'iIDProduct': this.obj_elasticity_product['iIDProduct'],
          'tProductCode': this.obj_elasticity_product['tProductCode'],
          'tProductName': this.obj_elasticity_product['tProductName'],
          'tPriceListName': this.obj_elasticity_product['tPriceListName'],
          'tProductType': this.obj_elasticity_product['tProductType'],
          'dMarkElasticity': parseFloat(this.obj_elasticity_product['dMarkElasticity']).toFixed(2),
        };

        this.fnGetRatingProducType(this.current_payload, this.id_version);
      }
    });
  }

  /** Funcion para cargar la DDl de competidores**/
  fnGetRatingProducType(current_payload, id_version?) {
    const self = this;
    self.configService.fnHttpGetListProductTypesVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.list_rating_produc_type_collection = JSON.parse(JSON.stringify(r.body));
        console.log('self.list_rating_produc_type_collection: ', self.list_rating_produc_type_collection);
        self.submitted = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        const error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }
  /** **/

  /** Funcion que envia los datos al servicio de actualizar **/
  fnUpdateDataElasticityProduct(obj) {
    this.submitted = true;

    this.elasticityProductService.fnHttpSetEditDataElasticityProduct(this.current_payload, this.obj_form_elasticity_product).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessUpdate.text);
        this.dismiss(false);
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }
  /** **/


  /** Funciones para cancelar y cerrar **/
  fnCancelEditElasticityProduct() {
    this.submitted = false;
    this.dismiss(true);
  }

  dismiss(modalAction) {
    this.ref.close(modalAction);
  }
  /** **/

  /*****(numberOnly)allows only numbers and decimals*****/

  numberOnly(evt): boolean {
    const charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 46 && evt.srcElement.value.split('.').length>1) {
        return false;
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
  }

}
