import { Component, OnInit } from '@angular/core';
import { UtilitiesService } from '../../shared/api/services/utilities.service';

@Component({
  selector: 'ngx-terms-conditions',
  templateUrl: './terms-conditions.component.html',
  styleUrls: ['./terms-conditions.component.scss']
})
export class TermsConditionsComponent implements OnInit {

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private utilitiesService: UtilitiesService,
  ) { }
  ngOnInit() {
    const self = this;
    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => { 
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    // });
  }

  fnGetLanguage(){
    const self = this;
    self.utilitiesService.fnGetLanguage('termsConditions', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
  }

}
