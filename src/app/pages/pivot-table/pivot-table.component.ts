import { Component, OnInit, ViewChild, Type } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { StateService } from '../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';
import { environment } from '../../../environments/environment';

import { UtilitiesService } from '../../shared/api/services/utilities.service';

import { WebDataRocksPivot } from '../../shared/components/webdatarocks/webdatarocks.angular4';

import { PivotTableService } from '../../shared/api/services/pivot-table.service';
import { ProductInformationService } from '../../shared/api/services/product-information.service';
import { constants } from 'os';

declare var $: any;

@Component({
  selector: 'ngx-pivot-table',
  templateUrl: './pivot-table.component.html',
  styleUrls: ['./pivot-table.component.scss'],
})
export class PivotTableComponent implements OnInit {

  @ViewChild('pivot1') child: WebDataRocksPivot;

  userStage: boolean = false;

  list_pivot_table: any = [];
  list_pivot_table_original: any = [];
  numItemsPage: any = null;
  currentPage: any = null;
  totalItems: any = null;

  loading: boolean = true;
  filter_state: boolean = false;
  current_payload: string = null;
  id_version: any = null;
  submitted: any = false;
  object_filter: any = {};
  data: any = {};
  quantiy_decimals: any = 0;
  bShowCents: any = null;
  config: any = null;
  reportSlice: any = [];
  reportSliceLocal: any = [];

  lang_nav: any = window.navigator.language;
  DATA_LANG: any = null;
  DATA_LANG_GENERAL: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    private pivotTableService: PivotTableService,
    private productInformationService: ProductInformationService,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('pivotTable', function (res_lang) {
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
      self.DATA_LANG_GENERAL = res_lang.general
    });

    this.currentPage = 1;
    this.numItemsPage = 10;
    this.reportSlice;
    this.reportSliceLocal = JSON.parse('{"columns":[{"uniqueName":"Measures"}],"measures":[{"uniqueName":"Units variation","aggregation":"average","format": "variations","availableAggregations":["count","distinctcount","average"]},{"uniqueName":"Gross contribution variation","aggregation":"average","format": "variations","availableAggregations":["count","distinctcount","average"]},{"uniqueName":"Price variation","aggregation":"average","format": "variations","availableAggregations":["count","distinctcount","average"]},{"uniqueName":"Incomes variation","aggregation":"average","format": "variations","availableAggregations":["count","distinctcount","average"]}]}');
    console.log('name', localStorage.getItem('name'));
    this.route.params.subscribe(params => {
      console.log('params: ', params);
      if (params.id_version) {
        this.id_version = params.id_version;
        this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          console.log('token: ', token);
          if (token.isValid()) {
            // here we receive a payload from the token and assigne it to our `user` variable
            this.current_payload = token.getValue();
            console.log('this.current_payload: ', this.current_payload);
            if (this.current_payload) {
              this.object_filter = [];
              this.fnGetAllPivotTable(this.current_payload, this.id_version);
              this.fnGetPivotTableState(this.current_payload, this.id_version);
              this.fnGetConfigProducts(this.current_payload, this.id_version);
              this.fnGetConfigStage(this.current_payload, this.id_version);
            }
          }
        });
      }
    });
  }

  /** Funcion para cambiar de grid a cards **/
  fnSwitchStage() {
    this.fnGetAllPivotTable(this.current_payload, this.id_version);
    this.fnUpdateConfigStage(this.current_payload, this.id_version, !this.userStage);
  }

  /** **/

  /** Funciones para cargar los datos y Ordenar por columna **/
  fnGetAllPivotTable(current_payload, id_version?) {
    this.submitted = true;

    this.pivotTableService.fnHttpGetAllPivotTable(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.filter_state = false;
        this.submitted = false;
        this.list_pivot_table = JSON.parse(JSON.stringify(r.body));

        this.list_pivot_table.forEach(datos => {

          const objtest = {
            'Average price suggested': datos.averagePriceSuggested,
            'Category': datos.category,
            'Current average price': datos.currentAveragePrice,
            'Current gross contribution': datos.currentGrossContribution,
            'Current net income': datos.currentNetIncome,
            'Current price': datos.currentPrice,
            'Current units': datos.currentUnits,
            'Gross contribution variation': datos.grossContributionVariation,
            'Incomes variation': datos.incomesVariation,
            'Price list': datos.priceList,
            'Price variation': datos.priceVariation,
            'Product code': datos.productCode,
            'Product name': datos.productName,
            'Product type': datos.productType,
            'Projected gross contribution': datos.projectedGrossContribution,
            'Projected net income': datos.projectedNetIncome,
            'Projected units': datos.projectedUnits,
            'Suggested price': datos.suggestedPrice,
            'Units variation': datos.unitsVariation,
          };

          this.list_pivot_table_original.push(objtest);
        });

        console.log('this.list_pivot_table_original: ', this.list_pivot_table_original);
      }
      if (r.status == 206) {
        this.loading = false;
        this.filter_state = false;
        this.submitted = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.loading = false;
      this.filter_state = false;
      this.submitted = false;
      console.log('err: ', err);
      this.utilitiesService.showToast('top-right', '', this.DATA_LANG.msgNoExist.text);
    });
  }

  fnGetPivotTableState(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);
    this.config = null;
    this.submitted = true;
    this.pivotTableService.fnHttpGetPivotTableState(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        this.reportSlice = JSON.parse(r.body);
        console.log('reportSlice: ', this.reportSlice);
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnGetConfigStage(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);
    this.config = null;
    this.submitted = true;
    this.productInformationService.fnHttpGetSuggestedPrice(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        this.userStage = JSON.parse(JSON.stringify(!r.body));
        console.log('userStage: ', this.userStage);
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnUpdateConfigStage(current_payload, id_version, Stage) {
    this.config = null;
    this.loading = true;
    this.submitted = true;
    this.productInformationService.fnHttpUpdateSuggestedPrice(current_payload, id_version, Stage).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.submitted = false;
      }
    }, err => {
      this.loading = false;
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnUpdatePivotTableState(current_payload, id_version, reportSliceSent) {
    this.loading = true;
    this.submitted = true;

    const objSentPivot = {
      'iIDVersion': id_version,
      'tState': reportSliceSent,
    };

    this.pivotTableService.fnHttpUpdatePivotTableState(current_payload, objSentPivot).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.submitted = false;
      }
    }, err => {
      this.loading = false;
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnGetConfigProducts(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);
    this.config = null;
    this.submitted = true;
    this.productInformationService.fnHttpGetConfigProducts(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        this.config = JSON.parse(JSON.stringify(r.body));
        console.log('this.config_products: ', this.config);
        this.bShowCents = r.body['bShowCents'];
        console.log('this.bShowCents: ', this.bShowCents);
        this.quantiy_decimals = (this.bShowCents) ? 2 : 0;
        console.log('this.quantiy_decimals: ', this.quantiy_decimals);
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  /** **/

  /** Funciones de la tabla dinamica **/

  onPivotReady(pivot: WebDataRocks.Pivot): void {
    console.log('[ready] WebDataRocksPivot', this.child);
  }

  onCustomizeCell(cell: WebDataRocks.CellBuilder, data: WebDataRocks.Cell): void {
    if (data.isClassicTotalRow) cell.addClass('fm-total-classic-r');
    if (data.isGrandTotalRow) cell.addClass('fm-grand-total-r');
    if (data.isGrandTotalColumn) cell.addClass('fm-grand-total-c');
  }

  onReportComplete(): void {
    setTimeout(() => {
      this.child.webDataRocks.off('reportcomplete');
      this.child.webDataRocks.setReport({
        dataSource: {
          data: this.list_pivot_table_original,
        },
        options: {
          grid: {
            type: 'classic',
            showTotals: false,
          },
          showCalculatedValuesButton: false,
          configuratorButton: false,
          showAggregationLabels: false,
        },
        slice: this.reportSlice == null ? this.reportSliceLocal : this.reportSlice,
        formats: [
          {
            name: 'variations',
            thousandsSeparator: ',',
            decimalSeparator: '.',
            decimalPlaces: 1,
            isPercent: true,
          },
          {
            name: '',
            thousandsSeparator: ',',
            decimalSeparator: '.',
            decimalPlaces: 2,
            isPercent: false,
          },
        ],
      });
      this.loading = false;
    }, 40000);
  }

  onCustomizeToolbar(toolbar): void {
    // get all tabs
    const tabs = toolbar.getTabs();
    toolbar.getTabs = function () {
      delete tabs[0]; // delete Connect button
      delete tabs[1]; // delete Open button
      delete tabs[2]; // delete Save button
      delete tabs[3]; // delete Export button
      delete tabs[4]; // delete separator
      delete tabs[5]; // delete Grid button
      delete tabs[6]; // delete files button
      delete tabs[7]; // delete fullscrem button
      return tabs;
    };
  }

  fieldsData(): void {
    this.child.webDataRocks.openFieldsList();
  }

  saveData(): void {
    const report = this.child.webDataRocks.getReport();
    this.reportSlice = JSON.stringify(report.slice);
    const report_Slice = JSON.parse(this.reportSlice);

    report_Slice.measures.forEach(datos => {
      if (datos.uniqueName.includes('variation')) {
        datos['format'] = 'variations';
      }

    });

    this.reportSlice = JSON.stringify(report_Slice);

    this.fnUpdatePivotTableState(this.current_payload, this.id_version, this.reportSlice);
  }

  /** **/


  /** Funciones para exportar**/
  exportData(type): void {
    this.child.webDataRocks.exportTo(type, { excelSheetName: 'Pivot table' });
  }
  /****/
}
