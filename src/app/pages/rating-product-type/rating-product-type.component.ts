import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { StateService } from '../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';
import { environment } from '../../../environments/environment';

import { FormControl, FormGroup, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';

import { UtilitiesService } from '../../shared/api/services/utilities.service';

import { RatingProductTypeService } from '../../shared/api/services/rating-product-type.service';
import { CategoriesService } from '../../shared/api/services/categories.service';

import { EditRatingProductTypeComponent } from './edit-rating-product-type/edit-rating-product-type.component';
import { DeleteRatingProductTypeComponent } from './delete-rating-product-type/delete-rating-product-type.component';
import { DeleteAllRatingProductTypeComponent } from './delete-all-rating-product-type/delete-all-rating-product-type.component';
import { AdvancedSearchRatingProductTypeComponent } from './advanced-search-rating-product-type/advanced-search-rating-product-type.component';

import { ModalsComponent } from '../../shared/components/modals/modals.component';

@Component({
  selector: 'ngx-rating-product-type',
  templateUrl: './rating-product-type.component.html',
  styleUrls: ['./rating-product-type.component.scss'],
})
export class RatingProductTypeComponent implements OnInit {

  url_host: any = environment.apiUrl;
  list_market_prices: any = null;
  layoud_component_display: String = 'grid';
  market_prices_original_collection: any = null;
  numItemsPage: any = null;
  currentPage: any = null;
  totalItems: any = null;
  loading: boolean = true;
  filter_state: boolean = false;
  items_filtered_advance_search: any = [];
  obj_market_prices: any = {};
  current_payload: string = null;
  id_version: any = null;
  submitted: any = false;
  fileToUpload: File = null;
  file_import_input: any = null;
  object_filter: any = {};
  data_market_prices: any = {};
  search_input: any = '';
  loadingChart: boolean = true;

  sorted: boolean = false;
  field_sort: any = null;
  state_sort_data: any = 'asc';

  filterState: boolean = false;

  text_searching: any = '';
 
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;
  DATA_LANG_GENERAL: any = null;
  
  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    private ratingProductTypeService: RatingProductTypeService,
    private categoriesService: CategoriesService,
  ) { }


  ngOnInit() {
    const self = this;
    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => { 
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    // });
    
    this.currentPage = 1;
    this.numItemsPage = 12;

    this.route.params.subscribe(params => {
      console.log('params: ', params);
      if (params.id_version) {
        this.id_version = params.id_version;
        this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
          console.log('token: ', token);
          if (token.isValid()) {
            // here we receive a payload from the token and assigne it to our `user` variable
            this.current_payload = token.getValue();
            console.log('this.current_payload: ', this.current_payload);
            if (this.current_payload) {
              this.object_filter['marketPrices'] = [];
              this.fnGetAllMarketPrice(this.current_payload, this.id_version, 1);
              return false;
            }
          }
        });
      }
    });
  }


  /** Funcion para cambiar de grid a cards **/
  fnSwitchViewData(state_view) {
    console.log('state_view: ', state_view);
    this.layoud_component_display = state_view;
  }
  /** **/


  /** Funciones para cargar los datos y Ordenar por columna **/
  fnGetAllMarketPrice(current_payload, id_version?, page?, text_search?) {

    let obj_Sort = null;

    if(this.sorted){
      obj_Sort = {
        'field' : this.field_sort,
        'dir': this.state_sort_data
      }
    }

    const object_data_send = {
      'products': [],
      'priceList': [],
      'category': [],
      'disbursement': [],
      'roleCategory': [],
      'page': (page) ? page : 1,
      'Sort': obj_Sort,
      'pageSize': 12,
      'tSearch': text_search,
    };

    this.list_market_prices = [];

    this.ratingProductTypeService.fnHttpGetAllRatingProductType(current_payload, id_version, object_data_send).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.filter_state = false;
        this.list_market_prices = JSON.parse(JSON.stringify(r.body.ratingProductTypes));
        this.market_prices_original_collection = JSON.parse(JSON.stringify(r.body.ratingProductTypes));
        this.totalItems = r.body['totalItems'];
        console.log('this.list_market_prices: ', this.list_market_prices);
        this.data_market_prices = this.list_market_prices;
        this.loadingChart = false;
        this.fnValidStateSearch(this.items_filtered_advance_search);
        // if (this.list_market_prices.length > 0) {
        // } else {
        //   this.utilitiesService.showToast('top-right', 'warning', 'Do not exist Market Prices, add new Market Prices!');
        // }
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  sortColumn(field) {
    console.log(`sortColumn(field)`);
    console.log('field: ', field);
    const self = this;

    self.sorted = true;
    self.field_sort = field;
    
    if (self.state_sort_data === 'asc') {
      self.state_sort_data = 'desc';
    } else {
      self.state_sort_data = 'asc';
    }
    if(this.filterState){
      this.fnGetAllMarketPricesFilter(this.current_payload, this.items_filtered_advance_search,  this.id_version);
    }else{
      this.fnGetAllMarketPrice(this.current_payload, this.id_version, 1, this.text_searching);    
    }
    this.currentPage = 1;
  }
  /** **/


  /** Funciones para filtrar y remover filtros **/
  fnFilterMarketPrices(market_prices, text_typing) {
    console.log('market_prices: ', market_prices);
    console.log('text_typing: ', text_typing);
    console.log('this.filter_state: ', this.filter_state);

    if (text_typing) {
      if (text_typing.length > 2) {
        console.log('Text ok');
        if (this.filter_state) {
          console.log('this.filter_state - 1: ', this.filter_state);
          this.fnGetAllMarketPricesFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
        } else {
          this.text_searching = text_typing;
          console.log('this.filter_state - 2: ', this.filter_state);
          this.fnGetAllMarketPrice(this.current_payload, this.id_version, 1, text_typing);
        }
      }
    } else {
      this.fnGetAllMarketPrice(this.current_payload, this.id_version, 1);
      console.log('Text not exist');
    }
  }

  fnGetAllMarketPricesFilter(current_payload, object_filter, id_version?, page?) {
    console.log('id_version: ', id_version);
    console.log('object_filter: ', object_filter);
    console.log('current_payload: ', current_payload);

    this.list_market_prices = [];
    this.fnGetAllMarketPricenAdvanceSearch(current_payload, this.id_version, object_filter, page);
  }

  fnGetAllMarketPricenAdvanceSearch(current_payload, id_version, data_object, page?) {
    console.log('current_payload: ', current_payload);
    const object_data_send = data_object;

    let obj_Sort = null;

    if(this.sorted){
      obj_Sort = {
        'field' : this.field_sort,
        'dir': this.state_sort_data
      }
    }

    object_data_send['Sort'] = obj_Sort;
    object_data_send['page'] = (page) ? page : 1;

    this.ratingProductTypeService.fnHttpGetAllRatingProductType(current_payload, id_version, object_data_send).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.loading = false;
        this.list_market_prices = JSON.parse(JSON.stringify(r.body.ratingProductTypes));
        console.log('this.list_market_prices: ', this.list_market_prices);
        this.market_prices_original_collection = JSON.parse(JSON.stringify(r.body.ratingProductTypes));
        console.log('this.market_prices_original_collection: ', this.market_prices_original_collection);
        this.totalItems = r.body['totalItems'];
        console.log('this.totalItems: ', this.totalItems);
        this.fnValidStateSearch(this.items_filtered_advance_search);
      }

      if (r.status == 206) {
        this.list_market_prices = [];
        this.market_prices_original_collection = [];
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  showModalAdvancedSearchMarketPrice(obj_market_prices) {
    console.log('obj_market_prices: ', obj_market_prices);
    console.log('this.items_filtered_advance_search: ', this.items_filtered_advance_search);
    obj_market_prices.id_version = this.id_version;
    obj_market_prices.items_filtered_advance_search = this.items_filtered_advance_search;
    console.log('obj_market_prices: ', obj_market_prices);
    this.dialogService.open(AdvancedSearchRatingProductTypeComponent, { context: obj_market_prices }).onClose.subscribe((res) => {
      console.log('res: ', res);
      if (res) {
        this.loading = true;
        this.filter_state = true;
        const data_object = res;
        console.log('data_object: ', data_object);
        this.items_filtered_advance_search = JSON.parse(JSON.stringify(res));
        this.fnGetAllMarketPricenAdvanceSearch(this.current_payload, this.id_version, data_object);
      }
    });
  }


  fnSetRemoveItemSearchFilteredAdvanced(index, data_collection) {
    console.log('index: ', index);
    console.log('data_collection: ', data_collection);
    data_collection.splice(index, 1);
    this.filter_state = this.fnValidStateSearch(this.items_filtered_advance_search);
    console.log('this.filter_state: ', this.filter_state);
    console.log('data_collection: ', data_collection);

    this.object_filter = this.items_filtered_advance_search;
    console.log('this.items_filtered_advance_search: ', this.items_filtered_advance_search);
    this.fnGetAllMarketPricesFilter(this.current_payload, this.items_filtered_advance_search, this.id_version);
  }

  fnClearAllFiltersSearch() {
    this.search_input = '';
    this.filterState = false;
    this.items_filtered_advance_search['products'] = [];
    this.items_filtered_advance_search['priceList'] = [];
    this.items_filtered_advance_search['category'] = [];
    this.items_filtered_advance_search['disbursement'] = [];
    this.items_filtered_advance_search['roleCategory'] = [];
    this.fnGetAllMarketPrice(this.current_payload, this.id_version);
  }

  fnValidStateSearch(collection) {
    console.log('collection: ', collection);
    if(typeof collection['category'] === "undefined"){
      console.log('indefinida')
      this.filterState = false;
      }else{
        console.log('collection: ', collection);
        if (collection['products'].length < 1 &&
          collection['priceList'].length < 1 &&
          collection['category'].length < 1 &&
          collection['disbursement'].length < 1 &&
          collection['roleCategory'].length < 1) {
          this.filterState = false;
          return false;
          } else {
            this.filterState = true;
            return true;
          }
      }
  }
  /****/


  /** Funciones para editar y borrar **/
  showModalEditMarketPrice(obj_market_prices) {
    console.log('obj_market_prices: ', obj_market_prices);
    // console.log('id_version: ', this.id_version);
    obj_market_prices.id_version = this.id_version;
    // obj_market_prices.iIDMarketPrice = obj_market_prices.iIDMarketPrice;
    console.log('obj_market_prices: ', obj_market_prices);

    this.dialogService.open(EditRatingProductTypeComponent, { context: { obj_market_prices } }).onClose.subscribe((res) => {
      console.log('res: ', res);
      console.log('obj_market_prices: ', obj_market_prices);
      if(res == false){
      this.id_version = obj_market_prices['id_version'];
      this.fnGetAllMarketPrice(this.current_payload, this.id_version, this.currentPage);
      }
    });
  }

  fnDeleteMarketPrices(data_market_prices) {
    data_market_prices.id_version = this.id_version;
    console.log('data_market_prices: ', data_market_prices);    
    this.dialogService.open(DeleteRatingProductTypeComponent, { context: data_market_prices }).onClose.subscribe((res) => {
      console.log('res: ', res);
      console.log('obj_market_prices: ', data_market_prices);
      if(res == false){
      this.id_version = data_market_prices['id_version'];
      this.fnGetAllMarketPrice(this.current_payload, this.id_version);
      this.currentPage = 1;
      }
    });
  }

  fnDeleteAllMarketPrice(obj_market_prices) {
    console.log('obj_market_prices: ', obj_market_prices);
    obj_market_prices.id_version = this.id_version;
    console.log('obj_market_prices: ', obj_market_prices);
    this.dialogService.open(DeleteAllRatingProductTypeComponent, { context: obj_market_prices }).onClose.subscribe((res) => {
      console.log('res: ', res);
      if(res == false){
      this.fnGetAllMarketPrice(this.current_payload, this.id_version);
      this.currentPage = 1;
      }
    });
  }
  /****/


  /** Funciones para exportar e importar **/
  fnExportMarketPrice() {
    const lang = localStorage.getItem('language_app');
    window.open(
      this.url_host + '/api/RatingProductType/GetExportRatingProductType?iIDVersion=' + this.id_version + '&language=' + lang,
      '_blank');
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.uploadFileToActivity(this.fileToUpload);
  }

  uploadFileToActivity(fileToUpload) {
    console.log('fileToUpload: ', fileToUpload);
    const end_point_url = '/api/RatingProductType/PostImportRatingProductType';
    const parameter = 'iIDVersion';
    this.loadingChart = true;
    this.utilitiesService.fnHttSetUploadFile(this.current_payload, this.fileToUpload, this.id_version, end_point_url, parameter).subscribe(data => {
      console.log('data: ', data);
      // do something, if upload success

      if (data.status == 200) {
        this.loadingChart = false;
        this.file_import_input = null;
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessImport.text);
        this.fnGetAllMarketPrice(this.current_payload, this.id_version);
      }
      if (data.status == 206) {
        this.loadingChart = false;
        const error = this.utilitiesService.fnSetErrors(null, data.body.message);
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, error => {
      this.loadingChart = false;
      console.log(error);
    });
    this.file_import_input = null;
  }
  /****/


  /** Funciones para Pagindo **/
  getPage(page: number) {
    console.log('page: ', page);
    page;
    this.loading = true;
    if(this.filterState){
      this.fnGetAllMarketPricesFilter(this.current_payload, this.items_filtered_advance_search, this.id_version, page);
    }else{
      this.fnGetAllMarketPrice(this.current_payload, this.id_version, page, this.text_searching);
    }
    this.currentPage = page;
  }
  
  /*********Show modal parameters function *********/

  open(_title,_body) {
    this.dialogService.open(ModalsComponent, {
      context: {
        title: _title,
        body:_body
      },
    });
  }

  fnGetLanguage(){
    const self = this;
    self.utilitiesService.fnGetLanguage('ratingProductType', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module,
      self.DATA_LANG_GENERAL = res_lang.general
    });
  }

}
