import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options, LabelType } from 'ng5-slider';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { RatingPerceivedService } from '../../../shared/api/services/rating-perceived.service';
import { RatingProductTypeService } from '../../../shared/api/services/rating-product-type.service';
import { ConfigService } from '../../../shared/api/services/config.service';

declare var $: any;

@Component({
  selector: 'ngx-edit-rating-product-type',
  templateUrl: './edit-rating-product-type.component.html',
  styleUrls: ['./edit-rating-product-type.component.scss'],
})

export class EditRatingProductTypeComponent implements OnInit {

  submitted: boolean = false;
  state_btn_next: boolean = false;
  state_btn_preview: boolean = true;
  current_payload: any = null;

  id_version: string;
  id_Product: string;

  list_rating_produc_type_collection: any = [];
  list_competitors: any = [];
  obj_form_market: any = {};

  select_competitor: any = {};
  input_product: any = '';
  input_prices: any = '';

  index_ant: number = null;

  competitor_collection: any = [];

  @Input() obj_market_prices: any;
 
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private ratingProductTypeService: RatingProductTypeService,
    private configService: ConfigService,
    protected ref: NbDialogRef<EditRatingProductTypeComponent>,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('ratingProductType', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();

        this.id_version = this.obj_market_prices['id_version'];
        this.id_Product = this.obj_market_prices['iIDProduct'];
        console.log('this.current_payload: ', this.current_payload);
        console.log('id_version======================>: ', this.id_version);
        console.log('obj_market_prices======================>: ', this.obj_market_prices);

        this.obj_form_market = {
          'iIDVersion': parseInt(this.id_version, 10),
          'iIDProduct': this.obj_market_prices['iIDProduct'],
          'tProductCode': this.obj_market_prices['tProductCode'],
          'tProductName': this.obj_market_prices['tProductName'],
          'tPriceListName': this.obj_market_prices['tPriceListName'],
          'ptDisbursement': {
            'iIDProductTypeVersion': this.obj_market_prices.ptDisbursement ? this.obj_market_prices.ptDisbursement['iIDProductTypeVersion'] : '',
            'tProductTypeName': this.obj_market_prices.ptDisbursement ? this.obj_market_prices.ptDisbursement['tProductTypeName'] : '',
          },
          'ptRoleCategory': {
            'iIDProductTypeVersion': this.obj_market_prices.ptRoleCategory ? this.obj_market_prices.ptRoleCategory['iIDProductTypeVersion'] : '',
            'tProductTypeName': this.obj_market_prices.ptRoleCategory ? this.obj_market_prices.ptRoleCategory['tProductTypeName'] : '',
          },
        };

        this.fnGetRatingProducType(this.current_payload, this.id_version);
      }
    });
  }

  /** Funcion para cargar la DDl de competidores**/
  fnGetRatingProducType(current_payload, id_version?) {
    const self = this;
    self.configService.fnHttpGetListProductTypesVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.list_rating_produc_type_collection = JSON.parse(JSON.stringify(r.body));
        console.log('self.list_rating_produc_type_collection: ', self.list_rating_produc_type_collection);
        self.submitted = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        const error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }
  /** **/

  /** Funcion que envia los datos al servicio de actalizar **/
  fnUpdateDataMarketPrices(obj) {
    this.submitted = true;

    this.ratingProductTypeService.fnHttpSetEditDataRatingProductTypes(this.current_payload, this.obj_form_market).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessUpdate.text);
        this.dismiss(false);
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }
  /** **/


  /** Funciones para cancelar y cerrar **/
  fnCancelEditMarketPrices() {
    this.submitted = false;
    this.dismiss(true);
  }

  dismiss(modalAction) {
    this.ref.close(modalAction);
  }
  /** **/

}
