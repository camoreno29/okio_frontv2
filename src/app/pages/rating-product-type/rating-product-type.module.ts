import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng5SliderModule } from 'ng5-slider';


import { AdvancedSearchRatingProductTypeComponent } from './advanced-search-rating-product-type/advanced-search-rating-product-type.component';
import { DeleteAllRatingProductTypeComponent } from './delete-all-rating-product-type/delete-all-rating-product-type.component';
import { DeleteRatingProductTypeComponent } from './delete-rating-product-type/delete-rating-product-type.component';
import { EditRatingProductTypeComponent } from './edit-rating-product-type/edit-rating-product-type.component';

const ENTRY_COMPONENTS = [
  AdvancedSearchRatingProductTypeComponent,
  DeleteAllRatingProductTypeComponent,
  DeleteRatingProductTypeComponent,
  EditRatingProductTypeComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    TooltipModule.forRoot(),
    ModalModule,
    NgSelectModule,
    FormsModule,
    NgxPaginationModule,
    Ng5SliderModule,
  ],
  declarations: [
    AdvancedSearchRatingProductTypeComponent,
    DeleteAllRatingProductTypeComponent,
    DeleteRatingProductTypeComponent,
    EditRatingProductTypeComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})

export class RatingProductTypeModule { }
