import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAllMarketPriceComponent } from './delete-all-market-price.component';

describe('DeleteAllMarketPriceComponent', () => {
  let component: DeleteAllMarketPriceComponent;
  let fixture: ComponentFixture<DeleteAllMarketPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteAllMarketPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAllMarketPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
