import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng5SliderModule } from 'ng5-slider';


import { EditPriceRelationshipsComponent } from './edit-price-relationships/edit-price-relationships.component';
import { DeletePriceRelationshipsComponent } from './delete-price-relationships/delete-price-relationships.component';
import { DeleteAllPriceRelationshipsComponent } from './delete-all-price-relationships/delete-all-price-relationships.component';
import { AdvancedSearchPriceRelationshipsComponent } from './advanced-search-price-relationships/advanced-search-price-relationships.component';

const ENTRY_COMPONENTS = [
  AdvancedSearchPriceRelationshipsComponent,
  DeleteAllPriceRelationshipsComponent,
  DeletePriceRelationshipsComponent,
  EditPriceRelationshipsComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    TooltipModule.forRoot(),
    ModalModule,
    NgSelectModule,
    FormsModule,
    NgxPaginationModule,
    Ng5SliderModule,
  ],
  declarations: [
    AdvancedSearchPriceRelationshipsComponent,
    DeleteAllPriceRelationshipsComponent,
    DeletePriceRelationshipsComponent,
    EditPriceRelationshipsComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})

export class PriceRelationshipseModule { }
