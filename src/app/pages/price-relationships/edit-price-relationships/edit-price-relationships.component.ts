import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options, LabelType } from 'ng5-slider';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { PriceRelationshipsService } from '../../../shared/api/services/price-relationships.service';
import { ProductInformationService } from '../../../shared/api/services/product-information.service';
import { PricesListService } from '../../../shared/api/services/prices-list.service';
import { ConfigService } from '../../../shared/api/services/config.service';

declare var $: any;

@Component({
  selector: 'ngx-edit-price-relationships',
  templateUrl: './edit-price-relationships.component.html',
  styleUrls: ['./edit-price-relationships.component.scss'],
})

export class EditPriceRelationshipsComponent implements OnInit {

  submitted: boolean = false;
  current_payload: any = null;

  id_version: string;
  id_Product: string;

  list_products_code_relationships_collection: any = [];
  list_products_name_relationships_collection: any = [];
  list_prices_lists_collection: any = [];
  obj_form_prices_relationships: any = {};
  object_data_send: any = {};
  data_reference_product_code: Object = {};
  data_reference_product_name: Object = {};
  data_reference_prices_lists: Object = {};


  @Input() obj_prices_relationships: any;

  lang_nav: any = window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private priceRelationshipsService: PriceRelationshipsService,
    private productInformationService: ProductInformationService,
    private pricesListService: PricesListService,
    private configService: ConfigService,
    protected ref: NbDialogRef<EditPriceRelationshipsComponent>,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('priceRelationships', function (res_lang) {
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });

    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        this.id_version = this.obj_prices_relationships['id_version'];
        this.id_Product = this.obj_prices_relationships['iIDProduct'];
        console.log('this.current_payload: ', this.current_payload);
        console.log('id_version======================>: ', this.id_version);
        console.log('obj_market_prices======================>: ', this.obj_prices_relationships);

        this.obj_form_prices_relationships = {
          'iIDVersion': parseInt(this.id_version, 10),
          'iIDProduct': this.obj_prices_relationships['iIDProduct'],
          'tProductCode': this.obj_prices_relationships['tProductCode'],
          'tProductName': this.obj_prices_relationships['tProductName'],
          'tPriceListName': this.obj_prices_relationships['tPriceListName'],
          'iIDReferenceProduct': this.obj_prices_relationships['iIDReferenceProduct'],
          'tProductCodeReference': this.obj_prices_relationships['tProductCodeReference'],
          'tProductNameReference': this.obj_prices_relationships['tProductNameReference'],
          'tPriceListNameReference': this.obj_prices_relationships['tPriceListNameReference'],
          'dRelationship': this.obj_prices_relationships['dRelationship'],
        };

        this.data_reference_product_code = this.obj_prices_relationships['tProductCodeReference'];
        this.data_reference_product_name = this.obj_prices_relationships['tProductNameReference'];
        this.data_reference_prices_lists = this.obj_prices_relationships['tPriceListNameReference'];
        if (this.data_reference_product_code != "") {
          this.changeDataRelation(this.data_reference_product_code);
        }
        else {

          this.fnGetListProductsCode(this.current_payload, this.id_version);
          this.fnGetListProducts(this.current_payload, this.id_version);
          this.fnGetPricesLists(this.current_payload, this.id_version);
        }

      }
    });
  }

  /** Funcion para cargar la DDl de competidores**/

  fnGetListProductsCode(current_payload, id_version?) {
    const self = this;
    self.productInformationService.fnHttpGetListProductsCode(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {

        self.list_products_code_relationships_collection = JSON.parse(JSON.stringify(r.body));
        console.log('self.list_products_code_relationships_collection: ', self.list_products_code_relationships_collection);
        self.submitted = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        const error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }


  fnGetListProducts(current_payload, id_version?) {
    const self = this;
    self.productInformationService.fnHttpGetListProductsName(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {

        self.list_products_name_relationships_collection = JSON.parse(JSON.stringify(r.body));
        console.log('self.list_products_name_relationships_collection: ', self.list_products_name_relationships_collection);
        self.submitted = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        const error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetPricesLists(current_payload, id_version?) {
    const self = this;
    self.pricesListService.fnHttpGetPricesListProducts(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {

        self.list_prices_lists_collection = JSON.parse(JSON.stringify(r.body));
        console.log('self.list_prices_lists_collection: ', self.list_prices_lists_collection);
        self.submitted = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        const error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }
  /** **/

  changeDataRelation(event) {
    console.log('event: ', event);

    if (event) {
      this.object_data_send = {
        'tProductCode': event.tProductCode ? event.tProductCode : event ? event : '',
        'iIDPriceList': event.iIDPriceList ? event.iIDPriceList : null,
      };

      const self = this;
      self.productInformationService.fnHttpGetListProductsandPrices(this.current_payload, this.id_version, this.object_data_send).subscribe(r => {
        console.log('r: ', r);
        if (r.status == 200) {

          self.list_products_code_relationships_collection = JSON.parse(JSON.stringify(r.body));
          self.list_products_name_relationships_collection = JSON.parse(JSON.stringify(r.body));
          self.list_prices_lists_collection = JSON.parse(JSON.stringify(r.body));
          console.log('self.list_products_code_relationships_collection: ', self.list_products_code_relationships_collection);
          console.log('self.list_products_name_relationships_collection: ', self.list_products_name_relationships_collection);
          console.log('self.list_prices_lists_collection: ', self.list_prices_lists_collection);
          self.submitted = false;
        }
        if (r.status == 206) {
          self.submitted = false;
          const error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
          console.log('error: ', error);
          self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
        }
      }, err => {
        console.log('err: ', err);
      });
    }
    else {
      this.fnGetListProductsCode(this.current_payload, this.id_version);
      this.fnGetListProducts(this.current_payload, this.id_version);
      this.fnGetPricesLists(this.current_payload, this.id_version);
    }



  }


  /** Funcion que envia los datos al servicio de actualizar **/
  fnUpdateDataPriceRelationships(obj) {
    this.submitted = true;
    // this.obj_form_prices_relationships.iIDReferenceProduct = this.data_reference_product_name['iIDProduct'] ?
    //   this.data_reference_product_name['iIDProduct'] : this.data_reference_prices_lists['iIDProduct'] ?
    //     this.data_reference_prices_lists['iIDProduct'] : '';
    this.obj_form_prices_relationships.iIDReferenceProduct = this.data_reference_product_name['iIDProduct'] ?
      this.data_reference_product_name['iIDProduct'] : this.data_reference_prices_lists['iIDProduct'] ?
        this.data_reference_prices_lists['iIDProduct'] : this.obj_form_prices_relationships.iIDReferenceProduct;
    this.obj_form_prices_relationships.tProductCodeReference = this.data_reference_product_code['tProductCode'] ?
      this.data_reference_product_code['tProductCode'] : this.data_reference_product_code ? this.data_reference_product_code : '';
    this.obj_form_prices_relationships.tProductNameReference = this.data_reference_product_name['tProductName'];
    this.obj_form_prices_relationships.tPriceListNameReference = this.data_reference_prices_lists['tPriceListName'];

    this.priceRelationshipsService.fnHttpSetEditDataPriceRelationships(this.current_payload, this.obj_form_prices_relationships).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessUpdate.text);
        this.dismiss(false);
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }
  /** **/


  /** Funciones para cancelar y cerrar **/
  fnCancelEditPriceRelationships() {
    this.submitted = false;
    this.dismiss(true);
  }

  dismiss(modalAction) {
    this.ref.close(modalAction);
  }
  /** **/

  /*****(currencyInputChanged)add thousands format*****/
  currencyInputChanged(val) {
    if (val) {
      val = this.format_number(val, '');
    }
    return val;
  }

  format_number(number, prefix) {
    let thousand_separator = ',',
      decimal_separator = '.',
      regex = new RegExp('[^' + decimal_separator + '\\d]', 'g'),
      number_string = number.replace(regex, '').toString(),
      split = number_string.split(decimal_separator),
      rest = split[0].length % 3,
      result = split[0].substr(0, rest),
      thousands = split[0].substr(rest).match(/\d{3}/g);

    if (thousands) {
      let separator = rest ? thousand_separator : '';
      result += separator + thousands.join(thousand_separator);
    }
    result = split[1] != undefined ? result + decimal_separator + split[1] : result;
    return prefix == undefined ? result : (result ? prefix + result : '');
  };

  /*****(numberOnly)allows only numbers and decimals*****/

  numberOnly(evt): boolean {
    const charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 46 && evt.srcElement.value.split('.').length > 1) {
      return false;
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

}
