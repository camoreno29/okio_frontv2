import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options } from 'ng5-slider';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CompetitorsService } from '../../../shared/api/services/competitors.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { PricesListService } from '../../../shared/api/services/prices-list.service';
import { ProductInformationService } from '../../../shared/api/services/product-information.service';
import { ConfigService } from '../../../shared/api/services/config.service';

@Component({
  selector: 'ngx-advanced-search-price-relationships',
  templateUrl: './advanced-search-price-relationships.component.html',
  styleUrls: ['./advanced-search-price-relationships.component.scss'],
})
export class AdvancedSearchPriceRelationshipsComponent implements OnInit {

  @Input() id_version: string;
  @Input() items_collection_filtered: any;

  submitted: boolean = false;
  current_payload: any = null;
  prices_relationships_data: any = {};
  list_products_collection: any = [];
  list_categories_collection: any = [];
  list_prices_lists_collection: any = [];
  state_loading: boolean = true;


  data_filter_products: Object = {};
  data_filter_price_list: Object = {};
  data_filter_categories: Object = {};
  data_filter_products_references: Object = {};
  data_filter_price_list_references: Object = {};
  min_value_Relation: number = null;
  max_value_Relation: number = null;
 
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private competitorsService: CompetitorsService,
    private categoriesService: CategoriesService,
    private pricesListService: PricesListService,
    private productInformationService: ProductInformationService,
    private configService: ConfigService,
    protected ref: NbDialogRef<AdvancedSearchPriceRelationshipsComponent>,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('priceRelationships', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
        console.log('id_version======================>: ', this.id_version);

        console.log('items_collection_filtered======================>: ', this.items_collection_filtered);

        this.data_filter_products = this.items_collection_filtered['products'];
        this.data_filter_price_list = this.items_collection_filtered['priceList'];
        this.data_filter_categories = this.items_collection_filtered['category'];
        this.data_filter_products_references = this.items_collection_filtered['productsReferences'];
        this.data_filter_price_list_references = this.items_collection_filtered['priceListReferences'];
        this.min_value_Relation = (this.items_collection_filtered['dRelation']) ? this.items_collection_filtered['dRelation']['dMin'] : null;
        this.max_value_Relation = (this.items_collection_filtered['dRelation']) ? this.items_collection_filtered['dRelation']['dMax'] : null;

        this.fnGetListCategories(this.current_payload, this.id_version);
        this.fnGetListProducts(this.current_payload, this.id_version);
        this.fnGetPricesLists(this.current_payload, this.id_version);
      }
    });
  }

  /** Funciones para cargar los datos **/
  fnGetListCategories(current_payload, id_version) {
    this.categoriesService.fnHttpGetDataListCategoriesByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_categories_collection = JSON.parse(JSON.stringify(r.body));
        this.submitted = false;
      }
      if (r.status == 206) {
        this.submitted = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnGetListProducts(current_payload, id_version) {
    const self = this;
    self.productInformationService.fnHttpGetListProducts(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_products_collection = JSON.parse(JSON.stringify(r.body));
        self.submitted = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        const error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetPricesLists(current_payload, id_version?) {
    const self = this;
    self.pricesListService.fnGetAllPricesListByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.list_prices_lists_collection = JSON.parse(JSON.stringify(r.body));
        console.log('self.list_prices_lists_collection: ', self.list_prices_lists_collection);
        self.submitted = false;
        this.state_loading = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        const error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }
  /** **/


  /** Funciones para enviar Datos al servicio cuando se cierrar el componente o cancelar **/
  fnAdvancedSearchPricesRelationships(prices_relationships_data) {
    this.submitted = true;
    console.log('prices_relationships_data: ', prices_relationships_data);
    const data_object = {
      'products': (prices_relationships_data.products) ? prices_relationships_data.products : [],
      'priceList': (prices_relationships_data.priceList) ? prices_relationships_data.priceList : [],
      'category': (prices_relationships_data.categories) ? prices_relationships_data.categories : [],
      'productsReferences': (prices_relationships_data.productsReferences) ? prices_relationships_data.productsReferences : [],
      'priceListReferences': (prices_relationships_data.priceListReferences) ? prices_relationships_data.priceListReferences : [],
      'dRelation': {
        'dMin': prices_relationships_data.min_value_Relation,
        'dMax': prices_relationships_data.max_value_Relation == 0 ? '' : prices_relationships_data.max_value_Relation,
      },
      'page': 1,
      'pageSize': 12,
      'tSearch': '',
    };
    console.log('data_object: ', data_object);
    this.dismiss(data_object);
  }

  fnCancelCreateNewPricesRelationships() {
    this.submitted = false;
    this.dismiss();

  }

  dismiss(object_response_onclose?) {
    console.log('object_response_onclose: ', object_response_onclose);
    if (object_response_onclose) {
      object_response_onclose['products'] = (this.data_filter_products) ? this.data_filter_products : [];
      object_response_onclose['priceList'] = (this.data_filter_price_list) ? this.data_filter_price_list : [];
      object_response_onclose['category'] = (this.data_filter_categories) ? this.data_filter_categories : [];
      object_response_onclose['productsReferences'] = (this.data_filter_products_references) ? this.data_filter_products_references : [];
      object_response_onclose['priceListReferences'] = (this.data_filter_price_list_references) ? this.data_filter_price_list_references : [];
      object_response_onclose['dRelation'] = (this.max_value_Relation) ? { 'dMin': this.min_value_Relation, 'dMax': this.max_value_Relation } : { 'dMin': this.min_value_Relation, 'dMax': null };

      this.ref.close(object_response_onclose);
    } else {
      this.ref.close();
    }
  }
  /** **/

}

