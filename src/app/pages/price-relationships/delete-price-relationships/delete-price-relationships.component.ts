import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

import { Router, ActivatedRoute } from '@angular/router';

import { StateService } from '../../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';

import { PriceRelationshipsService } from '../../../shared/api/services/price-relationships.service';

@Component({
  selector: 'ngx-delete-price-relationships',
  templateUrl: './delete-price-relationships.component.html',
  styleUrls: ['./delete-price-relationships.component.scss']
})
export class DeletePriceRelationshipsComponent implements OnInit {

  @Input() iIDVersion: String;
  @Input() iIDProduct: String;
  @Input() tProductCode: String;
  @Input() tProductName: String;
  tProduct: String;
  data_prices_relationships: any = {};
  id_category: any = null;
  current_payload: string = null;
  submitted: Boolean = false;

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    protected ref: NbDialogRef<DeletePriceRelationshipsComponent>,
    private priceRelationshipsService: PriceRelationshipsService,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('priceRelationships', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    // this.iIDVersion;
    console.log('this.iIDVersion: ', this.iIDVersion);
    console.log('this.iIDVersion: ', this.iIDProduct);
    console.log('this.data_prices_relationships: ', this.data_prices_relationships);
    this.tProduct = this.tProductCode + '-' + this.tProductName;

    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
      }
    });
  }


  fnSetDeletePriceRelationshipsById(iIDProduct) {
    this.submitted = true;
    this.priceRelationshipsService.fnHttpSetRestorePriceRelationshipsById(this.current_payload, iIDProduct).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessDelete.text);
        this.dismiss(false);
      }
      if (r.status == 206) {
        this.submitted = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  dismiss(modalAction) {
    this.ref.close(modalAction);
  }

  fnCancelPriceRelationships() {
    this.dismiss(true);
  }

}
