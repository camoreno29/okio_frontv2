import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { Router, ActivatedRoute } from '@angular/router';
import { StateService } from '../../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { PriceRelationshipsService } from '../../../shared/api/services/price-relationships.service';

@Component({
  selector: 'ngx-delete-all-price-relationships',
  templateUrl: './delete-all-price-relationships.component.html',
  styleUrls: ['./delete-all-price-relationships.component.scss'],
})
export class DeleteAllPriceRelationshipsComponent implements OnInit {

  @Input() id_version: String;
  category_data: any = {};
  current_payload: string = null;
  submitted: Boolean = false;
  search_input: any = '';
 
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    protected ref: NbDialogRef<DeleteAllPriceRelationshipsComponent>,
    private priceRelationshipsService: PriceRelationshipsService,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('priceRelationships', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.category_data.id_version = this.id_version;
    console.log('this.category_data: ', this.category_data);

    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
      }
    });
  }

  fnDeleteAllPriceRelationships() {
    this.submitted = true;
    this.priceRelationshipsService.fnHttpSetRestoreALLPriceRelationshipsById(this.current_payload, this.id_version).subscribe(r => {
      console.log('r: ', r);
      
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessDeleteAll.text);
        this.dismiss(false);
      }
      if (r.status == 206) {
        this.submitted = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnCancelDeleteAllPriceRelationships() {
    this.dismiss(true);
  }

  dismiss(modalAction) {
    this.ref.close(modalAction);
  }


}
