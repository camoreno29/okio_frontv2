import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options } from 'ng5-slider';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { MarketPriceService } from '../../../shared/api/services/market-price.service';
import { ProductInformationService } from '../../../shared/api/services/product-information.service';
import { CompetitorsService } from '../../../shared/api/services/competitors.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { PricesListService } from '../../../shared/api/services/prices-list.service';

@Component({
  selector: 'ngx-advanced-search-market-price',
  templateUrl: './advanced-search-market-price.component.html',
  styleUrls: ['./advanced-search-market-price.component.scss'],
})
export class AdvancedSearchMarketPriceComponent implements OnInit {

  @Input() id_version: string;
  @Input() items_filtered_advance_search: any;

  submitted: boolean = false;
  current_payload: any = null;
  market_prices_data: any = {};
  min_value_price: number = 0;
  max_value_price: number = 0;
  list_products_collection: any = [];
  list_competitors: any = [];
  list_categories_collection: any = [];
  list_prices_lists_collection: any = [];
  state_loading: boolean = true;


  data_filter_categories: any = [];
  data_filter_price_list: any = [];
  data_filter_competitors: any = [];
  data_filter_products: any = [];
  data_filter_products_competitor: string = '';
  data_filter_price_min: any = null;
  data_filter_price_max: any = null;
 
  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private marketPriceService: MarketPriceService,
    private productInformationService: ProductInformationService,
    private competitorsService: CompetitorsService,
    private categoriesService: CategoriesService,
    private pricesListService: PricesListService,
    protected ref: NbDialogRef<AdvancedSearchMarketPriceComponent>,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('marketPrices', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();
        console.log('this.current_payload: ', this.current_payload);
        console.log('id_version======================>: ', this.id_version);
        console.log('items_filtered_advance_search======================>: ', this.items_filtered_advance_search);

        this.data_filter_categories = this.items_filtered_advance_search['category'];
        this.data_filter_price_list = this.items_filtered_advance_search['priceList'];
        this.data_filter_competitors = this.items_filtered_advance_search['competitors'];
        this.data_filter_products = this.items_filtered_advance_search['products'];
        this.data_filter_products_competitor = this.items_filtered_advance_search['productsCompetitor'];
        this.data_filter_price_min = (this.items_filtered_advance_search['dPrices']) ? this.items_filtered_advance_search['dPrices']['dMin'] : null;
        this.data_filter_price_max = (this.items_filtered_advance_search['dPrices']) ? this.items_filtered_advance_search['dPrices']['dMax'] : null;

        this.fnGetListCompetitors(this.current_payload, this.id_version);
        this.fnGetListCategories(this.current_payload, this.id_version);
        this.fnGetListProducts(this.current_payload, this.id_version);
        this.fnGetPricesLists(this.current_payload, this.id_version);
      }
    });
  }

  /** Funciones para cargar los datos **/
  fnGetListCategories(current_payload, id_version) {
    this.categoriesService.fnHttpGetDataListCategoriesByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_categories_collection = JSON.parse(JSON.stringify(r.body));
        this.submitted = false;
      }
      if (r.status == 206) {
        this.submitted = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnGetListCompetitors(current_payload, id_version) {
    this.competitorsService.fnHttpGetDataListCompetitorsByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_competitors = JSON.parse(JSON.stringify(r.body));
        this.submitted = false;
      }
      if (r.status == 206) {
        this.submitted = false;
        const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }

  fnGetListProducts(current_payload, id_version) {
    const self = this;
    self.productInformationService.fnHttpGetListProducts(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_products_collection = JSON.parse(JSON.stringify(r.body));
        self.submitted = false;
        this.state_loading = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        const error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnGetPricesLists(current_payload, id_version?) {
    const self = this;
    self.pricesListService.fnGetAllPricesListByVersion(current_payload, id_version).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        self.list_prices_lists_collection = JSON.parse(JSON.stringify(r.body));
        console.log('self.list_prices_lists_collection: ', self.list_prices_lists_collection);
        self.submitted = false;
      }
      if (r.status == 206) {
        self.submitted = false;
        const error = self.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        self.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      console.log('err: ', err);
    });
  }
  /** **/


  /** Funciones para enviar Datos al servicio cuando se cierrar el componente o cancelar **/
  fnAdvancedSearchMarketPrice(market_prices_data) {
    this.submitted = true;
    console.log('market_prices_data: ', market_prices_data);
    const data_object = {
      'products': (market_prices_data.products) ? market_prices_data.products : [],
      'priceList': (market_prices_data.priceList) ? market_prices_data.priceList : [],
      'category': (market_prices_data.categories) ? market_prices_data.categories : [],
      'competitors': (market_prices_data.competitors) ? market_prices_data.competitors : [],
      'productsCompetitor': (market_prices_data.productsCompetitor) ? market_prices_data.productsCompetitor : null,
      'dPrices': {
        'dMin': this.min_value_price == null ? null : this.min_value_price,
        'dMax': this.max_value_price == null ? null : this.max_value_price,
      },
      'page': 1,
      'pageSize': 12,
      'tSearch': '',
    };
    console.log('data_object: ', data_object);
    this.dismiss(data_object);
  }

  fnCancelCreateNewMarketPrice() {
    this.submitted = false;
    this.dismiss();

  }

  dismiss(object_response_onclose?) {
    if (object_response_onclose) {
      object_response_onclose['category'] = (this.data_filter_categories) ? this.data_filter_categories : [];
      object_response_onclose['competitors'] = (this.data_filter_competitors) ? this.data_filter_competitors : [];
      object_response_onclose['products'] = (this.data_filter_products) ? this.data_filter_products : [];
      object_response_onclose['productsCompetitor'] = (this.data_filter_products_competitor) ? this.data_filter_products_competitor : null;
      object_response_onclose['priceList'] = (this.data_filter_price_list) ? this.data_filter_price_list : [];
      object_response_onclose['dPrices'] = { 'dMin': this.data_filter_price_min, 'dMax': this.data_filter_price_max } ;
      this.ref.close(object_response_onclose);
    } else {
      this.ref.close();
    }
  }
  /** **/

}

