import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';

import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng5SliderModule } from 'ng5-slider';


// import { MarketPriceComponent } from './market-price.component';
import { AdvancedSearchMarketPriceComponent } from './advanced-search-market-price/advanced-search-market-price.component';
import { DeleteAllMarketPriceComponent } from './delete-all-market-price/delete-all-market-price.component';
import { DeleteMarketPriceComponent } from './delete-market-price/delete-market-price.component';
import { EditMarketPriceComponent } from './edit-market-price/edit-market-price.component';

const ENTRY_COMPONENTS = [
  AdvancedSearchMarketPriceComponent,
  DeleteAllMarketPriceComponent,
  DeleteMarketPriceComponent,
  EditMarketPriceComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    TooltipModule.forRoot(),
    ModalModule,
    NgSelectModule,
    FormsModule,
    NgxPaginationModule,
    Ng5SliderModule,
  ],
  declarations: [
    // MarketPriceComponent,
    AdvancedSearchMarketPriceComponent,
    DeleteAllMarketPriceComponent,
    DeleteMarketPriceComponent,
    EditMarketPriceComponent,
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})

export class MarketPriceModule { }
