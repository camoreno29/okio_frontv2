import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { Options, LabelType } from 'ng5-slider';

import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { CategoriesService } from '../../../shared/api/services/categories.service';
import { RatingPerceivedService } from '../../../shared/api/services/rating-perceived.service';
import { MarketPriceService } from '../../../shared/api/services/market-price.service';

declare var $: any;

@Component({
  selector: 'ngx-edit-market-price',
  templateUrl: './edit-market-price.component.html',
  styleUrls: ['./edit-market-price.component.scss'],
})

export class EditMarketPriceComponent implements OnInit {

  submitted: boolean = false;
  state_btn_next: boolean = false;
  state_btn_preview: boolean = true;
  state_btn_update: boolean = true;
  current_payload: any = null;

  id_version: string;
  id_Product: string;

  list_competitors: any = [];
  obj_form_market: any = {};

  select_competitor: any = {};
  input_product: any = '';
  input_prices: any = '';

  index_ant: number = null;

  competitor_collection: any = [];

  data_competitors_values: any = [];

  @Input() obj_market_prices: any;

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private marketPriceService: MarketPriceService,
    protected ref: NbDialogRef<EditMarketPriceComponent>,
  ) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('marketPrices', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
    
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.current_payload = token.getValue();

        this.id_version = this.obj_market_prices['id_version'];
        this.id_Product = this.obj_market_prices['iIDProduct'];
        console.log('this.current_payload: ', this.current_payload);
        console.log('id_version======================>: ', this.id_version);
        console.log('obj_market_prices======================>: ', this.obj_market_prices);


        this.obj_form_market = {
          'iIDVersion': parseInt(this.id_version, 10),
          'iIDProduct': this.obj_market_prices['iIDProduct'],
          'iIDMarketPrice': this.obj_market_prices['iIDMarketPrice'],
          'tProductCode': this.obj_market_prices['tProductCode'],
          'tProductName': this.obj_market_prices['tProductName'],
          'tPriceListName': this.obj_market_prices['tPriceListName'],
        };
        console.log('this.select_competitor===========================>>>>: ', this.select_competitor);
        this.fnGetListCompetitors(this.current_payload, this.id_Product);
        this.fngetDataCompetitor(this.obj_market_prices);
        this.fnDataCompetitor(5);
        this.fnDataCompetitor(4);
        this.fnDataCompetitor(3);
        this.fnDataCompetitor(2);
        this.fnDataCompetitor(1);
      }
    });
  }

  /** Funcion para cargar la DDl de competidores**/
  fnGetListCompetitors(current_payload, id_Product) {
    this.marketPriceService.fnHttpGetDataListCompetitorsByProduct(current_payload, id_Product).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.list_competitors = JSON.parse(JSON.stringify(r.body));
        this.submitted = false;
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }
  /** **/


  /** Funciones para cargar los competidores con los datos que se envian desde la grilla **/
  fngetDataCompetitor(obj_market_prices) {
    for (let index = 1; index <= 5; index++) {
      const competitor = this.obj_market_prices['tCompetitor' + index];

      const obj_competitor = {
        'competitor': {
          'iIDCompetitor': competitor == null ? 0 : competitor.iIDCompetitor,
          'tCompetitorName': competitor == null ? '' : competitor.tCompetitorName,
        },
        'tProductCompetitor': this.obj_market_prices['tProductCompetitor' + index],
        'dPriceCompetitor': this.obj_market_prices['dPriceCompetitor' + index],
      };
      this.competitor_collection[index - 1] = obj_competitor;
    }
  }

  fnDataCompetitor(index) {

    this.state_btn_next = index == 5 ? true : false;
    this.state_btn_preview = index == 1 ? true : false;

    $('#btn-' + index).removeClass('pgp-btns_actions').addClass('pgp-btn-select');

    if (this.index_ant != null) {
      $('#btn-' + this.index_ant).removeClass('pgp-btn-select').addClass('pgp-btns_actions');
      this.competitor_collection[this.index_ant - 1].competitor = this.select_competitor;
      this.competitor_collection[this.index_ant - 1].tProductCompetitor = this.input_product;
      this.competitor_collection[this.index_ant - 1].dPriceCompetitor = this.input_prices;
    }

    this.index_ant = index;

    this.select_competitor = this.competitor_collection[index - 1].competitor;
    console.log('this.select_competitor: ', this.select_competitor);
    this.input_product = this.competitor_collection[index - 1].tProductCompetitor;
    console.log('this.input_product: ', this.input_product);
    this.input_prices = this.competitor_collection[index - 1].dPriceCompetitor;
    console.log('this.input_prices: ', this.input_prices);

    this.data_competitors_values[index - 1] = {
      'competitor':  this.competitor_collection[index - 1].competitor,
      'tProductCompetitor':  this.competitor_collection[index - 1].tProductCompetitor,
      'dPriceCompetitor':  this.competitor_collection[index - 1].dPriceCompetitor,
    };
    
    // if (this.data_competitors_values[index - 1]['competitor']['iIDCompetitor'] != 0 &&
    //     this.data_competitors_values[index - 1]['tProductCompetitor'] != null &&
    //     this.data_competitors_values[index - 1]['dPriceCompetitor'] != null) {
    //   this.state_btn_update = false;
    // } else {
    //   this.state_btn_update = true;
    // }

    console.log('this.data_competitors_values: ', this.data_competitors_values);
    // for (let ind_valid = 1; ind_valid <= 5; ind_valid++) {
    //   if (this.data_competitors_values[ind_valid - 1]['competitor']['iIDCompetitor'] != 0) {
    //   // if (this.select_competitor != 0 &&
    //   //     this.input_product != null &&
    //   //     this.input_prices != null) {
    //     if (this.data_competitors_values[ind_valid - 1]['tProductCompetitor'] != null && this.data_competitors_values[ind_valid - 1]['dPriceCompetitor'] != null) {
    //       this.state_btn_update = false;
    //       return;
    //     } else {
    //       this.state_btn_update = true;
    //     }
    //   } else {
    //     this.state_btn_update = true;
    //   }
    // }

  }
  /** **/


  /** Funciones para el boton siguiente y el anterio **/
  fnPreviewMarketPrices() {
    if (this.index_ant > 1 && this.index_ant <= 5) {
      const indexOrg = this.index_ant - 1;
      this.fnDataCompetitor(indexOrg);
    }
  }

  fnNextMarketPrices() {
    if (this.index_ant >= 1 && this.index_ant < 5) {
      const indexOrg = this.index_ant + 1;
      this.fnDataCompetitor(indexOrg);
    }
  }
  /** **/


  /** Funcion que envia los datos al servicio de actalizar **/
  fnUpdateDataMarketPrices() {
    this.submitted = true;

    this.fnDataCompetitor(this.index_ant);

    for (let index = 1; index <= 5; index++) {
      const competitor = this.competitor_collection[index - 1].competitor;     

      if (competitor.iIDCompetitor == 0) {
        this.obj_form_market['tCompetitor' + index] = null;
      } else {
        this.obj_form_market['tCompetitor' + index] = competitor;
      }

      this.obj_form_market['tProductCompetitor' + index] = this.competitor_collection[index - 1].tProductCompetitor;
      this.obj_form_market['dPriceCompetitor' + index] = this.competitor_collection[index - 1].dPriceCompetitor;
    }

    console.log('this.obj_form_market: ', this.obj_form_market);
    console.log('this.competitor_collection[0]: ', this.competitor_collection[0]);

    this.marketPriceService.fnHttpSetEditDataMarketPrices(this.current_payload, this.obj_form_market).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.submitted = false;
        console.log('r.status: ', r.status);
        this.utilitiesService.showToast('top-right', 'success', this.DATA_LANG.msgSuccessUpdate.text);
        this.dismiss(false);
      }
      if (r.status == 206) {
        this.submitted = false;
        let error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
        console.log('error: ', error);
        this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
      }
    }, err => {
      this.submitted = false;
      console.log('err: ', err);
    });
  }
  /** **/


  /** Funciones para cancelar y cerrar **/
  fnCancelEditMarketPrices() {
    this.submitted = false;
    this.dismiss(true);
  }

  dismiss(modalAction) {
    this.ref.close(modalAction);
  }
  /** **/

  /*****(numberOnly)allows only numbers and decimals*****/

  numberOnly(evt): boolean {
    const charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 46 && evt.srcElement.value.split('.').length>1) {
        return false;
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
  }

  /*****(currencyInputChanged)add thousands format*****/
  currencyInputChanged(val) {
    if(val){
    val = this.format_number(val, '');
    }
    return val;
  }

  format_number(number, prefix) {
    let thousand_separator = ',',
      decimal_separator = '.',
      regex = new RegExp('[^' + decimal_separator + '\\d]', 'g'),
      number_string = number.replace(regex, '').toString(),
      split = number_string.split(decimal_separator),
      rest = split[0].length % 3,
      result = split[0].substr(0, rest),
      thousands = split[0].substr(rest).match(/\d{3}/g);

    if (thousands) {
      let separator = rest ? thousand_separator : '';
      result += separator + thousands.join(thousand_separator);
    }
    result = split[1] != undefined ? result + decimal_separator + split[1] : result;
    return prefix == undefined ? result : (result ? prefix + result : '');
  };

  fnStateUpdate() {
    console.log('this.data_competitors_values =====>(1) ', this.data_competitors_values);
    console.log('this.data_competitors_values[this.index_ant - 1] =====>(2) ', this.data_competitors_values[this.index_ant - 1]);
    console.log('this.index_ant: ', this.index_ant);
    console.log('this.select_competitor: ', this.select_competitor);
    this.data_competitors_values[this.index_ant - 1]['competitor'] = this.select_competitor;
    this.data_competitors_values[this.index_ant - 1]['tProductCompetitor'] = this.input_product;
    this.data_competitors_values[this.index_ant - 1]['dPriceCompetitor'] = this.input_prices;
    for (let index = 1; index <= 5; index++) {
      if (this.data_competitors_values[index - 1]['competitor']['iIDCompetitor'] != 0) {
      // if (this.select_competitor != 0 &&
      //     this.input_product != null &&
      //     this.input_prices != null) {
        if (this.data_competitors_values[index - 1]['tProductCompetitor'] != null && this.data_competitors_values[index - 1]['dPriceCompetitor'] != null) {
          this.state_btn_update = false;
          return;
        } else {
          this.state_btn_update = true;
        }
      } else {
        this.state_btn_update = true;
      }
    }
    console.log('this.state_btn_update =====>(3) ', this.state_btn_update);
    console.log('this.data_competitors_values =====>(4) ', this.data_competitors_values);
  }

}
