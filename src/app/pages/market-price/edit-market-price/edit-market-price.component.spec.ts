import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMarketPriceComponent } from './edit-market-price.component';

describe('EditMarketPriceComponent', () => {
  let component: EditMarketPriceComponent;
  let fixture: ComponentFixture<EditMarketPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMarketPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMarketPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
