import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { CommonModule } from '@angular/common';

import { CPanelComponent } from './c-panel.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { EditModuleLanguageComponent } from './edit-module-language/edit-module-language.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [ThemeModule, NgSelectModule, FormsModule, CommonModule, NgxPaginationModule],
  declarations: [
    CPanelComponent,
    EditModuleLanguageComponent,
  ],
  entryComponents: [
    EditModuleLanguageComponent,
  ]
})
export class CPanelModule { }
