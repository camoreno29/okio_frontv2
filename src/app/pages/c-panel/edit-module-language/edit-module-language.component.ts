import { Component, OnInit, Input } from '@angular/core';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { NbDialogRef } from '@nebular/theme';
import { Router, ActivatedRoute } from '@angular/router';
import { StateService } from '../../../@core/utils';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';
import { CPanelService } from '../../../shared/api/services/c-panel.service';

@Component({
  selector: 'ngx-edit-module-language',
  templateUrl: './edit-module-language.component.html',
  styleUrls: ['./edit-module-language.component.scss']
})
export class EditModuleLanguageComponent implements OnInit {

  @Input() name_key: string;
  @Input() description: string;
  @Input() text: string;
  @Input() language: string;
  @Input() module: string;
  obj_form_edit_languages: any = {}; 
  submitted: boolean = false;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private utilitiesService: UtilitiesService,
    private cPanelService: CPanelService,
    protected ref: NbDialogRef<EditModuleLanguageComponent>,
  ) { }

  ngOnInit() {
    // this.obj_form_edit_languages['text'] = this.text;
  }

  fnEditLanguages(text){
    const self = this;
    let url;
    if(self.module == 'general_module'){
      url = 'languagesGeneral/'+ self.language +'/general_module/' + self.name_key ;
    } else {
      url = 'languages/'+ self.language +'/' + self.module + '/' + self.name_key ;
    }

    let obj = {
      'text': this.text,
    };
    let data_update = self.cPanelService.fnHttpUpdateDBObj(url, obj);
    self.utilitiesService.showToast('top-right', 'success', 'The text has been updated!');
    console.log('data_update: ', data_update);
    self.dismiss();
  }
  
  fnCancelEditLanguages() {
    this.submitted = false;
    this.dismiss();
  }

  dismiss() {
    this.ref.close();
  }

}
