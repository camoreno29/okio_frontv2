import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditModuleLanguageComponent } from './edit-module-language.component';

describe('EditModuleLanguageComponent', () => {
  let component: EditModuleLanguageComponent;
  let fixture: ComponentFixture<EditModuleLanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditModuleLanguageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditModuleLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
