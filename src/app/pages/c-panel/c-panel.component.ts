import { Component, OnInit } from '@angular/core';
import { UtilitiesService } from '../../shared/api/services/utilities.service';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database'; // Firebase modules for Database, Data list and Single object
import { NbMenuItem, NbMenuService, NbSidebarService, NbDialogService } from '@nebular/theme';
import { EditModuleLanguageComponent } from './edit-module-language/edit-module-language.component';
import { CPanelService } from '../../shared/api/services/c-panel.service';
 
declare var $: any;

@Component({
  selector: 'ngx-c-panel',
  templateUrl: './c-panel.component.html',
  styleUrls: ['./c-panel.component.scss']
})
export class CPanelComponent implements OnInit {

  DATA_LANG: any = [];
  select_lang: any = null;
  list_modules: any = [];
  list_modules_lang: any = null;
  collection_module: any = [];
  data_module_object: any = {};
  module_select: any = null;
  numItemsPage: any = null;
  currentPage: any = null;
  selected_item: any = null;
  active: number = 0;
  layoud_component_display: String = 'textos';

  replace_modules: any = [];

  constructor(
    private utilitiesService: UtilitiesService,
    private db: AngularFireDatabase,
    private sidebarService: NbSidebarService,
    private dialogService: NbDialogService,
    private cPanelService: CPanelService,
  ) { }

  ngOnInit() {
    const self = this;

    self.sidebarService.collapse('settings-sidebar');
    sessionStorage.setItem('iIDindex_card', null);
    sessionStorage.setItem('objmenu_select', null);

     /* *** START - JQuery definition *** */
    // JQuery ready
    $(document).ready(function () {
      $('#pgp-btn_toogle_side_bar').click(); // Emulate click display right sidebar to hide
      $('.menu-sidebar').removeClass('d-block').addClass('d-none'); // Hide left sidebar to this component
      $('#toggle-settings').removeClass('was-expanded').addClass('was-collapse'); // Hide right sidebar to this component
      $('#toggle-settings').removeClass('d-block').addClass('d-none'); // Hide right sidebar to this component
    });
    /* **** END - JQuery definition **** */

    self.fnGetLanguageList();
    self.currentPage = 1;
    self.numItemsPage = 10;
  }

  /********START - fnGetLanguageList*********/
  // Funcion que obtiene la lista de lenguajes disponibles
  fnGetLanguageList(){
    const self = this;
    self.DATA_LANG = [];
    self.cPanelService.fnHttpGetDBObj('languages').valueChanges().subscribe(data => {
      console.log('data: ', data);
      let collection = Object.keys(data);
      console.log('collection: ', collection);
      self.DATA_LANG = collection;
      console.log('self.DATA_LANG ==================== ', self.DATA_LANG);
      self.select_lang = self.DATA_LANG[0];
      self.fnGetListModules(self.select_lang);
    });
  }
  /********END - fnGetLanguageList*********/

  /********START - fnGetListModules*********/
  // Funcion que obtiene la lista de modulos disponibles
    fnGetListModules(lang){
      console.log('lang: ', lang);
      const self = this;
      self.list_modules = [];
      self.select_lang = lang;
      self.replace_modules.push({
        'original_data': 'general_module',
        'replace_data': 'general_module',
        });

      self.cPanelService.fnHttpGetDBObj('languages/'+ lang).valueChanges().subscribe(data => {
        console.log('res_module: ', data);
        self.list_modules_lang = data;
        let collection = Object.keys(data);
        console.log('collection: ', collection);
        collection.forEach(element => {
          self.replace_modules.push({
            'original_data': element,
            'replace_data': element.replace(/_/g, ' '),
          });
        });
        console.log('self.replace_modules: ', self.replace_modules);
        self.list_modules = collection;
        self.active = 0;
        self.fnGetListLabelModule(self.replace_modules[0].original_data);
      });
      console.log('self.select_lang=========>>>> ', self.select_lang);
    }
  /********END - fnGetListModules*********/

 /********START - fnGetListLabelModule*********/
  // Funcion que obtiene la lista de textos por modulo
    fnGetListLabelModule(module_lang){
      const self = this;
      self.currentPage = 1;

      if(module_lang == 'general_module'){
        console.log('module_lang: ', module_lang);
        self.cPanelService.fnHttpGetDBObj('languagesGeneral/'+ self.select_lang + '/general_module/').valueChanges().subscribe(data => {
          console.log('res_module: ', data);
          let labels_lang = data;
          self.module_select = module_lang;
          self.collection_module = Object.entries(JSON.parse(JSON.stringify(labels_lang)));
        });

      } else { 

        self.cPanelService.fnHttpGetDBObj('languages/'+ self.select_lang + '/' + module_lang).valueChanges().subscribe(data => {
          console.log('res_module: ', data);
          let labels_lang = data;
          self.module_select = module_lang;
          self.collection_module = Object.entries(JSON.parse(JSON.stringify(labels_lang)));
        });

      }

      // console.log('module_lang: ', module_lang);
      // console.log('self.list_modules_lang : ', self.list_modules_lang );
      // let labels_lang = self.list_modules_lang[module_lang];
      // self.module_select = module_lang;
      // console.log('labels_lang: ', labels_lang);
      // console.log('labels_lang: type ', typeof labels_lang );
      // self.collection_module = Object.entries(labels_lang);
    }
    /********END - fnGetListLabelModule*********/

    
  /********START - fnShowModalEditKey*********/
  // Funcion que muestra la pantalla de editar texto
    fnShowModalEditKey(data) {
      this.data_module_object.name_key = data[0];
      this.data_module_object.description = data[1]['description'];
      this.data_module_object.text = data[1]['text'];
      this.data_module_object.language = this.select_lang;
      this.data_module_object.module = this.module_select;
      console.log('this.data_module_object: ', this.data_module_object);

      this.dialogService.open(EditModuleLanguageComponent, { context: this.data_module_object }).onClose.subscribe((res) => {
        console.log('res: ', res);
      });
      this.fnGetListLabelModule(this.module_select);
    }
  /********END - fnShowModalEditKey*********/
  
    fnSwitchViewData(stateView){
      this.layoud_component_display = stateView;
    }

}
