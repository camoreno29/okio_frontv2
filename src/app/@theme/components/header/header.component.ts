import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { UserData } from '../../../@core/data/users';
import { AnalyticsService } from '../../../@core/utils';
import { LayoutService } from '../../../@core/utils';
import { SignOutService } from '../../../shared/api/services/sign-out.service';
/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';

import { PagesComponent } from '../../../pages/pages.component';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { SettingsVersionService } from '../../../shared/api/services/settings-version.service';
import { ConfigService } from '../../../shared/api/services/config.service';
import { Observable, Observer } from 'rxjs';

declare var $: any;
@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  @Input() position = 'normal';
  user = {};

  loadingCalculate: boolean = false;
  current_payload: string = null;

  current_lang: any = null;
  lang_list: any = [];
  DATA_LANG: any = null;

  show_badge_notifications: boolean = true;

  userMenu = [
    { title: 'My account' },
    { title: 'Terms & conditions' },
    { title: 'Log out' },
  ];

  notifications_show: any = [];

  notificationMenu: any = [];


  notifications_list: any = [];

  calculate_state: number = 0; 

  dataChange: Observable<any>;
  dataChangeObserver: any;

  constructor(private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private userService: UserData,
    private signOutService: SignOutService,
    private analyticsService: AnalyticsService,
    private layoutService: LayoutService,
    private pagesComponent: PagesComponent,
    private utilitiesService: UtilitiesService,
    private settingsVersionService: SettingsVersionService,
    private configService: ConfigService,
    public router: Router,
    private route: ActivatedRoute,
    private authService: NbAuthService) {
  }

  ngOnInit() {
    const self = this;

    const id_version = sessionStorage.getItem('id_version');

    console.log('self.notificationMenu.slice(0, 5): ', self.notificationMenu.slice(0, 5));
    self.fnGetLanguage();
    self.utilitiesService.dataChange.subscribe((data) => {
      if(data.lang){
        self.fnGetLanguage();
      }
      if(data.version){
        this.notificationMenu = [];
        this.notifications_show = [];
        this.notifications_list = [];
        self.fnGetCalculateNotifications(self.current_payload, data.version);
        self.fnGetAllNotificationsByVersion(self.current_payload, data.version);
      }
    });
    $(document).ready(function () {
      $('nb-context-menu').css('border', 'none');
    });
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        this.user = token.getPayload();
        this.current_payload = token.getValue();
        this.user['name'] = this.user['User']['tFirstName'] + ' ' + this.user['User']['tLastName'];
        self.fnGetAllNotificationsByVersion(self.current_payload, id_version);
        self.fnGetCalculateNotifications(self.current_payload, id_version);
      }
    });

    this.menuService.onItemClick().subscribe((response) => {
      this.onItemSelection(response);
    });
  }

  fnCollapse() {
    $('#sidebar').toggleClass('active');
  }

  onItemSelection(response) {
    const self = this;
    console.log('event: ', response.item.title);
    switch (response.item.title) {
      case this.DATA_LANG.menuMyAccount.text:
        self.router.navigate(['/pages/my-account']);
        break;
      case this.DATA_LANG.menuLogOut.text:
        self.utilitiesService.fnDestroySessionData(function (res_clean_session) {
          console.log('res_clean_session: ', res_clean_session);
          if (res_clean_session) {
            self.router.navigate(['/auth/login']);
          }
        });
        break;
      case this.DATA_LANG.menuTerms.text:
        self.router.navigate(['/pages/terms-conditions']);
        break;
    }
  }

  fnSetDataUser(user_name) {
    console.log('this.user: ', this.user);
    console.log('user_name: ', user_name);
    this.user['name'] = user_name;
  }

  fnGetAllNotificationsByVersion(current_payload, id_version?) {
    console.log('current_payload: ', current_payload);
    if(id_version){
      this.configService.fnHttpGetAllNotificationsByVersion(current_payload, id_version).subscribe(r => {
        console.log('r: ', r);
        if (r.status == 200) {
          this.notifications_list =  r.body.reverse();
          //r.body.reverse().forEach((element, key) => {
          this.notifications_list.forEach((element, key) => {
            if (!element.bHasItBeenSeen) {
              this.notificationMenu.push(r.body[key]);
            }
          });
          console.log('this.notificationMenu: ', this.notificationMenu.length);
          this.notifications_show = this.notificationMenu.slice(0, 3);
        }
      }, err => {
        console.log('err: ', err);
      });
    }
  }

  fnGetAllNotificationsLengthByVersion(current_payload, id_version?) {
    this.notificationMenu = [];
    console.log('current_payload: ', current_payload);
      this.configService.fnHttpGetAllNotificationsByVersion(current_payload, id_version).subscribe(r => {
        console.log('r: ', r);
        if (r.status == 200) {
          this.notifications_list =  r.body.reverse();
          console.log('this.notifications_list: ', this.notifications_list);
          //r.body.reverse().forEach((element, key) => {
          this.notifications_list.forEach((element, key) => {
            if (!element.bHasItBeenSeen) {
              this.notificationMenu.push(r.body[key]);
              console.log('this.notificationMenu: (lenght) ', this.notificationMenu);
            }
          });
          console.log('this.notificationMenu: ', this.notificationMenu.length);
        }
      }, err => {
        console.log('err: ', err);
      });
  }

  /**Funcion para mostrar u ocultar el boton calcular**/
  fnShowCalculate() {
    const id_project = sessionStorage.getItem('id_project');
    if (this.router.url === '/pages/projects' ||
      this.router.url === '/pages/versions-project/' + id_project ||
      this.router.url === '/pages/groups/' + id_project ||
      this.router.url === '/pages/c-panel' ||
      this.router.url === '/pages/settings-project/' + id_project) {
      return false;
    } else {
      return true;
    }
  }

  fnShowSelectLanguages() {
    if (this.router.url === '/pages/c-panel') {
      return false;
    } else {
      return true;
    }
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }

  fnGoHome() {
    if($('.settings-sidebar').hasClass('expanded')){
      $('#toggle-settings').click();
    }
    this.router.navigate(['/pages/projects']);
  }

  fnGoNotifications() {
    this.notifications_list.forEach(element => {
      if(!element.bHasItBeenSeen){
        element.bHasItBeenSeen = true;
        this.configService.fnHttpUpdateNotificationVersion(this.current_payload, element).subscribe(r => {
          console.log('r: ', r);
          if (r.status == 200) {
          }
        }, err => {
          console.log('err: ', err);
        });
      }
      // notifications_clear.push(element);
    });
    this.notificationMenu = [];
    this.router.navigate(['/pages/notifications']);
  }

  fnCleanNotifications() {
    // this.show_badge_notifications = false;
    const self = this;
    // let notifications_clear = [];
    let id_version = sessionStorage.getItem('id_version');
    if($('#dropdownNotifications').hasClass('show')){
      self.notifications_show = self.notificationMenu.slice(0, 3);
      // self.fnGetAllNotificationsByVersion(self.current_payload, id_version);
    } else {

      self.notifications_show.forEach(function(value, key) {
        console.log('key: ', key);
        console.log('value: ', value);
        console.log('self.notifications_show: ', self.notifications_show.length);

        
        if(!value.bHasItBeenSeen){
          value.bHasItBeenSeen = true;
          self.configService.fnHttpUpdateNotificationVersion(self.current_payload, value).subscribe(r => {
            console.log('r: ', r);
            if (r.status == 200) {
              if ((key + 1) != self.notifications_show.length) {
              } else {
                self.fnGetAllNotificationsLengthByVersion(self.current_payload, id_version);
              }
            }
          }, err => {
            console.log('err: ', err);
          });
        }
      });
    }
  }

  fnGetCalculateNotifications(current_payload, id_version?){
    if(id_version){
      this.configService.fnHttpGetCalculateNotificationsByVersion(current_payload, id_version).subscribe(r => {
        console.log('r: ', r);
        if (r.status == 200) {
          this.calculate_state = r.body[0].iIDStateMachine;
        }
      });
    }
  }

  fnCalculate() {

    const id_version = sessionStorage.getItem('id_version');

    if (id_version) {

      this.loadingCalculate = true;
      this.settingsVersionService.fnHttpGetMathModGeneral(this.current_payload, id_version).subscribe(r => {
        console.log('r: ', r);
        if (r.status == 200) {
          this.loadingCalculate = false;
          this.utilitiesService.showToast('top-right', 'success', 'Calculate successfully!');
          location.reload();
        }
        if (r.status == 206) {
          const error = this.utilitiesService.fnSetErrors(r.body.codMessage)[0];
          this.utilitiesService.showToast('top-right', 'warning', error, 'nb-alert');
        }
      }, err => {
        console.log('err: ', err);
      });

    }

  }

  setData(data: any) {
    this.dataChangeObserver.next(data);
  }

  onMenuClick() {
    console.log('Holaaaaaa menu');
  }

  fnGetLanguage() {
    const self = this;
    self.utilitiesService.fnGetLanguage('headerMenu', function (res_lang) {
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module;
      self.userMenu = [
        { title: self.DATA_LANG.menuMyAccount.text },
        { title: self.DATA_LANG.menuTerms.text },
        { title: self.DATA_LANG.menuLogOut.text },
      ];
    });
  }

}
