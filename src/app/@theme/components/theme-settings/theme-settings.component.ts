import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NbSidebarService, NbMenuService, NbDialogService } from '@nebular/theme';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';

import { StateService, LayoutService } from '../../../@core/utils';
import { ProjectService } from '../../../shared/api/services/project.service';
import { VersionService } from '../../../shared/api/services/version.service';
import { PagesComponent } from '../../../pages/pages.component';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';
// import { ProjectsComponent } from '../../../pages/projects/projects.component';
import { AddVersionComponent } from '../../../pages/versions/add-version/add-version.component';

import { Observable, Observer } from 'rxjs';

declare var $: any;

@Component({
  selector: 'ngx-theme-settings',
  styleUrls: ['./theme-settings.component.scss'],
  templateUrl: './theme-settings.component.html',
  // template: `
  //   <h6>LAYOUTS</h6>
  //   <div class="settings-row">
  //     <a *ngFor="let layout of layouts"
  //        href="#"
  //        [class.selected]="layout.selected"
  //        [attr.title]="layout.name"
  //        (click)="layoutSelect(layout)">
  //       <i [attr.class]="layout.icon"></i>
  //     </a>
  //   </div>
  //   <h6>SIDEBAR</h6>
  //   <div class="settings-row">
  //     <a *ngFor="let sidebar of sidebars"
  //        href="#"
  //        [class.selected]="sidebar.selected"
  //        [attr.title]="sidebar.name"
  //        (click)="sidebarSelect(sidebar)">
  //       <i [attr.class]="sidebar.icon"></i>
  //     </a>
  //   </div>
  //   <div class="settings-row">
  //     <div class="switcher">
  //       <ngx-layout-direction-switcher></ngx-layout-direction-switcher>
  //     </div>
  //   </div>
  // `,
})
export class ThemeSettingsComponent implements OnInit {

  layouts = [];
  sidebars = [];

  users: { name: string, title: string }[] = [
    { name: 'Company name 1', title: 'Company name 1' },
    { name: 'Company name 2', title: 'Company name 2' },
    { name: 'Company name 3', title: 'Company name 3' },
    { name: 'Company name 4', title: 'Company name 4' },
    { name: 'Company name 5', title: 'Company name 5' },
  ];

  current_payload: string = null;
  projects: any = null;
  id_project: any = null;
  name_project: any = null;
  list_versions: any = [];
  versions_original_collection: any = [];
  display_form: any = false;

  sidebarEnd = false;
  expanded = false;
  wasExpanded = false;

  id_company: any = null;
  id_version: any = null;
  company_name: string = null;

  selectedItem: any = null;

  lang_nav: any = window.navigator.language;
  DATA_LANG: any = null;

  constructor(
    protected stateService: StateService,
    private authService: NbAuthService,
    public router: Router,
    private route: ActivatedRoute,
    private pagesComponent: PagesComponent,
    // public projectComponenet: ProjectsComponent,
    private utilitiesService: UtilitiesService,
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private layoutService: LayoutService,
    private versionService: VersionService,
    private dialogService: NbDialogService,
    private projectService: ProjectService) {

    // this.stateService.getLayoutStates()
    //   .subscribe((layouts: any[]) => this.layouts = layouts);

    // this.stateService.getSidebarStates()
    //   .subscribe((sidebars: any[]) => this.sidebars = sidebars);
  }

  ngOnInit() {
    const self = this;
    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => {
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    // });
    /* *** START - JQuery definition *** */
    // JQuery ready
    $(document).ready(function () {
      $('.menu-sidebar').removeClass('d-block').addClass('d-none');

      $('.pgp-class_switch_version').click(function () {
        $('.pgp-class_switch_version').removeClass('pgp-list_active');
        var num_item = $(this).attr('name');
        $('#pgp-id_content_version_' + num_item).addClass('pgp-list_active');
      });

      $('#toggle-settings').on('click', function () {
        const data_project = JSON.parse(sessionStorage.getItem('project'));
        self.id_project = data_project.id_project;
        self.name_project = data_project.name_project;
        console.log('data_project: ', data_project);
        // self.fnListProjects();
        self.fnListVersions(self.id_project);
        // self.route.params.subscribe(params => {
        //   console.log('params: ', params.id_version);
        // });
      });
    });
    /* **** END - JQuery definition **** */
    // self.sidebarService.collapse('menuSidebar');

    self.stateService.getLayoutStates()
      .subscribe((layouts: any[]) => self.layouts = layouts);

    self.stateService.getSidebarStates()
      .subscribe((sidebars: any[]) => self.sidebars = sidebars);

    // self.companiesService.fnHttpGetCompanies()

    self.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      // console.log('token: ', token);
      if (token.isValid()) {
        // here we receive a payload from the token and assigne it to our `user` variable
        self.current_payload = token.getValue();
        // console.log('self.current_payload: ', self.current_payload);
        // self.fnListProjects(self.current_payload);
      }
    });
  }

  // fnStateNavigation() {
  //   const self = this;
  //   /* *** START - Service validate session on start *** */
  //   // this.utilitiesService.fnGetCurrentTokenSession() -- Implementation utility service
  //   self.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
  //     if (token.isValid()) {
  //       self.current_payload = token.getValue();
  //       // console.log('self.current_payload: ', self.current_payload);
  //       if (self.current_payload) {
  //         return 
  //       }
  //     }
  //   });
  //   /* **** END - Service validate session on start **** */
  // }

  fnListProjects(current_payload?) {
    let token = (current_payload) ? current_payload : this.current_payload;
    // console.log('token: ', token);
    this.projectService.fnHttpGetAllProjects(token).subscribe(r => {
      // console.log('r: ', r);
      if (r.status == 200) {
        this.projects = r.body.reverse();
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fnListVersions(id_project, current_payload?) {
    let token = (current_payload) ? current_payload : this.current_payload;
    // console.log('token: ', token);
    this.list_versions = [];
    this.versionService.fnHttpGetAllVersionsProject(token, id_project).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        // this.versions = r.body;        
        this.list_versions = JSON.parse(JSON.stringify(r.body));
        this.versions_original_collection = JSON.parse(JSON.stringify(r.body));
        // console.log('this.versions : ', this.versions);
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  fntProjectNameSelected(project) {
    const self = this;
    console.log('iIDProject: ', project.iIDProject);
    // self.pagesComponent.fnSetMenuConfig();

    if (self.current_payload) {
      sessionStorage.setItem('project.iIDProject', project.iIDProject);
      sessionStorage.setItem('data_company', JSON.stringify(project));
      self.versionService.fnHttpGetVersionDefaultByProject(self.current_payload, project.iIDProject).subscribe(r => {
        console.log('r: ', r);
        if (r.status == 200) {
          self.id_version = r.body.iIDVersion;
          self.router.navigate(['/pages/home', r.body.iIDVersion]);
        }
      }, err => {
        console.log('err: ', err);
      });
    }
    $('#toggle-settings').click();
  }

  fntVersionNameSelected(index, version, collection) {
    const self = this;
    console.log("index", index);
    console.log('version: ', version);
    collection[index]['bDefault'] = true;
    collection.forEach(function (value, key) {
      if (key === index) {
        collection[key]['bDefault'] = true;
      } else {
        collection[key]['bDefault'] = false;
      }
    });
    self.utilitiesService.setData({ version: version.iIDVersion });
    self.router.navigate(['/pages/home', version.iIDVersion]);
  }

  toggleSidebar() {
    console.log('toggleSidebar: ');
    const self = this;
    self.sidebarService.toggle(true, 'menu-sidebar');
    self.layoutService.changeLayoutSize();
  }

  //  fnShowFormAddCompany() {
  //    $('#toggle-settings').click();
  //    this.router.navigate(['/pages/add-project']);
  //  }

  open(id_project, id_company) {
    const self = this;
    console.log('id_project: ', id_project);
    console.log('id_company: ', id_company);
    const ddlProyect = false;

    const group_data = {
      'ddlProyect': ddlProyect,
      'iIDProject': id_project,
    };

    console.log('group_data: ', group_data);
    this.dialogService.open(AddVersionComponent, { context: group_data }).onClose.subscribe((res) => {
      this.fnListVersions(id_project);

      self.versionService.fnHttpGetVersionDefaultByProject(self.current_payload, group_data.iIDProject).subscribe(r => {
        console.log('r: ', r);
        if (r.status == 200) {
          self.router.navigate(['/pages/home', r.body.iIDVersion]);
        }
      }, err => {
        console.log('err: ', err);
      });

    });
  }


  fnShowListCompany() {
    this.display_form = false;
    console.log('this.display_form: ', this.display_form);
  }

  fnSaveDataCompany() {
    console.log('this.display_form: ', this.display_form);

    let data_object = {
      'iIDCompany': 0,
      'tCompanyLogo': 'test1',
      'tCompanyName': 'Test Name Company',
    };

    this.projectService.fnHttpSetSaveNewProject(this.current_payload, data_object).subscribe(r => {
      console.log('r: ', r);
      if (r.status == 200) {
        this.projects = r.body;
      }
    }, err => {
      console.log('err: ', err);
    });
  }

  layoutSelect(layout: any): boolean {
    this.layouts = this.layouts.map((l: any) => {
      l.selected = false;
      return l;
    });

    layout.selected = true;
    this.stateService.setLayoutState(layout);
    return false;
  }

  sidebarSelect(sidebars: any): boolean {
    this.sidebars = this.sidebars.map((s: any) => {
      s.selected = false;
      return s;
    });

    sidebars.selected = true;
    this.stateService.setSidebarState(sidebars);
    return false;
  }

  toggleSettings() {
    this.sidebarService.toggle(false, 'settings-sidebar');
    this.expanded = !this.expanded;
    this.wasExpanded = true;
  }

  fnSetSessionStorageCompanyData() {

  }

  fnGetLanguage() {
    const self = this;
    self.utilitiesService.fnGetLanguage('versions', function (res_lang) {
      console.log('res_lang::::::::::::::::::: ', res_lang);
      self.DATA_LANG = res_lang.module;
    });
  }

}
