import { Component, OnInit } from '@angular/core';
import { NbSidebarService } from '@nebular/theme';
import { StateService } from '../../../@core/utils';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';

@Component({
  selector: 'ngx-toggle-settings-button',
  styleUrls: ['./toggle-settings-button.component.scss'],
  template: `
    <button class="toggle-settings" id="toggle-settings"
            (click)="toggleSettings()"
            [class.expanded]="expanded"
            [class.sidebar-end]="sidebarEnd"
            [class.was-expanded]="wasExpanded"            
    >
      <div *ngIf="expanded" nbTooltip="{{ DATA_LANG?.toolHideVersions.text }}" nbTooltipPlacement="left">
        <i class="fas fa-angle-right"></i>
      </div>
      <div *ngIf="!expanded" nbTooltip="{{ DATA_LANG?.toolShowVersions.text }}" nbTooltipPlacement="left">
        <i class="fas fa-angle-left"></i>
      </div>
    </button>
  `,
})
export class ToggleSettingsButtonComponent {

  sidebarEnd = false;
  expanded = false;
  wasExpanded = false;
  DATA_LANG: any = null;
  constructor(private sidebarService: NbSidebarService, protected stateService: StateService, private utilitiesService: UtilitiesService,) {
    this.stateService.onSidebarState()
      .subscribe(({id}) => {
        this.sidebarEnd = id === 'end';
      });
  }

  ngOnInit() {
    const self = this;
    self.fnGetLanguage();
    // self.utilitiesService.dataChange.subscribe((data) => {
    //   console.log('data==================>>: ', data);
    //   self.fnGetLanguage();
    // });
  }

  fnGetLanguage(){
    const self = this;
    self.utilitiesService.fnGetLanguage('versions', function(res_lang){
      console.log('res_lang: ===================>', res_lang);
      self.DATA_LANG = res_lang.module;
    });
  }

  toggleSettings() {
    this.sidebarService.toggle(false, 'settings-sidebar');
    this.expanded = !this.expanded;
    this.wasExpanded = true;
  }
}
