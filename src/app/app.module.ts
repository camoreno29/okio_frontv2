/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { NgxPaginationModule } from 'ngx-pagination'; //
import { Ng5SliderModule } from 'ng5-slider';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ThemeModule } from './@theme/theme.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/* ************+ Import module NbPasswordAuthStrategy strategy ************ */
import { NbPasswordAuthStrategy, NbAuthModule, NbAuthJWTToken, NbPasswordAuthStrategyOptions } from '@nebular/auth';
/* ************+ Import module NbPasswordAuthStrategy strategy ************ */
import { AuthGuard } from './shared/api/services/auth-guard.service';
import { getDeepFromObject } from '@nebular/auth/helpers';

import { NbToastrModule } from '@nebular/theme';
import { NbTooltipModule } from '@nebular/theme';
import { NbDialogModule } from '@nebular/theme';
import { NbStepperModule } from '@nebular/theme';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { GridFilterPipe } from './shared/pipes/grid-filter.pipe';
import { GoogleChartsModule } from 'angular-google-charts';

import { environment } from '../environments/environment.prod';

/* ******************** START - Import libraries firebase ******************** */
// 1. Import the libs you need
// import { AngularFireModule } from '@angular/fire';
// import { AngularFirestoreModule } from '@angular/fire/firestore';
// import { AngularFireStorageModule } from '@angular/fire/storage';
// import { AngularFireAuthModule } from '@angular/fire/auth';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';

/* ********************* END - Import libraries firebase ********************* */
// /* ******************** START - Credentials connect firebase ******************** */
// // 2. Add your credentials from step 1
// const config = {
//   apiKey: 'AIzaSyA4NDrUL_AlkKO9lyXhOKTDhodKklqFo-M',
//   authDomain: 'pgplite-527fc.firebaseapp.com',
//   databaseURL: 'https://pgplite-527fc.firebaseio.com',
//   projectId: 'pgplite-527fc',
//   storageBucket: 'pgplite-527fc.appspot.com',
//   messagingSenderId: '74420760695',
//   appId: '1:74420760695:web:8b06f6c401e4c93e',
// };
// /* ********************* END - Credentials connect firebase ********************* */

// JQuery Import
import * as $ from 'jquery';
export interface NbAuthSocialLink {
  link?: string;
  url?: string;
  target?: string;
  title?: string;
  icon?: string;
}

const socialLinks: NbAuthSocialLink[] = [];

export const defaultSettings: any = {
  forms: {
    login: {
      redirectDelay: 500, // delay before redirect after a successful login, while success message is shown to the user
      strategy: 'email',  // strategy id key.
      rememberMe: true,   // whether to show or not the `rememberMe` checkbox
      showMessages: {     // show/not show success/error messages
        success: true,
        error: true,
      },
      socialLinks: socialLinks, // social links at the bottom of a page
    },
    register: {
      redirectDelay: 500,
      strategy: 'email',
      showMessages: {
        success: true,
        error: true,
      },
      terms: true,
      socialLinks: socialLinks,
    },
    requestPassword: {
      redirectDelay: 500,
      strategy: 'email',
      showMessages: {
        success: true,
        error: true,
      },
      socialLinks: socialLinks,
    },
    resetPassword: {
      redirectDelay: 500,
      strategy: 'email',
      showMessages: {
        success: true,
        error: true,
      },
      socialLinks: socialLinks,
    },
    logout: {
      redirectDelay: 500,
      strategy: 'email',
    },
    validation: {
      password: {
        required: true,
        minLength: 4,
        maxLength: 50,
      },
      email: {
        required: true,
      },
      fullName: {
        required: false,
        minLength: 4,
        maxLength: 50,
      },
    },
  },
};

const formSetting: any = {
  redirectDelay: 0,
  showMessages: {
    success: true,
    error: true,
  },
};

export function setReturnDataMessages(module: string, res: HttpResponse<Object>): Object[] { return [res]; }
export function setReturnDataErrors(module: string, res: HttpErrorResponse): Object[] { return [res]; }


@NgModule({
  declarations: [AppComponent, GridFilterPipe],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NbToastrModule.forRoot(),
    NbTooltipModule,
    TooltipModule.forRoot(),
    NbDialogModule.forRoot(),
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    NgxPaginationModule,
    Ng5SliderModule,
    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    NbStepperModule,
    GoogleChartsModule.forRoot(),
    /* ************+ START - Implement module NbPasswordAuthStrategy strategy ************ */
    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: 'email',

          token: {
            class: NbAuthJWTToken,
            key: 'payload', // this parameter tells where to look for the token
          },

          baseEndpoint: 'https://pgplitewebapi.azurewebsites.net',
          // baseEndpoint: 'http://localhost:8084',
          login: {
            endpoint: '/api/Auth/SignIn',
            method: 'post',
            redirect: {
              success: 'pages/projects',
              failure: 'auth/login',
            },
          },
          register: {
            endpoint: '/api/Auth/SignUp',
            method: 'post',
            redirect: {
              success: 'auth/login',
              failure: null,
            },
          },
          logout: {
            endpoint: '/api/Auth/SignOut',
            method: 'post',
          },
          requestPass: {
            endpoint: '/api/Auth/RememberPassword',
            method: 'post',
          },
          resetPass: {
            endpoint: '/api/Auth/ResetPassword',
            method: 'post',
          },

          messages: {
            key: 'codMessage', // this parameter tells where to look for the token
            getter: setReturnDataMessages,
          },

          errors: {
            key: 'codMessage',
            getter: setReturnDataErrors,
          },
        }),
      ],
      forms: {
        login: formSetting,
        register: formSetting,
        requestPassword: formSetting,
        resetPassword: formSetting,
        logout: {
          redirectDelay: 0,
        },
      },
    }),
    /* ************+* END - Implement module NbPasswordAuthStrategy strategy ************* */
    /* ************+ START - Imports firebase data connection ************ */
    // 3. Initialize
    // AngularFireModule.initializeApp(config),
    // AngularFirestoreModule, // firestore
    // AngularFireAuthModule, // auth
    // AngularFireStorageModule, // storage
    AngularFireModule.initializeApp(environment.firebaseConfig), // Main Angular fire module
    AngularFireDatabaseModule,  // Firebase database module
    /* ************+* END - Imports firebase data connection ************* */
  ],
  bootstrap: [AppComponent],
  exports: [GridFilterPipe],
  providers: [
    AuthGuard,
    { provide: APP_BASE_HREF, useValue: '/' },
  ],
})
export class AppModule {
}
