import { Output, Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  @Output() fire: EventEmitter<any> = new EventEmitter();
  @Output() fireCards: EventEmitter<any> = new EventEmitter();

  constructor() {
    console.log('shared service started');
  }

  change(MENU_ITEMS) {
    console.log('change started');
    this.fire.emit(MENU_ITEMS);
  }

  getEmittedValue() {
    return this.fire;
  }
}
