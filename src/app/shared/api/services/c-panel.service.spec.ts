import { TestBed } from '@angular/core/testing';

import { CPanelService } from './c-panel.service';

describe('CPanelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CPanelService = TestBed.get(CPanelService);
    expect(service).toBeTruthy();
  });
});
