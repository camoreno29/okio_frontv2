import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilitiesService } from '../services/utilities.service';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UpdatePasswordService {
  url_host: any = environment.apiUrl;
  urlSetUpdatePasswordUser: String = '';
  urlSetValidateTokenPasswordUser: String = '';
  data_headers_request: any = '';
  constructor(public http: HttpClient, private utility: UtilitiesService) { }

  fnSetDefineTokenAuthorization(payload) {
    this.data_headers_request = new HttpHeaders().set('Authorization', payload);
    return this.data_headers_request;
  }

  fnHttpSetValidateTokenPasswordUser(guid_user): Observable<any> {
    const headers = this.fnSetDefineTokenAuthorization(guid_user);
    this.urlSetValidateTokenPasswordUser = '/api/Auth/ValidatePasswordToken';
    return this.http.post(this.utility.fnGetHost() + this.urlSetValidateTokenPasswordUser, {},
      {
        observe: 'response',
        headers: headers,
      });
  }

  fnHttpSetUpdatePasswordUser(data_object): Observable<any> {
    this.urlSetUpdatePasswordUser = '/api/Auth/UpdatePassword';
    return this.http.put(this.utility.fnGetHost() + this.urlSetUpdatePasswordUser, data_object,
      {
        observe: 'response',
        headers: this.data_headers_request,
      });
  }
}
