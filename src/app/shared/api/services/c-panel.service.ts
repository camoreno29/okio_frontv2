import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database'; // Firebase modules for Database, Data list and Single object
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CPanelService {

  listRef: AngularFireList<any>;
  ObjRef: AngularFireObject<any>;

  constructor(
    private db: AngularFireDatabase,
  ) { }
  
  fnHttpGetDBObj(url){
    this.ObjRef = this.db.object(url);
    return this.ObjRef;
  }

  fnHttpUpdateDBObj(url, obj){
    this.db.object(url).update(obj);
  }

}
