import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilitiesService } from '../services/utilities.service';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PriceComparisonService {

  url_host: any = environment.apiUrl;
  data_headers_request: any = '';
  urlGetDataAllPriceComparison: any = '';
  urlGetGraphFiltersForecastResult: any = '';
  urlSetEditDataForecastResult: any = '';
  urlGetFiltersForecastResult: any = '';


  /******************************/
  urlGetFiltersMarketPrice: any = '';
  urlGetDataListCompetitorsByProject: any = '';
  urlSetEditDataMarketPrices: any = '';
  urlSetDeleteMarketPrices: any = '';
  urlSetDeleteAllMarketPrices: any = '';


  urlSetDataNewCategory: any = '';
  urlSetDeleteRatingValuePerceivedById: any = '';
  urlSetDeleteAllRatingsValuePerceived: any = '';

  constructor(public http: HttpClient, private utility: UtilitiesService) { }

  fnSetDefineTokenAuthorization(payload) {
    this.data_headers_request = new HttpHeaders().set('Authorization', payload);
    return this.data_headers_request;
  }

  fnHttpGetAllPriceComparison(guid_user, id_version, object_data_send): Observable<any> {
    
    console.log('object_data_send: ', object_data_send);
    console.log('guid_user: ', guid_user);
    console.log('id_version: ', id_version);
    const headers = this.fnSetDefineTokenAuthorization(guid_user);
    this.urlGetDataAllPriceComparison = '/api/PriceComparison/GetPriceComparisons?iIDVersion=' + id_version;
    return this.http.post(this.utility.fnGetHost() + this.urlGetDataAllPriceComparison, object_data_send,
      {
        observe: 'response',
        headers: headers,
        reportProgress: true,
      });
  }

  fnHttpGetGraphFiltersForecastResult(guid_user, id_version, object_data_send, iIDEscenario): Observable<any> {
    console.log('object_data_send: ', object_data_send);
    console.log('guid_user: ', guid_user);
    console.log('id_version: ', id_version);
    const headers = this.fnSetDefineTokenAuthorization(guid_user);
    this.urlGetGraphFiltersForecastResult = '/api/ForecastResult/GetGraphFiltersForecastResult?iIDVersion=' + id_version + '&iIDEscenario=' + iIDEscenario;
    return this.http.post(this.utility.fnGetHost() + this.urlGetGraphFiltersForecastResult, object_data_send,
      {
        observe: 'response',
        headers: headers,
        reportProgress: true,
      });
  }

  fnHttpSetEditDataForecastResult(guid_user, obj_form): Observable<any> {
    const headers = this.fnSetDefineTokenAuthorization(guid_user);
    this.urlSetEditDataForecastResult = '/api/ForecastResult/PutForecastResult';
    return this.http.put(this.utility.fnGetHost() + this.urlSetEditDataForecastResult, obj_form,
      {
        observe: 'response',
        headers: headers,
        reportProgress: true,
      });
  }

  fnHttpGetFiltersForecastResult(guid_user, id_version): Observable<any> {
    console.log('id_version: ', id_version);
    console.log('guid_user: ', guid_user);
    const headers = this.fnSetDefineTokenAuthorization(guid_user);
    this.urlGetFiltersForecastResult = '/api/ForecastResult/GetFiltersForecastResult?iIDVersion=' + id_version;
    return this.http.get(this.utility.fnGetHost() + this.urlGetFiltersForecastResult,
      {
        observe: 'response',
        headers: headers,
        reportProgress: true,
      });
  }





  fnHttpGetFiltersMarketPrice(guid_user, id_version): Observable<any> {
    console.log('guid_user: ', guid_user);
    const headers = this.fnSetDefineTokenAuthorization(guid_user);
    this.urlGetFiltersMarketPrice = '/api/MarketPrices/GetFiltersMarketPrices?iIDVersion=' + id_version;
    return this.http.get(this.utility.fnGetHost() + this.urlGetFiltersMarketPrice,
      {
        observe: 'response',
        headers: headers,
        reportProgress: true,
      });
  }

  fnHttpGetDataListCompetitorsByProduct(guid_user, id_product): Observable<any> {
    console.log('guid_user: ', guid_user);
    console.log('id_product: ', id_product);
    const headers = this.fnSetDefineTokenAuthorization(guid_user);
    this.urlGetDataListCompetitorsByProject = '/api/MarketPrices/GetListCompetitorsForProduct?iIDProduct=' + id_product;
    return this.http.get(this.utility.fnGetHost() + this.urlGetDataListCompetitorsByProject,
      {
        observe: 'response',
        headers: headers,
        reportProgress: true,
      });
  }

  fnHttpSetEditDataMarketPrices(guid_user, obj_form_market): Observable<any> {
    const headers = this.fnSetDefineTokenAuthorization(guid_user);
    this.urlSetEditDataMarketPrices = '/api/MarketPrices/PutMarketPrices';
    return this.http.put(this.utility.fnGetHost() + this.urlSetEditDataMarketPrices, obj_form_market,
      {
        observe: 'response',
        headers: headers,
        reportProgress: true,
      });
  }

  fnHttpSetDeleteMarketPriceById(guid_user, iIDMarketPrice): Observable<any> {
    const headers = this.fnSetDefineTokenAuthorization(guid_user);
    this.urlSetDeleteMarketPrices = '/api/MarketPrices/PutCleanMarketPrices?iIDMarketPrice=' + iIDMarketPrice;
    return this.http.put(this.utility.fnGetHost() + this.urlSetDeleteMarketPrices, {},
      {
        observe: 'response',
        headers: headers,
        reportProgress: true,
      });
  }

  fnHttpSetDeleteAllMarketPriceById(guid_user, iIDVersion): Observable<any> {
    const headers = this.fnSetDefineTokenAuthorization(guid_user);
    this.urlSetDeleteAllMarketPrices = '/api/MarketPrices/PutCleanAllMarketPrices?iIDVersion=' + iIDVersion;
    return this.http.put(this.utility.fnGetHost() + this.urlSetDeleteAllMarketPrices, {},
      {
        observe: 'response',
        headers: headers,
        reportProgress: true,
      });
  }

}
