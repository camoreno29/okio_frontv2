import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilitiesService } from '../services/utilities.service';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {

  url_host: any = environment.apiUrl;
  data_headers_request: any = '';
  urlGetGraphDashboard: any = '';
  urlGetDashboard: any = '';

  constructor(public http: HttpClient, private utility: UtilitiesService) { }

  fnSetDefineTokenAuthorization(payload) {
    this.data_headers_request = new HttpHeaders().set('Authorization', payload);
    return this.data_headers_request;
  }

  fnHttpGetDashboard(guid_user, id_version): Observable<any> {
    console.log('id_version: ', id_version);
    console.log('guid_user: ', guid_user);
    const headers = this.fnSetDefineTokenAuthorization(guid_user);
    this.urlGetGraphDashboard = '/api/Dashboard/GetHeaderDashboard?iIDVersion=' + id_version;
    return this.http.get(this.utility.fnGetHost() + this.urlGetGraphDashboard,
      {
        observe: 'response',
        headers: headers,
        reportProgress: true,
      });
  }


  fnHttpGetGraphDashboard(guid_user, id_version): Observable<any> {
    console.log('id_version: ', id_version);
    console.log('guid_user: ', guid_user);
    const headers = this.fnSetDefineTokenAuthorization(guid_user);
    this.urlGetDashboard = '/api/Dashboard/GetForecastResultDashboard?iIDVersion=' + id_version;
    return this.http.get(this.utility.fnGetHost() + this.urlGetDashboard,
      {
        observe: 'response',
        headers: headers,
        reportProgress: true,
      });
  }

}
