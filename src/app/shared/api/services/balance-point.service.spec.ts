import { TestBed } from '@angular/core/testing';

import { BalancePointService } from './balance-point.service';

describe('BalancePointService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BalancePointService = TestBed.get(BalancePointService);
    expect(service).toBeTruthy();
  });
});
