import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { UtilitiesService } from './utilities.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BalancePointService {
  url_host: any = environment.apiUrl;
  data_headers_request: any = '';
  urlGetFormulasGeneral: any = '';
  urlGetBalancePointResult: any = '';
  urlExportBalancePointResult: any = '';

  constructor(public http: HttpClient, private utility: UtilitiesService) { }

  fnSetDefineTokenAuthorization(payload) {
    this.data_headers_request = new HttpHeaders().set('Authorization', payload);
    return this.data_headers_request;
  }

  fnHttpGetFormulasGeneral(guid_user, data_object): Observable<any> {
    const headers = this.fnSetDefineTokenAuthorization(guid_user);
    this.urlGetFormulasGeneral = '/api/BalancePoint/GetformulasGeneral';
// return this.http.post(this.utility.fnGetHost() + this.urlGetFormulasGeneral, data_object,
    return this.http.post(this.utility.fnGetHost() + this.urlGetFormulasGeneral, data_object,
    {
        observe: 'response',
        headers: headers,
        reportProgress: true,
      });
  }

  fnHttpGetBalancePointResult(guid_user, data_object): Observable<any> {
    const headers = this.fnSetDefineTokenAuthorization(guid_user);
    this.urlGetBalancePointResult = '/api/BalancePoint/GetBalancePointResult';
    return this.http.post(this.utility.fnGetHost() + this.urlGetBalancePointResult, data_object,
      {
        observe: 'response',
        headers: headers,
        reportProgress: true,
      });
  }

  fnHttpExportBalancePointResult(guid_user, data_object): Observable<any> {
    const headers = this.fnSetDefineTokenAuthorization(guid_user);
    this.urlExportBalancePointResult = '/api/BalancePoint/GetExportBalancePointResult';
    return this.http.post(this.utility.fnGetHost() + this.urlExportBalancePointResult, data_object,
    {
      observe: 'response',
      headers: headers,
      reportProgress: true,
      responseType: 'blob',

    });
 }

}
