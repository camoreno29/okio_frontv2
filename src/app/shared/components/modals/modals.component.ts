import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { NbWindowService } from '@nebular/theme';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';

@Component({
  selector: 'ngx-modals',
  templateUrl: './modals.component.html',
  styleUrls: ['./modals.component.scss']
})
export class ModalsComponent implements OnInit {

  @Input() title: string;
  @Input() body: string;

  lang_nav: any =  window.navigator.language;
  DATA_LANG: any = null;

  constructor(protected ref: NbDialogRef<ModalsComponent>,
    private utilitiesService: UtilitiesService,) { }

  ngOnInit() {
    const self = this;
    self.utilitiesService.fnGetLanguage('modalHelp', function(res_lang){
      console.log('res_lang: ', res_lang);
      self.DATA_LANG = res_lang.module
    });
  }

  dismiss() {
    this.ref.close();
  }

}
