import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { NbTooltipModule } from '@nebular/theme';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ThemeModule } from '../../../@theme/theme.module';
import { ModalModule } from 'ngx-bootstrap/modal';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';


const ENTRY_COMPONENTS = [
];


@NgModule({
  imports: [
    ThemeModule,
    TooltipModule.forRoot(),
    ModalModule,
    NgSelectModule,
    FormsModule,
  ],
  declarations: [
  ],
  entryComponents: [
    ...ENTRY_COMPONENTS,
  ],
})
export class CardsModule { }
