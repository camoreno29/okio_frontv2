import { Component, OnInit, Input, EventEmitter, Output, AfterViewInit, OnChanges, SimpleChange, Self } from '@angular/core';
import { MenuService } from '../../../shared/api/services/menu.service';
import { HomeComponent } from '../../../pages/home/home.component';
import { SharedService } from '../../../shared/services/shared.service';
import { LayoutService } from '../../../@core/utils';
import { Router, ActivatedRoute } from '@angular/router';
import { NbMenuItem, NbMenuService, NbSidebarService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators';
import { forEach } from '@angular/router/src/utils/collection';
import { UtilitiesService } from '../../../shared/api/services/utilities.service';
import { ConfigService } from '../../../shared/api/services/config.service';

/* ************+ Import module auth ************ */
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';

declare let $: any;

@Component({
  selector: 'ngx-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss'],
})
export class CardsComponent implements OnInit, AfterViewInit, OnChanges {

  current_item: any;
  collection_cards: any[];
  data_cards_final: any;
  @Input() data_cards: any;
  @Input() position_selected: any;

  dataItems: any = ('data-items');
  itemsMainDiv: any = ('.MultiCarousel');
  carruselLength: number;
  star: number;
  showLeftoverCardRight: boolean = false;
  showLeft: boolean = false;

  message: any = null;
  @Output() messageEvent = new EventEmitter<{}>();

  menu_lang: any = {};

  languageCard: string;

  current_payload: string = null;
  arr_notifications: any = [];

  id_version = sessionStorage.getItem('id_version');

  constructor(private sharedService: SharedService,
    private layoutService: LayoutService,
    private configService: ConfigService,
    private utilitiesService: UtilitiesService,
    private authService: NbAuthService,
    ) {
  }

  ngOnInit() {
    const self = this;

    $(document).ready(function () {
      $('#Language').bind('DOMSubtreeModified', function () {
        self.fnGetLanguage();
      });
    });
    self.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      if (token.isValid()) {
        self.current_payload = token.getValue();
      }
    });
  }

  ngAfterViewInit() {
    console.log('data_cards: ', this.data_cards);
    console.log('position_selected: ', this.position_selected);
    this.collection_cards = this.data_cards;
    const self = this;
    $(document).ready(function () {
      self.current_item = self.data_cards.filter(d => d.selected === true)[0];
      self.star = 0;
      self.fnConfigDataCards();
      self.showLeftoverCardRight = self.data_cards.length > self.carruselLength ? true : false;
      self.showLeft = false;
      $(window).resize(function () {
        self.fnResize();
      });
    });
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    const self = this;
    if (changes['data_cards'] != undefined && !changes['data_cards'].firstChange) {
      $(document).ready(function () {
        self.current_item = self.data_cards.filter(d => d.selected === true)[0];
        if (self.position_selected > 0 && (self.position_selected > (self.carruselLength - 1))) {
          self.star = self.star + 1;
        } else {
          self.star = 0;
        }
        self.fnConfigDataCards();
        self.showLeftoverCardRight = ((self.data_cards.length > self.carruselLength) && (self.position_selected < (self.data_cards.length - 1))) ? true : false;
        self.showLeft = ((self.data_cards.length > self.carruselLength) && (self.position_selected >= (self.data_cards.length - 1))) ? true : false;
      });
    }
  }

  fnResize() {
    // this.current_item = this.data_cards.filter(d => d.selected === true)[0];
    const indexOriginal: number = this.data_cards.findIndex(d => d.iIDCard === this.current_item['iIDCard']);
    // this.star = 0;
    this.fnConfigDataCards(indexOriginal);
    const self = this;
    $(document).ready(function () {
      const index = self.data_cards_final.findIndex(d => d.iIDCard === self.current_item['iIDCard']);
      $('.pgp-content_start').removeClass('pgp-card_first_content_selected');
      $('#pgp-data_main_card_' + index).addClass('pgp-card_first_content_selected');
      self.showLeftoverCardRight = ((self.data_cards.length > self.carruselLength) && (indexOriginal < (self.data_cards.length - 1))) ? true : false;
      self.showLeft = ((self.data_cards.length > self.carruselLength) && (indexOriginal >= (self.data_cards.length - 1))) ? true : false;
    });
  }

  fnGetDataCardsFinal(indexOriginal?: number) {
    this.data_cards_final = [];
    const temporal = [];
    if (indexOriginal != null) {
      this.star = ((this.carruselLength - 1) < indexOriginal) ? this.star + 1 : this.star;
    }
    for (let i = 0; i < this.carruselLength; i++) {
      const item = this.data_cards[i + this.star];
      if (item != undefined) {
        temporal.push(item);
      }
    }
    const self = this;

    self.utilitiesService.fnGetLanguage('cardsMenu', function (res_lang) {
      self.data_cards_final = temporal;
      
      self.data_cards_final.forEach(element => {
        element.tNameOriginal = element.tCardName;
        let card_code = (element.tCardName).substring(0,4);
        card_code == 'card' ? element.tCardName = res_lang.module[element.tCardName]['text'] : element.tCardName;
      });
    });

  }

  fnConfigDataCards(indexOriginal?: number) {
    const self = this;
    self.carruselLength = 0;
    let btnParentSb = '';
    let itemsSplit: any;

    btnParentSb = $(self.itemsMainDiv).attr(self.dataItems);
    if (btnParentSb != undefined && btnParentSb != null) {
      itemsSplit = btnParentSb.split(',');

      const bodyWidth = $('body').width();

      if (bodyWidth >= 1280) {
        self.carruselLength = itemsSplit[4];
      } else if (bodyWidth >= 1024) {
        self.carruselLength = itemsSplit[3];
      } else if (bodyWidth >= 992) {
        self.carruselLength = itemsSplit[2];
      } else if (bodyWidth >= 768) {
        self.carruselLength = itemsSplit[1];
      } else {
        self.carruselLength = itemsSplit[0];
      }
      this.fnGetDataCardsFinal(indexOriginal);
    }
  }

  fnNext() {
    const self = this;
    const indexUltimo = this.carruselLength - 1;
    const index = this.data_cards_final.findIndex(d => d.iIDCard == this.current_item.iIDCard);
    const indexOriginal = this.data_cards.findIndex(d => d.iIDCard == this.current_item.iIDCard);
    const ultimo = this.data_cards.length - 1 == this.data_cards.findIndex(d => d.iIDCard == this.current_item.iIDCard);
    if (index < indexUltimo) {
      this.current_item = this.data_cards_final[indexOriginal + 1];
      this.fnSelectItemCard(this.current_item, index + 1, false);
    } else if (ultimo) {
      this.star = 0;
      this.fnGetDataCardsFinal();
      $(document).ready(function () {
        self.current_item = self.data_cards_final[0];
        self.fnSelectItemCard(self.current_item, 0, false);
        if (self.data_cards.length > self.carruselLength) {
          self.showLeftoverCardRight = true;
          self.showLeft = false;
        }
      });
    } else if (this.data_cards.length > this.carruselLength) {  
      this.star = this.star + 1;
      this.fnGetDataCardsFinal();
      $('.pgp-content_start').removeClass('pgp-card_first_content_selected');
      $(document).ready(function () {
        self.current_item = self.data_cards[indexOriginal + 1];
        self.fnSelectItemCard(self.current_item, indexUltimo, false);
      });
      if ((indexOriginal + 1) < (this.data_cards.length - 1)) {
        this.showLeftoverCardRight = true;
      } else {
        this.showLeftoverCardRight = false;
      }
      this.showLeft = true;
    }
  }

  fnPrevious() {
    const self = this;
    const index = this.data_cards_final.findIndex(d => d.iIDCard == this.current_item.iIDCard);
    const indexOriginal = this.data_cards.findIndex(d => d.iIDCard == this.current_item.iIDCard);
    let primero = false;
    if (index > 0) {
      this.current_item = this.data_cards_final[index - 1];
      this.fnSelectItemCard(this.current_item, index - 1, false);
      primero = 0 == this.data_cards.findIndex(d => d.iIDCard == this.current_item.iIDCard);
      this.showLeft = !primero;
    } else {
      $('.pgp-content_start').removeClass('pgp-card_first_content_selected');
      this.star = this.star - 1;
      this.fnGetDataCardsFinal();
      $(document).ready(function () {
        const i = indexOriginal > 0 ? indexOriginal - 1 : 0;
        self.current_item = self.data_cards[i];
        self.fnSelectItemCard(self.current_item, 0, false);
        primero = 0 == self.data_cards.findIndex(d => d.iIDCard == self.current_item.iIDCard);
        self.showLeft = !primero;
        if ((indexOriginal - 1) > 0) {
          self.showLeftoverCardRight = true;
        } else if (self.data_cards.length > self.carruselLength) {
          self.showLeftoverCardRight = true;
        } else {
          self.showLeftoverCardRight = false;
        }
      });
    }
  }

  fnLeftoverLeft() {
    const self = this;
    const indexOriginal = this.data_cards.findIndex(d => d.iIDCard == this.data_cards_final[0].iIDCard);
    let primero = false;
    $('.pgp-content_start').removeClass('pgp-card_first_content_selected');
    this.star = this.star - 1;
    this.fnGetDataCardsFinal();
    $(document).ready(function () {
      const i = indexOriginal > 0 ? indexOriginal - 1 : 0;
      self.current_item = self.data_cards[i];
      self.fnSelectItemCard(self.current_item, 0, false);
      primero = 0 == self.data_cards.findIndex(d => d.iIDCard == self.current_item.iIDCard);
      self.showLeft = !primero;
      if ((indexOriginal - 1) > 0) {
        self.showLeftoverCardRight = true;
      } else if (self.data_cards.length > self.carruselLength) {
        self.showLeftoverCardRight = true;
      } else {
        self.showLeftoverCardRight = false;
      }
    });
  }

  fnLeftoverRigth() {
    const self = this;

    this.star = this.star + 1;
    this.fnGetDataCardsFinal();
    $('.pgp-content_start').removeClass('pgp-card_first_content_selected');
    $(document).ready(function () {
      self.current_item = self.data_cards_final[self.carruselLength - 1];
      self.fnSelectItemCard(self.current_item, (self.carruselLength - 1), false);
      const indexOriginal = self.data_cards.findIndex(d => d.iIDCard == self.current_item.iIDCard);
      if (indexOriginal < (self.data_cards.length - 1)) {
        self.showLeftoverCardRight = true;
      } else {
        self.showLeftoverCardRight = false;
      }
      self.showLeft = true;
    });

  }

  fnBuildCollectionCards(data_cards, position_selected) {
    const collection_build = [];
    data_cards.forEach((value, key) => {
      const obj = {
        'iIDCard': value.iIDCard,
        'tCardName': value.tCardName,
        'tCardIcon': value.tCardIcon,
      };
      if (key === position_selected) {
        obj['selected'] = true;
      } else {
        obj['selected'] = false;
      }
      collection_build.push(obj);
    });

    this.collection_cards = collection_build;
  }

  fnSelectItemCard(item_card, index, html: boolean) { 
    if (html) {
      this.current_item = item_card;
    }
    console.log('index: ', index);
    console.log('item_card: ', item_card);
    const indexOriginal = this.data_cards.findIndex(d => d.iIDCard == this.current_item.iIDCard);
    $('.pgp-content_start').removeClass('pgp-card_first_content_selected');
    $('#pgp-data_main_card_' + index).addClass('pgp-card_first_content_selected');
    const object_send_event = {
      'collection_cards': this.collection_cards,
      'item_card': item_card,
      'index_card': indexOriginal,
    };
    this.messageEvent.emit(object_send_event);
    let id_version = sessionStorage.getItem('id_version');
    this.utilitiesService.setData({ version: id_version });
  }

  fnGetLanguage() {
    const self = this;
    self.utilitiesService.fnGetLanguage('cardsMenu', function (res_lang) {
      self.menu_lang = res_lang.module;
      self.data_cards_final.forEach(element => {
        element.tCardName = self.menu_lang[element.tNameOriginal]['text'];
      });
    });
  }

}
