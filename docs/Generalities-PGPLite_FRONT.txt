PASOS PARA CREAR MODULOS - ADMIN

El primer paso para empezar a estructurar la distribucion de archivos, y 
el orden de los modulos, es determinar que componentes son compartidos y 
como podemos guardar dichos componentes en un directorio comun. Para el 
ejemplo basico crearemos una distribucion de archivos similar a la siguiente:

directorio 'src/app':
-----------------------------------------------------------------------------------------------------------------------
-> @core
-> @theme
-> pages    // Este directorio contendra los modulos que tendra el proyecto
    -> _shared  // En este directorio se almacenran los componentes comunes y que se pueden reutilizar en otros modulos
        -> common // Directorio de componentes re-usables
        -> utilities //  Directorio de utilidades y funciones comunes
    -> modulo-1
    -> modulo-2
    -> modulo-3
        .
        .
        .
    -> modulo-n
-> shared
    -> api
        -> mock // En este directorio se almacenaran los archivos json que contendran informacion quemada por algun motivo
        -> services // En este directorio se almacenara los archivos de que intervien con las APIS (capa de datos)
    -> common //  Directorio de utilidades y funciones comunes y re-usables
- app-routing.module.ts
- app.component.ts
- app.module.ts
-----------------------------------------------------------------------------------------------------------------------
La anterior distribucion de archivos y modulos, nos permitira organizar todos los modulos descriptivamente y 
nos permitira re-usar codigo como sea posible sin necesidad de duplicar funciones o modulos.

GENERALIDADES
La estandaridad en la escritura del codigo es muy importante en el desarrollo de los proyectos futuros bajos las misma plantilla de desarrollo.
Estas generalidades nos ayudaran especificamente a: 

    - Desarrollar nuevos componentes y modulos de manera agil
    - Encontrar posibles fallos en nustra programacion de forma mas facil
    - Dar soporte eficaz y eficiente en nustros modulos y componentes

Basicamente estas generalidades nos permitiran tener un orden adecuado de todo el proyecto y escalable con el fin de avanzar en el desarrollo sin problemas y abierto para mejoras y optimimizaciones.

- Todas las variables que se declaren en cualquier archivo (css, js, html), deberan ser declaradas con camelCase
- Todas las clases que se declaren en cualquier archivo (js, scss), deberan ser declaradas en PascalCase
- Todos los metodos que se declaren en cualquier archivo (css, js, html), deberan ser declaradas con camelCase
- Siempre debemos de poner espacios alrededor de los operadores (= + – * /) y después de las comas.
- Al momento de generar las versiones de produccion, no deben haber console.log en ningun lugar del proyecto, esto es con el fin de evitar que a los usuarios les sea mas dificil ver la informacion de proceso
- Siempre debemos de terminar las declaraciones simples con punto y coma ";" , el lenguaje es capaza de determinarlas, pero lo mas recomendado es ponerlas para evitar posibles errores cuando el minifique los archivos en el empaquetamiento.
- Para declaraciones complejas tipo bucles o funciones debemos de seguir las siguientes normas:
    * La llave de apertura se pone en el extremo de la primera línea con un espacio delante
    * La llave de cierre se coloca en una línea nueva.
    * El final de una declaración de este tipo nunca lleva el punto y coma
        for (i = 0; i < 5; i++) {
            x += i;
        }
- Reglas de objetos
    * Se coloca el soporte de apertura en la misma línea que el nombre del objeto
    * Se utilizan dos puntos : más un espacio entre cada propiedad y su valor
    * Se utilizan comillas entorno a los valores de cadena no a lo numéricos
    * No se añade una coma después del último para propiedad – valor, incluso si es la ultima propiedad del objeto
    * La llave de cierre se coloca en una nueva línea
    * Se debe de terminar la definición del objeto con punto y coma

        var persona = {
            nombre: "Juana",
            apellido: "Calamidad",
            edad: 34,
            ojos: "verdes", 
        };
    * Los objetos también pueden escribirse en una línea utilizando espacios entre las propiedades:
        
        var persona = { nombre: "Juana", apellido: "Calamidad", edad: 34, ojos: "verdes" };

- Longitud de la línea inferior a 80 caracteres: Para facilitar la lectura, y si no coge en estos 80 caracteres lo mejor es romperla después de un operador o coma.
- Nombres de archivos: Para evitar cometer errores a la hora de leer archivos, se recomienda que estos tengan los nombres completamente en minúsculas como por ejemplo: logo.jpg
- Sangría: Se deben de utilizar 4 espacios para el sangrado entre bloques de código.


1 - Creamos un componente vacio con un nombre cualquiera, dentro del directorio 'pages'.
Crearemos el componente desde la consola con la herramienta de angular-cli de la siguiente manera:

    * Crear nuevo componente => ng g c pages/nombre-componente

2 - Creamos un archivo de tipo modulo dentro de el directorio del componente creado anteriormente:

    * Crear nuevo modulo => ng g m pages/nombre-modulo


